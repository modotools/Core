# ![ModoTools][logo]  Core
<!--- Pipeline and Coverage not setup yet
[![pipeline status](https://gitlab.com/modotools/Core/badges/master/pipeline.svg)](https://gitlab.com/modotools/Core/-/commits/master)
[![coverage report](https://gitlab.com/modotools/Core/badges/master/coverage.svg)](https://gitlab.com/modotools/Core/-/commits/master)
--->

## Content
* [Package info](#package-info)
* [Setup](#setup)
* [CodeStyle](#code-style)
* [Features](#features)
 * [Array Drawer](#array-drawer)
 * [Class Type Reference](#class-type-reference)
 * [Code Generator](#code-generator)
 * [Core Enums](#core-enums)
 * [EventMessenger](#event-messenger)
 * [Extensions](#extensions)
 * [ID Assets](#id-assets)
 * [Interfaces](#interfaces)
 * [Interface Container](#interface-container)
 * [MultiPropertyDrawer](#multipropertydrawer)
 * [Pool Attendant](#pool-attendant)
 * [Provider](#provider)
 * [SelectTypesPopup](#selecttypespopup)
 * [SimplePool](#simple-pool)
 * [Singleton](#singleton)
* [Credits](#credits)

## Package Info
I created this unity3d-package for anything that is not really game-specific or about game mechanics but more about making it easier to code and develop with unity. Extension-methods, commonly used types, pooling objects, singleton-pattern, basic code for editors, inspectors and so on.
Some of the code is copied from external sources, which are mentioned in the Credits sections.

## Setup
As a co-developer: create a submodule in your game-project (https://gitlab.com/modotools/Core.git) and point it to a relative forlder like *MyAwesomeGameProject/Core* 
Open your manifest (MyAwesomeGameProject/Packages/manifest.json)
and add this line to the dependencies:
"com.modotools.core": "file:../Core",

As a user: you can also add the git url as dependency, see https://docs.unity3d.com/Manual/upm-ui-giturl.html

### Core Settings
The core package will automatically create CoreSettings in the path "Assets/Settings/".
Here you can enable and disable features, menu items and custom inspectors for the package.

For various tools we use a Type Selector Popup, which will group items hierarchically by namespace.
Use MenuFromNamespaceFilter to replace or ignore specific namespace paths when selecting types for usability.

![Core Settings](./Documentation~/CoreSettings.png)
![Core Settings B](./Documentation~/CoreSettingsB.png)

1. Here you can enable and disable features, menu items and custom inspectors for the package. (It will place defines in mcs.rsp and csc.rsp files. Wait a bit for the code to be recompiled.)
2. Replace paths of menu items, you can also change menu-shortcut highlighted in [yellow](https://docs.unity3d.com/ScriptReference/MenuItem.html).
3. For various tools we use a Type Selector Popup, which will group items hierarchically by namespace.
Use MenuFromNamespaceFilter to replace or ignore specific namespace paths when selecting types for usability.
In this example the base path "ScriptableUtility" will be ignored, but sub-paths "ActionConfigs" and "Variables" will be replaced with an empty string.
This way the elements will be on the root level of the type selector as you can see here:

![Type Selector](./Documentation~/SelectTypesPopup.png)

(f.e. types in namespace ScriptableUtility.ActionConfigs.Logic are now just in category "Logic")

## Code Style
[style readme](./Documentation~/CodeStyle.md)

## Features

### Array Drawer
Copied from [ArrayDrawer] by @garettbass.
Can be enabled/ disabled in Core Settings.
When you experience some problems in modifying/ saving data, try disabling Array Drawer.

![lists before after][ArrayDrawer img]

[Array Drawer/](./Runtime/ArrayDrawer/)
[Editor/Array Drawer/](./Editor/ArrayDrawer/)

### Class Type Reference
Copied and refactored from [ClassTypeReference]

Used to be able to link class type in inspector. 
For exmaple in [IDConfig](./Runtime/Unity/Types/ID/IDConfig.cs)
we have this code:
```cs
[ClassExtends(typeof(IDAsset))]
public ClassTypeReference IDType; 
public IDAsset[] Ids = new IDAsset[0];
```
`ClassExtendsAttribute` makes sure that the class type we setup in `IDType` is of type `IDAsset`. In the custom editor IDAssets can then be created (ScriptableObjects), which need to inherit the selected type.

example editor:
![editor-image](./Documentation~/IDConfigClassTypeSelector.png)

[ClassTypeReference.cs](./Runtime/Unity/Types/ClassTypeReference.cs)
see also [ID Assets](#id-assets),
[SelectTypesPopup](#selecttypespopup)

### Code Generator
#### Code Generator Config
Use [CodeGeneratorConfig](./Editor/Tools/CodeGeneratorConfig.cs) to create code via text-template.

`TextAsset.txt` can look like this:
```
{0}

namespace {1}
{{
    [CreateAssetMenu(menuName = "Scriptable/Events/{2}")]
    public class {2}Event : ScriptableEvent<{3}> {{}}
}}
```
The parser will replace double brackets `{{` with single brackets `{` replace all occurances of `{0}`, `{1}`, `{2}` etc. with the corresponding array element value in `ReplaceArgs`. 

#### Enum Generator Config
Use [EnumGeneratorConfig](./Editor/Tools/EnumGeneratorConfig.cs) to create enums. This is useful if you want to allow the designer to add types of things (weapons, attachment-points, collision-materials, etc.) which should be selectable in some other inspector for identification. Consider using [ID-assets](#id-assets) instead (ID assets can also create enums additionally to the ID-config).

You can use interface `IEnumGenerator` to generate enum with custom configs. 
Use method `GenerateEnum(IEnumGenerator)` in [EnumGeneratorConfigInspector.cs](./Editor/Tools/EnumGeneratorConfigInspector.cs) this is also used by ID-config.

### Core Enums
[Enums.cs](./Runtime/Types/Enums.cs)
When using complex methods with bool as a return type it is often not clear what the bool that is returned represents.
We use these enums instead of bool to make it more readable. (f.e. `OperationResult.OK` or `OperationResult.Error`, `ChangeCheck.Changed` or `ChangeCheck.NotChanged`).

### EventMessenger
[EventMessenger](./Runtime/Events/EventMessenger.cs)

Whenever you want to communicate things in code without adding a lot of dependencies or requiring designer to link the event in inspector, you can use events. This implementation of an event messenger is very simple. 
When using it you first define event-structs with the information you need. For example:
```cs
public struct DamageEvent
{
    public GameObject DamagedTarget;
    public GameObject Aggressor;
}
```

Firing the event:

```cs
var dmgData = new DamageEvent(collidedWith, Aggressor, dir, dmgInfo);
  EventMessenger.TriggerEvent(dmgData);
```

Listening to events is then as easy as this:

```cs
public class LevelStatTracker : IEventListener<KillEvent>, 
        IEventListener<ReviveEvent>,
        IEventListener<DamageEvent>
{
        public void StartTracking()
        {
            EventMessenger.AddListener<KillEvent>(this);
            EventMessenger.AddListener<ReviveEvent>(this);
            EventMessenger.AddListener<DamageEvent>(this);
        }

        public void StopTracking()
        {
            EventMessenger.RemoveListener<KillEvent>(this);
            EventMessenger.RemoveListener<ReviveEvent>(this);
            EventMessenger.RemoveListener<DamageEvent>(this);
        }
        public void OnEvent(KillEvent @event)
        {
            //... do stuff
        }
        public void OnEvent(ReviveEvent @event)
        {
            //... do stuff
        }
        public void OnEvent(DamageEvent @event)
        {
            //... do stuff
        }
}
```

### Extensions

You can find some useful extension methods here:

[Extensions](./Runtime/Extensions/)

[Unity Extensions](./Runtime/Unity/Extensions/)

Some usefule extensions 

* Animator.IsPlaying

  check whether an animation is playing

* GameObject.SetActiveWithTransition

  uses ITransitionActivation if available to play transition before activating/ deactivating 

* GameObject.Get

  uses IProvider to get components out of the object

* Object.DestroyEx

  calls DestroyImmediate when in in editor and not playing

* VectorExtensions 

  for easily converting between vector2 vector3 and int-vectors.

### ID Assets
You can create ID Configs, which are little factories for ID assets. `default menu: Assets/Create/Tools/IDConfig`
These ID assets are ScriptableObjects (assets in the project) which can be used for identification-purposes.
This is useful if you want to allow the designer to add types of things (weapons, attachment-points, collision-materials, etc.), which can then be selected in some other inspector. You can set it up, so it also generates an enum-code-file. 

ID assets also help if you want to define and use a type of categories in a base package, where the elements of this category should not be known in this package, but are dependent on the game. (If you define an enum you have to define the elements also).

### Interfaces
Find some common interfaces here:
[Interfaces](./Runtime/Interface/)
[Unity Interfaces](./Runtime/Unity/Interface/)


### Interface Container
Copied and refactored from [IUnified]
With this tool you can serialize interfaces and select them in the inspector, which is really useful if you want to separate dependencies by using a lot of interfaces.

### MultiPropertyDrawer
This enables you to use multiple propertydrawers for one member and was inspired by this thread: https://forum.unity.com/threads/drawing-a-field-using-multiple-property-drawers.479377/

### Pool Attendant
Copied with some modifications from [PoolAttendant].

### Provider


### SelectTypesPopup
Use this whenever you want the user to select class types.

Useful if you want some factory-pattern-like customization.
For example you have a config of actions and the user picks actions that should be added to the config to customize game flow.

Example Editor:
![editor-image](./Documentation~/IDConfigClassTypeSelector.png)

### Singleton
[Singleton.cs](./Runtime/Types/Singleton.cs)

Readability is usually favored, but in this case we use the letter "I", instead of writing "Instance". Once you learned and inherited this symbol, it will be much easier to write and therefore access the singleton, which is what you want with singletons.

### SimplePool
[SimplePool.cs](./Runtime/Types/SimplePool.cs)

This pool is not for GameObjects, but for simple C# objects and lists.
This enables you to write something like 

```cs
using (var goListScoped = SimplePool<List<GameObject>>.I.GetScoped())
{
    var goList = goListScoped.Obj;
    // You have a nice clean list of GameObjects to work with here…
    // the list is reused and avoids a lot of allocation and 
    // deallocation
}
// list is returned at the end of the scope
```

## Credits
* [ClassTypeReference]
* [IUnified]
* [PoolAttendant]
* [ArrayDrawer]

[ClassTypeReference]: https://github.com/rotorz/unity3d-class-type-reference
[IUnified]: https://assetstore.unity.com/packages/tools/localization/iunified-12117
[PoolAttendant]: https://unitylist.com/p/wwo/Unity-Pool-Attendant
[ArrayDrawer]: https://unitylist.com/p/egf/Unity-Extensions.Array-Drawer

[ArrayDrawer img]: https://raw.githubusercontent.com/garettbass/UnityExtensions.ArrayDrawer/master/Before-After.png "before after"

[logo]: ./Documentation~/modotool.png "modotool-icon"
