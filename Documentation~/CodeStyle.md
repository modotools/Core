# Code Style
<!-- todo: read https://docs.google.com/document/d/1-eUWZ0lWREFu5iH-ggofwnixDDQqalOoT4Yc0NpWR3k/edit check what to take away -->

## Content
* [Philosophy](#philosophy)
* [Tao of Programming](#tao-of-programming)
* [Code Style](#code-style)
* [Naming](#naming)
 * [Namespaces](#namespaces)
 * [Variables](#variables)
 * [Class](#class)
 * [Interfaces](#interfaces)
 * [Data](#data)
* [Code Order](#code-order)
* [File Structure](#file-structure)
* [Todos in Code](#todos-in-code)

## Philosophy
The vision of modotools is to create highly modular tools and reusable code. So that one day you can just click together the gameplay elements you want to use without getting to much into coding the things that have been coded a hundred million times by various different game developers.

The vision of modotools is heavily inspired by RPG Maker, GameMaker and Unity. It tries to achieve the simplicity and childlike "play around"-workflow of RPG Maker (without need for code) with the flexibility of unity.

To achieve this vision and modularity it is super important to reduce complexity of such an ambitious project by reducing the depencies.
To reduce dependencies it is useful to 
* split the project into separate packages.
* use interfaces (a lot)
* instead of commenting your code, make code chunks small and separable. The method names often replace the need for a comment.
* Think about code like building blocks:
 * Components
 * Actions
 * Events 
* Use composition instead of inheritance. (You can only inherit one class, but multiple interfaces).

## Tao of Programming
* Don't hestitate to just rush into coding. The faster you make mistakes, the faster you learn to avoid them and build an intuition of good code design.
* Recognize when you have to create ugly hacks to make something work and redesign the base system to better fit all requirements instead of working around it. It is good to refactor the code often and shape it over and over like an artist. Start big and rough and then go over to the details.
Reiterate until everything falls neatly into place.
* Don’t let yourself be overwhelmed by construction sites in your code, just keep going step by step. Trust in your capabilities and go with the flow.
* If you don’t understand some piece of advice, don’t listen to it. It is more important to practice and make mistakes on your own. Many pieces of advice are often misleading, especially if they follow the format of “never do this” or ”always do that”. Some coding advice is usually correct in most business environments but not so much in video games. Or it makes sense for big teams, but not for small teams or if you are solo-dev.
* Don't care if you fail.

## Code Style
### Code Style Tools
Resharper
### Code that documents itself
#### Using Early Outs
#### Return values
Often we want to get multiple things out of a method, so it is a good practice to implement methods with out paramters instead of a return type. This way we don't have to ask ourselves, which return value is more important when adding more paramters. 

Also the readability for complex methods suffers if we do OperationX but also GetY. We want to avoid method names like DoOperationXAndGetY.
For this purpose see also [Core Enums](../Readme.md#core-enums) 
and [Enums.cs](../Runtime/Types/Enums.cs)

#### Interface Extensions

## Naming

### Namespaces
Use namespaces following the same structure as the folder hierarchy.

### Variables
use m_variable for private and protected members of classes.
when name is reserved add "@": `@ref @value`

### Class
Loose guideline for naming classes. Only apply where it makes sense.
* Abstract classes can be named `NameAbstract` or `NameBase`.
* MonoBehaviours that have an Update method can be named `NameBehaviour`.
* MonoBehaviours with Actions (methods to be linked by events to execute them) -> `NameAC` (Action Component)
* MonoBehaviours with Action that call a routine -> `NameRC` (Routine Component)
* MonoBehaviours with UnityEvents -> `NameEC` (Event Component)
* MonoBehaviours with just data -> `NameDC` (Data Component)
* Other MonoBehaviours (Unique/ SpecialCase, f.e. only called via specific context or for debugDraw) -> `NameComponent` or just `CustomName`
* GameEvents -> `NameEvent`
* Scriptable Actions -> `NameAction` & `NameActionConfig`
* ScriptablObjects -> `NameConfig`

### Interfaces
Use big letter I for interfaces like `IName`. Try ro make interface name readable like a sentence starting with "I". F.e. `IUpdateAttachmentPoints` ("I update attachment points"). (following the philosopy "Code that documents itself").

When using [InterfaceContainer][interface-container] you should name it `RefIName`.
You can use [`IProvider`][provider] interface to request interfaces from gameObjects.

### Data


## Code Order
Loose guideline for structuring blocks in code.

Primary rule of thumb:
1. properties.
2. members.
3. methods

Secondary rule of thumb:
1. public static
2. public
3. protected and internal
4. private

Using regions to further structure code by topic is encouraged.

## File Structure

## Todos in Code
Write todos like this `//todo X [TAG]: task-text`

replace `X` with a number from 0-5, where

* 5 Super Important - will do it ASAP.
* 4 Important - Must have
* 3 Medium importance - I don't yet how important/ Want have
* 2 Mid-low importance - minor cleanup / Nice to have
* 1 low importance - after all else / when bored
* 0 not important - I don't know what it means anymore/ Do it never

This way todos are sorted nicely by priority in visual studio task list when sorting by description.

replace `[TAG]` with your custom tag, f.e. `[FEATURE-XY]` to find todos for the feature you are working on easily. 

[interface-container]:(./link)
[provider]:(./link)