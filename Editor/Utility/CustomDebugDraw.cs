using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility
{
    /// <summary> Helper for debug-drawing other shapes via UnityEditor.Handles </summary>
    public class CustomHandleDraw
    {
        /// <summary> draws bounds box </summary>
        public static void DrawBounds(Bounds b, Color color)
        {
            var p0 = b.min;
            var p1 = p0 + (Vector3.right * b.size.x);
            var p2 = p0 + (Vector3.up * b.size.y);
            var p7 = b.max;
            var p3 = p7 - (Vector3.forward * b.size.z);
            var p4 = p0 + (Vector3.forward * b.size.z);
            var p5 = p7 - (Vector3.up * b.size.y);
            var p6 = p7 - (Vector3.right * b.size.x);

            using (new ColorScope(color))
            {
                Handles.DrawLine(p0, p1);
                Handles.DrawLine(p0, p2);
                Handles.DrawLine(p3, p1);
                Handles.DrawLine(p3, p2);

                Handles.DrawLine(p4, p5);
                Handles.DrawLine(p4, p6);
                Handles.DrawLine(p7, p5);
                Handles.DrawLine(p7, p6);

                Handles.DrawLine(p0, p4);
                Handles.DrawLine(p1, p5);
                Handles.DrawLine(p2, p6);
                Handles.DrawLine(p3, p7);
            }
        }
    }
}