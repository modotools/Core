﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Extensions;
using Core.Editor.PopupWindows;
using Core.Types;
using Core.Unity.Types;
using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Utility.InterfaceReference
{
    [CustomPropertyDrawer(typeof(IInterfaceContainer), true)]
    public class InterfaceContainerPropertyDrawer : PropertyDrawer
    {
        static IInterfaceContainer m_lastContainer;
        static Object m_lastTarget;
        static Object m_copyObj;
        
        public static float GetPropertyHeight() => 18f;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) =>
            DoGUI(position, property, label);
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) =>
            GetPropertyHeight();

        public static void DoGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var target = property.serializedObject.targetObject;
            
            var interfaceContainer = property.GetValue<IInterfaceContainer>();
            var containerType = interfaceContainer.GetContainedType();
            if (containerType == null)
            {
                EditorGUI.HelpBox(position, "containerType is null!", MessageType.Error);
                return;
            }

            using (var hs = new HorizontalRectScope(ref position, 18f))
            {
                var objProp = property.FindPropertyRelative(InterfaceContainerBase.Editor_ObjectFieldPropName);
                var btnWidth = 20f;
                var labelWidth = EditorGUIUtility.labelWidth;
                EditorGUI.LabelField(hs.Get(labelWidth), new GUIContent($"{property.name} ({containerType}):"));
                var restWidth = hs.Rect.width;

                var objRef = objProp.objectReferenceValue;
                var guiContent = EditorGUIUtility.ObjectContent(objRef, typeof(Object));
                var style = new GUIStyle("TextField")
                {
                    fixedHeight = 16, imagePosition = objRef ? ImagePosition.ImageLeft : ImagePosition.TextOnly
                };

                var mainRect = hs.Get(restWidth - btnWidth - btnWidth - 5);
                if (GUI.Button(mainRect, guiContent, style ) && objRef)
                    EditorGUIUtility.PingObject(objRef);
                UpdateDragAndDrop(mainRect, target, interfaceContainer, containerType);
                UpdateContext(mainRect, target, interfaceContainer);
                
                var mono = target as MonoBehaviour;
                // var scriptable = target as ScriptableObject;

                var isMono = mono != null;
                var scenePickAllowed = isMono;
                
                var sceneIcon = EditorGUIUtility.FindTexture("d_UnityLogo");
                var folderIcon = EditorGUIUtility.FindTexture("Folder Icon");

                var findInScene = false;
                var findInSceneRect = hs.Get(btnWidth);
                using (new EditorGUI.DisabledScope(!scenePickAllowed))
                    findInScene = GUI.Button(findInSceneRect, sceneIcon, EditorStyles.miniButtonRight);

                var findInProjectRect = hs.Get(btnWidth);
                var findInProject = GUI.Button(findInProjectRect, folderIcon, EditorStyles.miniButtonRight);

                Rect popupRect = default;
                PopupWindowContent popup = null;
                if (findInScene)
                {
                    var scene = mono.gameObject.scene;
                    var roots = scene.GetRootGameObjects();
                    var monoPopup = new SelectMonoBehavioursPopup();
                    PopulateContentFromScene(roots, containerType, monoPopup);

                    popup = monoPopup;
                    popupRect = findInSceneRect;
                }

                if (findInProject)
                {
                    var scPopup = new SelectScriptableObjectsPopup();
                    PopulateFromProject(containerType, scPopup);
                    
                    popup = scPopup;
                    popupRect = findInProjectRect;
                }

                if (popup != null)
                {
                    m_lastTarget = target;
                    m_lastContainer = interfaceContainer;
                    PopupWindow.Show(popupRect, popup);
                }
            }
        }
        
        static void PopulateFromProject(Type containerType, SelectScriptableObjectsPopup popup)
        {
            using (var objs = SimplePool<List<ScriptableObject>>.I.GetScoped())
            {
                var guids = AssetDatabase.FindAssets($"t:{typeof(ScriptableObject)}");
                foreach (var g in guids)
                {
                    var path = AssetDatabase.GUIDToAssetPath(g);
                    var allAssetsAtPath = AssetDatabase.LoadAllAssetsAtPath(path);
                    foreach (var o in allAssetsAtPath)
                    {
                        if (!(o is ScriptableObject so))
                            continue;
                        if (!containerType.IsInstanceOfType(o))
                            continue;
                        objs.Obj.Add(so);
                    }

                    // Debug.Log($"{AssetDatabase.GUIDToAssetPath(g)}");
                } // AssetDatabase.FindAssets()

                popup.SetContent(objs.Obj, SetFromProject);
            }
        }

        static void PopulateContentFromScene(IEnumerable<GameObject> roots, Type containerType, SelectMonoBehavioursPopup popup)
        {
            using (var objs = SimplePool<List<MonoBehaviour>>.I.GetScoped())
            {
                foreach (var r in roots)
                {
                    var allComponents = r.GetComponentsInChildren<MonoBehaviour>(true)
                        .Where(containerType.IsInstanceOfType);

                    foreach (var monoBehaviour in allComponents)
                        objs.Obj.Add(monoBehaviour);
                }

                popup.SetContent(objs.Obj, SetFromScene);
            }
        }
        
        static void SetFromProject(object userdata)
        {
            var sc = (ScriptableObject) userdata;
            m_lastContainer.SetObject(sc);
            EditorUtility.SetDirty(m_lastTarget);
        }

        static void SetFromScene(object userdata)
        {
            var mono = (MonoBehaviour) userdata;
            m_lastContainer.SetObject(mono);
            EditorUtility.SetDirty(m_lastTarget);
        }
        
        static void SetFromAny(object userdata)
        {
            var mono = (Object) userdata;
            m_lastContainer.SetObject(mono);
            EditorUtility.SetDirty(m_lastTarget);
        }
        static void Copy() => m_copyObj = m_lastContainer.GetObject();
        static void Paste()
        {
            m_lastContainer.SetObject(m_copyObj);
            EditorUtility.SetDirty(m_lastTarget);
        }

        static void FilterObjectReferences(IEnumerable<Object> objRefs, Type containerType, List<Object> filteredList)
        {
            foreach (var oRef in objRefs)
            {
                if (containerType.IsInstanceOfType(oRef))
                {
                    filteredList.Add(oRef);
                    continue;
                }
                if (oRef is GameObject go)
                {
                    filteredList.AddRange(go.GetComponents<Component>().Where(containerType.IsInstanceOfType));
                    continue;
                }
                if (oRef is ScriptableObject so)
                {
                    var assets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(so));
                    filteredList.AddRange(assets.Where(containerType.IsInstanceOfType));
                }
            }
        }
        
        static void UpdateContext(Rect mainRect, Object target, IInterfaceContainer container)
        {
            var currentEvent = Event.current;

            if (currentEvent.isMouse && currentEvent.button == 1 && currentEvent.type == EventType.MouseUp)
            {
                m_lastTarget = target;
                m_lastContainer = container;
                
                var menu = new GenericMenu();
                menu.AddItem(new GUIContent("Copy"), false, Copy);
                menu.AddItem(new GUIContent("Paste"), false, Paste);
                menu.ShowAsContext();
            }
        }

        static void UpdateDragAndDrop(Rect dropArea, Object target, IInterfaceContainer container, Type containerType)
        {
            var currentEvent = Event.current;
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (currentEvent.rawType)
            {
                case EventType.DragExited:
                case EventType.DragUpdated:
                case EventType.DragPerform:
                if (!dropArea.Contains(currentEvent.mousePosition))
                    break;

                using (var scoped = SimplePool<List<Object>>.I.GetScoped())
                {
                    FilterObjectReferences(DragAndDrop.objectReferences, containerType, scoped.Obj);
                    DragAndDrop.visualMode = scoped.Obj.Count > 0 ? DragAndDropVisualMode.Copy : DragAndDropVisualMode.Rejected;
                    
                    if (currentEvent.type == EventType.DragPerform)
                    {
                        if (scoped.Obj.Count == 1)
                        {
                            DragAndDrop.AcceptDrag();
                            currentEvent.Use();
                            container.SetObject(scoped.Obj.FirstOrDefault());
                        }
                        else
                        {
                            m_lastTarget = target;
                            m_lastContainer = container;
                            var objPopup = new SelectObjectPopup(SetFromAny, scoped.Obj.ToArray());
                            PopupWindow.Show(dropArea, objPopup);
                        }
                    }
                }
                break;
            }
        }


    }
}