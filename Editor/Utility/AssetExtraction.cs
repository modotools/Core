﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Core.Editor.Tools;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility
{
    public static class AssetExtraction
    {
        public struct SubAssetMapping
        {
            public Object OrigAsset;
            public string OrigGUID;
            public long OrigLocalId;

            public Object CloneAsset;
            public string CloneGUID;
            public long CloneLocalId;

            public override string ToString()
                => $"{OrigAsset} {OrigGUID} {OrigLocalId} {CloneAsset} {CloneLocalId} {CloneGUID}";
        }

        public static void Extract(SerializedObject mainAsset,
           ScriptableObject asset,
           SerializedProperty linkToProperty)
        {
            var mainAssetObj = mainAsset.targetObject;
            var hierarchy = SubAssetHierarchy.GetSubAssetHierarchy(mainAssetObj);
            if (SubAssetHierarchy.TryFindSubAssetInHierarchy(asset, hierarchy, out var subAssetHierarchy) == FindResult.NotFound)
            {
                Debug.LogError("SubAsset is not part of linear hierarchy! Cannot extract. Other asset might depend on it.");
                return;
            }

            var name = asset is INamed named ? named.Name : asset.name;
            name = Regex.Replace(name, "[^\\w\\._]", "");

            var path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(asset)) + "/" + name + ".asset";
            var extractedAsset = Object.Instantiate(asset);
            extractedAsset.name = name;
            AssetDatabase.CreateAsset(extractedAsset, path);

            using (var mappingScope = SimplePool<List<SubAssetMapping>>.I.GetScoped())
            {
                var mapping = mappingScope.Obj;
                ExtractSubSubAssets(extractedAsset, subAssetHierarchy, mapping);

                AssetDatabase.SaveAssets();
                ReplaceAllGuids(path, mapping);

                linkToProperty.objectReferenceValue = AssetDatabase.LoadAssetAtPath(path, typeof(ScriptableObject));
                mainAsset.ApplyModifiedProperties();

                asset.DestroyEx();
                foreach (var entry in mapping)
                    entry.OrigAsset.DestroyEx();
            }

            AssetDatabase.Refresh();
        }


        public static void Integrate(SerializedObject mainAsset, ScriptableObject itAction, SerializedProperty linkToProperty)
        {
            if (!AssetDatabase.IsMainAsset(itAction))
            {
                Debug.LogError("Cannot integrate assets that are part of other assets");
                return;
            }
            var hierarchy = SubAssetHierarchy.GetSubAssetHierarchy(itAction);

            var mainAssetObj = mainAsset.targetObject;
            var clone = Object.Instantiate(itAction);
            clone.name = itAction.name;
            clone.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(clone, mainAssetObj);
            linkToProperty.objectReferenceValue = clone;
            mainAsset.ApplyModifiedProperties();

            using (var mappingScope = SimplePool<List<SubAssetMapping>>.I.GetScoped())
            {
                var mapping = mappingScope.Obj;

                ExtractSubSubAssets(mainAssetObj, hierarchy, mapping);

                AssetDatabase.SaveAssets();
                ReplaceAllGuids(AssetDatabase.GetAssetPath(mainAssetObj), mapping);
            }

            AssetDatabase.Refresh();
        }

        public static void ExtractSubSubAssets(Object mainAsset, SubAssetHierarchy.SubAssetData subAsset, ICollection<SubAssetMapping> mapping)
        {
            if (subAsset.SubAssets.IsNullOrEmpty())
                return;
            foreach (var contained in subAsset.SubAssets)
                ExtractSubSubAsset(mainAsset, contained, mapping);
        }

        public static void ExtractSubSubAsset(Object mainAsset, SubAssetHierarchy.SubAssetData subAsset, ICollection<SubAssetMapping> mapping)
        {
            if (subAsset.Asset == null)
                return;

            if (!AssetDatabase.TryGetGUIDAndLocalFileIdentifier(subAsset.Asset, out var originalGuid, out long originalLocalId))
            {
                Debug.LogError($"ExtractSubSubAsset Fail!\n" +
                               $"TryGetGUIDAndLocalFileIdentifier failed for {subAsset.Asset}");
                return;
            }

            var subSubClone = Object.Instantiate(subAsset.Asset);
            subSubClone.name = subAsset.Asset.name;
            subSubClone.hideFlags = HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(subSubClone, mainAsset);

            if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(subSubClone, out var guid, out long localId))
            {
                var map = new SubAssetMapping()
                {
                    CloneAsset = subSubClone,
                    CloneGUID = guid,
                    CloneLocalId = localId,
                    OrigAsset = subAsset.Asset,
                    OrigGUID = originalGuid,
                    OrigLocalId = originalLocalId
                };
                mapping.Add(map);
                Debug.Log($"Added Mapping {map}");
            }

            else Debug.LogError($"ExtractSubSubAsset Fail!\n" +
                                $"TryGetGUIDAndLocalFileIdentifier of newly cloned asset failed " +
                                $"for clone {subSubClone} of {subAsset.Asset}");

            // recursive
            ExtractSubSubAssets(mainAsset, subAsset, mapping);
        }

        public static void ReplaceAllGuids(string assetPath, IEnumerable<SubAssetMapping> mapping)
        {
            var contents = File.ReadAllText(assetPath);

            Debug.Log("File read");
            foreach (var entry in mapping)
            {
                var oldRefStringA = $"{{fileID: {entry.OrigLocalId}, guid: {entry.OrigGUID}, type: 2}}";
                var oldRefStringB = $"{{fileID: {entry.OrigLocalId}, guid: {entry.OrigGUID},";

                //var oldRefString3 = $"{{fileID: {entry.OrigLocalId}, guid: {entry.OrigGUID}, type: 3}}";
                var newRefStringA = $"{{fileID: {entry.CloneLocalId}}}";
                var newRefStringB = $"{{fileID: {entry.CloneLocalId},";

                contents = contents.Replace(oldRefStringA, newRefStringA);
                contents = contents.Replace(oldRefStringB, newRefStringB);
                //contents = contents.Replace(oldRefString3, newRefString);
            }

            File.WriteAllText(assetPath, contents);
            Debug.Log("File saved");
        }
    }
}
