//#define MATRIX_SCALE_ELEMENTS

using System;
using System.Collections.Generic;
using Core.Editor.Interface.GUI;
using UnityEngine;

namespace Core.Editor.Utility.NodeGUI
{
    public static class GUINodeOperations
    {
        public static Vector2 GetGUIElementPos(this IGUIElement element, IGUIGrid grid) => 
            0.5f * grid.DrawRect.size + ((element.Pos + grid.Offset) * grid.Zoom);

        public static Vector2 FromGUIPos(this Vector2 guiPos, IGUIGrid grid) => 
            (guiPos - 0.5f * grid.DrawRect.size) / grid.Zoom - grid.Offset;

        public static Rect GetGUIElementRect(this IGUIElement element, IGUIGrid grid) => 
            new Rect(element.GetGUIElementPos(grid), grid.Zoom * element.Size);

        public static void DrawElement(this IGUIElement element, IGUIGrid grid)
        {
#if MATRIX_SCALE_ELEMENTS
            var pos = element.GetGUIElementPos(grid);
            var weirdOffset = (1 - 1 / grid.Zoom) * -element.Size.y* grid.Zoom;
            pos.y += weirdOffset;
            var nodeRect = new Rect(0, 0, element.Size.x, element.Size.y);

            var originalGUIMatrix = GUI.matrix;

            var translation = Matrix4x4.TRS(new Vector3(pos.x, pos.y,0), Quaternion.identity, Vector3.one);
            var scale = Matrix4x4.Scale(new Vector3(grid.Zoom, grid.Zoom, 1.0f));
            GUI.matrix = translation * scale;
            element.DrawElement(nodeRect);
            GUI.matrix = originalGUIMatrix;
#else
            var nodeRect = element.GetGUIElementRect(grid);
            element.DrawElement(nodeRect);
#endif
        }

        public static void DrawElement(this IGUIElement element, Rect nodeRect)
        {
            Texture icon = element is IGUIWithIcon guiIcon ? guiIcon.Icon : null;
            var text = element is IGUIWithText txt ? txt.Text : null;

            var c = new GUIContent() {image = icon, text = text };
            var isOn = (element is IGUISelectable selectable && selectable.IsSelected);
            if (Event.current.type == EventType.Repaint)
                element.Style.Draw(nodeRect, c, false, isOn, isOn, isOn);

            //DrawNodeIcon(nodeRect, node);
            //DrawNodeName(nodeRect, node);
        }

        public static Vector2 GetIncomingPos(this IGUIConnectable node, IGUIGrid g)
        {
            var rect = node.GetGUIElementRect(g);
            return GetConnectionPos(rect, node.IncomingPos);
        }

        public static Vector2 GetOutgoingPos(this IGUIConnectable node, IGUIGrid g)
        {
            var rect = node.GetGUIElementRect(g);
            return GetConnectionPos(rect, node.OutgoingPos);
        }

        public static Vector2 GetConnectionPos(Rect nodeRect, ConnectionPos pos)
        {
            switch (pos)
            {
                case ConnectionPos.None: return default;
                case ConnectionPos.Left: return new Vector2(nodeRect.xMin, nodeRect.center.y);
                case ConnectionPos.Up: return new Vector2(nodeRect.center.x, nodeRect.yMin);
                case ConnectionPos.Right: return new Vector2(nodeRect.xMax, nodeRect.center.y);
                case ConnectionPos.Down: return new Vector2(nodeRect.center.x, nodeRect.yMax);
                default: throw new ArgumentOutOfRangeException(nameof(pos), pos, null);
            }
        }

        public static bool ProcessEvents(this IGUIElement thisElement, IGUIGrid grid, List<IGUISelectable> selectedNodes, ref IGUIDraggable draggedNode)
        {
            var nodeRect = thisElement.GetGUIElementRect(grid);
            nodeRect.position += grid.EventRect.position;
            if (thisElement is IGUISelectable thisAsSelectable)
                thisAsSelectable.IsSelected = selectedNodes.Contains(thisAsSelectable);
            var isDragged = draggedNode == thisElement;
            var e = Event.current;
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (e.type)
            {
                case EventType.MouseDown when nodeRect.Contains(e.mousePosition):
                    thisElement.OnMouseDown(selectedNodes, ref draggedNode);
                    return true;
                case EventType.MouseDrag when isDragged:
                    if (thisElement is IGUIDraggable thisAsDraggable)
                        thisAsDraggable.Pos += e.delta / grid.Zoom;
                    e.Use();
                    return true;
                case EventType.MouseUp when isDragged:
                    draggedNode = null;
                    break;
            }

            return false;
        }

        static void OnMouseDown(this IGUIElement thisElement, List<IGUISelectable> selectedNodes, ref IGUIDraggable draggedNode)
        {
            var e = Event.current;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (e.button)
            {
                case 0:
                    if (thisElement is IGUIDraggable thisAsDraggable)
                        draggedNode = thisAsDraggable;
                    if (thisElement is IGUISelectable thisAsSelectable)
                    {
                        selectedNodes.Clear();
                        selectedNodes.Add(thisAsSelectable);
                        thisAsSelectable.IsSelected = true;
                    }
                    
                    GUI.changed = true;
                    break;
                case 1:
                    if (thisElement is IGUIWithContextMenu thisAsWithContextMenu)
                        thisAsWithContextMenu.ContextMenu.ShowAsContext();
                    e.Use();
                    break;
            }
        }

        //static void DrawNodeIcon(Rect nodeRect, IGUINode node)
        //{
        //    if (node.Icon == null)
        //        return;
        //    // too small for icon
        //    if(_!(nodeRect.size.x > 30.0f))
        //        return;
        //    var sizeDiff = nodeRect.size - node.IconSize;
        //    var centeredPos = nodeRect.position + 0.5f * sizeDiff;
        //    var imageRect = new Rect(centeredPos, node.IconSize);
        //    GUI.DrawTexture(imageRect, node.Icon);
        //}
        //static void DrawNodeName(Rect nodeRect, IGUINode node)
        //{
        //    if (node.Name.IsNullOrEmpty())
        //        return;
        //    var textRect = new Rect(nodeRect);
        //    var textStyle = EditorStyles.miniBoldLabel;
        //    var textContent = new GUIContent(node.Name);
        //    textRect.size = textStyle.CalcSize(textContent) + 2.0f * Vector2.one;
        //    textRect.x += (nodeRect.width - textRect.size.x) * 0.5f + node.TextOffset.x;
        //    textRect.y -= textRect.size.y + node.TextOffset.y;
        //    GUI.Label(textRect, node.Name, textStyle);
        //}
        //public static Rect GetConnectionPointLPos(Rect nodeRect, Vector2 conPSize)
        //{
        //    var pos = new Vector2(nodeRect.x - conPSize.x + 8, nodeRect.y + (nodeRect.height * 0.5f) - conPSize.y * 0.5f);
        //    return new Rect(pos, conPSize);
        //}
        //public static Rect GetConnectionPointDPos(Rect nodeRect, Vector2 conPSize)
        //{
        //    var pos = new Vector2(nodeRect.x + (nodeRect.width * 0.5f) - conPSize.x * 0.5f, nodeRect.y + nodeRect.height - 8);
        //    return new Rect(pos, conPSize);
        //}
        //public static Rect GetConnectionPointRPos(Rect nodeRect, Vector2 conPSize)
        //{
        //    var pos = new Vector2(nodeRect.x + nodeRect.width - 8, nodeRect.y + (nodeRect.height * 0.5f) - conPSize.y * 0.5f);
        //    return new Rect(pos, conPSize);
        //}
        //public static void DrawConnectionPoint(Rect pos) //, Vector2 offset)
        //{
        //    GUI.Box(pos, "", CustomStyles.DarkSkinButton);
        //}
    }
}