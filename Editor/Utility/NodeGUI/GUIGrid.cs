﻿using Core.Editor.Interface.GUI;
using UnityEngine;

namespace Core.Editor.Utility.NodeGUI
{
    public class GUIGrid : IGUIGrid, IRectSelection
    {
        public Vector2 Offset { get; set; }
        public Rect DrawRect { get; set; }
        public Rect EventRect { get; set; }
        public bool Selecting { get; set; }
        public Rect CurrentSelection { get; set; }

        public Color GridColor { get; set; }
        public bool Dragging { get; set; }
        public float Zoom { get; set; }

        public float CoarseGridSpacing;
        public float CoarseGridOpacity;
        public float FineGridSpacing => CoarseGridSpacing * 0.2f;
        public float FineGridOpacity => CoarseGridOpacity * 0.5f;

        public Rect GetNodeSelection()
        {
            var min = CurrentSelection.position.FromGUIPos(this);
            var max = CurrentSelection.max.FromGUIPos(this);
            return new Rect(CurrentSelection.position.FromGUIPos(this), max-min);
        }
    }
}