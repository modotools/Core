
using System;
using System.Collections.Generic;
using Core.Editor.Interface.GUI;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility.NodeGUI
{
    using GUIElementAction = Action<IGUIElement>;

    public class GUIElementContextMenuFactory
    {
        public Dictionary<string, GUIElementAction> GUIElementActions = new Dictionary<string, GUIElementAction>();

        public GenericMenu CreateMenu(IGUIElement node)
        {
            var menu = new GenericMenu();
            foreach (var action in GUIElementActions)
            {
                menu.AddItem(new GUIContent(action.Key), false, () => action.Value?.Invoke(node));
            }

            return menu;
        }
    }
}