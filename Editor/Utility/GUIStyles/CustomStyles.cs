﻿using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility.GUIStyles
{
    public static class CustomStyles
    {
        static GUIStyle m_sortListStyleHighlight;

        public static GUIStyle SortListStyleHighlight
        {
            get
            {
                if (m_sortListStyleHighlight != null)
                    return m_sortListStyleHighlight;
                m_sortListStyleHighlight = GUI.skin.FindStyle("MeTransitionSelectHead");
                return m_sortListStyleHighlight;
            }
        }


        static GUIStyle m_miniFoldoutStyle;
        /// <summary>
        /// Style for foldouts
        /// </summary>
        public static GUIStyle MiniFoldoutStyle
        {
            get
            {
                if (m_miniFoldoutStyle != null)
                    return m_miniFoldoutStyle;
                m_miniFoldoutStyle = new GUIStyle(EditorStyles.foldout) { fontSize = 10 };
                return m_miniFoldoutStyle;
            }
        }

        static readonly Color k_selectedColor = new Color(0.5f, 0.5f, 0.75f, 1.0f);
        static GUIStyle m_selectableLabel;
        /// <summary>
        /// Style for SelectableLabel
        /// </summary>
        public static GUIStyle SelectableLabel
        {
            get
            {
                if (m_selectableLabel != null)
                    return m_selectableLabel;

                m_selectableLabel = new GUIStyle(EditorStyles.largeLabel);
                var state = new GUIStyleState { background = new Texture2D(1, 1) };
                m_selectableLabel.richText = true;
                state.background.SetPixel(0, 0, k_selectedColor);
                state.background.Apply();

                state.textColor = Color.white;
                m_selectableLabel.onNormal = state;
                return m_selectableLabel;
            }
        }


        static GUIStyle m_richLabel;
        /// <summary>
        /// Style for Rich Labels
        /// </summary>
        public static GUIStyle RichLabel
        {
            get
            {
                if (m_richLabel != null)
                    return m_richLabel;

                m_richLabel = new GUIStyle(EditorStyles.label) {richText = true};
                return m_richLabel;
            }
        }


        static GUIStyle m_grayBackground;
        /// <summary>
        /// Style for SelectableLabel
        /// </summary>
        public static GUIStyle Gray
        {
            get
            {
                if (m_grayBackground?.normal != null 
                    && m_grayBackground.normal.background != null)
                    return m_grayBackground;

                m_grayBackground = new GUIStyle();
                StyleHelpers.InitStyleOnOff(m_grayBackground, Texture2D.grayTexture, Texture2D.whiteTexture);

                // var texture = new Texture2D(1, 1);
                // texture.SetPixel(0, 0, new Color(0.5f, 0.5f, 0.5f, 1.0f));
                // texture.Apply();
                // var state = new GUIStyleState { background = new Texture2D(1, 1) };
                // state.textColor = Color.white;
                // m_grayBackground.normal = state;
                // m_grayBackground.onNormal = state;
                // m_grayBackground.active = state;
                // m_grayBackground.onActive = state;
                // m_grayBackground.focused = state;
                // m_grayBackground.onFocused = state;
                // m_grayBackground.hover = state;
                // m_grayBackground.onHover = state;
                return m_grayBackground;
            }
        }
    }
}