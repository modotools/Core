using System.Linq;
using Core.Unity.Utility;
using UnityEngine;
using UnityEditor;

namespace Core.Editor.Utility
{
    [InitializeOnLoad]
    public class HierarchyHighlightManager
    {
        static HierarchyHighlightManager()
        {
            EditorApplication.hierarchyWindowItemOnGUI -= HierarchyHighlight_OnGUI;
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyHighlight_OnGUI;
        }

        static void HierarchyHighlight_OnGUI(int inSelectionID, Rect inSelectionRect)
        {
            var go_label = EditorUtility.InstanceIDToObject(inSelectionID) as GameObject;

            if (go_label == null) 
                return;
            var label = go_label.GetComponent<IHierarchyHighlighter>();

            if (label == null || Event.current.type != EventType.Repaint)
                return;
            
            var objectIsSelected = Selection.instanceIDs.Contains(inSelectionID);

            ColorStyle(label, objectIsSelected, out var bkCol, out var textCol, out var textStyle);

            var offset = new Rect(inSelectionRect.position + new Vector2(18f, 0f), inSelectionRect.size);
            
            DrawBackground(offset, bkCol, label, objectIsSelected);

            EditorGUI.LabelField(offset, go_label.name, new GUIStyle()
            {
                normal = new GUIStyleState() { textColor = textCol },
                fontStyle = textStyle
            });

            EditorApplication.RepaintHierarchyWindow();
        }

        static void DrawBackground(Rect inSelectionRect, Color bkCol, IHierarchyHighlighter label, bool objectIsSelected)
        {
            //Only draw background if background color is not completely transparent
            if (bkCol.a <= 0f) 
                return;
            var backgroundOffset = new Rect(inSelectionRect.position, inSelectionRect.size);

            //If the background has transparency, draw a solid color first
            if (label.DefaultStyle.BackgroundColor.a < 1f || objectIsSelected)
            {
                // todo: Pull background color from GUI.skin Style
                EditorGUI.DrawRect(backgroundOffset, label.DefaultBackgroundColor);
            }

            //Draw background
            EditorGUI.DrawRect(backgroundOffset, bkCol);
        }

        static void ColorStyle(IHierarchyHighlighter label, bool isSelected, out Color bkCol, out Color textCol, out FontStyle textStyle)
        {
            bkCol = label.DefaultStyle.BackgroundColor;
            textCol = label.DefaultStyle.TextColor;
            textStyle = label.DefaultStyle.TexStyle;
            
            if (!label.gameObject.activeInHierarchy)
                InactiveColor(label, ref bkCol, ref textCol, ref textStyle);
            else if (isSelected)
                SelectedColor(label, ref bkCol, ref textCol, ref textStyle);
        }

        static void SelectedColor(IHierarchyHighlighter label, ref Color bkCol, ref Color textCol, ref FontStyle textStyle)
        {
            if (label.SelectedStyle.Active)
            {
                bkCol = label.SelectedStyle.BackgroundColor;
                textCol = label.SelectedStyle.TextColor;
                textStyle = label.SelectedStyle.TexStyle;
            }
            else
            {
                bkCol = Color.Lerp(GUI.skin.settings.selectionColor, bkCol, 0.3f);
            }
        }

        static void InactiveColor(IHierarchyHighlighter label, ref Color bkCol, ref Color textCol, ref FontStyle textStyle)
        {
            if (label.InactiveStyle.Active)
            {
                bkCol = label.InactiveStyle.BackgroundColor;
                textCol = label.InactiveStyle.TextColor;
                textStyle = label.InactiveStyle.TexStyle;
            }
            else
            {
                if (bkCol != label.DefaultBackgroundColor)
                    bkCol.a *= 0.5f; //Reduce opacity by half
                textCol.a *= 0.5f;
            }
        }
    }
}