using UnityEngine;

namespace Core.Editor.Utility
{
    public static class RichTextUtility
    {
        public static string Colored(string text, Color color) => $"<color=#{ColorUtility.ToHtmlStringRGBA(color)}>{text}</color>";
        public static string Bold(string text) => $"<b>{text}</b>";
        public static string Italic(string text) => $"<i>{text}</i>";
        public static string Sized(string text, int size) => $"<size={size}>{text}</size>";

        /// <summary>
        /// This is only useful for text meshes and renders a section of text with a material specified by the parameter.
        /// The value is an index into the text mesh's array of materials as shown by the inspector.
        /// </summary>
        public static string Material(string text, int idx) => $"<material={idx}>{text}</material>";
        
        /// <summary>
        /// <para><see href="https://docs.unity3d.com/Packages/com.unity.ugui@1.0/manual/StyledText.html"> Link </see></para>
        /// This is only useful for text meshes and renders an image inline with the text. 
        /// It takes parameters that specify the material to use for the image, the image height in pixels,
        /// and a further four that denote a rectangular area of the image to display.
        /// Unlike the other tags, quad does not surround a piece of text
        /// and so there is no ending tag - the slash character is placed at the end of the
        /// initial tag to indicate that it is "self-closing".
        /// </summary>
        /// <remarks>
        /// This selects the material at position in the renderer's material array
        /// and sets the height of the image to 20 pixels. The rectangular area of image
        /// starts at given by the x, y, width and height values, which are all given
        /// as a fraction of the unscaled width and height of the texture.
        /// </remarks>
        public static string Quad(string text, int idx, int size, float x, float y, float w, float h) 
            => $"<quad material={idx} size={size} x={x} y={y} width={w} height={h}>{text}</quad>";

    }
}