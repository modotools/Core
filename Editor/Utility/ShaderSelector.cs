using UnityEditor;
using Core.Editor.PopupWindows;

namespace Core.Editor.Utility
{
    public static class ShaderSelector
    {
        public static SelectTypesPopup CreateShaderSelectorPopup(GenericMenu.MenuFunction2 menuFunc)
        {
            var menu = new SelectTypesPopup();

            foreach (var shdInfo in ShaderUtil.GetAllShaderInfo())
            {
                var path = shdInfo.name;
                menu.AddTypeItem(path, menuFunc, shdInfo.name);
            }

            return menu;
        }
    }
}

