using System;
using Core.Editor.Interface;
using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility
{
    /// <summary> Helper for GUI </summary>
    public static class CustomGUI
    {
        static readonly GUIStyle k_splitterHorizontal;
        static readonly GUIStyle k_splitterVertical;

        static CustomGUI()
        {
            k_splitterHorizontal = new GUIStyle
            {
                normal = {background = EditorGUIUtility.whiteTexture},
                stretchWidth = true,
                margin = new RectOffset(0, 0, 7, 7)
            };
            k_splitterVertical = new GUIStyle
            {
                normal = {background = EditorGUIUtility.whiteTexture},
                stretchHeight = true,
                margin = new RectOffset(7, 7, 0, 0)
            };
        }

        static readonly Color k_splitterColor = EditorGUIUtility.isProSkin ? new Color(0.157f, 0.157f, 0.157f) : new Color(0.5f, 0.5f, 0.5f);
        static readonly Color k_primaryActiveColor = Color.cyan;
        static readonly Color k_secondaryActiveColor = Color.magenta;
        static readonly Color k_combinedActiveColor = Color.yellow;


        /// <summary> GUILayout Splitter </summary>
        /// <param name="thickness"> thickness of splitter </param>
        public static void HSplitter(float thickness = 1) => Splitter(k_splitterHorizontal, k_splitterColor, GUILayout.Height(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="color"> Color of Splitter</param>
        /// <param name="thickness"> thickness of splitter </param>
        public static void HSplitter(Color color, float thickness = 1)
            => Splitter(k_splitterHorizontal, color, GUILayout.Height(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="thickness"> thickness of splitter </param>
        /// <param name="splitterStyle"> style for splitter </param>
        public static void HSplitter(GUIStyle splitterStyle, float thickness = 1f)
            => Splitter(splitterStyle, k_splitterColor, GUILayout.Height(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="thickness"> thickness of splitter </param>
        public static void VSplitter(float thickness = 1) => Splitter(k_splitterVertical, k_splitterColor, GUILayout.Width(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="color"> Color of Splitter</param>
        /// <param name="thickness"> thickness of splitter </param>
        public static void VSplitter(Color color, float thickness = 1)
            => Splitter(k_splitterVertical, color, GUILayout.Width(thickness));

        /// <summary> GUILayout Splitter </summary>
        /// <param name="thickness"> thickness of splitter </param>
        /// <param name="splitterStyle"> style for splitter </param>
        public static void VSplitter(GUIStyle splitterStyle, float thickness = 1f)
            => Splitter(splitterStyle, k_splitterColor, GUILayout.Height(thickness));

        public static void Splitter(GUIStyle splitterStyle, Color color, params GUILayoutOption[] options)
        {
            var position = GUILayoutUtility.GetRect(GUIContent.none, splitterStyle, options);

            if (Event.current.type != EventType.Repaint)
                return;
            var restoreColor = GUI.color;
            GUI.color = color;
            splitterStyle.Draw(position, false, false, false, false);
            GUI.color = restoreColor;
        }


        public static bool ButtonOrReturnKey(string buttonText, params GUILayoutOption[] options)
            => ButtonOrKey(buttonText, KeyCode.Return, options);

        public static bool ButtonOrKey(string buttonText, KeyCode k, params GUILayoutOption[] options)
        {
            var e = Event.current;
            return GUILayout.Button(buttonText, options) || 
                   ((e.type == EventType.KeyDown || e.type == EventType.KeyUp)
                    && e.keyCode == k);
        }

        /// <summary> draws button that can be activated like a toggle </summary>
        /// <param name="active"> is the button active </param>
        /// <param name="text"> text of button </param>
        /// <param name="options"> GUILayout-options </param>
        /// <returns> was button pressed </returns>
        // ReSharper disable FlagArgument
        public static bool ActivityButton(bool active, string text, bool disableWhenActive = true,
            GUIStyle style = null, params GUILayoutOption[] options)
            => ActivityButton(active, new GUIContent(text), disableWhenActive, style, options);
        public static bool ActivityButton(bool active, GUIContent text, bool disableWhenActive = true, GUIStyle style = null, params GUILayoutOption[] options)
        {
            if (style == null)
                style = EditorStyles.toolbarButton;
            var prevCol = GUI.backgroundColor;
            var buttonPressed = false;
            if (active)
                GUI.backgroundColor = k_primaryActiveColor;

            using (new EditorGUI.DisabledScope(active && disableWhenActive))
            {
                if (GUILayout.Button(text, style, options))
                    buttonPressed = true;
            }

            if (active)
                GUI.backgroundColor = prevCol;

            return buttonPressed;
        }

        public static bool ActivityButton(bool active, string text, Texture2D tex, params GUILayoutOption[] options)
        {
            var prevCol = GUI.backgroundColor;
            var buttonPressed = false;
            if (active)
                GUI.backgroundColor = k_primaryActiveColor;

            using (new EditorGUI.DisabledScope(active))
            {
                var content = new GUIContent(text, tex);
                if (GUILayout.Button(content, options))
                    buttonPressed = true;
            }

            if (active)
                GUI.backgroundColor = prevCol;

            return buttonPressed;
        }

        /// <summary> draws button that can have two different toggle states combined </summary>
        public static bool ActivityButton2(bool active1, bool active2, string text, params GUILayoutOption[] options)
        {
            var prevCol = GUI.backgroundColor;
            var buttonPressed = false;
            if (active1)
                GUI.backgroundColor = k_primaryActiveColor;
            if (active2)
                GUI.backgroundColor = k_secondaryActiveColor;
            if (active1 && active2)
                GUI.backgroundColor = k_combinedActiveColor;

            if (GUILayout.Button(text, options))
                buttonPressed = true;

            if (active1 || active2)
                GUI.backgroundColor = prevCol;

            return buttonPressed;
        }

        /// <summary> draws button that can have two different toggle states combined and a texture </summary>
        public static bool ActivityButton2(bool active1, bool active2, string text, Texture2D texture, params GUILayoutOption[] options)
        {
            var prevCol = GUI.backgroundColor;
            var buttonPressed = false;
            if (active1)
                GUI.backgroundColor = k_primaryActiveColor;
            if (active2)
                GUI.backgroundColor = k_secondaryActiveColor;
            if (active1 && active2)
                GUI.backgroundColor = k_combinedActiveColor;

            var content = new GUIContent(text, texture);
            if (GUILayout.Button(content, options))
                buttonPressed = true;

            if (active1 || active2)
                GUI.backgroundColor = prevCol;

            return buttonPressed;
        }
        // ReSharper restore FlagArgument

        static string m_lastFocusedControl;
        static void UpdateCachedFocusName(IRepaintable editor = null)
        {
            // we cache focus name because it didn't return correctly when Event is repaint f.e.
            if (Event.current.type != EventType.Layout) 
                return;
            var newFocused = GUI.GetNameOfFocusedControl();
            if (string.Equals(newFocused, m_lastFocusedControl))
                return;
            // update GUI when it is affected by this
            m_lastFocusedControl = newFocused;
            editor?.Repaint();
        }
        static void FocusControl(string ctrlName, IRepaintable editor = null)
        {
            GUI.FocusControl(ctrlName);
            m_lastFocusedControl = ctrlName;
            editor?.Repaint();
        }
        static void OnLeaveButtonRect_ReleaseFocus(bool isFocused,IRepaintable editor = null)
        {
            if (!isFocused)
                return;
            if (Event.current.type != EventType.Repaint)
                return;
            var buttonRect = GUILayoutUtility.GetLastRect();

            if (!buttonRect.Contains(Event.current.mousePosition))
                FocusControl("", editor);
        }

        public static bool SafetyButton(string ctrlName, 
            CustomGUIData locked, CustomGUIData unlocked,
            IRepaintable editor,
            params GUILayoutOption[] options)
        {
            UpdateCachedFocusName(editor);

            var isFocused = string.Equals(m_lastFocusedControl, ctrlName);
            var buttonData = isFocused ? unlocked : locked;
            GUI.SetNextControlName(ctrlName);
            var color = buttonData.UseColor ? buttonData.Color : GUI.color;
            //Debug.Log(color);
            using (new ColorScope(color))
            {
                var buttonPressed = GUILayout.Button(buttonData.Content, options);
                OnLeaveButtonRect_ReleaseFocus(isFocused, editor);
                    
                if (!buttonPressed) 
                    return false;
            }                

            if (isFocused)
                return true;

            FocusControl(ctrlName);
            return false;
        }

        public static bool SafetyButton(string textLocked, string textUnlocked, IRepaintable editor,
            params GUILayoutOption[] options)
        {
            var ctrlName = $"{textLocked}{textUnlocked}";
            return SafetyButton(ctrlName,
                new CustomGUIData { Content = new GUIContent(textLocked) },
                new CustomGUIData { Content = new GUIContent(textUnlocked) },
                editor, options);
        }

        public static void ReadonlyTextField(string value, params GUILayoutOption[] layoutOptions)
        {
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.TextField(value, layoutOptions);
            EditorGUI.EndDisabledGroup();
        }
    }

    public struct CustomGUIData
    {
        public CustomGUIData(GUIContent content, Color c)
        {
            Content = content;

            UseColor = true;
            Color = c;
        }
        public bool UseColor;
        public Color Color;
        public GUIContent Content;
    }

    public class FoldoutScope : IDisposable
    {
        readonly bool m_foldout;
        public FoldoutScope(ref bool foldOut, GUIContent content)
        {
            GUILayout.BeginVertical(EditorStyles.helpBox);
            foldOut = EditorGUILayout.Foldout(foldOut, content, true);
            m_foldout = foldOut;
            if (!m_foldout)
                return;

            //som indent
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
        }
        public FoldoutScope(ref bool foldOut, string content) : this(ref foldOut, new GUIContent(content)) { }

        public void Dispose()
        {
            if (m_foldout)
            {
                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }
    }

    public class IndentScope : IDisposable
    {
        bool m_indent;
        public IndentScope(bool indent)
        {
            m_indent = indent;
            if (!m_indent)
                return;
            GUILayout.BeginVertical(EditorStyles.helpBox);
            //som indent
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
        }

        public void Dispose()
        {
            if (!m_indent)
                return;

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }
    }

    public class ActivityFoldoutScope : IDisposable
    {
        readonly bool m_foldout;
        public ActivityFoldoutScope(ref bool foldOut, string content, GUIStyle style)
            : this(ref foldOut, new GUIContent(content), style)
        {}
        public ActivityFoldoutScope(ref bool foldOut, GUIContent content, GUIStyle style)
        {
            var clicked = CustomGUI.ActivityButton(foldOut, content, false, style);
            if (clicked)
                foldOut = !foldOut;

            m_foldout = foldOut;
            if (!m_foldout)
                return;
            GUILayout.BeginVertical(EditorStyles.helpBox);
            //som indent
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
        }

        public void Dispose()
        {
            if (!m_foldout) return;
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }
    }

}