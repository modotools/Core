using Core.Editor.Attribute;
using Core.Editor.PopupWindows;
using Core.Editor.Tools;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Utility.GUITools;
using Core.Unity;
using UnityEditor;
using UnityEngine;

using static UnityEditor.Editor;

namespace Core.Editor.Utility.GUITools
{
    public static class CommonSubConfigEditorGUI
    {
        //public static ScriptableBaseAction LastSelection;

        #region Data structs
        // for lists:
        public struct ConfigListModifications
        {
            //public int AddIdx;
            public int SetIdx;
            public int DeleteIdx;
            public int CutPasteIdx;
            public int MoveIdxA;
            public int MoveIdxB;

            public static ConfigListModifications Default => new ConfigListModifications
                { // AddIdx = -1, 
                    SetIdx=-1, CutPasteIdx = -1, DeleteIdx = -1, MoveIdxA = -1, MoveIdxB = -1};
        }
        public struct ConfigMenuData
        {
            public SelectTypesPopup Menu;
            public Rect ButtonRect;
            public static ConfigMenuData Default => new ConfigMenuData {};
        }

        public struct ConfigListData
        {
            public SerializedObject SObject;
            public string PropertyPath;
            public SerializedProperty ConfigListProperty;

            public Color ConfigNotSetColor;
            public int TypeSelectionIdx;

            public UnityEditor.Editor[] CachedNestedEditor;

            public string LabelFormat;
            public GUIContent[] CustomLabels;

            public IGUIData GUIData;
        }

        public struct ConfigListIterator
        {
            public int Idx;
            public int Count;
            public ScriptableObject Config;
            public SerializedProperty ConfigProperty;

            public IGUIData ConfigGUIData => Config as IGUIData;
            public IEnableData EnableData => Config as IEnableData;
        }
        
        // for single slots:
        public struct ConfigData
        {
            public SerializedObject SObject;

            public string PropertyPath;
            public ScriptableObject Config;
            public SerializedProperty ConfigProperty;
            public UnityEditor.Editor CachedEditor;
            public bool FoldOut;
            public bool CutPasteEnabled;

            public Color ConfigNotSetColor;
            public GUIContent Label;

            public INamed NamedConfig => Config as INamed;
            public string Name => NamedConfig != null ? NamedConfig.Name : Config.name;
        }
        #endregion

        #region Utility
        public static Color ConfigNotSetColorDefault => new Color(1f, 0.75f, 0f);
        public static void ColorGUI(IGUIData target)
        {                
            target.GUIColor = EditorGUILayout.ColorField(GUIContent.none, target.GUIColor,
                false, false,false, GUILayout.Width(6f));
            target.UseColor = EditorGUILayout.Toggle(target.UseColor, GUILayout.Width(15f));

            //if (!target.GUIData.UseColor)
            //    target.GUIData.GUIColor = Color.white;
        }

        public static void SafeDestroy(Object mainAsset, Object asset)
        {
            var mainAssetPath = AssetDatabase.GetAssetPath(mainAsset);
            var path = AssetDatabase.GetAssetPath(asset);
            var partOfAsset = string.Equals(path, mainAssetPath);

            // when not part of main asset, nothing is destroyed, only reference is removed
            if (!partOfAsset)
                return;

            var mainHierarchy = SubAssetHierarchy.GetSubAssetHierarchy(mainAsset);
            // when not part of linear hierarchy, nothing is destroyed, other asset might still reference the asset in question
            if (SubAssetHierarchy.TryFindSubAssetInHierarchy(asset, mainHierarchy, out var subAssetHierarchy) ==
                FindResult.NotFound)
                return;

            SafeDestroy(subAssetHierarchy);
        }

        static void SafeDestroy(SubAssetHierarchy.SubAssetData data)
        {
            foreach (var subHierarchy in data.SubAssets) 
                SafeDestroy(subHierarchy);

            if (data.Asset != null)
                data.Asset.DestroyEx();
        }

        #endregion

        #region GUI
        public static ChangeCheck DefaultConfigListElementHeader(
            ref ConfigListData listData,
            ref ConfigListIterator it, 
            ref ConfigListModifications mod,
            ref ConfigMenuData menuData)
        {
            var changed = false;
            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("x", GUILayout.Width(25)))
                    mod.DeleteIdx = it.Idx;

                GUILayout.Space(10);

                var prevColor = GUI.color;
                var prevBGColor = GUI.backgroundColor;

                if (it.ConfigGUIData != null)
                {
                    if (it.ConfigGUIData.UseColor)
                        GUI.backgroundColor = it.ConfigGUIData.GUIColor;

                    if (it.EnableData != null)
                        it.EnableData.Enabled = EditorGUILayout.Toggle(it.EnableData.Enabled,GUILayout.Width(25f));

                    var prevFoldOut = it.ConfigProperty.isExpanded;
                    it.ConfigProperty.isExpanded = EditorGUILayout.BeginFoldoutHeaderGroup(it.ConfigProperty.isExpanded, it.ConfigGUIData.Name);
                    it.ConfigProperty.isExpanded = true;
                    if (prevFoldOut != it.ConfigProperty.isExpanded)
                    {
                        if (it.ConfigProperty.serializedObject.targetObject is INamed n)
                            Debug.Log($"{n.Name} | {it.ConfigProperty.propertyPath} | {it.ConfigGUIData.Name}\n" +
                                      $"prev: {prevFoldOut} {it.ConfigProperty.isExpanded}");
                        it.ConfigProperty.serializedObject.ApplyModifiedProperties();
                        changed = true;
                    }   
                    //    LastSelection = it.Config;

                    ColorGUI(it.ConfigGUIData);
                }
                else
                {
                    GUI.color = listData.ConfigNotSetColor;
                    var path = it.ConfigProperty.propertyPath.Split('.');
                    EditorGUILayout.LabelField(path[path.Length-1], GUILayout.Width(50f));
                    GUILayout.FlexibleSpace();
                }

                GUILayout.Space(10);

                var nested = IsNested(listData.SObject, it.Config);
                using (new EditorGUILayout.HorizontalScope(EditorStyles.helpBox, GUILayout.Width(nested ? 235f:385f)))
                {
                    ConfigReferenceGUI(listData.SObject, it.Config, it.ConfigProperty, nested, GUIContent.none);

                    SetBehaviourGUI(it.Config, ref listData, ref menuData, out var setBehaviour, it.Idx);
                    MoveElementGUI(it, ref mod);

                    CutPasteGUI(it.Config, out var cutPaste);
                    if (cutPaste)
                        mod.CutPasteIdx = it.Idx;
                    if (setBehaviour)
                        mod.SetIdx = it.Idx;
                }

                GUI.backgroundColor = prevBGColor;
                GUI.color = prevColor;
                EditorGUILayout.EndFoldoutHeaderGroup();
            }

            return changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }
        
        public static void ConfigGUI(ref ConfigData configData, ref ConfigMenuData menuData, 
            out bool removeBehaviour, out bool setBehaviour, out bool cutPaste)
        {
            removeBehaviour = false;
            setBehaviour = false;

            if (configData.Config == null)
                ConfigGUINoConfig(ref configData, ref menuData, out setBehaviour, out cutPaste);
            else
                ConfigGUIWithConfig(ref configData, out removeBehaviour, out cutPaste);
        }
        #endregion

        #region Private GUI Methods
        static void MoveElementGUI(ConfigListIterator it, ref ConfigListModifications mod)
        {
            using (new EditorGUI.DisabledGroupScope(it.Idx == 0))
                if (GUILayout.Button("^", GUILayout.Width(25)))
                {
                    mod.MoveIdxA = it.Idx - 1;
                    mod.MoveIdxB = it.Idx;
                }

            using (new EditorGUI.DisabledGroupScope(it.Idx == it.Count - 1))
                if (GUILayout.Button("v", GUILayout.Width(25)))
                {
                    mod.MoveIdxA = it.Idx;
                    mod.MoveIdxB = it.Idx + 1;
                }
        }

        static void ConfigGUIWithConfig(ref ConfigData configData, out bool removeBehaviour, out bool cutPaste)
        {
            cutPaste = false;
            CreateCachedEditor(configData.Config, null, ref configData.CachedEditor);
            using (new EditorGUILayout.HorizontalScope())
            {
                if (configData.Label != null)
                    EditorGUILayout.LabelField(configData.Label, GUILayout.Width(50f));
                GUILayout.Space(10);

                removeBehaviour = GUILayout.Button("x", GUILayout.Width(25));
                GUILayout.Space(10);

                //var prevFoldOut = configData.FoldOut;
                configData.FoldOut = EditorGUILayout.Foldout(configData.FoldOut, configData.Name, true);
                //if (configData.FoldOut!= prevFoldOut)
                //    LastSelection = configData.Config;

                ConfigReferenceGUI(configData.SObject, configData.Config, configData.ConfigProperty, configData.Label);
                if (configData.CutPasteEnabled)
                    CutPasteGUI(configData.Config, out cutPaste);

                if (!configData.FoldOut)
                    return;
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(20);

                using (new EditorGUILayout.VerticalScope())
                    configData.CachedEditor.OnInspectorGUI();
            }
        }

        public static void ConfigReferenceGUI(SerializedObject serializedObject, ScriptableObject config,
            SerializedProperty property, GUIContent content = null)
        {
            var nested = IsNested(serializedObject, config);
            ConfigReferenceGUI(serializedObject, config, property, nested, content);
        }

        static void ConfigReferenceGUI(SerializedObject serializedObject, ScriptableObject config, SerializedProperty property, 
            bool nested, GUIContent label)
        {
            if (nested)
            {
                var extract = GUILayout.Button("Extract", GUILayout.Width(100f));
                if (extract)
                    AssetExtraction.Extract(serializedObject, config, property);
            }
            else
            {
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    var width = 140f;
                    if (label != null && label != GUIContent.none)
                    {
                        const float labelWidth = 70f;
                        EditorGUILayout.LabelField(label, GUILayout.Width(labelWidth));
                        width -= labelWidth;
                    }
                    EditorGUILayout.PropertyField(property, GUIContent.none, GUILayout.Width(width));
                    if (check.changed)
                        serializedObject.ApplyModifiedProperties();
                }
                if (config != null && GUILayout.Button("Integrate", GUILayout.Width(70f)))
                    AssetExtraction.Integrate(serializedObject, config, property);
            }
        }

        static bool IsNested(SerializedObject serializedObject, Object config)
        {
            var mainAssetPath = AssetDatabase.GetAssetPath(serializedObject.targetObject);

            var nested = false;
            if (config != null)
                nested = AssetDatabase.GetAssetPath(config).Equals(mainAssetPath);
            return nested;
        }

        static void ConfigGUINoConfig(ref ConfigData configData, ref ConfigMenuData menuData, 
            out bool setBehaviour, 
            out bool cutPaste)
        {
            cutPaste = false;
            using (new EditorGUILayout.HorizontalScope())
            {
                using (new ColorScope(configData.Config == null ? configData.ConfigNotSetColor : Color.white))
                {
                    ConfigReferenceGUI(configData.SObject, configData.Config, configData.ConfigProperty, configData.Label);
                    SetBehaviourGUI(configData.Config, ref menuData, out setBehaviour);
                    if (configData.CutPasteEnabled)
                        CutPasteGUI(configData.Config, out cutPaste);
                }
            }

            if (configData.CachedEditor != null)
                configData.CachedEditor.DestroyEx();
        }

        
        static void SetBehaviourGUI(Object config, 
            ref ConfigMenuData menuData, 
            out bool setBehaviour)
        {
            setBehaviour = config == null && GUILayout.Button("Set Behaviour");
            if (Event.current.type == EventType.Repaint)
                menuData.ButtonRect = GUILayoutUtility.GetLastRect();
        }

        static void SetBehaviourGUI(Object config, 
            ref ConfigListData listData,
            ref ConfigMenuData menuData, 
            out bool setBehaviour, 
            int setTypeIdx =-1)
        {
            SetBehaviourGUI(config, ref menuData, out setBehaviour);
            if (setBehaviour) 
                listData.TypeSelectionIdx = setTypeIdx;
        }

        static void CutPasteGUI(Object config, out bool cutPaste)
        {
            var cutPasteText = config != null
                ? "Cut" : "Paste";
            using (new EditorGUI.DisabledGroupScope(config == null 
                                                    && !CopyPasteOperation.HasData))
                cutPaste = GUILayout.Button(cutPasteText, GUILayout.Width(60));
        }
        #endregion
    }
}