using Core.Types;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Utility.GUITools
{
    public static class CommonGUIOperations
    {
        public static FlowResult AvoidSceneGUIConflicts(out Event e)
        {
            e = Event.current;

            if (e.alt)
                return FlowResult.Stop;
            if (e.button == 2) // middle mouse button
                return FlowResult.Stop;
            // prevent default SceneGUI stuff part 1, e.Use() for part2
            var controlId = GUIUtility.GetControlID(FocusType.Passive);
            if (e.type == EventType.Layout)
                HandleUtility.AddDefaultControl(controlId);
            return FlowResult.Continue;
        }
    }
}