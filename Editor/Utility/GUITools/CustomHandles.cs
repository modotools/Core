using UnityEditor;
using UnityEngine;

using ColorScope = Core.Unity.Utility.GUITools.ColorScope;

namespace Core.Editor.Utility.GUITools
{
    public static class CustomHandles 
    {
        public static Vector3 PositionHandle(Vector3 pos, Quaternion rot, 
            Color x, Color y, Color z, 
            float size = 1f)
        {
            var cap = new Handles.CapFunction(Handles.ArrowHandleCap);
            size = HandleUtility.GetHandleSize(pos) * size;
            var prevColor = Handles.color;
            Handles.color = x;
            pos = Handles.Slider(pos, rot * Vector3.right, size, cap, -1f);
            Handles.color = y;
            pos = Handles.Slider(pos, rot*Vector3.up, size, cap, -1f);
            Handles.color = z;
            pos = Handles.Slider(pos, rot*Vector3.forward, size, cap, -1f);
            Handles.color = prevColor;

            return pos;
        }

        public static void DrawText(Vector3 pos, Vector2 screenOffset, string text, Color textColor,
            GUIStyle style = null)
        {
            var screenPos = CalculateScreenPosition(pos, out var z);
            var r = GetTextRect(screenPos + screenOffset, text, style);
            var adjustedV2 = new Vector2(r.min.x, r.center.y);
            var wPosAdjusted = ScreenToWorldPosition(adjustedV2, z);

            if (style == null)
                style = GUI.skin.label;
            using (new ColorScope(textColor))
                Handles.Label(wPosAdjusted, text, style);
        }
        public static void DrawText(Vector3 pos, string text, Color textColor, GUIStyle style = null)
        {
            var screenPos = CalculateScreenPosition(pos, out var z);
            var r = GetTextRect(screenPos, text, style);
            var adjustedV2 = new Vector2(r.min.x, r.center.y);
            var wPosAdjusted = ScreenToWorldPosition(adjustedV2, z);

            if (style == null)
                style = GUI.skin.label;
            using (new ColorScope(textColor))
                Handles.Label(wPosAdjusted, text, style);
        }
        public static Rect GetTextRect(Vector2 centerPosition, string text, GUIStyle style = null)
        {
            if (style == null)
                style = GUI.skin.label;
            var size = style.CalcSize(new GUIContent(text));

            var textRect = new Rect(centerPosition.x - (size.x / 2),
                centerPosition.y, size.x, size.y);
            return textRect;
        }
        public static Vector2 CalculateScreenPosition(Vector3 position, out float z)
        {
            var sv = SceneView.currentDrawingSceneView;
            var cam = sv.camera;
            var v3 = cam == null ? default : cam.WorldToScreenPoint(position);
            z = v3.z;
            return new Vector2(v3.x, v3.y);
        }
        public static Vector3 ScreenToWorldPosition(Vector2 position, float z)
        {
            var v3 = new Vector3(position.x, position.y, z);
            var sv = SceneView.currentDrawingSceneView;
            var cam = sv.camera;
            return cam == null ? default : cam.ScreenToWorldPoint(v3);
        }
    }
}