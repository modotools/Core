using System.Collections.Generic;
using System.Linq;
using Core.Types;
using Core.Unity.Attributes;
using UnityEditor;

using UnityEditor.Experimental.SceneManagement;
using UnityEngine;

namespace Core.Editor.Utility
{
    [InitializeOnLoad]
    internal class HierarchyIcon
    {
        static readonly Dictionary<int, List<string>> k_hierarchyObjects = new Dictionary<int, List<string>>();
        static readonly Dictionary<string, Texture2D> k_hierarchyIcons = new Dictionary<string, Texture2D>();
        
        static HierarchyIcon()
        {
            EditorApplication.hierarchyChanged += UpdateObjects;
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyItemOnGUI;
        }

        static void UpdateObjects()
        {
            if (EditorApplication.isCompiling || EditorApplication.isUpdating)
                return;

            foreach (var keyValues in k_hierarchyObjects) 
                SimplePool<List<string>>.I.Return(keyValues.Value);
            k_hierarchyObjects.Clear();

            AddComponentsInScene();
            AddComponentsInPrefabView();
        }

        static void AddComponentsInPrefabView()
        {
            var stage = PrefabStageUtility.GetCurrentPrefabStage();
            if (stage == null)
                return;

            var hierarchyIcons = stage.FindComponentsOfType<Component>().OfType<IHierarchyIcon>();
            CacheHierarchyObjectComponents(hierarchyIcons);
        }

        static void AddComponentsInScene()
        {
            var hierarchyIcons = Object.FindObjectsOfType(typeof(Component), true).OfType<IHierarchyIcon>();
            CacheHierarchyObjectComponents(hierarchyIcons);
        }
        
        static void CacheHierarchyObjectComponents(IEnumerable<IHierarchyIcon> hierarchyIcons)
        {
            foreach (var icon in hierarchyIcons)
            {
                var id = icon.gameObject.GetInstanceID();
                if (!k_hierarchyObjects.ContainsKey(id))
                {
                    var list = SimplePool<List<string>>.I.Get();
                    list.Add(icon.HierarchyIcon);
                    k_hierarchyObjects.Add(id, list);
                }   
                else if (!k_hierarchyObjects[id].Contains(icon.HierarchyIcon)) 
                    k_hierarchyObjects[id].Add(icon.HierarchyIcon);
            }
        }

        static void HierarchyItemOnGUI(int instanceID, Rect selectionRect)
        {
            // place the icon to the right of the list:
            var r = new Rect(selectionRect);
            r.x = r.x + r.width - 20;
            r.width = 18;

            if (!k_hierarchyObjects.ContainsKey(instanceID))
                return;

            foreach (var icon in k_hierarchyObjects[instanceID])
            {
                if (!k_hierarchyIcons.ContainsKey(icon))
                    k_hierarchyIcons[icon] = Resources.Load(icon,  typeof(Texture2D)) as Texture2D;

                GUI.Label(r, k_hierarchyIcons[icon]);
                r.x -= 20f;
            }
        }
    }
}