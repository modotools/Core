﻿using System;
using System.IO;
using Core.Editor.Inspector;
using Core.Extensions;
using Core.Types;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Tools
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(CodeGeneratorConfig))]
    public class CodeGeneratorConfigInspector : BaseInspector<CodeGeneratorConfigEditor>
    {
        public override void OnInspectorGUI()
        {
            if (targets != null && targets.Length > 1)
            {
                OnMultiEditInspector();
                return;
            }

            base.OnInspectorGUI();
        }

        void OnMultiEditInspector()
        {
            if (!GUILayout.Button("Generate"))
                return;

            if (targets == null || targets.Length <= 1) 
                return;

            foreach (var t in targets)
            {
                if (!(t is CodeGeneratorConfig conf))
                    continue;

                CodeGeneratorConfigEditor.GenerateCode(conf);
            }
        }
    }

    public class CodeGeneratorConfigEditor : BaseEditor<CodeGeneratorConfig>
    {
        public override string Name => "Code Generator Config";

        bool m_enableEdit;

        // Start is called before the first frame update
        public override void OnGUI(float width)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var disabled = File.Exists(Target.OutputFile) && !m_enableEdit;
                if (disabled && !m_enableEdit && GUILayout.Button("Enable Edit"))
                    m_enableEdit = true;

                using (new EditorGUI.DisabledScope(disabled))
                {
                    BaseInspector.DrawPropertiesExcluding(SerializedObject, 
                        nameof(CodeGeneratorConfig.Template), 
                        nameof(CodeGeneratorConfig.ReplaceArgs));
                }

                EditorGUILayout.PropertyField(SerializedObject.FindProperty(nameof(CodeGeneratorConfig.Template)),
                    true);

                if (Target.ReplaceArgsEnum.Type != null)
                    NamedArguments();
                else EditorGUILayout.PropertyField(SerializedObject.FindProperty(nameof(CodeGeneratorConfig.ReplaceArgs)),
                    true);

                if (check.changed)
                    SerializedObject.ApplyModifiedProperties();
            }

            var path = AssetDatabase.GetAssetPath(Target.Folder);
            var ext = Path.GetExtension(path);
            if (!ext.IsNullOrEmpty())
                Target.Folder = null;

            GUILayout.Label($"OutputFile: {Target.OutputFile}");

            GenerateButtonGUI();
        }

        public ChangeCheck ArgumentsOnlyGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                if (Target.ReplaceArgsEnum.Type != null)
                    NamedArguments();
                else if (SerializedObject!= null)
                    EditorGUILayout.PropertyField(SerializedObject.FindProperty(nameof(CodeGeneratorConfig.ReplaceArgs)),
                    true);

                if (!check.changed)
                    return ChangeCheck.NotChanged;
                SerializedObject.ApplyModifiedProperties();
                return ChangeCheck.Changed;
            }
        }
        void NamedArguments()
        {
            var names = Enum.GetNames(Target.ReplaceArgsEnum.Type);
            var replaceArgsProp = SerializedObject.FindProperty(nameof(CodeGeneratorConfig.ReplaceArgs));

            var i = 0;
            EditorGUILayout.PropertyField(replaceArgsProp, false);

            if (!replaceArgsProp.isExpanded || !replaceArgsProp.NextVisible(true))
                return;

            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(replaceArgsProp, false);

            while (replaceArgsProp.NextVisible(true))
            {
                if (!i.IsInRange(names))
                    continue;
                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.LabelField(names[i]);
                    EditorGUILayout.PropertyField(replaceArgsProp, GUIContent.none, true);
                }

                ++i;
            }

            EditorGUI.indentLevel--;
        }

        public void GenerateButtonGUI(string btnText = "Generate")
        {
            if (!GUILayout.Button(btnText))
                return;

            m_enableEdit = false;
            GenerateCode(Target);
        }

        public static void GenerateCode(CodeGeneratorConfig config)
        {
            var textAssetPath = AssetDatabase.GetAssetPath(config.Template);
            using (var streamReader = new StreamReader(textAssetPath))
            using (var streamWriter = new StreamWriter(config.OutputFile))
            {
                // ReSharper disable once CoVariantArrayConversion
                while (!streamReader.EndOfStream)
                {
                    var line = string.Format(streamReader.ReadLine() ?? throw new InvalidOperationException(), config.ReplaceArgs);
                    //line = line.Replace("\\n", "\n");
                    if (line.Contains("\\n"))
                    {
                        var splitLines = line.Split(new[] { "\\n" }, StringSplitOptions.None);
                        foreach (var l in splitLines)
                        {
                            streamWriter.WriteLine(l);
                        }
                    }
                    else
                        streamWriter.WriteLine(line);
                }
            }
            AssetDatabase.Refresh();
        }
    }
}
