﻿using System;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Tools
{
    public class TextInputDialog : EditorWindow
    {
        const string k_inputName = "TextInputDialog_TextField";
        public string PromptText = "Enter your input:";
        public Action<string> Callback;

        void OnGUI()
        {
            EditorGUILayout.BeginVertical(GUIStyle.none, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUILayout.LabelField(PromptText);
            GUI.SetNextControlName(k_inputName);
            var inputText = EditorGUILayout.DelayedTextField("");
            EditorGUILayout.EndVertical();

            if (string.IsNullOrEmpty(inputText))
                EditorGUI.FocusTextInControl(k_inputName);
            else
            {
                Callback(inputText);
                Close();
            }
        }

        void OnLostFocus() => Close();

        public static void Prompt(string title, string promptText, Action<string> callback)
        {
            var window = CreateInstance<TextInputDialog>();
            window.minSize = new Vector2(300, 50);
            window.maxSize = window.minSize;
            window.titleContent = new GUIContent(title);
            window.Callback = callback;
            window.PromptText = promptText;
            window.ShowUtility();
        }
    }
}