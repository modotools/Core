﻿using System.Collections.Generic;
using Core.Unity.Interface;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace Core.Editor.Tools
{
#if CORE_ED_CREATE_ASSET_MENU_GEN_CONF
    [CreateAssetMenu(menuName = MenuStrings.CreateCoreAsset + "/" + nameof(EnumGeneratorConfig))]
#endif
    public class EnumGeneratorConfig : ScriptableObject, IEnumGenerator
    {
        #pragma warning disable 0649
        [SerializeField, FormerlySerializedAs("EnumName")]
        internal string m_enumName = "MyEnum";
        [SerializeField, FormerlySerializedAs("Folder")]
        internal DefaultAsset m_folder;
        [SerializeField, FormerlySerializedAs("NameSpaceStart")]
        internal int m_nameSpaceStart;
        [SerializeField, FormerlySerializedAs("EnumEntries")]
        internal string[] m_enumEntries = { "Default" };
        #pragma warning restore 0649

        public string EnumName => m_enumName;
        public DefaultAsset Folder => m_folder;
        public int NameSpaceStart => m_nameSpaceStart;
        public IEnumerable<string> EnumEntries => m_enumEntries;

        public string OutputFile => $"{AssetDatabase.GetAssetPath(Folder)}/{EnumName}.cs";

        public string NameSpace
        {
            get
            {
                if (Folder == null)
                    return "";

                var folder = $"{AssetDatabase.GetAssetPath(Folder)}";
                var folders = folder.Split('/');
                if (folders.Length == 0)
                    return "";

                var nameSpace = "";
                for (var i = NameSpaceStart; i < (folders.Length - 1); i++)
                {
                    nameSpace += $"{folders[i]}.";
                }

                nameSpace += folders[folders.Length - 1];
                return nameSpace;
            }
        }

    }
}
