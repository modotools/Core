#if CORE_ED_NAV_HISTORY
using System;
using System.Linq;
using Core.Extensions;
using Core.Unity.Extensions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Tools.NavigationHistory
{
    public class NavigationHistoryObject
    {
        public readonly Object UEObject;
        public bool IsPinned;

        public readonly string AssetPath;
        public readonly string GUID;

        public readonly Texture2D Icon;
        public bool IsAsset() => AssetPath.Length > 0;
        public bool IsNonFolderAsset() => IsAsset() && !NavHistoryOperations.IsFolder(AssetPath);
        public bool IsFolder() => IsAsset() && NavHistoryOperations.IsFolder(AssetPath);

        public NavigationHistoryObject(Object ueObj)
        {
            UEObject = ueObj;
            AssetPath = AssetDatabase.GetAssetPath(UEObject);
            GUID = AssetDatabase.AssetPathToGUID(AssetPath);

            Icon = AssetDatabase.GetCachedIcon(AssetPath) as Texture2D;
            if (Icon != null)
                return;
            if (!IsAsset()) 
                Icon = EditorGUIUtility.ObjectContent(null, typeof(GameObject)).image as Texture2D;
        }

        public override bool Equals(object o) => o is NavigationHistoryObject navObj && UEObject.Equals(navObj.UEObject);

        // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
        public override int GetHashCode() => base.GetHashCode();
    }

    [Serializable]
    public struct NavHistoryDataSerialized
    {
        public int InstanceID;

        public string Scene;
        public string ScenePath;

        public string AssetPath;
        public string GUID;

        public bool IsPinned;
    }

    public static class NavHistoryOperations
    {
        public static bool IsFolder(string path) { return path.IndexOf(".", StringComparison.Ordinal) == -1; }
        
        static string GetHierarchicalPath(GameObject go)
        {
            if (go == null)
                return "";
            var path = go.name;
            var tr = go.transform;
            return tr.Parents().Aggregate(path, (current, p) => p.name + "/" + current);
        }

        internal static NavHistoryDataSerialized NavToSerialized(NavigationHistoryObject nav)
        {
            NavHistoryDataSerialized serialized = default;

            if (nav.UEObject == null)
                return serialized;

            serialized.AssetPath = nav.AssetPath;
            serialized.GUID = nav.GUID;
            serialized.InstanceID = nav.UEObject.GetInstanceID();
            if (nav.UEObject is GameObject go && !go.scene.name.IsNullOrEmpty())
            {
                serialized.Scene = go.scene.name;
                serialized.ScenePath = GetHierarchicalPath(go);
            }

            serialized.IsPinned = nav.IsPinned;
            return serialized;
        }

        internal static NavigationHistoryObject NavFromSerialized(NavHistoryDataSerialized serialized)
        {
            var path = serialized.AssetPath;
            var obj = AssetDatabase.LoadMainAssetAtPath(path);
            if (obj == null && !serialized.GUID.IsNullOrEmpty())
                path = AssetDatabase.GUIDToAssetPath(serialized.GUID);
            obj = AssetDatabase.LoadMainAssetAtPath(path);
            if (obj == null)
                obj = EditorUtility.InstanceIDToObject(serialized.InstanceID);
            if (obj == null) 
                obj = GameObject.Find(serialized.ScenePath);
            return obj == null 
                ? null
                : new NavigationHistoryObject(obj) { IsPinned = serialized.IsPinned };
        }
    }
}

#endif