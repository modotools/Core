#if CORE_ED_NAV_HISTORY
using System;
using System.Collections.Generic;
using System.Reflection;
using Core.Extensions;
using Core.Types;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.ShortcutManagement;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using static Core.Editor.Tools.NavigationHistory.NavHistoryOperations;
using Object = UnityEngine.Object;

// todo 2: improve performance,
namespace Core.Editor.Tools.NavigationHistory
{
    [InitializeOnLoad]
    public class NavigationHistory 
    {
        const int k_historySize = 10;

        static int m_pinnedElements;

        public static bool FilterSceneObj = false;
        public static bool FilterAssets = false;
        public static bool FilterFolders = false;

        public static List<NavigationHistoryObject> History;
        public static int SelectedId=-1;

        static string m_lastGuid = "";
        static Object m_lastObject;

        static bool m_doNavigateBack;
        static bool m_doNavigateForward;

        public static Action DrawAction = null;

        static NavigationHistory() => EditorApplication.delayCall += Init;

        static bool m_initialized;

        static void Init()
        {
            History = new List<NavigationHistoryObject>();
            EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyChangeListener;

            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorApplication.update += Update;
            SceneView.duringSceneGui += OnSceneGUI;
            
            var onInitObjects = Selection.objects;

            if (EditorApplication.isPlayingOrWillChangePlaymode || !m_initialized)
            {
                Selection.activeObject = null;
                Load();
                
                if (onInitObjects != null)
                    Selection.objects = onInitObjects;
            }
            SelectedId = 0;
            m_initialized = true;
        }

        public static void Save(string key = "")
        {
            var serializedString = "";
            var addComma = false;
            foreach (var t in History)
            {
                if (t.UEObject == null)
                    continue;

                if (addComma)
                    serializedString += ";";
                var json = JsonUtility.ToJson(NavToSerialized(t));

                serializedString += json;
                addComma = true;
            }

            EditorPrefs.SetString("NavigationHistory" + key, serializedString);
        }

        public static void Load(string key = "")
        {
            if (!EditorPrefs.HasKey("NavigationHistory" + key))
                return;
        
            ClearAll();
            var serializedString = EditorPrefs.GetString("NavigationHistory" + key);
            var splitString = serializedString.Split(';');
            foreach (var element in splitString)
            {
                var serialized = JsonUtility.FromJson<NavHistoryDataSerialized>(element);
                var nho = NavFromSerialized(serialized);
                if (nho == null)
                    continue;
                if (History.Find(a=>a.UEObject == nho.UEObject) != null)
                    continue;
                History.Add(nho);
            }
            SelectedId = History.Count; 
        } 
         
        static void ClearAll()
        {
            History.Clear();
            m_pinnedElements = 0;
            SelectedId = -1;
        }

        public static void UnpinAll()
        {
            var i = History.Count - 1;
            while (i >= 0)
            {
                UnPinAt(i);
                i--;
            }
        }

        public static void ClearUnpinned(int from = 0)
        {
            var i = History.Count - 1;
            while (i >= from)
            {
                if (!History[i].IsPinned)
                    RemoveAt(i);
                
                i--;
            }
        }

        // ReSharper disable once FlagArgument
        public static void ChangePinStateAt(int i, bool pin)
        {
            if (History[i].IsPinned == pin) return;
            m_pinnedElements += pin ? 1 : -1;
            History[i].IsPinned = pin;
            Save();
        }

        public static void PinAt(int i) => ChangePinStateAt(i, true);

        static void UnPinAt(int i) => ChangePinStateAt(i, false);

        public static void RemoveAt(int i)
        {
            UnPinAt(i);
            History.RemoveAt(i);
            if (SelectedId > i)
                SelectedId--;
            else if (SelectedId == i)
            {
                //Selection.activeObject = null;
                SelectedId = History.Count;
            }
        }

        static bool RemoveIfNullAt(int i)
        {
            if (History.Count <= i || History[i].UEObject != null)
                return false;
            RemoveAt(i);
            return true;
        }

        public static void CleanupHistory()
        {
            var i = 0;
            for (; i < History.Count; )
            {
                if (!RemoveIfNullAt(i))
                    ++i;
            }
        }

        [MenuItem(MenuStrings.NavigationWindow)]
        static void MenuCall() => 
            EditorWindow.GetWindow<NavigationHistoryWindow>(false, "Navigation History");

        // [Shortcut(nameof(NavigationHistory)+"/"+nameof(NavigateBack), KeyCode.Mouse3)]
        [MenuItem(MenuStrings.NavigateBackward)]
        static void NavigateBack()
        {
            if (SelectedId != 0)
                m_doNavigateBack = true;
        }

        // [Shortcut(nameof(NavigationHistory)+"/"+nameof(NavigateForth), KeyCode.Mouse4)]
        [MenuItem(MenuStrings.NavigateForward)]
        static void NavigateForth()
        {
            if (SelectedId != (History.Count-1))
                m_doNavigateForward = true;
        }

        static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            CleanupHistory();
            Load();
        }

        static bool m_backButton;
        static bool m_forwardButton;
        static void Update()
        {           
            // InputSystem.Update();

            var mouse = Mouse.current;
            if (mouse.backButton.isPressed && !m_backButton)
                NavigateBack();
            else if (mouse.forwardButton.isPressed && !m_forwardButton)
                NavigateForth();

            m_backButton = mouse.backButton.isPressed;
            m_forwardButton = mouse.forwardButton.isPressed;
            
            UpdateNavigation();
        }

        static void UpdateNavigation()
        {
            if (m_doNavigateBack && !FirstSelected())
            {
                m_doNavigateBack = false;
                SelectPrevious();
            }

            if (m_doNavigateForward && !LastSelected())
            {
                m_doNavigateForward = false;
                SelectNext();
            }
        }
        // todo 3: do this not on sceneGUI only (confusing), either add more hooks (eg. nav-window update/gui)
        // todo 3: or add non-saved editor-helper-update-object
        static void OnSceneGUI(SceneView sceneView)
        {
            UpdateNavigation();
            // RepaintAllViews is extremely expensive and should not be called frequently.
            //UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
        }

        static OperationResult ShowFolderContentsWithReflection(int i)
        {
            var projectBrowserType = Type.GetType("UnityEditor.ProjectBrowser,UnityEditor");
            if (projectBrowserType == null)
            {
                Debug.LogError("Can't find UnityEditor.ProjectBrowser type!");
                return OperationResult.Error;
            }

            var lastProjectBrowser = projectBrowserType.GetField("s_LastInteractedProjectBrowser",
                BindingFlags.Static | BindingFlags.Public);
            if (lastProjectBrowser == null)
            {
                Debug.LogError("Can't find s_LastInteractedProjectBrowser field!");
                return OperationResult.Error;
            }

            var lastProjectBrowserInstance = lastProjectBrowser.GetValue(null);
            var projectBrowserViewMode =
                projectBrowserType.GetField("m_ViewMode", BindingFlags.Instance | BindingFlags.NonPublic);

            if (projectBrowserViewMode == null)
            {
                Debug.LogError("Can't find m_ViewMode field!");
                return OperationResult.Error;
            }

            // 0 - one column, 1 - two column
            var viewMode = (int) projectBrowserViewMode.GetValue(lastProjectBrowserInstance);
            if (viewMode != 1)
                return OperationResult.Error;

            var showFolderContents = projectBrowserType.GetMethod("ShowFolderContents",
                BindingFlags.NonPublic | BindingFlags.Instance);

            if (showFolderContents == null)
            {
                Debug.LogError("Can't find ShowFolderContents method!");
                return OperationResult.Error;
            }

            var folder = History[i].UEObject;
            showFolderContents.Invoke(lastProjectBrowserInstance,
                new object[] {folder.GetInstanceID(), true});
            return OperationResult.OK;
        }

        public static void SelectObject(int i)
        {
            if (!SelectionValidAt(ref i))
                return;

            var folderContentsShown = false;
            if (History[i].IsAsset() && History[i].IsFolder())
                folderContentsShown = ShowFolderContentsWithReflection(i) == OperationResult.OK;
            if (!folderContentsShown)
                Selection.activeObject = History[i].UEObject;
            
            SelectedId = i;
        }

        static bool SelectionValidAt(ref int i)
        {
            while (RemoveIfNullAt(i))
            {
                if (History.Count <= i)
                    i--;
            }

            return History.Count > i;
        }

        static void SelectNext()
        {
            if (SelectedId >= (History.Count-1))
                return;

            SelectedId++;
            SelectObject(SelectedId);
        }

        static void SelectPrevious()
        {
            if (SelectedId <= 0)
                return;

            SelectedId--;
            SelectObject(SelectedId);
        }

        static bool FirstSelected() => SelectedId <= 0;

        static bool LastSelected() => SelectedId >= (History.Count - 1);

        static void OnHierarchyChangeListener(int instanceID, Rect selectionRect) => CheckSelection();

        static void CheckSelection()
        {
            if (FilterSceneObj && FilterAssets && FilterFolders)
                return;

            GetActiveObj(out var activeObj);

            var lastInHistory = SelectedId >= 0 && 
                                 History.Count > SelectedId &&
                                 activeObj == History[SelectedId].UEObject;

            if (activeObj != null && !lastInHistory)
            {
                if (!UpdateHistoryForSelection(activeObj))
                    return;
            }

            DrawAction?.Invoke();
        }

        // returns true if drawing is ok?
        static bool UpdateHistoryForSelection(Object activeObj)
        {
            var nho = new NavigationHistoryObject(activeObj);

            var filterA = FilterAssets && nho.IsNonFolderAsset();
            var filterF = FilterFolders && nho.IsFolder();
            var filterS = FilterSceneObj && !nho.IsAsset();
            if (filterA || filterF || filterS)
                return false;
            
            //ClearUnpinned(SelectedId + 1);
            var alreadyInListIdx = History.IndexOf(nho);
            if (alreadyInListIdx != -1)
            {
                if (History[alreadyInListIdx].IsPinned)
                {
                    SelectedId = History.Count;
                    return false;
                }

                History.RemoveAt(alreadyInListIdx);
            }

            History.Add(nho);
            if (History.Count > (k_historySize + m_pinnedElements))
            {
                for (var i = 0; i < History.Count; ++i)
                {
                    if (History[i].IsPinned)
                        continue;
                    History.RemoveAt(i);
                    break;
                }
            }

            SelectedId = History.Count - 1;
            Save();
            return true;
        }

        static void GetActiveObj(out Object activeObj)
        {
            activeObj = null;
            if (Selection.activeObject != m_lastObject)
            {
                m_lastObject = Selection.activeObject;
                activeObj = m_lastObject;
                if (Selection.assetGUIDs.Length > 0)
                    m_lastGuid = Selection.assetGUIDs[0];
                return;
            }

            if (Selection.assetGUIDs.Length <= 0 || m_lastGuid == Selection.assetGUIDs[0])
                return;

            // two column folder changed!
            m_lastGuid = Selection.assetGUIDs[0];

            var path = AssetDatabase.GUIDToAssetPath(m_lastGuid);
            if (IsFolder(path))
                activeObj = AssetDatabase.LoadAssetAtPath(path, typeof(Object));
        }
    }
}

#endif