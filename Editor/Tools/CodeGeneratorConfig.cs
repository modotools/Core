﻿using Core.Unity.Types;
using Core.Unity.Types.Attribute;
using UnityEditor;
using UnityEngine;


 //todo 3: improve on this, expose variable names? kill #if UNDEFINED_SYMBOL -sections?
namespace Core.Editor.Tools
{
#if CORE_ED_CREATE_ASSET_MENU_GEN_CONF
    [CreateAssetMenu(menuName = MenuStrings.CreateCoreAsset+"/"+nameof(CodeGeneratorConfig))]
#endif
    public class CodeGeneratorConfig : ScriptableObject
    {
        public DefaultAsset Folder;
        public string FileName;

        public TextAsset Template;

        [ClassTypeConstraint(AllowAbstract = false, AllowEnum = true, AllowInterface = false, 
            AllowNoNamespace = false, AllowRegular = false)]
        public ClassTypeReference ReplaceArgsEnum;
        public string[] ReplaceArgs = {};
        public string OutputFile => $"{AssetDatabase.GetAssetPath(Folder)}/{FileName}.cs";
    }
}
