﻿using System;
using System.Collections.Generic;
using System.IO;
using Core.Types;
using UnityEditor;

namespace Core.Editor.Tools
{
    public class UnityGuidRegeneratorMenu
    {
#if CORE_ED_MENU_REGEN_GUIDS
        [MenuItem(MenuStrings.RegenerateGuids)]
#endif
        public static void RegenerateGUIDs()
        {
            if (!EditorUtility.DisplayDialog("GUIDs regeneration",
                "You are going to start the process of GUID regeneration. This may have unexpected results. \n\nMAKE A PROJECT BACKUP BEFORE PROCEEDING!",
                "Regenerate GUIDs", "Cancel")) return;
            try
            {
                AssetDatabase.StartAssetEditing();

                var path = Path.GetFullPath(".") + Path.DirectorySeparatorChar + "Assets";
                var regenerator = new UnityGuidRegenerator(path);
                regenerator.RegenerateGuids();
            }
            finally
            {
                AssetDatabase.StopAssetEditing();
                EditorUtility.ClearProgressBar();
                AssetDatabase.Refresh();
            }
        }
    }

    internal class UnityGuidRegenerator
    {
        static readonly string[] k_defaultFileExtensions = {
            "*.meta",
            "*.mat",
            "*.anim",
            "*.prefab",
            "*.unity",
            "*.asset",
            "*.guiskin",
            "*.fontsettings",
            "*.controller",
        };

        readonly string m_assetsPath;

        public UnityGuidRegenerator(string assetsPath) => m_assetsPath = assetsPath;

        public void RegenerateGuids(string[] regeneratedExtensions = null)
        {
            if (regeneratedExtensions == null)
                regeneratedExtensions = k_defaultFileExtensions;

            var filesPaths = GetListOfWorkingFiles(regeneratedExtensions);

            // Create dictionary to hold old-to-new GUID map
            var guidOldToNewMap = new Dictionary<string, string>();
            var guidsInFileMap = new Dictionary<string, List<string>>();

            // We must only replace GUIDs for Resources present in Assets. 
            // Otherwise built-in resources (shader, meshes etc) get overwritten.
            var ownGuids = new HashSet<string>();

            // Traverse all files, remember which GUIDs are in which files and generate new GUIDs
            if (CollectGUIDs(filesPaths, ownGuids, guidOldToNewMap, guidsInFileMap) == OperationResult.Error)
                return;

            // Traverse the files again and replace the old GUIDs
            ReplaceGUIDs(guidsInFileMap, ownGuids, guidOldToNewMap);

            EditorUtility.ClearProgressBar();
        }

        void ReplaceGUIDs(Dictionary<string, List<string>> guidsInFileMap, 
            ICollection<string> ownGuids, IReadOnlyDictionary<string, string> guidOldToNewMap)
        {
            var counter = -1;
            var guidsInFileMapKeysCount = guidsInFileMap.Keys.Count;
            foreach (var filePath in guidsInFileMap.Keys)
            {
                EditorUtility.DisplayProgressBar("Regenerating GUIDs", MakeRelativePath(m_assetsPath, filePath),
                    counter / (float) guidsInFileMapKeysCount);
                counter++;

                var contents = File.ReadAllText(filePath);
                foreach (var oldGuid in guidsInFileMap[filePath])
                {
                    if (!ownGuids.Contains(oldGuid))
                        continue;

                    var newGuid = guidOldToNewMap[oldGuid];
                    if (string.IsNullOrEmpty(newGuid))
                        throw new NullReferenceException("newGuid == null");

                    contents = contents.Replace("guid: " + oldGuid, "guid: " + newGuid);
                }

                File.WriteAllText(filePath, contents);
            }
        }

        OperationResult CollectGUIDs(IReadOnlyCollection<string> filesPaths, ISet<string> ownGuids, 
            IDictionary<string, string> guidOldToNewMap, IDictionary<string, List<string>> guidsInFileMap)
        {
            var counter = 0;
            foreach (var filePath in filesPaths)
            {
                if (EditorUtility.DisplayCancelableProgressBar("Scanning Assets folder",
                    MakeRelativePath(m_assetsPath, filePath),
                    counter / (float) filesPaths.Count))
                {
                    UnityEngine.Debug.LogWarning("GUID regeneration canceled");
                    return OperationResult.Error;
                }

                var contents = File.ReadAllText(filePath);

                var guids = GetGuids(contents);
                var isFirstGuid = true;
                foreach (var oldGuid in guids)
                {
                    // First GUID in .meta file is always the GUID of the asset itself
                    if (isFirstGuid && Path.GetExtension(filePath) == ".meta")
                    {
                        ownGuids.Add(oldGuid);
                        isFirstGuid = false;
                    }

                    // Generate and save new GUID if we haven't added it before
                    if (!guidOldToNewMap.ContainsKey(oldGuid))
                    {
                        var newGuid = Guid.NewGuid().ToString("N");
                        guidOldToNewMap.Add(oldGuid, newGuid);
                    }

                    if (!guidsInFileMap.ContainsKey(filePath))
                        guidsInFileMap[filePath] = new List<string>();

                    if (!guidsInFileMap[filePath].Contains(oldGuid))
                        guidsInFileMap[filePath].Add(oldGuid);
                }

                counter++;
            }

            return OperationResult.OK;
        }

        List<string> GetListOfWorkingFiles(string[] regeneratedExtensions)
        {
            var filesPaths = new List<string>();
            foreach (var extension in regeneratedExtensions)
            {
                var files = Directory.GetFiles(m_assetsPath, extension, SearchOption.AllDirectories);
                filesPaths.AddRange(files);
            }

            return filesPaths;
        }

        static IEnumerable<string> GetGuids(string text)
        {
            const string guidStart = "guid: ";
            const int guidLength = 32;
            var textLength = text.Length;
            var guidStartLength = guidStart.Length;
            var guids = new List<string>();

            var index = 0;
            while (index + guidStartLength + guidLength < textLength)
            {
                index = text.IndexOf(guidStart, index, StringComparison.Ordinal);
                if (index == -1)
                    break;

                index += guidStartLength;
                var guid = text.Substring(index, guidLength);
                index += guidLength;

                if (IsGuid(guid))
                {
                    guids.Add(guid);
                }
            }

            return guids;
        }

        static bool IsGuid(string text)
        {
            foreach (var c in text)
            {
                var isNum = c >= '0' && c <= '9';
                var isAlpha = c >= 'a' && c <= 'z';
                if (!(isNum || isAlpha))
                    return false;
            }

            return true;
        }

        static string MakeRelativePath(string fromPath, string toPath)
        {
            var fromUri = new Uri(fromPath);
            var toUri = new Uri(toPath);

            var relativeUri = fromUri.MakeRelativeUri(toUri);
            var relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            return relativePath;
        }
    }
}