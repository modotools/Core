using Core.Editor.Data;
using Core.Editor.Utility;
using Core.Events;
using Core.Interface;
using Core.Unity.Interface;
using UnityEditor;
using UnityEngine;
using static Core.Editor.Utility.CustomGUI;

namespace Core.Editor.Tools
{
    /// <summary> Utility for renaming assets </summary>
    public class RenamePopup : EditorWindow
    {
        const string k_inputControlName = "nameInput";

        static RenamePopup Current { get; set; }

        Object m_target;
        string m_input;
        string m_prevName;

        bool m_firstFrame = true;

        bool InputValid => m_input != null && m_input.Trim() != "";

        /// <summary>
        /// Show a rename popup for an asset at mouse position. Will trigger reimport of the asset on apply.
        /// </summary>
        public static RenamePopup Show(Object target, float width = 200)
        {
            var window = GetWindow<RenamePopup>(true, "Rename " + target.name, true);
            if (Current != null)
                Current.Close();

            Current = window;
            window.m_target = target;

            var name= target is INameable nameable
                ? nameable.Name : target.name;
            
            window.m_prevName = name;
            window.m_input = name;
            window.minSize = new Vector2(100, 44);
            window.position = new Rect(0, 0, width, 44);
            window.UpdatePositionToMouse();
            return window;
        }

        void UpdatePositionToMouse()
        {
            if (Event.current == null)
                return;
            Vector3 mousePoint = GUIUtility.GUIToScreenPoint(Event.current.mousePosition);
            var pos = position;
            pos.x = mousePoint.x - position.width * 0.5f;
            pos.y = mousePoint.y - 10;
            position = pos;
        }

        // Make the popup close on lose focus
        void OnLostFocus() => Close();

        void OnGUI()
        {
            if (m_firstFrame)
            {
                UpdatePositionToMouse();
                m_firstFrame = false;
            }

            GUI.SetNextControlName(k_inputControlName);
            m_input = EditorGUILayout.TextField(m_input);
            EditorGUI.FocusTextInControl(k_inputControlName);

            var e = Event.current;

            var buttonText = InputValid ? "Apply" : "Revert to default";

            if (ButtonOrReturnKey(buttonText) && InputValid) 
                Rename(m_input);

            if (e.isKey && e.keyCode == KeyCode.Escape)
                Close();
        }

        void Rename(string newName)
        {
            IScriptableObject parent = null;

            var so = m_target as IScriptableObject;
            if (so != null)
                parent = so.Parent();

            if (m_target is INameable nameable)
                nameable.SetName(newName);
            else m_target.name = newName;

            EventMessenger.TriggerEvent(new AssetRenamed()
            {
                RenamedAsset = m_target,
                NewName = newName,
                OldName = m_prevName
            });

            if (m_target is IRenamedHandler r)
                r.OnRenamed();

            // hotfix unity_bug
            if (parent != null && parent != so.Parent())
                AssetDatabase.SetMainObject((Object) parent, AssetDatabase.GetAssetPath(m_target));

            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(m_target));
            Close();
            //m_target.TriggerOnValidate();
        }

        void OnDestroy() => EditorGUIUtility.editingTextField = false;
    }
}