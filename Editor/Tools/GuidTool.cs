using UnityEditor;
using UnityEngine;

namespace Core.Editor.Tools
{
    public class GuidTool : EditorWindow
    {
        string m_guid;
        Object m_asset;

#if CORE_ED_MENU_GUID_TOOL
        [MenuItem(MenuStrings.GuidTool)]
#endif
        public static void DoFindByGuidMenu()
        {
            var wnd = GetWindow<GuidTool>();
            if (wnd == null)
                return;
            wnd.titleContent = new GUIContent("Guid<>Path");
            wnd.Show();
        }

        void OnGUI()
        {
            EditorGUILayout.HelpBox("This tool provides the ability to resolve asset GUID to path and vice versa. " +
                                    "Just paste a guid in corresponding field and if the asset exists in the project, " +
                                    "the object field below holds a reference to it.", MessageType.Info);

            GUILayout.Space(10);

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    m_guid = EditorGUILayout.TextField("GUID", m_guid);
                    if (GUILayout.Button("Paste", GUILayout.Width(50)))
                    {
                        m_guid = EditorGUIUtility.systemCopyBuffer;
                        GUI.changed = true;
                    }
                }

                if (check.changed)
                    m_asset = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GUIDToAssetPath(m_guid));
            }

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_asset = EditorGUILayout.ObjectField("Asset", m_asset, typeof(Object), false);

                if (check.changed)
                    m_guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(m_asset));
            }
        }
    }
}