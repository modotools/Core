using UnityEditor;
using UnityEngine;

namespace Core.Editor.Extensions
{
    public static class GenericMenuExtension
    {
        public static void AddItem(this GenericMenu menu, GUIContent content, bool on, GenericMenu.MenuFunction func, bool enabled)
        {
            if (enabled)
                menu.AddItem(content, @on, func);
            else
                menu.AddDisabledItem(content, @on);
        }
    }
}