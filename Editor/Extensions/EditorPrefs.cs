using UnityEditor;
using UnityEngine;
using Core.Extensions;

namespace Core.Editor.Extensions
{
    public static class CustomEditorPrefs
    {
        public static void SetColor(string key, Color value)
        {
            EditorPrefs.SetFloat(key + "/R", value.r);
            EditorPrefs.SetFloat(key + "/G", value.g);
            EditorPrefs.SetFloat(key + "/B", value.b);
            EditorPrefs.SetFloat(key + "/A", value.a);
        }

        public static Color GetColor(string key, Color defColor = default)
        {
            var r = EditorPrefs.GetFloat(key + "/R", defColor.r);
            var g = EditorPrefs.GetFloat(key + "/G", defColor.g);
            var b = EditorPrefs.GetFloat(key + "/B", defColor.b);
            var a = EditorPrefs.GetFloat(key + "/A", defColor.a);
            return new Color(r, g, b, a);
        }

        public static void ClearPrefsColor(string key)
        {
            EditorPrefs.DeleteKey($"{key}/R");
            EditorPrefs.DeleteKey($"{key}/G");
            EditorPrefs.DeleteKey($"{key}/B");
            EditorPrefs.DeleteKey($"{key}/A");
        }

        public static void SetGuid(string key, Object o)
        {
            var guid = "";
            if (o != null)
                guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(o));

            EditorPrefs.SetString(key, guid);
        }

        public static T GetAssetFromGuid<T>(string key) where T : Object
        {
            var guid = EditorPrefs.GetString(key, "");
            var path = AssetDatabase.GUIDToAssetPath(guid);
            return !path.IsNullOrEmpty() ? AssetDatabase.LoadAssetAtPath<T>(path) : null;
        }

        public static Vector3Int GetVector3Int(string key, Vector3Int defaultValue = default)
        {
            var x = EditorPrefs.GetInt($"{key}.X", defaultValue.x);
            var y = EditorPrefs.GetInt($"{key}.Y", defaultValue.y);
            var z = EditorPrefs.GetInt($"{key}.Z", defaultValue.z);
            return new Vector3Int(x,y,z);
        }

        public static void SetVector3Int(string key, Vector3Int value)
        {
            EditorPrefs.SetInt($"{key}.X", value.x);
            EditorPrefs.SetInt($"{key}.Y", value.y);
            EditorPrefs.SetInt($"{key}.Z", value.z);
        }
        public static void ClearPrefsVector3Int(string key)
        {
            EditorPrefs.DeleteKey($"{key}.X");
            EditorPrefs.DeleteKey($"{key}.Y");
            EditorPrefs.DeleteKey($"{key}.Z");
        }

        public static Rect GetRect(string key, Rect defaultValue = default)
        {
            var x = EditorPrefs.GetFloat($"{key}.X", defaultValue.x);
            var y = EditorPrefs.GetFloat($"{key}.Y", defaultValue.y);
            var w = EditorPrefs.GetFloat($"{key}.W", defaultValue.width);
            var h = EditorPrefs.GetFloat($"{key}.H", defaultValue.height);

            return new Rect(x, y,w,h);
        }

        public static void SetRect(string key, Rect value)
        {
            EditorPrefs.SetFloat($"{key}.X", value.x);
            EditorPrefs.SetFloat($"{key}.Y", value.y);
            EditorPrefs.SetFloat($"{key}.W", value.width);
            EditorPrefs.SetFloat($"{key}.H", value.height);
        }
        public static void ClearPrefsRect(string key)
        {
            EditorPrefs.DeleteKey($"{key}.X");
            EditorPrefs.DeleteKey($"{key}.Y");
            EditorPrefs.DeleteKey($"{key}.W");
            EditorPrefs.DeleteKey($"{key}.H");
        }
    }
}
