﻿
using System;
using System.Linq;
using UnityEditor.Animations;
using UnityEngine;

namespace Core.Editor.Extensions
{
    public static class AnimatorControllerExtensions
    {
        public static int CountAllStates(this AnimatorController controller)
        {
            return controller.layers.Select(layer => layer.stateMachine).Select(sm => sm.states.Length).Sum();
        }

        public static void GetAllStateNames(this AnimatorController controller, out string[] states)
        {
            var length = controller.CountAllStates();
            states = new string[length];

            var state = 0;
            foreach (var layer in controller.layers)
            {
                var sm = layer.stateMachine;
                for (var i = 0; i < sm.states.Length; i++, state++)
                {
                    states[state] = sm.states[i].state.name;
                }
            }
        }

        public static string GetClipName(this AnimatorController controller, string stateName)
        {
            foreach (var layer in controller.layers)
            {
                var sm = layer.stateMachine;
                foreach (var state in sm.states)
                {
                    if (state.state == null)
                        continue;
                    
                    if (string.Equals(state.state.name, stateName))
                        return state.state.motion == null 
                            ? "" : state.state.motion.name;
                }
            }

            return "";
        }


        public static AnimationClip GetClip(this AnimatorController controller, string clipName) => 
            Array.Find(controller.animationClips, (c) 
                => string.Equals(c.name, clipName));
    }
}
