using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.PopupWindows;
using Core.Extensions;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Extensions
{
    public static class EditorExtensions
    {
        public static void ReflectionGetAllTypes(Type type, out List<Type> typeList, out string[] names, out string[] namespaces)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsAbstract);

            typeList = types.ToList();
            GetNamesAndNamespacesForTypes(typeList, out names, out namespaces);
        }

        public static void GetNamesAndNamespacesForTypes(IEnumerable<Type> typeList, out string[] names, out string[] namespaces)
        {
            var count = typeList.Count();
            names = new string[count];
            namespaces = new string[count];
            var i = 0;
            foreach (var t in typeList)
            {
                namespaces[i] = t.Namespace;
                names[i] = t.Name;
                ++i;
            }
        }

        public static GenericMenu CreateGenericMenuFromTypes(List<Type> types, string[] names, string[] namespaces,
            string nameSpaceRoot, GenericMenu.MenuFunction2 menuFunc)
        {
            var menuFromNamespaces = new MenuFromNamespace() { ReplaceNamespaces = new[] { new Tuple<string, string>(nameSpaceRoot, "")} };
            // this ensures previous functionality
            var parts = nameSpaceRoot.Split('.');
            menuFromNamespaces.SkipRoots = new[] {parts[0]};

            return CreateGenericMenuFromTypes(types, names, namespaces, menuFromNamespaces, menuFunc);
        }

        public static GenericMenu CreateGenericMenuFromTypes(List<Type> types, string[] names, string[] namespaces,
            MenuFromNamespace settings, GenericMenu.MenuFunction2 menuFunc)
        {
            var menu = new GenericMenu();
            for (var i = 0; i < namespaces.Length; i++)
            {
                var nameSpace = namespaces[i];
                if (CreateGenericMenuFromTypes_SkipForNameSpace(ref nameSpace, settings))
                    continue;

                nameSpace = $"{nameSpace.Replace('.', '/')}";

                var path = names[i];
                if (!string.IsNullOrEmpty(nameSpace))
                    path = $"{nameSpace}/{names[i]}";

                menu.AddItem(new GUIContent(path), false, menuFunc, types[i]);
            }

            return menu;
        }




        public static SelectTypesPopup CreateTypeSelectorPopup(List<Type> types, string[] names, string[] namespaces,
            string nameSpaceRoot, GenericMenu.MenuFunction2 menuFunc)
        {
            var menuFromNamespaces = new MenuFromNamespace() { ReplaceNamespaces = new[] { new Tuple<string, string>(nameSpaceRoot, "") } };
            // this ensures previous functionality
            var parts = nameSpaceRoot.Split('.');
            menuFromNamespaces.SkipRoots = new[] { parts[0] };

            return CreateTypeSelectorPopup(types, names, namespaces, menuFromNamespaces, menuFunc);
        }

        public static SelectTypesPopup CreateTypeSelectorPopup(List<Type> types, string[] names, string[] namespaces,
            MenuFromNamespace settings, GenericMenu.MenuFunction2 menuFunc)
        {
            var menu = new SelectTypesPopup();
            for (var i = 0; i < namespaces.Length; i++)
            {
                var nameSpace = namespaces[i];
                if (CreateGenericMenuFromTypes_SkipForNameSpace(ref nameSpace, settings))
                    continue;

                nameSpace = $"{nameSpace.Replace('.', '/')}";

                var path = names[i];
                if (!string.IsNullOrEmpty(nameSpace))
                    path = $"{nameSpace}/{names[i]}";

                menu.AddTypeItem(path, menuFunc, types[i]);
            }

            return menu;
        }


        public struct MenuFromNamespace
        {
            // first we iterate through cut-namespaces, if something is found skip is ignored
            // so skip roots are currently expected to never go deeper then a namespace in cutNamespace
            // (if CutNamespace is Game.Actions skip-namespace shouldn't be Game.Actions.Internal)
            public Tuple<string, string>[] ReplaceNamespaces;
            public string[] SkipRoots;

            public static MenuFromNamespace Default => new MenuFromNamespace()
            {
                ReplaceNamespaces = new Tuple<string, string>[0],
                SkipRoots = new string[0]
            };
        }

        static bool CreateGenericMenuFromTypes_SkipForNameSpace(ref string nameSpace,
            MenuFromNamespace settings)
        {
            if (!settings.ReplaceNamespaces.IsNullOrEmpty())
            {
                foreach (var (find, replace) in settings.ReplaceNamespaces)
                {
                    if (!nameSpace.StartsWith(find))
                        continue;

                    nameSpace = nameSpace.Replace(find, replace);
                    if (nameSpace.StartsWith("."))
                        nameSpace = nameSpace.Remove(0, 1);
                    return false;
                }
            }

            if (settings.SkipRoots == null)
                return false;
            foreach (var skipRoot in settings.SkipRoots)
                if (nameSpace.StartsWith(skipRoot)) // internal stuff - ignore 
                    return true;

            return false;
        }
    }
}