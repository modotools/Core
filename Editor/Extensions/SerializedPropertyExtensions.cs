﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Extensions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Extensions
{
	// todo 1: Adjusted to work with arrays, test if always works (lists etc)
    /// <summary>
    /// Extension class for SerializedProperties
    /// See also: http://answers.unity3d.com/questions/627090/convert-serializedproperty-to-custom-class.html
    /// </summary>
    public static class SerializedPropertyExtensions
    {
        // <summary>
        // Get the object the serialized property holds by using reflection
        // </summary>
        // <typeparam name="T">The object type that the property contains</typeparam>
        // <param name="property"></param>
        // <returns>Returns the object type T if it is the type the property actually contains</returns>
        //public static T GetValue<T>(this SerializedProperty property)
        //{
        //    return GetNestedObject<T>(property.propertyPath, GetSerializedPropertyRoot(property),true);
        //}

        /// <summary>
        /// Set the value of a field of the property with the type T
        /// </summary>
        /// <typeparam name="T">The type of the field that is set</typeparam>
        /// <param name="property">The serialized property that should be set</param>
        /// <param name="value">The new value for the specified property</param>
        /// <returns>Returns if the operation was successful or failed</returns>
        public static bool SetValue<T>(this SerializedProperty property, T value)
        {

            object obj = GetSerializedPropertyRoot(property);
            //Iterate to parent object of the value, necessary if it is a nested object
            var fieldStructure = property.propertyPath.Split('.');
            for (var i = 0; i < fieldStructure.Length - 1; i++)
            {
                obj = GetFieldOrPropertyValue<object>(fieldStructure[i], obj);
            }
            var fieldName = fieldStructure.Last();

            return SetFieldOrPropertyValue(fieldName, obj, value);

        }

        public static Object GetSerializedPropertyRoot(SerializedProperty property)
        {
            return property.serializedObject.targetObject;
        }

        /// <summary>
        /// Iterates through objects to handle objects that are nested in the root object
        /// </summary>
        /// <typeparam name="T">The type of the nested object</typeparam>
        /// <param name="path">Path to the object through other properties e.g. PlayerInformation.Health</param>
        /// <param name="obj">The root object from which this path leads to the property</param>
        /// <param name="includeAllBases">Include base classes and interfaces as well</param>
        /// <returns>Returns the nested object cast to the type T</returns>
        public static T GetNestedObject<T>(string path, object obj, bool includeAllBases = false)
        {
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var part in path.Split('.'))
            {
                if (part.Equals("Array"))
                {
                    continue;
                }
                if (part.StartsWith("data["))
                {
                    var idxStr = part.Substring(5, part.Length - 6);
                    var idx = Int32.Parse(idxStr);
                    if (!(obj is ICollection collectionObj))
                       return default;
                    var enumerator = collectionObj.GetEnumerator();
                    enumerator.MoveNext();
                    for (var i = 0; i < idx; i++)
                        enumerator.MoveNext();
                    obj = enumerator.Current;
                    continue;
                }
                obj = GetFieldOrPropertyValue<object>(part, obj, includeAllBases);
            }
            return (T)obj;
        }

        public static T GetFieldOrPropertyValue<T>(string fieldName, object obj, 
            // ReSharper disable once FlagArgument
            bool includeAllBases = false, 
            BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
        {
            if (obj == null)
                return default;
            var field = obj.GetType().GetField(fieldName, bindings);
            if (field != null) return (T)field.GetValue(obj);

            var property = obj.GetType().GetProperty(fieldName, bindings);
            if (property != null) return (T)property.GetValue(obj, null);

            if (!includeAllBases)
                return default;
            foreach (var type in GetBaseClassesAndInterfaces(obj.GetType()))
            {
                field = type.GetField(fieldName, bindings);
                if (field != null) return (T)field.GetValue(obj);

                property = type.GetProperty(fieldName, bindings);
                if (property != null) return (T)property.GetValue(obj, null);
            }

            return default;
        }

        public static bool SetFieldOrPropertyValue(string fieldName, object obj, object value, 
            // ReSharper disable once FlagArgument
            bool includeAllBases = false, 
            BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
        {
            var field = obj.GetType().GetField(fieldName, bindings);
            if (field != null)
            {
                field.SetValue(obj, value);
                return true;
            }

            var property = obj.GetType().GetProperty(fieldName, bindings);
            if (property != null)
            {
                property.SetValue(obj, value, null);
                return true;
            }

            if (!includeAllBases)
                return false;

            foreach (var type in GetBaseClassesAndInterfaces(obj.GetType()))
            {
                field = type.GetField(fieldName, bindings);
                if (field != null)
                {
                    field.SetValue(obj, value);
                    return true;
                }

                property = type.GetProperty(fieldName, bindings);
                if (property == null)
                    continue;

                property.SetValue(obj, value, null);
                return true;
            }
            return false;
        }

        public static IEnumerable<Type> GetBaseClassesAndInterfaces(this Type type, 
            // ReSharper disable once FlagArgument
            bool includeSelf = false)
        {
            var allTypes = new List<Type>();

            if (includeSelf)
                allTypes.Add(type);

            if (type.BaseType == typeof(object))
            {
                allTypes.AddRange(type.GetInterfaces());
            }
            else
            {
                allTypes.AddRange(
                    Enumerable
                        .Repeat(type.BaseType, 1)
                        .Concat(type.GetInterfaces())
                        .Concat(type.BaseType.GetBaseClassesAndInterfaces())
                        .Distinct());
            }

            return allTypes;
        }





        #region Simple string path based extensions

        /// <summary>
        /// Returns the path to the parent of a SerializedProperty
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static string TopmostParentPath(this SerializedProperty prop)
        {
            var parent = prop.propertyPath;
            var firstDot = prop.propertyPath.IndexOf('.');
            if (firstDot > 0)
                parent = prop.propertyPath.Substring(0, firstDot);
            return parent;
        }

        /// <summary>
        /// Returns the path to the parent of a SerializedProperty
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static string ParentPath(this SerializedProperty prop)
        {
            var lastDot = prop.propertyPath.LastIndexOf('.');
            return lastDot == -1 
                ? "" 
                : prop.propertyPath.Substring(0, lastDot);
        }

        /// <summary>
        /// Returns the parent of a SerializedProperty, as another SerializedProperty
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static SerializedProperty GetParentProp(this SerializedProperty prop)
        {
            var parentPath = prop.ParentPath();
            return prop.serializedObject.FindProperty(parentPath);
        }
        #endregion

        /// <summary>
        /// Set isExpanded of the SerializedProperty and propogate the change up the hierarchy
        /// </summary>
        /// <param name="prop"></param>
        /// <param name="expand">isExpanded value</param>
        public static void ExpandHierarchy(this SerializedProperty prop, bool expand = true)
        {
            while (true)
            {
                prop.isExpanded = expand;
                var parent = GetParentProp(prop);
                if (parent != null)
                {
                    prop = parent;
                    expand = true;
                    continue;
                }

                break;
            }
        }

        #region Reflection based extensions
        // http://answers.unity3d.com/questions/425012/get-the-instance-the-serializedproperty-belongs-to.html

        /// <summary>
        /// Use reflection to get the actual data instance of a SerializedProperty
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static T GetValue<T>(this SerializedProperty prop)
        {
            var path = prop.propertyPath.Replace(".Array.data[", "[");
            object obj = prop.serializedObject.targetObject;
            var elements = path.Split('.');
            foreach (var element in elements)
            {
                if (element.Contains("["))
                {
                    var elementName = element.Substring(0, element.IndexOf("[", StringComparison.Ordinal));
                    var index = Convert.ToInt32(element.Substring(element.IndexOf("[", StringComparison.Ordinal)).Replace("[", "").Replace("]", ""));
                    obj = GetValue(obj, elementName, index);
                }
                else
                    obj = GetValue(obj, element);
            }

            if (obj is T o)
                return o;
            return default;
        }

        public static Type GetTypeReflection(this SerializedProperty prop)
        {
            var obj = GetParent<object>(prop);
            if (obj == null)
                return null;

            var objType = obj.GetType();
            const BindingFlags bindingFlags = BindingFlags.GetField
                                              | BindingFlags.GetProperty
                                              | BindingFlags.Instance
                                              | BindingFlags.NonPublic
                                              | BindingFlags.Public;
            var field = objType.GetField(prop.name, bindingFlags);
            return field == null 
                ? null 
                : field.FieldType;
        }

        /// <summary>
        /// Uses reflection to get the actual data instance of the parent of a SerializedProperty
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static T GetParent<T>(this SerializedProperty prop)
        {
            var path = prop.propertyPath.Replace(".Array.data[", "[");
            object obj = prop.serializedObject.targetObject;
            var elements = path.Split('.');
            foreach (var element in elements.Take(elements.Length - 1))
            {
                if (element.Contains("["))
                {
                    var elementName = element.Substring(0, element.IndexOf("[", StringComparison.Ordinal));
                    var index = Convert.ToInt32(element.Substring(element.IndexOf("[", StringComparison.Ordinal))
                        .Replace("[", "")
                        .Replace("]", ""));

                    obj = GetValue(obj, elementName, index);
                }
                else
                {
                    obj = GetValue(obj, element);
                }
            }
            return (T)obj;
        }

        static object GetValue(object source, string name)
        {
            if (source == null)
                return null;

            var type = source.GetType();
            const BindingFlags bindings = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy |
                                          BindingFlags.Instance | BindingFlags.IgnoreCase;
            var f = type.GetField(name, bindings);
            if (f != null)
                return f.GetValue(source);

            // for generic base class some fields might only be found this way: (fe. Context-Variables)
            if (type.BaseType != null)
            {
                var baseFieldInfo = type.BaseType.GetField(name, bindings);
                if (baseFieldInfo != null)
                    return baseFieldInfo.GetValue(source);
            }

            var p = type.GetProperty(name, bindings);
            return p == null 
                ? null 
                : p.GetValue(source, null);
        }

        static object GetValue(object source, string name, int index)
        {
            if (!(GetValue(source, name) is IEnumerable enumerable))
                return null;
            var enm = enumerable.GetEnumerator();
            while (index-- >= 0)
                enm.MoveNext();
            return enm.Current;
        }

        /// <summary>
        /// Use reflection to check if SerializedProperty has a given attribute
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static bool HasAttribute<T>(this SerializedProperty prop)
        {
            var attributes = GetAttributes<T>(prop);
            if (attributes != null)
                return attributes.Length > 0;
            return false;
        }

        public static T GetAttribute<T>(this SerializedProperty prop) where T :class
        {
            var attributes = GetAttributes<T>(prop);
            if (attributes.IsNullOrEmpty())
                return null;
            return (T) attributes[0];
        }


        /// <summary>
        /// Use reflection to get the attributes of the SerializedProperty
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="prop"></param>
        /// <returns></returns>
        public static object[] GetAttributes<T>(this SerializedProperty prop)
        {
            var obj = GetParent<object>(prop);
            if (obj == null)
                return new object[0];

            var attrType = typeof(T);
            var objType = obj.GetType();
            const BindingFlags bindingFlags = BindingFlags.GetField
                                              | BindingFlags.GetProperty
                                              | BindingFlags.Instance
                                              | BindingFlags.NonPublic
                                              | BindingFlags.Public;
            var field = objType.GetField(prop.name, bindingFlags);
            return field != null 
                ? field.GetCustomAttributes(attrType, true)
                : new object[0];
        }

        /// <summary>
        /// Find properties in the serialized object of the given type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="enterChildren"></param>
        /// <returns></returns>
        public static SerializedProperty[] FindPropsOfType<T>(this SerializedObject obj, bool enterChildren = false)
        {
            var foundProps = new List<SerializedProperty>();
            var propType = typeof(T);

            var iterProp = obj.GetIterator();
            iterProp.Next(true);

            if (!iterProp.NextVisible(enterChildren))
                return foundProps.ToArray();
            do
            {
                var propValue = iterProp.GetValue<T>();
                if (propValue != null)
                {
                    foundProps.Add(iterProp.Copy());
                    continue;
                }

                if (iterProp.propertyType != SerializedPropertyType.ObjectReference)
                    continue;
                if (iterProp.objectReferenceValue != null && iterProp.objectReferenceValue.GetType() == propType)
                    foundProps.Add(iterProp.Copy());

            }
            while (iterProp.NextVisible(enterChildren));

            return foundProps.ToArray();
        }

        #endregion


        public static void DoCopyPaste(SerializedProperty from, SerializedProperty to)
        {
            if (@from.propertyType != to.propertyType)
                return;

            switch (@from.propertyType)
            {
                case SerializedPropertyType.Integer: to.intValue = @from.intValue; break;
                case SerializedPropertyType.Boolean: to.boolValue = @from.boolValue; break;
                case SerializedPropertyType.Float: to.floatValue = @from.floatValue; break;
                case SerializedPropertyType.String: to.stringValue = @from.stringValue; break;
                case SerializedPropertyType.Color: to.colorValue = @from.colorValue; break;
                case SerializedPropertyType.ObjectReference: to.objectReferenceValue = @from.objectReferenceValue; break;
                case SerializedPropertyType.LayerMask: to.intValue = @from.intValue; break;
                case SerializedPropertyType.Enum: to.enumValueIndex = @from.enumValueIndex; break;
                case SerializedPropertyType.Vector2: to.vector2Value = @from.vector2Value; break;
                case SerializedPropertyType.Vector3: to.vector3Value = @from.vector3Value; break;
                case SerializedPropertyType.Vector4: to.vector4Value = @from.vector4Value; break;
                case SerializedPropertyType.Rect: to.rectValue = @from.rectValue; break;
                case SerializedPropertyType.ArraySize: to.intValue = @from.intValue; break;
                case SerializedPropertyType.Character: to.intValue = @from.intValue; break;
                case SerializedPropertyType.AnimationCurve: to.animationCurveValue = @from.animationCurveValue; break;
                case SerializedPropertyType.Bounds: to.boundsValue = @from.boundsValue; break;
                case SerializedPropertyType.Gradient: to.boolValue = @from.boolValue; break;
                case SerializedPropertyType.Quaternion: to.quaternionValue = @from.quaternionValue; break;
                case SerializedPropertyType.ExposedReference: to.exposedReferenceValue = @from.exposedReferenceValue; break;
                case SerializedPropertyType.Vector2Int: to.vector2IntValue = @from.vector2IntValue; break;
                case SerializedPropertyType.Vector3Int: to.vector3IntValue = @from.vector3IntValue; break;
                case SerializedPropertyType.RectInt: to.rectIntValue = @from.rectIntValue; break;
                case SerializedPropertyType.BoundsInt: to.boundsIntValue = @from.boundsIntValue; break;

                case SerializedPropertyType.Generic: break;
                case SerializedPropertyType.FixedBufferSize: break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static void Clear(SerializedProperty prop)
        {
            switch (prop.propertyType)
            {
                case SerializedPropertyType.Integer: prop.intValue = default; break;
                case SerializedPropertyType.Boolean: prop.boolValue = default; break;
                case SerializedPropertyType.Float: prop.floatValue = default; break;
                case SerializedPropertyType.String: prop.stringValue = default; break;
                case SerializedPropertyType.Color: prop.colorValue = default; break;
                case SerializedPropertyType.ObjectReference: prop.objectReferenceValue = default; break;
                case SerializedPropertyType.LayerMask: prop.intValue = default; break;
                case SerializedPropertyType.Enum: prop.enumValueIndex = default; break;
                case SerializedPropertyType.Vector2: prop.vector2Value = default; break;
                case SerializedPropertyType.Vector3: prop.vector3Value = default; break;
                case SerializedPropertyType.Vector4: prop.vector4Value = default; break;
                case SerializedPropertyType.Rect: prop.rectValue = default; break;
                case SerializedPropertyType.ArraySize: prop.intValue = default; break;
                case SerializedPropertyType.Character: prop.intValue = default; break;
                case SerializedPropertyType.AnimationCurve: prop.animationCurveValue = default; break;
                case SerializedPropertyType.Bounds: prop.boundsValue = default; break;
                case SerializedPropertyType.Gradient: prop.boolValue = default; break;
                case SerializedPropertyType.Quaternion: prop.quaternionValue = default; break;
                case SerializedPropertyType.ExposedReference: prop.exposedReferenceValue = default; break;
                case SerializedPropertyType.Vector2Int: prop.vector2IntValue = default; break;
                case SerializedPropertyType.Vector3Int: prop.vector3IntValue = default; break;
                case SerializedPropertyType.RectInt: prop.rectIntValue = default; break;
                case SerializedPropertyType.BoundsInt: prop.boundsIntValue = default; break;
                case SerializedPropertyType.Generic: break;
                case SerializedPropertyType.FixedBufferSize: break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

#if undefined
        #region https://gist.github.com/capnslipp/8516384
        /// @note: switch/case derived from the decompilation of SerializedProperty's internal SetToValueOfTarget() method.
	    public static T Value<T>(this SerializedProperty prop)
	    {
		    var valueType = typeof(T);
		    
		    // First, do special Type checks
		    if (valueType.IsEnum)
			    return (T)Enum.ToObject(valueType, prop.enumValueIndex);
		    
		    // Next, check for literal UnityEngine struct-types
		    // @note: ->object->ValueT double-casts because C# is too dumb to realize that that the ValueT in each situation is the exact type needed.
		    // 	e.g. `return thisSP.colorValue` spits _error CS0029: Cannot implicitly convert type `UnityEngine.Color' to `ValueT'_
		    // 	and `return (ValueT)thisSP.colorValue;` spits _error CS0030: Cannot convert type `UnityEngine.Color' to `ValueT'_
		    if (typeof(Color).IsAssignableFrom(valueType))
			    return (T)(object)prop.colorValue;
            if (typeof(LayerMask).IsAssignableFrom(valueType))
                return (T)(object)prop.intValue;
            if (typeof(Vector2).IsAssignableFrom(valueType))
                return (T)(object)prop.vector2Value;
            if (typeof(Vector3).IsAssignableFrom(valueType))
                return (T)(object)prop.vector3Value;
            if (typeof(Rect).IsAssignableFrom(valueType))
                return (T)(object)prop.rectValue;
            if (typeof(AnimationCurve).IsAssignableFrom(valueType))
                return (T)(object)prop.animationCurveValue;
            if (typeof(Bounds).IsAssignableFrom(valueType))
                return (T)(object)prop.boundsValue;
            if (typeof(Gradient).IsAssignableFrom(valueType))
                return (T)(object)SafeGradientValue(prop);
            if (typeof(Quaternion).IsAssignableFrom(valueType))
                return (T)(object)prop.quaternionValue;

            // Next, check if derived from UnityEngine.Object base class
		    if (typeof(Object).IsAssignableFrom(valueType))
			    return (T)(object)prop.objectReferenceValue;
		    
		    // Finally, check for native type-families
		    if (typeof(int).IsAssignableFrom(valueType))
			    return (T)(object)prop.intValue;
            if (typeof(bool).IsAssignableFrom(valueType))
                return (T)(object)prop.boolValue;
            if (typeof(float).IsAssignableFrom(valueType))
                return (T)(object)prop.floatValue;
            if (typeof(string).IsAssignableFrom(valueType))
                return (T)(object)prop.stringValue;
            if (typeof(char).IsAssignableFrom(valueType))
                return (T)(object)prop.intValue;

            // And if all fails, throw an exception.
		    throw new NotImplementedException("Unimplemented propertyType "+prop.propertyType+".");
	    }

        public static dynamic Value(this SerializedProperty prop)
        {
            switch (prop.propertyType) {
                case SerializedPropertyType.Integer:
                    return prop.intValue;
                case SerializedPropertyType.Boolean:
                    return prop.boolValue;
                case SerializedPropertyType.Float:
                    return prop.floatValue;
                case SerializedPropertyType.String:
                    return prop.stringValue;
                case SerializedPropertyType.Color:
                    return prop.colorValue;
                case SerializedPropertyType.ObjectReference:
                    return prop.objectReferenceValue;
                case SerializedPropertyType.LayerMask:
                    return prop.intValue;
                case SerializedPropertyType.Enum:
                    var enumI = prop.enumValueIndex;
                    return new KeyValuePair<int, string>(enumI, prop.enumNames[enumI]);
                case SerializedPropertyType.Vector2:
                    return prop.vector2Value;
                case SerializedPropertyType.Vector3:
                    return prop.vector3Value;
                case SerializedPropertyType.Rect:
                    return prop.rectValue;
                case SerializedPropertyType.ArraySize:
                    return prop.intValue;
                case SerializedPropertyType.Character:
                    return (char)prop.intValue;
                case SerializedPropertyType.AnimationCurve:
                    return prop.animationCurveValue;
                case SerializedPropertyType.Bounds:
                    return prop.boundsValue;
                case SerializedPropertyType.Gradient:
                    return SafeGradientValue(prop);
                case SerializedPropertyType.Quaternion:
                    return prop.quaternionValue;
			
                default:
                    throw new NotImplementedException("Unimplemented propertyType "+prop.propertyType+".");
            }
        }

        /// Access to SerializedProperty's internal gradientValue property getter,
        /// in a manner that will only soft break (returning null) if the property changes or disappears in future Unity revs.
        static Gradient SafeGradientValue(SerializedProperty sp)
        {
            const BindingFlags instanceAnyPrivacyBindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var propertyInfo = typeof(SerializedProperty).GetProperty(
                "gradientValue",
                instanceAnyPrivacyBindingFlags,
                null,
                typeof(Gradient),
                new Type[0],
                null
            );
            if (propertyInfo == null)
                return null;
		
            var gradientValue = propertyInfo.GetValue(sp, null) as Gradient;
            return gradientValue;
        }
        #endregion
#endif
    }
}