﻿/// <summary>
/// MIT License - Copyright(c) 2019 Ugo Belfiore
/// </summary>

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using static UnityEditor.EditorGUILayout;

/// <summary>
/// all of this function must be called in OnSceneGUI
/// </summary>
public static class ExtGUI
{
    /// <summary>
    /// This function have to be called at the end of your GUI.Window() function.
    /// It eat the clic event, so it doesn't propagate thought things below.
    /// </summary>
    /// <param name="current"></param>
    public static void PreventClicGoThought(Event current)
    {
        if ((current.type == EventType.MouseDrag || current.type == EventType.MouseDown)
            && current.button == 0)
        {
            Event.current.Use();
        }
    }

}
