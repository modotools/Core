using System;
using System.Linq;

namespace Core.Editor.Attribute
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ParentEditorAttribute : System.Attribute
    {
        readonly Type[] m_validStateTypes;

        public ParentEditorAttribute(Type t) => m_validStateTypes = new []{t};
        public ParentEditorAttribute(Type[] t) => m_validStateTypes = t;

        public bool IsValid(Type t) => m_validStateTypes.Contains(t);
    }
}