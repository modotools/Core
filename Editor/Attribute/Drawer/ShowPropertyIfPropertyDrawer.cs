﻿using System;
using Core.Editor.Extensions;
using Core.Unity.Attributes;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Attribute
{
    // todo 1: currently works only for c# properties
    // ReSharper disable once UnusedType.Global
    // used via reflection
    public class ShowPropertyIfPropertyDrawer : MultiPropertyEditorDrawer
    {
        public override Type PreviewForAttribute => typeof(ShowPropertyIfAttribute);

        public override bool IsVisible(MultiPropertyAttribute attr, SerializedProperty property)
        {
            var showIfAttribute = (ShowPropertyIfAttribute)attr;
            return GetAttributeResult(showIfAttribute, property);
        }

        static bool GetAttributeResult(ShowPropertyIfAttribute showPropertyIfAttr, SerializedProperty property)
        {
            var enabled = true;

            var propertyPath = property.propertyPath;
            var parentPath = propertyPath.Substring(0, propertyPath.Length - property.name.Length - 1);
            var parent = property.serializedObject.FindProperty(parentPath);
            var parentObj = parent.GetValue<object>();

            //var conditionPath = propertyPath.Replace(property.name, showIfAttr.ConditionalSourceField);

            var sourcePropertyValue = parentObj.GetType().GetProperty(showPropertyIfAttr.ConditionalSourceField);

            if (sourcePropertyValue != null)
                enabled = (bool) sourcePropertyValue.GetValue(parentObj);
            else
                Debug.LogWarning("Attempting to use a ShowIfAttribute but no matching SourcePropertyValue found in object: " + showPropertyIfAttr.ConditionalSourceField);

            return enabled;
        }
    }
}
