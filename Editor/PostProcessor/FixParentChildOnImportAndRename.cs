using System.Linq;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Interface;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Processor
{
    /// <summary>
    /// This asset processor resolves an issue with the new v2 AssetDatabase system present on 2019.3 and later. When
    /// renaming a <see cref="IParentAsset"/> asset, it appears that sometimes the v2 AssetDatabase will swap which asset
    /// is the main asset (present at top level) between the <see cref="IParentAsset"/> and one of its <see cref="IScriptableObject"/>
    /// sub-assets. As a workaround until Unity fixes this, this asset processor checks all renamed assets and if it
    /// finds a case where a <see cref="IScriptableObject"/> has been made the main asset it will swap it back to being a sub-asset
    /// and rename the IChildAsset to the default name for that IChildAsset type.
    /// </summary>
    internal sealed class FixParentChildOnImportAndRename : AssetPostprocessor
    {
        static void OnPostprocessAllAssets(
            string[] importedAssets,
            string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            foreach (var assetPath in importedAssets) 
                FixParentChildAtPath(assetPath);
            foreach (var assetPath in movedAssets) 
                FixParentChildAtPath(assetPath);
        }

        static void FixParentChildAtPath(string assetPath)
        {
            var mainObj = AssetDatabase.LoadMainAssetAtPath(assetPath);

            if (!(mainObj is IScriptableObject mainSo))
                return;
            var allAssets = AssetDatabase.LoadAllAssetsAtPath(assetPath);
            var parentMarker = allAssets.FirstOrDefault(a => a is IParentMarker) as IParentMarker;

            var fixApplicable = parentMarker != null;

            if (!fixApplicable)
                return;
            if (ReferenceEquals(parentMarker.Parent, mainObj))
                return;

            Debug.Assert(AssetDatabase.IsMainAsset(mainObj));

            AssetDatabase.SetMainObject((Object) parentMarker.Parent, assetPath);
            AssetDatabase.ImportAsset(assetPath);

            var mainName = mainSo.name;
            if (mainName == null || mainName.Trim().IsNullOrEmpty()) 
                mainSo.name = mainSo.DefaultName();

            EditorUtility.SetDirty(mainObj);
        }
    }
}