using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Core.Extensions;
using UnityEditor;

using Core.Unity.Attributes;

namespace Core.Editor.PostProcessor
{
    /// <summary>
    /// Specify a texture name from your assets which you want to be assigned as an icon to the MonoScript.
    /// <see cref="EditorIcon"/>
    /// </summary>
    public class EditorIconPostProcessor : AssetPostprocessor
    {
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            var metaChangedForAssets = new List<string>();
            foreach (var assetPath in importedAssets) 
                OnPostProcessFile(assetPath, metaChangedForAssets);

            if (metaChangedForAssets.Count <= 0) 
                return;
            foreach (var assetPath in metaChangedForAssets) 
                AssetDatabase.ImportAsset(assetPath);
        }

        static void OnPostProcessFile(string assetPath, ICollection<string> metaChangedForAssets)
        {
            var metaPath = $"{assetPath}.meta";
            var isScript = assetPath.EndsWith(".cs");
            var assetExists = File.Exists(assetPath);
            var metaExists = File.Exists(metaPath);
            var isValidItem = assetExists && metaExists && isScript;

            if (!isValidItem)
                return;

            if (!ExtractIconName(assetPath, 
                out var iconName))
                return;

            if (!FindGUIDFromIconName(iconName, out var guid)) 
                return;

            SetIconGUIDInMetaFile(assetPath, metaChangedForAssets, metaPath, guid);
        }

        static void SetIconGUIDInMetaFile(string assetPath, ICollection<string> metaChangedForAssets, string metaPath, string guid)
        {
            // Read meta for script
            var scriptMetaTextLines = File.ReadAllLines(metaPath);
            var metaIconLine = $"icon: {{fileID: 2800000, guid: {guid}, type: 3}}";
            for (var i = 0; i < scriptMetaTextLines.Length; ++i)
            {
                var line = scriptMetaTextLines[i];
                // Find icon line
                if (!line.Contains("icon: ")
                    || line.Contains(metaIconLine))
                    continue;

                var indexIconKeyName = line.IndexOf("icon: ", StringComparison.Ordinal);
                var indexAfterClosingBrace = line.IndexOf("}", indexIconKeyName, StringComparison.Ordinal) + 1;
                var newLine = line.Replace(line.Substring(indexIconKeyName, indexAfterClosingBrace - indexIconKeyName),
                    metaIconLine);
                scriptMetaTextLines[i] = newLine;
                File.WriteAllLines(metaPath, scriptMetaTextLines);
                metaChangedForAssets.Add(assetPath);
                break;
            }
        }

        static bool FindGUIDFromIconName(string iconName, out string iconGuid)
        {
            // Find guid based on icon name from attr
            var iconGuids = AssetDatabase.FindAssets($"{iconName} t:texture2D");
            var iconGuidsList = iconGuids.ToList();
            iconGuid = null;
            foreach (var guid in iconGuidsList)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var name = Path.GetFileNameWithoutExtension(path);
                iconGuid = guid;
                if (string.Equals(iconName, name))
                    break;
            }

            return !iconGuid.IsNullOrEmpty();
        }

        static bool ExtractIconName(string assetPath, out string iconName)
        {
            iconName = "";
            var scriptText = File.ReadAllText(assetPath);
            var containsEditorIconAttr = scriptText.Contains("[EditorIcon(");

            if (!containsEditorIconAttr)
                return false;

            // Extract icon name from attribute
            // We are assuming that template strings are not used
            var attrIconNameStartIndex = scriptText.IndexOf("[EditorIcon(", StringComparison.Ordinal) + 13;
            var attrIconNameLength = scriptText.IndexOf("\")", attrIconNameStartIndex, StringComparison.Ordinal) -
                                     attrIconNameStartIndex;
            iconName = scriptText.Substring(attrIconNameStartIndex, attrIconNameLength);
            return true;
        }
    }
}
