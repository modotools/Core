﻿//using System;
//using System.IO;
//using Core.Extensions;
//using Core.Unity.Interface;
//using JetBrains.Annotations;
//using UnityEditor;
//using UnityEngine;
//using Object = UnityEngine.Object;
//namespace Core.Editor.PostProcessor
//{
//    public class DeleteSubAssetsWithMissingScript : UnityEditor.AssetModificationProcessor
//    {
//        const bool k_iDontLikeThis = true;
//        /// <summary> Automatically delete Child sub-assets before deleting their script.
//        /// This is important to do, because you can't delete null sub assets.
//        /// <para/> For another workaround, see: https://gitlab.com/RotaryHeart-UnityShare/subassetmissingscriptdelete </summary>
//        [UsedImplicitly]
//        static AssetDeleteResult OnWillDeleteAsset(string path, RemoveAssetOptions options)
//        {
//            if (k_iDontLikeThis)
//                return AssetDeleteResult.DidNotDelete;
//            if (IsChildAssetScript(path, out var scriptType))
//                return AssetDeleteResult.DidNotDelete;
//            DeleteSubAssets(scriptType);
//            // We didn't actually delete the script. Tell the internal system to carry on with normal deletion procedure
//            return AssetDeleteResult.DidNotDelete;
//        }

//        static void DeleteSubAssets(Type scriptType)
//        {
//            // Find all ScriptableObjects using this script
//            var guids = AssetDatabase.FindAssets("t:" + scriptType);
//            //foreach (var guid in guids)
//            //{
//            //    var assetPath = AssetDatabase.GUIDToAssetPath(guid);
//            //    var allObjs = AssetDatabase.LoadAllAssetRepresentationsAtPath(assetPath);
//            //    foreach (var subObj in allObjs)
//            //    {
//            //        if (_!(subObj is IChildAsset childAsset))
//            //            continue;
//            //        if (childAsset.GetType()_!= scriptType)
//            //            continue;
//            //        if (childAsset.Parent == null)
//            //            continue;
//            //        // Delete the child and notify the user
//            //        Debug.LogWarning(childAsset.name + " of " + childAsset.Parent
//            //                         + " depended on deleted script and has been removed automatically.",
//            //            (Object)childAsset.Parent);
//            //        CollectionExtensions.Remove(ref childAsset.Parent.ChildAssets, subObj as ScriptableObject);
//            //    }
//            //}
//        }
//        static bool IsChildAssetScript(string path, out Type scriptType)
//        {
//            scriptType = default;
//            // Skip processing anything without the .cs extension
//            if (Path.GetExtension(path)_!= ".cs")
//                return false;
//            // Get the object that is requested for deletion
//            var obj = AssetDatabase.LoadAssetAtPath<Object>(path);
//            // If we aren't deleting a script, return
//            if (_!(obj is MonoScript))
//                return false;
//            // Check script type. Return if deleting a non-child script
//            var script = obj as MonoScript;
//            scriptType = script.GetClass();
//            return scriptType_!= null && typeof(IChildAsset).IsAssignableFrom(scriptType);
//        }
//    }
//}