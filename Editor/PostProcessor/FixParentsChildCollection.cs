﻿//using System.Linq;
//using Core.Unity.Interface;
//using JetBrains.Annotations;
//using UnityEditor;
//using UnityEngine;
//namespace Core.Editor.PostProcessor
//{
//    internal class FixParentsChildCollection : UnityEditor.AssetModificationProcessor
//    {
//        // todo: don't like current implementation OnReloadEditor (rather would do on open/load asset)
//        /// <summary> Automatically re-add loose node assets to the Graph node list </summary>
//        [InitializeOnLoadMethod, UsedImplicitly]
//        static void OnReloadEditor() => Fix();
//        static void Fix()
//        {
//            // Find all IParentAsset assets
//            var guids = AssetDatabase.FindAssets("t:" + typeof(IParentAsset));
//            foreach (var guid in guids)
//            {
//                var assetPath = AssetDatabase.GUIDToAssetPath(guid);
//                var graph = (IParentAsset) AssetDatabase.LoadAssetAtPath(assetPath, typeof(IParentAsset));
//                var childObjs = graph.ChildAssets.ToList();
//                childObjs.RemoveAll(x => x == null); // Remove null items
//                var objs = AssetDatabase.LoadAllAssetRepresentationsAtPath(assetPath);
//                // Ensure that all IChildAsset are present in the child list
//                foreach (var obj in objs)
//                {
//                    var so = obj as ScriptableObject;
//                    // Ignore null and non-node sub assets
//                    if (_!(so is IChildAsset)) 
//                        continue;
//                    if (!childObjs.Contains(so))
//                        childObjs.Add(so);
//                }
//                graph.ChildAssets = childObjs.ToArray();
//            }
//        }
//    }
//}