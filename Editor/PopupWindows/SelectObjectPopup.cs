using System;
using System.Linq;
using Core.Editor.Utility;
using Core.Extensions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.PopupWindows
{
    public class SelectObjectPopup : PopupWindowContent
    {
        public SelectObjectPopup(GenericMenu.MenuFunction2 onOK, Object[] objects,
            GenericMenu.MenuFunction cancel = null)
        {
            m_onOK = onOK;
            m_onCancel = cancel;
            m_objects = objects;
            
            m_options = new string[m_objects.Length];
            for (var i = 0; i < m_objects.Length; i++)
            {
                var obj = m_objects[i];
                m_options[i] = $"[{i}] {obj.name} {obj.GetType()}";
            }
        }

        readonly GenericMenu.MenuFunction m_onCancel;
        readonly GenericMenu.MenuFunction2 m_onOK;
        readonly Object[] m_objects;
        readonly string[] m_options;

        Object m_selected;

        bool m_ok = true;
        bool m_popupFocusedOnce;

        public static Vector2 WindowSize => new Vector2(400, 50);
        public override Vector2 GetWindowSize() => WindowSize;

        public override void OnGUI(Rect rect)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Select: ", EditorStyles.boldLabel);

                var idx = Array.IndexOf(m_objects, m_selected);
                GUI.SetNextControlName($"{nameof(SelectObjectPopup)}.Popup");
                idx = EditorGUILayout.Popup(idx, m_options);
                if (!m_popupFocusedOnce)
                {
                    GUI.FocusControl($"{nameof(SelectObjectPopup)}.Popup");
                    m_popupFocusedOnce = true;
                }

                m_selected = idx.IsInRange(m_objects) ? m_objects[idx] : null;
                if (check.changed) 
                    GUI.FocusControl($"{nameof(SelectObjectPopup)}.OK");
            }

            using (new GUILayout.HorizontalScope())
            {
                using (new EditorGUI.DisabledScope(!m_ok))
                {
                    GUI.SetNextControlName($"{nameof(SelectObjectPopup)}.OK");
                    if (CustomGUI.ButtonOrReturnKey("OK"))
                    {
                        m_onOK.Invoke(m_selected);
                        EditorWindow.GetWindow<PopupWindow>().Close();
                    }
                }

                if (CustomGUI.ButtonOrKey("Cancel", KeyCode.Escape))
                {
                    m_onCancel?.Invoke();
                    EditorWindow.GetWindow<PopupWindow>().Close();
                }
            }
        }
    }
}