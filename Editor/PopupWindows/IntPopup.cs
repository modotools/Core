using System;
using Core.Editor.Utility;
using Core.Extensions;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.PopupWindows
{
    public class IntPopup : PopupWindowContent
    {
        public IntPopup(int initNumber,
            GenericMenu.MenuFunction2 onOK, 
            GenericMenu.MenuFunction cancel = null)
        {
            m_input = initNumber;
            m_onOK = onOK;
            m_onCancel = cancel;
        }

        readonly GenericMenu.MenuFunction m_onCancel;
        readonly GenericMenu.MenuFunction2 m_onOK;

        int m_input;
        bool m_popupFocusedOnce;

        public static Vector2 WindowSize => new Vector2(400, 50);
        public override Vector2 GetWindowSize() => WindowSize;

        public override void OnGUI(Rect rect)
        {
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Name: ", EditorStyles.boldLabel);
                GUI.SetNextControlName($"{nameof(IntPopup)}.Input");
               
                m_input = EditorGUILayout.IntField(m_input);

                if (!m_popupFocusedOnce)
                {
                    GUI.FocusControl($"{nameof(IntPopup)}.Input");
                    m_popupFocusedOnce = true;
                }
            }

            using (new GUILayout.HorizontalScope())
            {
                GUI.SetNextControlName($"{nameof(IntPopup)}.OK");
                if (CustomGUI.ButtonOrReturnKey("OK"))
                {
                    m_onOK.Invoke(m_input);
                    EditorWindow.GetWindow<PopupWindow>().Close();
                }
                if (CustomGUI.ButtonOrKey("Cancel", KeyCode.Escape))
                {
                    m_onCancel?.Invoke();
                    EditorWindow.GetWindow<PopupWindow>().Close();
                }
            }
        }
    }
}