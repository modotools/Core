using System;
using Core.Editor.Utility;
using Core.Extensions;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.PopupWindows
{
    public class NamePopup : PopupWindowContent
    {
        public NamePopup(GenericMenu.MenuFunction2 onOK, 
            GenericMenu.MenuFunction cancel = null,
            CheckName checkName = null, 
            string[] options = null)
        {
            m_onOK = onOK;
            m_onCancel = cancel;
            m_onNameChange = checkName;
            m_options = options;
        }

        public delegate bool CheckName(string name);

        readonly GenericMenu.MenuFunction m_onCancel;
        readonly GenericMenu.MenuFunction2 m_onOK;
        readonly CheckName m_onNameChange;
        readonly string[] m_options;

        string m_inputText;

        bool m_ok;
        bool m_popupFocusedOnce;

        public static Vector2 WindowSize => new Vector2(400, 50);
        public override Vector2 GetWindowSize() => WindowSize;

        public override void OnGUI(Rect rect)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Name: ", EditorStyles.boldLabel);
                GUI.SetNextControlName($"{nameof(NamePopup)}.Input");
                var useDropdown = m_options != null;
                if (useDropdown)
                {
                    var idx = Array.IndexOf(m_options, m_inputText);
                    idx = EditorGUILayout.Popup(idx, m_options);
                    m_inputText = idx.IsInRange(m_options) ? m_options[idx] : "";
                }
                else m_inputText = GUILayout.TextField(m_inputText);

                if (!m_popupFocusedOnce)
                {
                    GUI.FocusControl($"{nameof(NamePopup)}.Input");
                    m_popupFocusedOnce = true;
                }

                if (check.changed)
                {
                    m_ok = m_onNameChange == null || m_onNameChange.Invoke(m_inputText);
                    if (m_ok && useDropdown)
                        GUI.FocusControl($"{nameof(NamePopup)}.OK");
                }

            }

            using (new GUILayout.HorizontalScope())
            {
                using (new EditorGUI.DisabledScope(!m_ok))
                {
                    GUI.SetNextControlName($"{nameof(NamePopup)}.OK");
                    if (CustomGUI.ButtonOrReturnKey("OK"))
                    {
                        m_onOK.Invoke(m_inputText);
                        EditorWindow.GetWindow<PopupWindow>().Close();
                    }
                }
                if (CustomGUI.ButtonOrKey("Cancel", KeyCode.Escape))
                {
                    m_onCancel?.Invoke();
                    EditorWindow.GetWindow<PopupWindow>().Close();
                }
            }
        }
    }
}