using System;
using System.Collections.Generic;
using System.Linq;
using Core.Unity.Types;
using Core.Editor.Extensions;
using Core.Extensions;
using Core.Types;
using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Types
{
    [CustomPropertyDrawer(typeof(ActivityState), true)]
    public class ActivityStateDrawer : PropertyDrawer
    {
        public struct PropertyData
        {
            public GUIContent Label;
            public ActivityState State;
            public Type EnumType;
            public Array EnumValues;
            public string[] EnumNames;

            public SerializedObject SerializedObject;
            public Transform Transform;

            public SerializedProperty MainProperty;
            public SerializedProperty DataProp;
            public SerializedProperty EditorStateProp;

            public int EditorState;
            public int EditorStateMask;
            public bool HasInvalidTargets;
        }

        static readonly float k_lineHeight = EditorGUIUtility.singleLineHeight;

        #region Unity Methods
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (GetPropertyData(property, label, out var propData, out var err) == OperationResult.Error)
                return EditorStyles.helpBox.CalcSize(new GUIContent(err)).y;

            var height = k_lineHeight;
            if (propData.HasInvalidTargets)
                height += k_lineHeight;

            height += k_lineHeight * propData.EnumValues.Length;
            if (!propData.EditorState.IsInRange(0, propData.EnumValues.Length)) 
                return height;

            GetStateData(propData, propData.EditorState, out _, out _, out var stateMask);
            using (var objListScoped = SimplePool<List<GameObject>>.I.GetScoped())
            {
                var targetList = objListScoped.Obj;
                GetTargetsWithStateMask(propData.DataProp, stateMask, targetList);
                height += k_lineHeight; //add button
                height += k_lineHeight * targetList.Count;
            }

            return height;
        }
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (GetPropertyData(property, label, out var propData, out var error) == OperationResult.Error)
            {
                EditorGUI.HelpBox(position, error, MessageType.Error);
                return;
            }

            OnGUI(position, propData);
        }
        #endregion

        #region GUI
        void OnGUI(Rect position, PropertyData propertyData)
        {
            using (var vs = new VerticalRectScope(ref position, position.height))
            {
                var labelRect = vs.Get(k_lineHeight);
                GUI.Label(labelRect, propertyData.Label);

                if (propertyData.HasInvalidTargets) 
                    DrawCleanupOldStates(vs.Get(k_lineHeight), propertyData);

                DrawStates(propertyData, vs);
            }
        }

        static void DrawStates(PropertyData propertyData, VerticalRectScope vs)
        {
            for (var i = 0; i < propertyData.EnumValues.Length; i++) 
                DrawState(propertyData, vs, i);
        }

        static void DrawState(PropertyData propertyData, VerticalRectScope vs, int i)
        {
            GetStateData(propertyData, i, out var stateName, out var stateValue,
                out var stateMask);

            var isStateActive = stateValue == propertyData.EditorState;

            var stateButtonRect = vs.Get(k_lineHeight);
            stateButtonRect.xMin += 3f;
            stateButtonRect.width -= 3f;

            using (new ColorScope(isStateActive ? Color.cyan : Color.white))
            {
                if (GUI.Button(stateButtonRect, stateName))
                {
                    propertyData.EditorStateProp.intValue = stateValue;
                    SetState(propertyData, stateMask);
                }
            }

            if (!isStateActive)
                return;

            GameObject newTarget;
            GameObject removeTarget = null;

            using (var objListScoped = SimplePool<List<GameObject>>.I.GetScoped())
            {
                var targetList = objListScoped.Obj;
                GetTargetsWithStateMask(propertyData.DataProp, stateMask, targetList);

                var newTargetRect = vs.Get(k_lineHeight);
                using (var hs = new HorizontalRectScope(ref newTargetRect, newTargetRect.height))
                {
                    GUI.Label(hs.Get(50f), "Add:");
                    newTarget = EditorGUI.ObjectField(hs.Get(hs.Rect.width), null, 
                        typeof(GameObject), true) as GameObject;
                }

                foreach (var target in targetList)
                {
                    var targetRect = vs.Get(k_lineHeight);
                    using (var hs = new HorizontalRectScope(ref targetRect, targetRect.height))
                    {
                        if (GUI.Button(hs.Get(20f), "x")) 
                            removeTarget = target;
                        using (new EditorGUI.DisabledGroupScope(true))
                            EditorGUI.ObjectField(hs.Get(hs.Rect.width), target, 
                                typeof(GameObject), true);
                    }
                }
            }

            if (newTarget != null && IsTargetAllowed(propertyData, newTarget))
            {
                AddTargetToState(propertyData, newTarget, stateMask);
                SetState(propertyData, stateMask);
            }

            if (removeTarget != null)
            {
                RemoveTargetFromState(propertyData, removeTarget, stateMask);
                SetState(propertyData, stateMask);
            }
        }

        static bool IsTargetAllowed(PropertyData propertyData, GameObject target)
        {
            if (target == null)
                return false;
            if (EditorUtility.IsPersistent(target))
                return false;

            var isChild = target.transform.IsChildOf(propertyData.Transform);
            return isChild;
        }

        static void AddTargetToState(PropertyData propertyData, GameObject newTarget, int stateMask)
        {
            var idx = FindIndexWithTarget(propertyData, newTarget);
            if (idx == -1)
            {
                idx = propertyData.DataProp.arraySize;
                propertyData.DataProp.InsertArrayElementAtIndex(idx);
                GetDataElement(propertyData.DataProp, idx, 
                    out var stateMaskProp, 
                    out var targetProp);

                targetProp.objectReferenceValue = newTarget;
                stateMaskProp.intValue = stateMask;
            }
            else
            {
                GetDataElement(propertyData.DataProp, idx, out var stateMaskProp, 
                    out _);
                stateMaskProp.intValue |= stateMask;
            }
        }

        static int FindIndexWithTarget(PropertyData propertyData, GameObject target)
        {
            var dataProp = propertyData.DataProp;
            for (var i = 0; i < dataProp.arraySize; i++)
            {
                GetDataElement(dataProp, i, out var stateMaskProp, out var targetProp);
                if (ReferenceEquals(targetProp.objectReferenceValue, target))
                    return i;
            }

            return -1;
        }

        void DrawCleanupOldStates(Rect pos, PropertyData propData)
        {
            var invalid = InvalidMask(propData.EnumValues);
            using (var objListScoped = SimplePool<List<GameObject>>.I.GetScoped())
            {
                var targetList = objListScoped.Obj;
                GetTargetsWithStateMask(propData.DataProp, invalid, targetList);
                if (targetList.IsNullOrEmpty())
                    return;

                using (new ColorScope(Color.red))
                {
                    if (!GUI.Button(pos, "Cleanup")) 
                        return;
                    foreach (var t in targetList) 
                        RemoveTargetFromState(propData, t, invalid);
                }
            }
        }
        #endregion

        #region static Methods
        static void GetStateData(PropertyData propertyData, int i, 
            out string stateName, out int stateValue, out int stateMask)
        {
            stateName = propertyData.EnumNames[i];
            stateValue = (int) propertyData.EnumValues.GetValue(i);
            stateMask = 1 << stateValue;
        }
        static void GetDataElement(SerializedProperty dataProp, int i, 
            out SerializedProperty stateMaskProp,
            out SerializedProperty targetProp)
        {
            var data = dataProp.GetArrayElementAtIndex(i);
            stateMaskProp = data.FindPropertyRelative(nameof(ActivityState.ActivityStateData.StateMask));
            targetProp = data.FindPropertyRelative(nameof(ActivityState.ActivityStateData.Target));
        }

        static int InvalidMask(Array enumValues) =>
            enumValues.Cast<object>()
                .Select((t, i) => (int) enumValues.GetValue(i))
                .Select(stateValue => 1 << stateValue)
                .Aggregate(~0, (current, stateMask) 
                    => current & ~stateMask);

        static void GetTargetsWithStateMask(SerializedProperty dataProp, int stateMask,
            ICollection<GameObject> result)
        {
            for (var i = 0; i < dataProp.arraySize; i++)
            {
                GetDataElement(dataProp, i, 
                    out var stateMaskProp, 
                    out var targetProp);

                if (targetProp == null)
                    continue;

                var isActiveInState = (stateMaskProp.intValue & stateMask) != 0;
                if (isActiveInState)
                    result.Add(targetProp.objectReferenceValue as GameObject);
            }
        }
        
        static void RemoveTargetFromState(PropertyData propData, GameObject target, int stateMask)
        {
            for (var i = propData.DataProp.arraySize -1; i >= 0; i--)
            {
                GetDataElement(propData.DataProp, i, 
                    out var stateMaskProp, 
                    out var targetProp);

                if (!ReferenceEquals(targetProp.objectReferenceValue, target))
                    continue;

                stateMaskProp.intValue &= ~stateMask;
                if (stateMaskProp.intValue != 0) 
                    continue;

                propData.DataProp.DeleteArrayElementAtIndex(i);
                // if (target!= null) target.SetActive(true);
            }
        }

        static void SetState(PropertyData propertyData, int stateMask)
        {
            using (var disableListScoped = SimplePool<List<GameObject>>.I.GetScoped())
            using (var enableListScoped = SimplePool<List<GameObject>>.I.GetScoped())
            {
                var disableList = disableListScoped.Obj;
                var enableList = enableListScoped.Obj;
                
                GetTargetsWithStateMask(propertyData.DataProp, ~0, disableList);
                GetTargetsWithStateMask(propertyData.DataProp, stateMask, enableList);
                disableList.RemoveAll(e => enableList.Contains(e));
                foreach (var d in disableList)
                    d.SetActive(false);
                foreach (var e in enableList)
                    e.SetActive(true);
            }
        }

        static OperationResult GetPropertyData(SerializedProperty property, GUIContent label, out PropertyData propertyData, out string error)
        {
            error = default;
            propertyData = default;

            var activityState = property.GetValue<ActivityState>();
            if (activityState == null)
            {
                error = "Could not get ActivityState for property!";
                return OperationResult.Error;
            }

            var enumType = activityState.GetType().GenericTypeArguments[0];
            if (enumType == null)
            {
                error = "Could not get generic type of ActivityState!";
                return OperationResult.Error;
            }
            //var genericActivityState = typeof(ActivityState<>);

            var enumNames = Enum.GetNames(enumType);
            var enumValues = Enum.GetValues(enumType);

            if (enumNames.IsNullOrEmpty())
            {
                error = "No Enum-names for ActivityState!";
                return OperationResult.Error;
            }

            var dataProp = property.FindPropertyRelative(ActivityState.Editor_DataPropName);
            if (dataProp == null)
            {
                error = $"Property {ActivityState.Editor_DataPropName} was not found for ActivityState!";
                return OperationResult.Error;
            }

            var editorStateProp = property.FindPropertyRelative(ActivityState.Editor_EditorStatePropName);
            if (editorStateProp == null)
            {
                error = $"Property {ActivityState.Editor_EditorStatePropName} was not found for ActivityState!";
                return OperationResult.Error;
            }

            var serializedObject = property.serializedObject;
            var target = serializedObject.targetObject;
            if (serializedObject == null || target == null)
            {
                error = "SerializedObject or target invalid!";
                return OperationResult.Error;
            }
            if (!(target is MonoBehaviour mono))
            {
                error = "target is not MonoBehaviour!";
                return OperationResult.Error;
            }

            bool invalidTargets;
            using (var objListScoped = SimplePool<List<GameObject>>.I.GetScoped())
            {
                var targetList = objListScoped.Obj;
                GetTargetsWithStateMask(dataProp, InvalidMask(enumValues), targetList);
                invalidTargets = !targetList.IsNullOrEmpty();
            }

            var editorState = editorStateProp.intValue;
            var editorStateMask = 1 << editorState;

            propertyData = new PropertyData()
            {
                Label = label,
                State = activityState,
                EnumType = enumType,
                EnumNames = enumNames,
                EnumValues = enumValues,
                DataProp = dataProp,
                SerializedObject = serializedObject,
                Transform = mono.transform,
                MainProperty = property,
                EditorStateProp = editorStateProp,
                EditorState = editorState,
                EditorStateMask = editorStateMask,
                HasInvalidTargets = invalidTargets,
            };

            return OperationResult.OK;
        }
        #endregion
    }
}
