﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Attribute;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Types.Attribute;
using Core.Unity.Types.Fsm;
using Core.Unity.Utility.GUITools;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

using static Core.Utils;
using static Core.Unity.Types.Attribute.ContextDrawerAttribute;

using MessageType = UnityEditor.MessageType;
using Object = UnityEngine.Object;

namespace Core.Editor.Types.Fsm
{
    [CustomPropertyDrawer(typeof(IFSMStateRef), true)]
    [CustomPropertyDrawer(typeof(IFSMEventRef), true)]
    public class FSMFallbackDrawer : PropertyDrawer
    {
        FSMEventDrawer.VarProperties m_current;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return FSMEventDrawer.GetProperties(null, property, out m_current, out var err) ==
                   OperationResult.Error 
                ? EditorStyles.helpBox.CalcSize(new GUIContent(err)).y
                : FSMEventDrawer.GetPropertyHeight(m_current);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            => FSMEventDrawer.DoGUI(null, ref position, property, label);
    }

    [UsedImplicitly]
    public class FSMEventDrawer : MultiPropertyEditorDrawer
    {
        public override Type PreviewForAttribute => typeof(FSMDrawerAttribute);

        internal enum FSMRefType
        {
            Event,
            State,
            Invalid
        }

        internal struct VarProperties
        {
            internal SerializedProperty MainProperty;
            internal SerializedProperty NameProperty;
            internal SerializedProperty FSMContainerProp;
            internal SerializedProperty FSMProperty;

            internal FSMRefType Type;

            internal bool FullHide;

            internal ContextDrawerAttributeData Attr;
        }

        static IFSMComponent m_lastCachedFsm;

        static string[] m_cachedEventOptions = new string[0];
        static string[] m_cachedStateOptions = new string[0];

        //const float k_height = 16f;

        const float k_defaultLineHeight = 18f;
        const float k_defaultIndent = 20f;
        static bool m_showContext;

        #region Public Overrides
        public override float GetPropertyHeight(MultiPropertyAttribute attr, SerializedProperty property, GUIContent label, float height)
        {
            var h =  GetProperties(attr, property, out var varProperties, out var err) == OperationResult.Error 
                ? EditorStyles.helpBox.CalcSize(new GUIContent(err)).y
                : GetPropertyHeight(varProperties);
            return h;
        }
        public override void OnGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property,
            GUIContent label)
            => DoGUI(attr, ref position, property, label);
        #endregion

        #region Height
        internal static float GetPropertyHeight(VarProperties varProperties)
        {
            if (varProperties.FullHide)
                return 0f;

            var valueHeight = k_defaultLineHeight;
            if (varProperties.Attr.BreakAfterLabel)
                valueHeight += k_defaultLineHeight;

            var ctxHeight = k_defaultLineHeight;
            if (!m_showContext || ContextConditionCheck(varProperties.Attr.ShowContext, varProperties.FSMProperty) ==
                FlowResult.Stop)
                ctxHeight = 0f;

            return ctxHeight + valueHeight;
        }

        #endregion
        
        #region Utility Methods
        static T GetTarget<T>(VarProperties varProperties) where T : class
        {
            var target = varProperties.MainProperty.serializedObject.targetObject;
            if (target is Component c)
            {
                return c.TryGetComponent<T>(out var tComp) 
                    ? tComp 
                    : default;
            }
            if (!(target is IScriptableObject childAsset))
                return target as T;

            var parent = childAsset.Parent();
            return parent as T;
        }

        static FlowResult ContextConditionCheck(Condition condition, SerializedProperty prop)
        {
            switch (condition)
            {
                case Condition.Never:
                case Condition.OnlyWhenNull when prop.objectReferenceValue != null:
                    return FlowResult.Stop;
            }

            return FlowResult.Continue;
        }
        #endregion

        #region GUI
        internal static void DoGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property,
            GUIContent label)
        {
            var changes = false;

            if (GetProperties(attr, property, out var varProps, out var problems) == OperationResult.Error)
            {
                EditorGUI.HelpBox(position, problems, MessageType.Error);
                return;
            }
            changes |= (NonGUIOperations(varProps) == ChangeCheck.Changed);
            
            if (!varProps.FullHide)
            {
                using (var scope = new EditorGUI.PropertyScope(position, label, property))
                    changes |= OnGUI_PropertyScoped(ref position, scope, varProps) == ChangeCheck.Changed;
            }

            if (changes)
                varProps.MainProperty.serializedObject.ApplyModifiedProperties();
        }
        
        static ChangeCheck OnGUI_PropertyScoped(ref Rect position, EditorGUI.PropertyScope scope, VarProperties varProperties)
        {
            var brk = varProperties.Attr.BreakAfterLabel;

            var changes = false;       
            using (var vertical = new VerticalRectScope(ref position, position.height))
            {
                using (var horizontal = new HorizontalRectScope(ref vertical.Rect, k_defaultLineHeight))
                {
                    ContextFoldoutButtonGUI(varProperties, horizontal);
                    DrawLabel(scope, varProperties, horizontal);

                    // usually draw value in same line if it fits
                    var restRect = horizontal.Get(horizontal.Rect.width);
                    if (!brk && ValueGUI(restRect, varProperties) == ChangeCheck.Changed)
                        changes = true;
                }

                if (brk) // if value does not fit, next line
                    using (var horizontal = new HorizontalRectScope(ref vertical.Rect, k_defaultLineHeight))
                    {
                        // some indent
                        horizontal.Get(k_defaultIndent);
                        var rest = horizontal.Get(horizontal.Rect.width);
                        changes |= (ValueGUI(rest, varProperties) == ChangeCheck.Changed);
                    }

                using (var horizontal = new HorizontalRectScope(ref vertical.Rect, k_defaultLineHeight))
                {
                    // some indent
                    horizontal.Get(k_defaultIndent);

                    // draw context field
                    var restRect = horizontal.Get(horizontal.Rect.width);
                    changes |= SetContextGUI(restRect, varProperties) == ChangeCheck.Changed;
                }
            }

            return changes ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }

        static void ContextFoldoutButtonGUI(VarProperties varProperties, HorizontalRectScope horizontal)
        {
            var ctxMissing = varProperties.FSMProperty.objectReferenceValue == null;
            var ctxMissingWarningColor = (ctxMissing) ? Color.red : Color.white;
            using (new ColorScope(ctxMissingWarningColor))
            {
                if (ContextConditionCheck(varProperties.Attr.ShowContext, varProperties.FSMProperty) !=
                    FlowResult.Stop)
                    m_showContext = (GUI.Toggle(horizontal.Get(20f), m_showContext, "", EditorStyles.foldout));
            }
        }

        
        static void DrawLabel(EditorGUI.PropertyScope scope, VarProperties varProperties,
            HorizontalRectScope horizontal)
        {
            if (varProperties.Attr.HideLabel)
                return;

            var typeInfo = "";
            switch (varProperties.Type)
            {
                case FSMRefType.Event: typeInfo = "(E)";break;
                case FSMRefType.State: typeInfo = "(S)";break;
            }
            var label = $"{scope.content.text} {typeInfo}";
            var size = EditorStyles.label.CalcSize(new GUIContent(label));
            size.x = Mathf.Clamp(size.x + 5f, 100f, 200f);
            var labelRect = horizontal.Get(size.x);
            EditorGUI.LabelField(labelRect, label);
        }

        static ChangeCheck ValueGUI(Rect r, VarProperties varProperties)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var options = GetOptions(varProperties);
                var usePopup = !options.IsNullOrEmpty();

                if (usePopup)
                    PopupGUI(r, varProperties);
                else 
                    EditorGUI.PropertyField(r, varProperties.NameProperty, GUIContent.none);

                return check.changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
            }
        }

        static void PopupGUI(Rect position, VarProperties varProperties)
        {
            var names = GetOptions(varProperties);
            var selIdx = Array.IndexOf(names, varProperties.NameProperty.stringValue);

            if (!varProperties.FullHide)
                selIdx = EditorGUI.Popup(position, selIdx, names);

            if (selIdx != -1)
                varProperties.NameProperty.stringValue = names[selIdx];
        }

        static ChangeCheck SetContextGUI(Rect r, VarProperties varProperties)
        {
            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            if (!m_showContext || ContextConditionCheck(varProperties.Attr.ShowContext, varProperties.FSMProperty) == FlowResult.Stop) 
                return ChangeCheck.NotChanged;

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUI.PropertyField(r, varProperties.FSMContainerProp);
                return check.changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
            }
        }
        #endregion

        #region Auto-Config & Helping User
        static ChangeCheck NonGUIOperations(VarProperties varProperties)
        {
            var changes = ContextSelf(varProperties) == ChangeCheck.Changed;
            CacheOptions(varProperties);
            changes |= AutoSelectByName(varProperties) == ChangeCheck.Changed;

            return changes ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }
        static ChangeCheck ContextSelf(VarProperties varProperties)
        {
            if (ContextConditionCheck(varProperties.Attr.ContextSelf, varProperties.FSMProperty) == FlowResult.Stop)
                return ChangeCheck.NotChanged;

            var fsmObjProp = varProperties.FSMProperty;

            var prev = fsmObjProp.objectReferenceValue as IProvider<IFSMConfig>;
            var target = GetTarget<IProvider<IFSMConfig>>(varProperties);

            if (prev == target || target == null) 
                return ChangeCheck.NotChanged;

            varProperties.FSMProperty.objectReferenceValue = (Object) target;
            return ChangeCheck.Changed;
        }

        static ChangeCheck AutoSelectByName(VarProperties varProperties)
        {
            var name = varProperties.Attr.AutoSelectName;
            if (string.IsNullOrEmpty(name))
                return ChangeCheck.NotChanged;

            var options = GetOptions(varProperties);

            // todo 1: auto-select only when null setting?
            var idx = Array.IndexOf(options, name);
            if (idx == -1)
                return ChangeCheck.NotChanged;

            if (varProperties.NameProperty.stringValue == options[idx])
                return ChangeCheck.NotChanged;

            varProperties.NameProperty.stringValue = options[idx];
            return ChangeCheck.Changed;
        }

        static string[] GetOptions(VarProperties varProperties)
        {
            switch (varProperties.Type)
            {
                case FSMRefType.Event: return m_cachedEventOptions;
                case FSMRefType.State: return m_cachedStateOptions;
            }

            return null;
        }
        
        static void CacheOptions(VarProperties varProperties)
        {
            var fsmComp = varProperties.FSMProperty.objectReferenceValue as IFSMComponent;
            if (fsmComp == null)
            {
                m_cachedEventOptions = new string[0];
                m_cachedStateOptions = new string[0];
                m_lastCachedFsm = null;
                return;
            }
            
            var eventNames = fsmComp.FsmConfigs.SelectMany(fc => fc.EventNames).ToArray();
            var stateNames = fsmComp.FsmConfigs.SelectMany(fc => fc.StateNames).ToArray();

            var eventOptionCount = eventNames.Count() + 1;
            var stateOptionCount = stateNames.Count() + 1;

            var countChanges = eventOptionCount != m_cachedEventOptions.Length ||
                               stateOptionCount != m_cachedStateOptions.Length;

            if (m_lastCachedFsm == fsmComp && !countChanges)
                return;

            m_lastCachedFsm = fsmComp;

            CacheOption(eventNames, out m_cachedEventOptions);
            CacheOption(stateNames, out m_cachedStateOptions);
        }

        static void CacheOption(IReadOnlyList<string> namedVars, out string[] cachedOptions)
        {
            cachedOptions = new string[namedVars.Count + 1];
            for (var i = 0; i < namedVars.Count; i++)
                cachedOptions[i] = namedVars[i];
            cachedOptions[namedVars.Count] = Name.Ignore;
        }

        #endregion

        #region Init
        internal static OperationResult GetProperties(MultiPropertyAttribute attr, SerializedProperty property, 
            out VarProperties varProperties, out string problems)
        {
            varProperties = default;
            problems = "";

            GetAttributeValues(attr, out var data);

            var fsmContainerProp = property.FindPropertyRelative(FSMEventExt.FSM_PropName);
            var fsmProperty = fsmContainerProp?.FindPropertyRelative(InterfaceContainerBase.Editor_ObjectFieldPropName);
            var eventNameProperty = property.FindPropertyRelative(FSMEventExt.FSM_EventNamePropName);
            var stateNameProperty = property.FindPropertyRelative(FSMEventExt.FSM_StateNamePropName);

            GetType(eventNameProperty, stateNameProperty, out var type, out var nameProp);

            if (type == FSMRefType.Invalid || NULL.IsAny(fsmProperty, nameProp))
            {
                problems = ($"Not all properties have been found! On {property.propertyPath} \n" +
                            $"FOUND: fsmContainerProp {fsmContainerProp != null} fsmProperty {fsmProperty != null} \n" +
                            $"eventNameProperty {eventNameProperty != null} stateNameProperty {stateNameProperty != null}");
                return OperationResult.Error;
            }
            // for intellisense/ resharper
            Debug.Assert(fsmProperty != null);
            var fullyLinked = fsmProperty.objectReferenceValue != null 
                              && !nameProp.stringValue.IsNullOrEmpty();

            // todo: check whether state and event can be found in fsm-thingy
            var fullHide = data.ShowOnlyIfUnlinked && fullyLinked;

            varProperties = new VarProperties()
            {
                MainProperty = property,
                NameProperty = nameProp,
                FSMContainerProp = fsmContainerProp,
                FSMProperty = fsmProperty,

                Type = type,
                FullHide = fullHide,

                Attr = data
            };

            return OperationResult.OK;
        }

        static void GetAttributeValues(MultiPropertyAttribute attr, 
            out ContextDrawerAttributeData data)
        {
            data = ContextDrawerAttributeData.Default;
            if (!(attr is FSMDrawerAttribute vda))
                return;

            data = vda.Data;
        }

        static void GetType(SerializedProperty @event, SerializedProperty @state, out FSMRefType type,
            out SerializedProperty nameProp)
        {
            if (@event != null)
            {
                type = FSMRefType.Event;
                nameProp = @event;
            }
            else if (@state != null)
            {
                type = FSMRefType.State;
                nameProp = @state;
            }
            else
            {
                type = FSMRefType.Invalid;
                nameProp = null;
            }
        }
        #endregion
    }
}

//static readonly List<string> k_hideProperties = new List<string>();
//public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//{
//    //if (k_hideProperties.Contains(property.propertyPath))
//        //return;
//    var fsmContainerProp = property.FindPropertyRelative(FSMEventExt.FSM_PropName);
//    var fsmProperty = fsmContainerProp.FindPropertyRelative(InterfaceContainerBase.Editor_ObjectFieldPropName);
//    var eventNameProperty = property.FindPropertyRelative(FSMEventExt.FSM_EventNamePropName);
//    var stateNameProperty = property.FindPropertyRelative(FSMEventExt.FSM_StateNamePropName);
//    if (eventNameProperty!= null)
//        label.text += " (E)";
//    if (stateNameProperty != null)
//        label.text += " (S)";
//    position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
//    position = new Rect(position.x, position.y, position.width, k_height);
//    position = FSMReferenceGUI(position, property, fsmProperty);
//    string[] autoSelectNames;
//    if (attribute is FSMDrawerAttribute drawer && !drawer.AutoSelectName.IsNullOrEmpty())
//        autoSelectNames = new[] { drawer.AutoSelectName, property.name, property.displayName};
//    else
//        autoSelectNames = new[] { property.name, property.displayName};
//    var fsm = (IFSMComponent) fsmProperty.objectReferenceValue;
//    if (eventNameProperty != null)
//        FSMEventGUI(position, fsm, property, eventNameProperty, autoSelectNames);
//    if (stateNameProperty != null)
//        FSMStateGUI(position, fsm, property, stateNameProperty, autoSelectNames);
//}
//Rect FSMReferenceGUI(Rect position, SerializedProperty property, SerializedProperty fsmProperty)
//{
//    var component = (property.serializedObject.targetObject as Component);
//    var self = attribute is FSMDrawerAttribute drawer && drawer.Self;
//    var fsmIsOnSelf = self && component != null;
//    if (fsmIsOnSelf)
//    {
//        var fsmComponents = component.gameObject.GetComponents<IFSMComponent>();
//        if (fsmComponents.Length == 1)
//            fsmProperty.objectReferenceValue = (Object) fsmComponents[0];
//    }
//    var pos = new Rect(position.x, position.y, position.width * 0.45f, position.height);
//    EditorGUI.PropertyField(pos, fsmProperty, GUIContent.none);
//    position = new Rect(pos.x + position.width * 0.5f, pos.y, position.width * 0.45f, position.height);
//    if (fsmProperty.objectReferenceValue == null 
//        || fsmProperty.objectReferenceValue is IFSMComponent)
//        return position;
//    if (fsmProperty.objectReferenceValue is GameObject go &&
//        go.TryGetComponent(out IFSMComponent fsmComponent))
//        fsmProperty.objectReferenceValue = (Object) fsmComponent;
//    if (fsmProperty.objectReferenceValue is Component c &&
//        c.TryGetComponent(out fsmComponent))
//        fsmProperty.objectReferenceValue = (Object) fsmComponent;
//    return position;
//}
//void FSMStateGUI(Rect position, IFSMComponent fsmComponent,
//    SerializedProperty parentProperty, SerializedProperty stateNameProperty,
//    IEnumerable<string> autoSelectNames)
//{
//    //Debug.Log(AssetDatabase.GetAssetPath(parentProperty.serializedObject.targetObject));
//    var fsmConfig = fsmComponent != null
//        ? fsmComponent.Fsm
//        : AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GetAssetPath(parentProperty.serializedObject.targetObject)) as IFSMConfig;
//    if (fsmConfig == null)
//    {
//        EditorGUI.PropertyField(position, stateNameProperty, GUIContent.none);
//        return;
//    }
//    var stateOptionLength = fsmConfig.StateNames.Length + 1;
//    var needCacheStates = !m_cachedStateOptions.IsInitializedWith(stateOptionLength) || m_lastCachedFsm != fsmConfig;
//    if (needCacheStates)
//        CacheOptions(fsmConfig);
//    var disabled = AutoSelect(stateNameProperty, autoSelectNames, m_cachedStateOptions);
//    var idx = Array.IndexOf(m_cachedStateOptions, stateNameProperty.stringValue);
//    using (new EditorGUI.DisabledScope(disabled))
//    {
//        idx = EditorGUI.Popup(position, idx, m_cachedStateOptions);
//        if (!disabled && idx >= 0 && idx < m_cachedStateOptions.Length)
//            stateNameProperty.stringValue = m_cachedStateOptions[idx];
//    }
//    // we don't need the user to see or config the field, if it auto-linked itself successfully
//    var self = attribute is FSMDrawerAttribute drawer && drawer.Self;
//    //if (disabled && self)
//        //k_hideProperties.Add(parentProperty.propertyPath);
//}
//void FSMEventGUI(Rect position, IFSMComponent fsmComponent,
//    SerializedProperty parentProperty, SerializedProperty eventNameProperty,
//    IEnumerable<string> autoSelectNames)
//{
//    //Debug.Log(AssetDatabase.GetAssetPath(parentProperty.serializedObject.targetObject));
//    var fsmConfig = fsmComponent != null
//        ? fsmComponent.Fsm
//        : AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GetAssetPath(parentProperty.serializedObject.targetObject)) as IFSMConfig;
//    if (fsmConfig == null)
//    {
//        EditorGUI.PropertyField(position, eventNameProperty, GUIContent.none);
//        return;
//    }
//    var eventOptionLength = fsmConfig.EventNames.Length + 1;
//    var needCacheEvents = !m_cachedEventOptions.IsInitializedWith(eventOptionLength) || m_lastCachedFsm != fsmConfig;
//    if (needCacheEvents)
//        CacheOptions(fsmConfig);
//    var disabled = AutoSelect(eventNameProperty, autoSelectNames, m_cachedEventOptions);
//    var idx = Array.IndexOf(m_cachedEventOptions, eventNameProperty.stringValue);
//    using (new EditorGUI.DisabledScope(disabled))
//    {
//        idx = EditorGUI.Popup(position, idx, m_cachedEventOptions);
//        if (!disabled && idx >= 0 && idx < m_cachedEventOptions.Length)
//            eventNameProperty.stringValue = m_cachedEventOptions[idx];
//    }
//    // we don't need the user to see or config the field, if it auto-linked itself successfully
//    var self = attribute is FSMDrawerAttribute drawer && drawer.Self;
//    //if (disabled && self)
//       //k_hideProperties.Add(parentProperty.propertyPath);
//}
//static bool AutoSelect(SerializedProperty property, IEnumerable<string> fallBacks, string[] options)
//{
//    foreach (var fallBack in fallBacks)
//    {
//        var idx = Array.IndexOf(options, fallBack);
//        if (idx < 0 || idx >= options.Length)
//            continue;
//        property.stringValue = options[idx];
//        return true;
//    }
//    return false;
//}