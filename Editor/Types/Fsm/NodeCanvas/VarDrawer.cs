﻿#if NODE_CANVAS

using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Types.Fsm;
using NodeCanvas.Framework;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

// IngredientDrawerUIE
namespace Core.Editor.Types.Fsm.Obsolete
{
    [CustomPropertyDrawer(typeof(INodeCanvasVariableRef))]
    [CustomPropertyDrawer(typeof(BBGameObjectRef))]
    public class FSMEventDrawer : PropertyDrawer
    {
        static IBlackboard m_lastCachedBoard;

        static string[] m_cachedEventOptions;
        static string[] m_cachedStateOptions;

        static string[] m_cachedStringVarOptions;
        static string[] m_cachedIntVarOptions;
        static string[] m_cachedFloatVarOptions;
        static string[] m_cachedBoolVarOptions;
        static string[] m_cachedColorVarOptions;
        static string[] m_cachedObjectVarOptions;
        static string[] m_cachedGameObjectVarOptions;

        const float k_height = 16f;
        static readonly List<string> k_hideProperties = new List<string>();

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (k_hideProperties.Contains(property.propertyPath))
                return 0;
            
            var variableValueProperty = property.FindPropertyRelative("m_value");
            var variableNameProperty = property.FindPropertyRelative("m_variableName");
            if (variableNameProperty == null)
                return k_height;

            var add = 0f;
            if (variableValueProperty != null)
                add += EditorGUI.GetPropertyHeight(variableValueProperty);

            return (k_height * 2) + add;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (k_hideProperties.Contains(property.propertyPath))
                return;

            var blackBoardProperty = property.FindPropertyRelative("m_blackboard");
            var variableNameProperty = property.FindPropertyRelative("m_variableName");
            var variableValueProperty = property.FindPropertyRelative("m_value");

            if (variableNameProperty != null)
                label.text += " (V)";

            //EditorGUI.BeginProperty(position, label, property);
            //var initialPos = position;
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            position = new Rect(position.x, position.y, position.width, k_height);

            position = BlackboardReferenceGUI(position, property, blackBoardProperty);

            var autoSelectNames = new[] { property.name, property.displayName};

            if (variableNameProperty != null)
                FSMVariableGUI(position, blackBoardProperty.objectReferenceValue as Blackboard, property, variableNameProperty, variableValueProperty, autoSelectNames);
        }

        void FSMVariableGUI(Rect position, Blackboard blackboard,
            SerializedProperty parentProperty, SerializedProperty nameProperty, SerializedProperty valueProperty,
            IEnumerable<string> autoSelectNames)
        {
            var disabled = false;
            if (blackboard == null)
            {
                EditorGUI.PropertyField(position, nameProperty, GUIContent.none);
            }
            else
            {
                GetVarOptions(blackboard, valueProperty, out var options, out var optionLength);

                var needCacheStates = !options.IsInitializedWith(optionLength) || !ReferenceEquals(m_lastCachedBoard, blackboard);
                if (needCacheStates)
                {
                    CacheOptions(blackboard);
                    GetVarOptions(blackboard, valueProperty, out options, out _);
                }

                disabled = AutoSelect(nameProperty, autoSelectNames, options);
                var idx = Array.IndexOf(options, nameProperty.stringValue);

                using (new EditorGUI.DisabledScope(disabled))
                {
                    idx = EditorGUI.Popup(position, idx, options);
                    if (!disabled && idx >= 0 && idx < options.Length)
                        nameProperty.stringValue = options[idx];
                }
            }
            position = new Rect(position.x, position.y + k_height, position.width, position.height);

            // var drawerAttr = attribute as FSMDrawerAttribute;
            // var noOverride = drawerAttr != null && !drawerAttr.EditVariable;
            // if (noOverride)
            // {
            //     // we don't need the user to see or config the field, if it auto-linked itself successfully
            //     if (disabled && drawerAttr.Self)
            //         k_hideProperties.Add(parentProperty.propertyPath);
            //
            //     return;
            // }

            position.height = EditorGUI.GetPropertyHeight(valueProperty);
            if (position.height > k_height + float.Epsilon)
            {
                position.x = 0;
                position.width *= 2;
            }
            EditorGUI.PropertyField(position, valueProperty, GUIContent.none, true);
        }

        Rect BlackboardReferenceGUI(Rect position, SerializedProperty property, SerializedProperty fsmProperty)
        {
            var component = (property.serializedObject.targetObject as Component);

            // var self = attribute is FSMDrawerAttribute drawer && drawer.Self;
            // var fsmIsOnSelf = self && component != null;
            Blackboard blackboard = null;
            //
            // if (fsmIsOnSelf)
            // {
            Blackboard[] blackboards = component.gameObject.GetComponents<Blackboard>();
            if (blackboards.Length == 1)
                blackboard = blackboards[0];
            // }

            if (blackboard != null)
                fsmProperty.objectReferenceValue = blackboard;
            else
            {
                var pos = new Rect(position.x, position.y, position.width * 0.45f, position.height);
                EditorGUI.PropertyField(pos, fsmProperty, GUIContent.none);
                position = new Rect(pos.x + position.width * 0.5f, pos.y, position.width * 0.45f, position.height);
            }

            return position;
        }

        static void GetVarOptions(IBlackboard blackboard, SerializedProperty valueProperty, out string[] options, out int optionLength)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (valueProperty.propertyType)
            {
                case SerializedPropertyType.Boolean:
                    options = m_cachedBoolVarOptions;
                    optionLength = blackboard.variables.Values.Count(v=>v.varType == typeof(bool)) + 1;
                    break;
                case SerializedPropertyType.Color:
                    options = m_cachedColorVarOptions;
                    optionLength = blackboard.variables.Values.Count(v=>v.varType == typeof(Color)) + 1;
                    break;
                case SerializedPropertyType.Float:
                    options = m_cachedFloatVarOptions;
                    optionLength = blackboard.variables.Values.Count(v=>v.varType == typeof(float)) + 1;
                    break;
                case SerializedPropertyType.Integer:
                    options = m_cachedIntVarOptions;
                    optionLength = blackboard.variables.Values.Count(v=>v.varType == typeof(int)) + 1;
                    break;
                //currently only GameObject:
                case SerializedPropertyType.ObjectReference:
                    options = m_cachedGameObjectVarOptions;
                    optionLength = blackboard.variables.Values.Count(v=>v.varType == typeof(GameObject)) + 1;
                    break;
                case SerializedPropertyType.String:
                    options = m_cachedStringVarOptions;
                    optionLength = blackboard.variables.Values.Count(v=>v.varType == typeof(string)) + 1;
                    break;
                default:
                    options = m_cachedObjectVarOptions;
                    optionLength = blackboard.variables.Values.Count(v=>v.varType == typeof(UnityEngine.Object)) + 1;
                    break;
            }
        }
        
        static void CacheOptions(IBlackboard blackboard)
        {
            k_hideProperties.Clear();

            m_lastCachedBoard = blackboard;

            var vars= blackboard.variables.Values;
            CacheOption(vars.Where(v=>v.varType == typeof(string)).ToArray(), out m_cachedStringVarOptions);
            CacheOption(vars.Where(v=>v.varType == typeof(int)).ToArray(), out m_cachedIntVarOptions);
            CacheOption(vars.Where(v=>v.varType == typeof(float)).ToArray(), out m_cachedFloatVarOptions);
            CacheOption(vars.Where(v=>v.varType == typeof(bool)).ToArray(), out m_cachedBoolVarOptions);
            CacheOption(vars.Where(v=>v.varType == typeof(Color)).ToArray(), out m_cachedColorVarOptions);
            CacheOption(vars.Where(v=>v.varType == typeof(Object)).ToArray(), out m_cachedObjectVarOptions);
            CacheOption(vars.Where(v=>v.varType == typeof(GameObject)).ToArray(), out m_cachedGameObjectVarOptions);
        }

        static void CacheOption(IReadOnlyList<Variable> namedVars, out string[] cachedOptions)
        {
            cachedOptions = new string[namedVars.Count + 1];
            for (var i = 0; i < namedVars.Count; i++)
                cachedOptions[i] = namedVars[i].name;
            cachedOptions[namedVars.Count] = Name.Ignore;
        }

        static bool AutoSelect(SerializedProperty property, IEnumerable<string> fallBacks, string[] options)
        {
            foreach (var fallBack in fallBacks)
            {
                var idx = Array.IndexOf(options, fallBack);
                if (idx < 0 || idx >= options.Length)
                    continue;

                property.stringValue = options[idx];
                return true;
            }

            return false;
        }
    }
}

#endif