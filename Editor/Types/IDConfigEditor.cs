﻿using Core.Unity.Extensions;
using Core.Unity.Types.ID;
using System;
using System.Collections.Generic;
using System.IO;
using Core.Editor.Inspector;
using Core.Editor.Tools;
using UnityEditor;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;
using UEditor = UnityEditor.Editor;

namespace Core.Editor.Types
{

    [CustomEditor(typeof(IDConfig), true)]
    public class IDConfigInspector : BaseInspector<IDConfigEditor>{}
    
    public class IDConfigEditor : BaseEditor<IDConfig>
    {
        IDConfig m_lastTarget;

        // todo 1: updating after rename does not work 
        //IDAsset m_lastModified;
        public override void OnGUI(float width)
        {
            if (m_lastTarget != Target)
                InitTarget();

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var typeProp = SerializedObject.FindProperty(nameof(IDConfig.IDType));
                var folderProp = SerializedObject.FindProperty(IDConfig.Editor_FolderPropName);
                var nameSpaceStartProp = SerializedObject.FindProperty(IDConfig.Editor_NamespaceStartPropName);
                var nameSpaceOverrideProp = SerializedObject.FindProperty(IDConfig.Editor_NamespaceOverridePropName);

                EditorGUILayout.PropertyField(typeProp);
                EditorGUILayout.PropertyField(folderProp);
                EditorGUILayout.PropertyField(nameSpaceStartProp);
                EditorGUILayout.PropertyField(nameSpaceOverrideProp);

                if (check.changed)
                    SerializedObject.ApplyModifiedProperties();
            }
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var deleteIdx = -1;
                for (var i = 0; i < Target.Ids.Length; i++)
                    ID_GUI(i, ref deleteIdx);

                DeleteVariable(deleteIdx);
                if (GUILayout.Button("Add ID"))
                    AddID();

                if (check.changed)
                    UpdateChanges();
            }

            GUILayout.Label($"OutputFile: {Target.OutputFile}");
            GUILayout.Label($"NameSpaceName: {Target.NameSpace}");

            using (new EditorGUI.DisabledGroupScope(Target.NameSpace.IsNullOrEmpty()))
                if (GUILayout.Button("Generate Enum")) 
                    EnumGeneratorConfigEditor.GenerateEnum(Target);
        }

        void UpdateChanges()
        {
            SerializedObject.Update();
            //if (m_lastModified!= null) 
            //    EditorUtility.SetDirty(m_lastModified);

            EditorUtility.SetDirty(Target);
            AssetDatabase.SaveAssets();
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(Target));
            //if (m_lastModified!= null)
            //    AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(m_lastModified));
            //m_lastModified = null;
        }

        void ID_GUI(int i, ref int deleteIdx)
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("X", GUILayout.Width(20)))
                    deleteIdx = i;
                //using (var check = new EditorGUI.ChangeCheckScope())
                //{
                Target.Ids[i].name = EditorGUILayout.DelayedTextField(Target.Ids[i].name);
                //    if (check.changed)
                //        m_lastModified = target.Ids[i];
                //}
            }
        }

        void DeleteVariable(int deleteIdx)
        {
            if (deleteIdx == -1)
                return;

            Target.Ids[deleteIdx].DestroyEx();

            RemoveAt(ref Target.Ids, deleteIdx);
        }

        void InitTarget()
        {
            m_lastTarget = Target;

            var childAssets = new List<IDAsset>();
            if (Target == null)
                return;

            var objs = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(Target));
            foreach (var o in objs)
            {
                if (o == null || AssetDatabase.IsMainAsset(o))
                    continue;
                if (!(o is IDAsset idAsset))
                    continue;
                childAssets.Add(idAsset);
            }

            childAssets.Sort((a, b) => string.Compare(a.name, b.name, StringComparison.Ordinal));
            Target.Ids = childAssets.ToArray();
        }

        void AddID()
        {
            var newID = ScriptableObject.CreateInstance(Target.IDType.Type) as IDAsset;
            //if (AssetDatabase.IsSubAsset(target))
            //    newVariable.hideFlags = HideFlags.HideInInspector | HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(newID, Target);
            Add(ref Target.Ids, newID);

            //UpdateChanges();
        }
    }
}
