﻿using Core.Editor.Inspector;
using Core.Unity.Types;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Types
{
    [CustomEditor(typeof(PrefabRef))]
    public class PrefabRefInspector : BaseInspector<PrefabRefEditor>
    {
    }

    public class PrefabRefEditor : BaseEditor<PrefabRef>
    {
        GameObject m_prefab;
        public override void Init(object parentContainer)
        {
            if (Target == null)
                return;
            base.Init(parentContainer);
            m_prefab = Target.Prefab;
        }

        public override void OnGUI(float width)
        {
            base.OnGUI(width);

            if (m_prefab != Target.Prefab)
            {
                m_prefab = Target.Prefab;
                Target.Editor_RemovePreview();
                Target.Editor_SpawnPreview();
            }

            if (Target.transform.childCount <= 0)
            {
                if (!GUILayout.Button("Spawn Preview"))
                    return;

                Target.Editor_SpawnPreview();
            }
            else if (GUILayout.Button("Remove Preview")) 
                Target.Editor_RemovePreview();
        }
    }
}