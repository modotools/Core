﻿using System;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Types.Attribute;
using Core.Unity.Utility.GUITools;
using JetBrains.Annotations;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

using static Core.Utils;
using static Core.Unity.Types.Attribute.ContextDrawerAttribute;
using MessageType = UnityEditor.MessageType;

// IngredientDrawerUIE
namespace Core.Editor.types
{
    [CustomPropertyDrawer(typeof(AnimatorStateRef))]
    [CustomPropertyDrawer(typeof(AnimatorTriggerRef))]
    [CustomPropertyDrawer(typeof(AnimatorBoolRef))]
    [CustomPropertyDrawer(typeof(AnimatorFloatRef))]
    public class AnimTypeFallbackDrawer : PropertyDrawer
    {
        AnimTypeDrawer.VarProperties m_current;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return AnimTypeDrawer.GetProperties(null, property, out m_current, out var err) ==
                   OperationResult.Error
                ? EditorStyles.helpBox.CalcSize(new GUIContent(err)).y
                : AnimTypeDrawer.GetPropertyHeight(m_current);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            => AnimTypeDrawer.DoGUI(null, ref position, property, label);
    }

    [UsedImplicitly]
    public class AnimTypeDrawer : MultiPropertyEditorDrawer
    {
        public override Type PreviewForAttribute => typeof(AnimTypeDrawerAttribute);

        internal struct VarProperties
        {
            internal SerializedProperty MainProperty;
            internal SerializedProperty AnimatorProperty;
            internal SerializedProperty ControllerProperty;

            internal SerializedProperty NameProperty;

            internal SerializedProperty VarValueProperty;

            internal AnimRefType Type;
            internal bool FullHide;

            internal bool HideAnimator;

            internal ContextDrawerAttributeData Attr;
        }
        internal enum AnimRefType
        {
            State,
            Parameter,
            //Variable, treat Variable == Parameter
            Invalid
        }

        static AnimatorController m_lastCached;

        static string[] m_cachedStateOptions;

        static string[] m_cachedIntVarOptions;
        static string[] m_cachedFloatVarOptions;
        static string[] m_cachedBoolVarOptions;
        static string[] m_cachedTriggerVarOptions;

        const float k_defaultLineHeight = 18f;
        const float k_defaultIndent = 20f;

        static bool m_showContext;

        #region Public Overrides
        public override float GetPropertyHeight(MultiPropertyAttribute attr, SerializedProperty property, GUIContent label, float height)
        {
            var h = GetProperties(attr, property, out var varProperties, out var err) == OperationResult.Error
                ? EditorStyles.helpBox.CalcSize(new GUIContent(err)).y
                : GetPropertyHeight(varProperties);
            return h;
        }
        public override void OnGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property,
            GUIContent label)
            => DoGUI(attr, ref position, property, label);
        #endregion

        #region Height
        internal static float GetPropertyHeight(VarProperties varProperties)
        {
            if (varProperties.FullHide)
                return 0f;

            var valueHeight = k_defaultLineHeight;
            if (varProperties.Attr.BreakAfterLabel)
                valueHeight += k_defaultLineHeight;

            // both context fields in same line?
            var ctxHeight = k_defaultLineHeight; //NoContext(varProperties)?2*k_defaultLineHeight:k_defaultLineHeight;
            if (!m_showContext || ContextConditionCheck(varProperties.Attr.ShowContext, varProperties) ==
                FlowResult.Stop)
                ctxHeight = 0f;

            return ctxHeight + valueHeight;
        }
        #endregion

        #region Utility Methods
        static void GetTarget(VarProperties varProperties, out Animator anim, out IProvider<RuntimeAnimatorController> ctrlProvider)
        {
            anim = null;
            object target = varProperties.MainProperty.serializedObject.targetObject;
            switch (target)
            {
                case Component c when c.TryGetComponent(out anim):
                    ctrlProvider = new AnimatorAsControllerProvider() { Animator = anim };
                    return;
                case Component c when c.TryGetComponent(out ctrlProvider):
                    return;
                case IScriptableObject childAsset:
                    target = childAsset.Parent();
                    break;
            }

            ctrlProvider = target as IProvider<RuntimeAnimatorController>;
        }
        static AnimatorController GetController(VarProperties varProperties)
        {
            var animatorProperty = varProperties.AnimatorProperty;
            var controllerProperty = varProperties.ControllerProperty;

            AnimatorController controller = null;
            if (animatorProperty != null)
            {
                var animator = (animatorProperty.objectReferenceValue as Animator);
                if (animator != null)
                    controller = animator.runtimeAnimatorController as AnimatorController;
            }

            if (controllerProperty != null && controller == null)
                controller = controllerProperty.objectReferenceValue as AnimatorController;
            return controller;
        }
        static bool NoContext(VarProperties varProperties) =>
            varProperties.AnimatorProperty.objectReferenceValue == null &&
            varProperties.ControllerProperty.objectReferenceValue == null;
        static FlowResult ContextConditionCheck(Condition condition, VarProperties varProperties)
        {
            switch (condition)
            {
                case Condition.Never:
                case Condition.OnlyWhenNull when NoContext(varProperties):
                    return FlowResult.Stop;
            }

            return FlowResult.Continue;
        }
        #endregion

        #region GUI
        public static void DoGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property,
            GUIContent label)
        {
            var changes = false;

            if (GetProperties(attr, property, out var varProps, out var problems) == OperationResult.Error)
            {
                EditorGUI.HelpBox(position, problems, MessageType.Error);
                return;
            }
            changes |= (NonGUIOperations(varProps) == ChangeCheck.Changed);

            if (!varProps.FullHide)
            {
                using (var scope = new EditorGUI.PropertyScope(position, label, property))
                    changes |= OnGUI_PropertyScoped(ref position, scope, varProps) == ChangeCheck.Changed;
            }

            if (changes)
                varProps.MainProperty.serializedObject.ApplyModifiedProperties();
        }

        static ChangeCheck OnGUI_PropertyScoped(ref Rect position, EditorGUI.PropertyScope scope, VarProperties varProperties)
        {
            var brk = varProperties.Attr.BreakAfterLabel;

            var changes = false;
            using (var vertical = new VerticalRectScope(ref position, position.height))
            {
                using (var horizontal = new HorizontalRectScope(ref vertical.Rect, k_defaultLineHeight))
                {
                    ContextFoldoutButtonGUI(varProperties, horizontal);
                    DrawLabel(scope, varProperties, horizontal);

                    // usually draw value in same line if it fits
                    var restRect = horizontal.Get(horizontal.Rect.width);
                    if (!brk && ValueGUI(restRect, varProperties) == ChangeCheck.Changed)
                        changes = true;
                }

                if (brk) // if value does not fit, next line
                    using (var horizontal = new HorizontalRectScope(ref vertical.Rect, k_defaultLineHeight))
                    {
                        // some indent
                        horizontal.Get(k_defaultIndent);
                        var rest = horizontal.Get(horizontal.Rect.width);
                        changes |= (ValueGUI(rest, varProperties) == ChangeCheck.Changed);
                    }

                using (var horizontal = new HorizontalRectScope(ref vertical.Rect, k_defaultLineHeight))
                {
                    // some indent
                    horizontal.Get(k_defaultIndent);

                    // draw context field
                    var restRect = horizontal.Get(horizontal.Rect.width);
                    changes |= SetContextGUI(restRect, varProperties) == ChangeCheck.Changed;
                }
            }

            return changes ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }

        static void ContextFoldoutButtonGUI(VarProperties varProperties, HorizontalRectScope horizontal)
        {
            var ctxMissingWarningColor = (NoContext(varProperties)) ? Color.red : Color.white;
            using (new ColorScope(ctxMissingWarningColor))
            {
                if (ContextConditionCheck(varProperties.Attr.ShowContext, varProperties) != FlowResult.Stop)
                    m_showContext = (GUI.Toggle(horizontal.Get(20f), m_showContext, "", EditorStyles.foldout));
            }
        }

        static void DrawLabel(EditorGUI.PropertyScope scope, VarProperties varProperties,
            HorizontalRectScope horizontal)
        {
            if (varProperties.Attr.HideLabel)
                return;

            var typeInfo = "";
            switch (varProperties.Type)
            {
                case AnimRefType.Parameter: typeInfo = "(P)"; break;
                case AnimRefType.State: typeInfo = "(S)"; break;
            }

            var label = $"{scope.content.text} {typeInfo}";
            var size = EditorStyles.label.CalcSize(new GUIContent(label));
            size.x = Mathf.Clamp(size.x + 5f, 100f, 200f);
            var labelRect = horizontal.Get(size.x);
            EditorGUI.LabelField(labelRect, label);
        }

        static ChangeCheck ValueGUI(Rect r, VarProperties varProperties)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var options = GetOptions(varProperties);
                var usePopup = !options.IsNullOrEmpty();

                if (usePopup)
                    PopupGUI(r, varProperties);
                else
                    EditorGUI.PropertyField(r, varProperties.NameProperty, GUIContent.none);

                return check.changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
            }
        }

        static void PopupGUI(Rect position, VarProperties varProperties)
        {
            var names = GetOptions(varProperties);
            var selIdx = Array.IndexOf(names, varProperties.NameProperty.stringValue);

            if (!varProperties.FullHide)
                selIdx = EditorGUI.Popup(position, selIdx, names);

            if (selIdx != -1)
                varProperties.NameProperty.stringValue = names[selIdx];
        }

        static string[] GetOptions(VarProperties varProperties)
        {
            switch (varProperties.Type)
            {
                case AnimRefType.State: return m_cachedStateOptions;
                case AnimRefType.Parameter: return GetParamOptions(varProperties);
            }

            return null;
        }

        static string[] GetParamOptions(VarProperties varProperties)
        {
            if (varProperties.VarValueProperty == null)
                return m_cachedTriggerVarOptions;
            
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (varProperties.VarValueProperty.propertyType)
            {
                case SerializedPropertyType.Boolean: return m_cachedBoolVarOptions;
                case SerializedPropertyType.Float: return m_cachedFloatVarOptions; 
                case SerializedPropertyType.Integer: return m_cachedIntVarOptions;
            }

            return null;
        }

        static ChangeCheck SetContextGUI(Rect r, VarProperties varProperties)
        {
            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            if (!m_showContext || ContextConditionCheck(varProperties.Attr.ShowContext, varProperties) == FlowResult.Stop)
                return ChangeCheck.NotChanged;

            using (var horizontal = new HorizontalRectScope(ref r, k_defaultLineHeight))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var animNull = varProperties.AnimatorProperty.objectReferenceValue == null;
                var controllerNull = varProperties.ControllerProperty.objectReferenceValue == null;
                var drawAnimatorProp = (controllerNull || !animNull) && !varProperties.HideAnimator;
                var drawControllerProp = animNull;
                var drawBoth = drawAnimatorProp && drawControllerProp;
                var size = drawBoth ? 0.5f * r.width : r.width;

                if (drawAnimatorProp)
                    EditorGUI.PropertyField(horizontal.Get(size), varProperties.AnimatorProperty, GUIContent.none);
                if (drawControllerProp)
                    EditorGUI.PropertyField(horizontal.Get(size), varProperties.ControllerProperty, GUIContent.none);

                return check.changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
            }
        }
        #endregion

        #region Auto-Config & Helping User
        static ChangeCheck NonGUIOperations(VarProperties varProperties)
        {
            var changes = ContextSelf(varProperties) == ChangeCheck.Changed;
            CacheOptions(varProperties);
            changes |= AutoSelectByName(varProperties) == ChangeCheck.Changed;

            return changes ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }
        static ChangeCheck ContextSelf(VarProperties varProperties)
        {
            if (ContextConditionCheck(varProperties.Attr.ContextSelf, varProperties) == FlowResult.Stop)
                return ChangeCheck.NotChanged;

            var animObjProp = varProperties.AnimatorProperty;
            var ctrlObjProp = varProperties.ControllerProperty;

            var prevAnim = animObjProp.objectReferenceValue as Animator;
            var prevCtrl = ctrlObjProp.objectReferenceValue as AnimatorController;

            GetTarget(varProperties, out var animator, out var ctrlProvider);

            RuntimeAnimatorController rCtrl = null;
            ctrlProvider?.Get(out rCtrl);

            var ctrl = rCtrl as AnimatorController;

            var nothingChanged = animator == prevAnim && ctrl == prevCtrl;
            var nothingFound = ctrl == null && animator == null;

            if (nothingChanged || nothingFound)
                return ChangeCheck.NotChanged;

            varProperties.AnimatorProperty.objectReferenceValue = animator;
            varProperties.ControllerProperty.objectReferenceValue = ctrl;
            return ChangeCheck.Changed;
        }
        static ChangeCheck AutoSelectByName(VarProperties varProperties)
        {
            var name = varProperties.Attr.AutoSelectName;
            if (string.IsNullOrEmpty(name))
                return ChangeCheck.NotChanged;

            var options = GetOptions(varProperties);
            if (options == null)
                return ChangeCheck.NotChanged;

            // todo 1: auto-select only when null setting?
            var idx = Array.IndexOf(options, name);
            if (idx == -1)
                return ChangeCheck.NotChanged;

            if (varProperties.NameProperty.stringValue == options[idx])
                return ChangeCheck.NotChanged;

            varProperties.NameProperty.stringValue = options[idx];
            return ChangeCheck.Changed;
        }

        static void CacheOptions(VarProperties varProperties)
        {
            var ctrl = GetController(varProperties);
            if (m_lastCached == ctrl)
                return;

            ClearCached();

            m_lastCached = ctrl;
            if (ctrl == null)
                return;

            ctrl.GetAllStateNames(out m_cachedStateOptions);

            CacheVariablesOption(ctrl.parameters, AnimatorControllerParameterType.Bool, out m_cachedBoolVarOptions);
            CacheVariablesOption(ctrl.parameters, AnimatorControllerParameterType.Int, out m_cachedIntVarOptions);
            CacheVariablesOption(ctrl.parameters, AnimatorControllerParameterType.Float, out m_cachedFloatVarOptions);
            CacheVariablesOption(ctrl.parameters, AnimatorControllerParameterType.Trigger, out m_cachedTriggerVarOptions);
        }

        static void ClearCached()
        {
            m_cachedStateOptions = null;
            m_cachedBoolVarOptions = null;
            m_cachedIntVarOptions = null;
            m_cachedFloatVarOptions = null;
            m_cachedTriggerVarOptions = null;
        }

        static void CacheVariablesOption(AnimatorControllerParameter[] parameters, AnimatorControllerParameterType type, out string[] cachedOptions)
        {
            var found = Array.FindAll(parameters, a => a.type == type);
            var length = found.Length;
            cachedOptions = new string[length + 1];
            for (var i = 0; i < found.Length; i++)
                cachedOptions[i] = found[i].name;
            cachedOptions[found.Length] = Name.Ignore;
        }
        #endregion

        internal static OperationResult GetProperties(MultiPropertyAttribute attr, SerializedProperty property,
            out VarProperties varProperties, out string problems)
        {
            varProperties = default;
            problems = "";

            GetAttributeValues(attr, out var data, out var hideAnimator);

            var animatorProperty = property.FindPropertyRelative(AnimatorTypePropNames.AnimatorPropName);
            var controllerProperty = property.FindPropertyRelative(AnimatorTypePropNames.ControllerPropName);
            var stateNameProperty = property.FindPropertyRelative(AnimatorTypePropNames.StatePropName);
            var parameterNameProperty = property.FindPropertyRelative(AnimatorTypePropNames.ParameterPropName);
            var variableValueProperty = property.FindPropertyRelative(AnimatorTypePropNames.ValuePropName);

            GetType(stateNameProperty, parameterNameProperty, out var type, out var nameProp);

            if (type == AnimRefType.Invalid || NULL.IsAny(animatorProperty, controllerProperty))
            {
                problems = ($"Not all properties have been found! On {property.propertyPath} \n" +
                            $"FOUND: animatorProperty {animatorProperty != null} controllerProperty {controllerProperty != null} \n" +
                            $"parameterNameProperty {parameterNameProperty != null} stateNameProperty {stateNameProperty != null}");
                return OperationResult.Error;
            }

            // for intellisense/ resharper
            Debug.Assert(animatorProperty != null && controllerProperty != null);

            var controllerProviderLinked = (animatorProperty.objectReferenceValue != null ||
                                            controllerProperty.objectReferenceValue != null);
            var fullyLinked = controllerProviderLinked && !nameProp.stringValue.IsNullOrEmpty();

            // todo: check whether state or parameter can be found in controller
            var fullHide = data.ShowOnlyIfUnlinked && fullyLinked;

            varProperties = new VarProperties()
            {
                MainProperty = property,
                NameProperty = nameProp,
                AnimatorProperty = animatorProperty,
                ControllerProperty = controllerProperty,
                VarValueProperty = variableValueProperty,

                Type = type,
                FullHide = fullHide,
                HideAnimator = hideAnimator,

                Attr = data
            };

            return OperationResult.OK;
        }

        static void GetAttributeValues(MultiPropertyAttribute attr,
            out ContextDrawerAttributeData data, out bool hideAnimator)
        {
            data = ContextDrawerAttributeData.Default;
            hideAnimator = false;

            if (!(attr is AnimTypeDrawerAttribute vda))
                return;

            data = vda.Data;
            hideAnimator = vda.HideAnimator;
        }

        static void GetType(SerializedProperty @state, SerializedProperty @param, out AnimRefType type,
            out SerializedProperty nameProp)
        {
            if (@state != null)
            {
                type = AnimRefType.State;
                nameProp = @state;
            }
            else if (@param != null)
            {
                type = AnimRefType.Parameter;
                nameProp = @param;
            }
            else
            {
                type = AnimRefType.Invalid;
                nameProp = null;
            }
        }
    }
}

        //public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        //{
        //    //if (k_hideProperties.Contains(property.propertyPath))
        //    //    return;
        //    var attr = attribute as AnimTypeDrawerAttribute;
        //    var indent = EditorGUI.indentLevel;
        //    EditorGUI.indentLevel = 0;
        //    GetProperties(property, out var animatorProperty, out var controllerProperty, out var stateNameProperty,
        //            out var parameterNameProperty, out var variableValueProperty);
        //    position = Label(position, property, label, stateNameProperty, parameterNameProperty, attr);
        //    position = new Rect(position.x, position.y, position.width, k_height);
        //    position = AnimReferenceGUI(position, property, animatorProperty, controllerProperty);
        //    var autoSelectNames = AutoSelectNames(property, attr);
        //    var controller = GetController(animatorProperty, controllerProperty);
        //    if (stateNameProperty != null)
        //        AnimStateGUI(position, controller, stateNameProperty, autoSelectNames);
        //    if (parameterNameProperty != null)
        //        AnimVariableGUI(position, controller, parameterNameProperty, variableValueProperty, autoSelectNames);
        //    EditorGUI.indentLevel = indent;
        //}
        //static string[] AutoSelectNames(SerializedProperty property, AnimTypeDrawerAttribute attr)
        //{
        //    string[] autoSelectNames;
        //    if (attr != null && !attr.AutoSelectName.IsNullOrEmpty())
        //        autoSelectNames = new[] { attr.AutoSelectName, property.name, property.displayName };
        //    else
        //        autoSelectNames = new[] { property.name, property.displayName };
        //    return autoSelectNames;
        //}
        //static void AnimVariableGUI(Rect position, VarProperties varProperties)
        //{
        //    var fallBack = AutoSelectNames(varProperties.MainProperty, varProperties.Attr);
        //    var controller = GetController(varProperties);
        //    string[] options = null;
        //    if (controller == null)
        //        EditorGUI.PropertyField(position, varProperties.NameProperty, GUIContent.none);
        //    else
        //    {
        //        GetVarOptions(controller, varProperties.VarValueProperty, ref options, out var optionLength);
        //        var needCacheStates = !options.IsInitializedWith(optionLength) || m_lastCached != controller;
        //        if (needCacheStates)
        //        {
        //            CacheOptions(controller);
        //            GetVarOptions(controller, varProperties.VarValueProperty, ref options, out _);
        //        }
        //        var idx = Array.IndexOf(options, varProperties.NameProperty.stringValue);
        //        idx = EditorGUI.Popup(position, idx, options);
        //        if (idx >= 0 && idx < options.Length)
        //            varProperties.NameProperty.stringValue = options[idx];
        //        else
        //            Fallback(varProperties.NameProperty, fallBack, options);
        //    }
        //    //position = new Rect(position.x, position.y + k_height, position.width, position.height);
        //    // var noOverride = attribute is AnimTypeDrawerAttribute drawer && !drawer.ValueOverride;
        //    // if (noOverride)
        //    //    return;
        //    //if (valueProperty == null)
        //    //    return;
        //    //EditorGUI.PropertyField(position, valueProperty, GUIContent.none);
        //}
        //Rect AnimReferenceGUI(Rect position, SerializedProperty property,
        //    SerializedProperty animatorProperty,
        //    SerializedProperty controllerProperty)
        //{
        //    var drawer = attribute as AnimTypeDrawerAttribute;
        //    var mono = (property.serializedObject.targetObject as MonoBehaviour);
        //    var self = drawer != null && drawer.Self;
        //    if (drawer != null && drawer.HideAnimatorField)
        //        return position;
        //    Animator animator;
        //    var animatorIsOnSelf = self && mono != null;
        //    if (animatorIsOnSelf && (animator = mono.gameObject.GetComponent<Animator>()))
        //    {
        //        if (animatorProperty != null)
        //            animatorProperty.objectReferenceValue = animator;
        //        if (controllerProperty != null)
        //            controllerProperty.objectReferenceValue = animator.runtimeAnimatorController;
        //    }
        //    else
        //    {
        //        var pos = new Rect(position.x, position.y, position.width * 0.45f, position.height);
        //        var animNull = animatorProperty == null || animatorProperty.objectReferenceValue == null;
        //        var controllerNull = controllerProperty == null || controllerProperty.objectReferenceValue == null;
        //        var drawAnimatorProp = (animatorProperty != null && controllerNull) || !animNull;
        //        var drawControllerProp = (controllerProperty != null && animNull);
        //        if (drawAnimatorProp)
        //        {
        //            EditorGUI.PropertyField(pos, animatorProperty, GUIContent.none);
        //            if (drawControllerProp)
        //                pos.y += k_defaultLineHeight;
        //        }
        //        if (drawControllerProp)
        //            EditorGUI.PropertyField(pos, controllerProperty, GUIContent.none);
        //        position = new Rect(pos.x + position.width * 0.5f, pos.y, position.width * 0.45f, position.height);
        //    }
        //    return position;
        //}
        //static void GetVarOptions(AnimatorController controller, SerializedProperty valueProperty, ref string[] options, out int optionLength)
        //{
        //    optionLength = 0;
        //    if (controller == null)
        //        return;
        //    if (valueProperty == null)
        //    {
        //        options = m_cachedTriggerVarOptions;
        //        optionLength = controller.parameters.Count(a => a.type == AnimatorControllerParameterType.Trigger) + 1;
        //        return;
        //    }
        //    // ReSharper disable once SwitchStatementMissingSomeCases
        //    switch (valueProperty.propertyType)
        //    {
        //        case SerializedPropertyType.Boolean:
        //            options = m_cachedBoolVarOptions;
        //            optionLength = controller.parameters.Count(a => a.type == AnimatorControllerParameterType.Bool) + 1;
        //            break;
        //        case SerializedPropertyType.Float:
        //            options = m_cachedFloatVarOptions;
        //            optionLength = controller.parameters.Count(a => a.type == AnimatorControllerParameterType.Float) + 1;
        //            break;
        //        case SerializedPropertyType.Integer:
        //            options = m_cachedIntVarOptions;
        //            optionLength = controller.parameters.Count(a => a.type == AnimatorControllerParameterType.Int) + 1;
        //            break;
        //    }
        //}
        //static void AnimStateGUI(Rect position, VarProperties varProperties)
        //{
        //    var fallBack = AutoSelectNames(varProperties.MainProperty, varProperties.Attr);
        //    var controller = GetController(varProperties);
        //    if (controller == null)
        //        EditorGUI.PropertyField(position, varProperties.NameProperty, GUIContent.none);
        //    else
        //    {
        //        var stateOptionLength = controller.CountAllStates();
        //        var needCacheStates = !m_cachedStateOptions.IsInitializedWith(stateOptionLength) || m_lastCached != controller;
        //        if (needCacheStates)
        //            CacheOptions(controller);
        //        var idx = Array.IndexOf(m_cachedStateOptions, varProperties.NameProperty.stringValue);
        //        idx = EditorGUI.Popup(position, idx, m_cachedStateOptions);
        //        if (idx >= 0 && idx < m_cachedStateOptions.Length)
        //            varProperties.NameProperty.stringValue = m_cachedStateOptions[idx];
        //        else
        //            Fallback(varProperties.NameProperty, fallBack, m_cachedStateOptions);
        //    }
        //}
        //static void Fallback(SerializedProperty property, string[] fallBacks, string[] options)
        //{
        //    foreach (var fallBack in fallBacks)
        //    {
        //        var idx = Array.IndexOf(options, fallBack);
        //        if (idx < 0 || idx >= options.Length)
        //            continue;
        //        property.stringValue = options[idx];
        //        break;
        //    }
        //}