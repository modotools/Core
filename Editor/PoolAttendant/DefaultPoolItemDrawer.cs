﻿using Core.Unity.Utility.GUITools;
using Core.Unity.Utility.PoolAttendant;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.PoolAttendant
{
    [CustomPropertyDrawer(typeof(DefaultPoolItem))]
    public class DefaultPoolItemDrawer : PropertyDrawer
    {
        SerializedProperty m_prefab;
        SerializedProperty m_size;
        SerializedProperty m_delay;
        SerializedProperty m_explicit;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
        }

        public override void OnGUI(Rect pos, SerializedProperty property, GUIContent label)
        {
            m_prefab = property.FindPropertyRelative(nameof(DefaultPoolItem.Prefab));
            m_size = property.FindPropertyRelative(nameof(DefaultPoolItem.Size));
            m_delay = property.FindPropertyRelative(nameof(DefaultPoolItem.DelayInactivateForFrames));
            m_explicit = property.FindPropertyRelative(nameof(DefaultPoolItem.ExplicitlyManaged));

            EditorGUI.BeginProperty(pos, label, property);

            using (var hs = new HorizontalRectScope(ref pos, pos.height))
            {
                EditorGUI.PropertyField(hs.Get(40f), m_size, GUIContent.none);

                m_prefab.objectReferenceValue = EditorGUI.ObjectField(hs.Get(hs.Rect.width - 85f), "",
                    m_prefab.objectReferenceValue, typeof(GameObject), false);
                //var space1 = hs.Get(2f);
                EditorGUI.PropertyField(hs.Get(40f), m_delay, GUIContent.none);
                var space2 = hs.Get(5f);
                EditorGUI.PropertyField(hs.Get(40f), m_explicit, GUIContent.none);
            }

            EditorGUI.EndProperty();
        }
    }
}