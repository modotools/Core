﻿using Core.Unity.Utility.PoolAttendant;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.PoolAttendant
{
    public static class CreatePoolSettings
    {
#if CORE_ED_MENU_CREATE_POOL
        [MenuItem(MenuStrings.CreatePoolSettings)]
#endif
        public static void Create()
        {
            var settings = Resources.Load<PoolSettings>(Pool.SettingsLoadPath);

            if (settings != null)
            {
                Debug.LogWarning("PoolSettings already exists at Path \"Resources\\PoolSettings\"");
                return;
            }

            if (!AssetDatabase.IsValidFolder("Assets\\Resources")) AssetDatabase.CreateFolder("Assets", "Resources");

            settings = ScriptableObject.CreateInstance<PoolSettings>();
            var path = AssetDatabase
                .GenerateUniqueAssetPath(
                    $"Assets\\Resources\\{Pool.SettingsLoadPath}.asset");

            AssetDatabase.CreateAsset(settings, path);
            AssetDatabase.SaveAssets();
        }
    }
}
