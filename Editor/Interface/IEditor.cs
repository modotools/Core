using Core.Unity.Types;
using System;
using Core.Interface;
using UnityEditor;
using Object = UnityEngine.Object;

namespace Core.Editor.Interface
{
    public interface IEditor : INamed
    {
        void Init(object parentContainer);
        void Terminate();
        void SetActive(bool active);

        void OnGUI(float width);
        object ParentContainer { get; }
    }

    public interface IInspectorEditor : IEditor
    {
        Object TargetObj { get; }
        SerializedObject SerializedObject { get; }
        void SetTarget(Object target, SerializedObject serializedObject);

        DefaultGUISettings DefaultGUISettings { get; set; }
    }

    public struct DefaultGUISettings
    {
        public bool DefaultGUIEnabled;
        public bool CanUnlockDefaultGUI;
        public bool DrawDefaultGUIFoldout;

        public static DefaultGUISettings Default => new DefaultGUISettings()
            {DefaultGUIEnabled = true, DrawDefaultGUIFoldout = false, CanUnlockDefaultGUI = false};
    }

    public interface IInspectorEditor<T> : IInspectorEditor
    {
        T Target { get; }
        void SetTarget(T target, SerializedObject serializedObject);
    }

    public interface IEditorExtras
    {
        bool ShowAlways { get; }
        bool ShowLabel { get; }
    }

    public interface ISceneGUICallable
    {
        void OnSceneGUI(SceneView sceneView);
    }

    public interface IRepaintable
    {
        void Repaint();
    }
    [Serializable]
    public class RefIEditor : InterfaceContainer<IEditor> {}
}