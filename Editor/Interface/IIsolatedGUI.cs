
namespace Core.Editor.Interface
{
	/// <summary>
    /// When something is not viewed in Inspector, but in Isolation.
    /// The GUI could decide to show more information in nested editors,
    /// that would be redundant when viewing in inspector
    /// </summary>
    public interface IIsolatedGUI
    {
        void IsolatedGUI(float width);
    }
}