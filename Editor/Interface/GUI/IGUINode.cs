using UnityEditor;
using UnityEngine;

namespace Core.Editor.Interface.GUI
{
    public interface IGUIElement
    {
        Vector2 Pos { get; }
        Vector2 Size { get; }
        GUIStyle Style { get; }
    }

    public interface IGUIDraggable : IGUIElement
    {
        new Vector2 Pos { get; set; }
    }

    public interface IGUIWithText : IGUIElement
    {
        string Text { get; }
    }
    public interface IGUIWithIcon : IGUIElement
    {
        Texture2D Icon { get; }
        // good default: nodeRect.size - 20.0f * Vector2.one;
        Vector2 IconSize { get; }
    }

    public interface IGUISelectable : IGUIElement
    {
        bool IsSelected { get; set; }
    }
    public interface IGUIWithContextMenu : IGUIElement
    {
        GenericMenu ContextMenu { get; }
    }
}