using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Editor.Attribute;
using Core.Editor.Utility.GUIStyles;
using Core.Extensions;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace Core.Editor.Interface
{
    public abstract class EditorState : IEditorState, IRepaintable
    {
        const string k_editorPrefVisible = "Visible";
        protected abstract Type ThisType { get; }
        public virtual void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            m_editors = CreateEditorsForType(ThisType);

            foreach (var e in m_editors) 
                e.Init(this);

            InitVisibility();
        }

        void InitVisibility()
        {
            m_showEditors = new AnimBool[m_editors.Length];
            for (var i = 0; i < m_editors.Length; i++)
            {
                m_showEditors[i] = new AnimBool();
                m_showEditors[i].valueChanged.AddListener(Repaint);
                //Debug.Log($"{m_editors[i].GetType().Name}");
                m_showEditors[i].target = EditorPrefs.GetBool($"{m_editors[i].GetType().Name}.{k_editorPrefVisible}", false);
            }
        }

        public void Repaint()
        {
            if (m_parent is IRepaintable r)
                r.Repaint();
        }

        public virtual void Terminate()
        {
            foreach (var e in m_editors)
                e.Terminate();
        }
        public void SetActive(bool active)
        {
            if (m_editors.IsNullOrEmpty())
                return;
            foreach (var e in m_editors)
                e?.SetActive(active);
        }

        public virtual void OnGUI(float width)
        {
            HeaderGUI(width);

            Debug.Assert(m_showEditors.Length == m_editors.Length);
            for (var i = 0; i < m_editors.Length; i++)
            {
                var e = m_editors[i];
                var extras = m_editors[i] as IEditorExtras;

                var prevVisible = m_showEditors[i].target;

                using (new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(width - 22)))
                {
                    using (new GUILayout.HorizontalScope())
                    {
                        if (extras?.ShowAlways != true)
                        {
                            m_showEditors[i].target = EditorGUILayout.Foldout(m_showEditors[i].target,
                                e.Name, true, CustomStyles.MiniFoldoutStyle);
                        }
                        else
                        {
                            m_showEditors[i].target = true;
                            if (extras?.ShowLabel == true) 
                                EditorGUILayout.LabelField(e.Name);
                        }
                        //...
                    }

                    using (var fade = new EditorGUILayout.FadeGroupScope(m_showEditors[i].faded))
                    {
                        if (fade.visible)
                            e.OnGUI(width);
                    }

                    if (prevVisible != m_showEditors[i].target)
                        EditorPrefs.SetBool($"{m_editors[i].GetType().Name}.{k_editorPrefVisible}",
                            m_showEditors[i].target);
                }

            }
        }

        public void Expand(IEditor e)
        {
            var idx = m_editors.FirstIndexWhere(ed => ed == e);
            if (idx == -1)
                return;
            if (idx.IsInRange(m_showEditors))
                m_showEditors[idx].value = true;
        }

        protected abstract void HeaderGUI(float width);

        public abstract string Name { get; }
        public abstract Type ParentEditor { get; }
        public abstract bool IsApplicable { get; }
        public object ParentContainer { get; private set; }

        IEditor m_parent;
        IEditor[] m_editors;
        AnimBool[] m_showEditors;

        public IEnumerable<IEditor> SubEditors => m_editors;
        public void SetParent(IEditor parent) => m_parent = parent;

        static IEditor[] CreateEditorsForType(Type stateType)
        {
            var types = GetStateEditorTypes(stateType).ToArray();
            return CreateEditors(types);
        }
        static IEditor[] CreateEditors(IReadOnlyList<Type> types)
        {
            var editors = new IEditor[types.Count];
            for (var i = 0; i < types.Count; i++)
                editors[i] = (IEditor) Activator.CreateInstance(types[i]);
            return editors;
        }

        static IEnumerable<Type> GetStateEditorTypes(Type stateType)
        {
            var editorType = typeof(IEditor);

            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => editorType.IsAssignableFrom(type)
                            && editorType != type
                            && !type.IsAbstract)
                .Select(type => new {t = type, attr = type.GetCustomAttribute<EditorStateAttribute>()})
                .Where(pair => pair.attr?.IsValid(stateType) == true)
                .OrderBy(pair => pair.attr.Order).Select(p => p.t);

            return types;
        }
    }
}
