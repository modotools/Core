using Core.Editor.Inspector;
using Core.Editor.Tools;
using Core.Editor.Utility;
using Core.Types;
using Core.Unity.Utility.Debug;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Settings
{
    [CustomEditor(typeof(CoreSettings))]
    internal class CoreSettingsInspector : BaseInspector<CoreSettingsEditor> { }

    internal class CoreSettingsEditor : BaseEditor<CoreSettings>
    {
        public override string Name => "Core Settings";

        bool m_changed;
        CodeGeneratorConfigEditor m_menuStringsEditor;

        bool m_showDefines;
        bool m_showMenuStrings;
        bool m_showNamespaceMenuFilter;
        bool m_showDebugTags;

        public override void OnGUI(float width)
        {
            DefinesGUI();
            MenuStringsGUI();
            NamespaceFilterGUI();
            DebugTagsGUI();
        }

        private void DebugTagsGUI()
        {
            using (new FoldoutScope(ref m_showDebugTags, "Show Debug Tags"))
            {
                if (!m_showDebugTags)
                    return;
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    var ffProp = SerializedObject.FindProperty(nameof(CoreSettings.DebugFirstFrames));
                    EditorGUILayout.PropertyField(ffProp);

                    var debugTagsProp = SerializedObject.FindProperty(nameof(CoreSettings.DebugTags));

                    EditorGUILayout.PropertyField(debugTagsProp);
                    if (!check.changed)
                        return;
                    SerializedObject.ApplyModifiedProperties();
                    OnChanged();

                    Target.ApplyDebugSettings();
                }
            }
        }

        void NamespaceFilterGUI()
        {
            using (new FoldoutScope(ref m_showNamespaceMenuFilter, "Show Namespace Filter"))
            {
                if (!m_showNamespaceMenuFilter)
                    return;

                var menuFormNamespaceProp = SerializedObject.FindProperty(nameof(CoreSettings.MenuFromNamespaceFilter));
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    EditorGUILayout.PropertyField(menuFormNamespaceProp);
                    if (!check.changed)
                        return;
                    SerializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }
        }

        void DefinesGUI()
        {
            using (new FoldoutScope(ref m_showDefines, "Show Defines"))
            {
                if (!m_showDefines)
                    return;

                GUILayout.Label("Defines: ", EditorStyles.boldLabel);
                var defineChanges = false;

                foreach (var group in CoreSettings.K_PossibleDefinesForCore)
                {
                    GUILayout.Label($"{@group.GroupName}", EditorStyles.boldLabel);
                    foreach (var def in @group.Defines)
                    {
                        var prevToggled = Target.ExtraDefinesForCore.Contains(def.Define);
                        var toggled = EditorGUILayout.Toggle(new GUIContent($"  {def.Text}", def.Tooltip), prevToggled);
                        if (prevToggled == toggled)
                            continue;

                        if (toggled)
                            Target.ExtraDefinesForCore.Add(def.Define);
                        else
                            Target.ExtraDefinesForCore.Remove(def.Define);

                        defineChanges = true;
                    }
                }

                if (defineChanges)
                    DefinesChanged();
            }
        }

        void MenuStringsGUI()
        {
            using (new FoldoutScope(ref m_showMenuStrings, "Show Menu Strings"))
            {
                if (!m_showMenuStrings)
                    return;
                if (Target.MenuStrings == null)
                    CoreSettingsUpdater.LinkMenuStrings(Target);
                if (Target.MenuStrings == null)
                    return;

                GUILayout.Label("Menu-Strings: ", EditorStyles.boldLabel);

                CreateEditor(Target.MenuStrings, ref m_menuStringsEditor);
                if (m_menuStringsEditor == null)
                    return;
                var changeCheck = m_menuStringsEditor.ArgumentsOnlyGUI();
                m_changed |= (changeCheck == ChangeCheck.Changed);

                m_menuStringsEditor.GenerateButtonGUI("Update Menu");
            }
        }

        void DefinesChanged()
        {
            EditorUtility.SetDirty(Target);
            AssetDatabase.SaveAssets();
            CoreSettingsUpdater.UpdateDefines(Target);
        }

        public override void Terminate()
        {
            base.Terminate();
            if (m_changed)
                CodeGeneratorConfigEditor.GenerateCode(Target.MenuStrings);
        }
    }
}

