﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Core.Editor.Extensions;
using Core.Editor.Tools;
using Core.Extensions;
using Core.Types;
using Core.Unity.Interface;
using Core.Unity.Types.ID;
using Core.Unity.Utility.Debug;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Settings
{
    public class CoreSettings : ScriptableObject, IDependencies, IParentMarker
    {
        #region Data structs
        public readonly struct DefineGroup
        {
            public DefineGroup(string gn, DefineData[] data)
            {
                GroupName = gn;
                Defines = data;
            }
            public readonly string GroupName;
            public readonly DefineData[] Defines;
        }

        public readonly struct DefineData
        {
            public DefineData(string txt, string def, string tooltip= null)
            {
                Text = txt;
                Tooltip = tooltip;
                Define = def;
            }

            public readonly string Text;
            public readonly string Define;
            public readonly string Tooltip;
        }

        [Serializable]
        public struct DebugTagSetting
        {
            public DebugID ID;
            public DebugMode Mode;
        }
        #endregion

        const string k_configsPath = "Assets/Settings/";
        internal const string K_ConfigLinksPath = k_configsPath + "CoreSettings.asset";

        internal static readonly string K_FullConfigLinksPath = Path.GetFullPath(".") + Path.DirectorySeparatorChar + K_ConfigLinksPath;

        internal static readonly DefineGroup[] K_PossibleDefinesForCore = {
            new DefineGroup("Add Menu Items", new []
            {
                new DefineData("Regenerate GUIDS", "CORE_ED_MENU_REGEN_GUIDS"),
                new DefineData("Create Pool", "CORE_ED_MENU_CREATE_POOL"),
                new DefineData("Menu GUID Tool", "CORE_ED_MENU_GUID_TOOL"),
            }),
            new DefineGroup("Inspector", new []
            {
                new DefineData("Enhanced Transform Inspector", "CORE_ED_INSPECTOR_TRANSFORM"),
            }),
            new DefineGroup("Enable Feature", new []
            {
                new DefineData("Code Generator Configs", "CORE_ED_CREATE_ASSET_MENU_GEN_CONF"),
                new DefineData("Navigation History", "CORE_ED_NAV_HISTORY"),
                new DefineData("Playmaker", "CORE_PLAYMAKER"),
                new DefineData("NodeCanvas", "NODE_CANVAS"),
            }),
        };

        public List<string> ExtraDefinesForCore = new List<string>();

        public List<DebugTagSetting> DebugTags = new List<DebugTagSetting>();
        public int DebugFirstFrames;

        public CodeGeneratorConfig MenuStrings;
        public MenuFromNamespaceFilter MenuFromNamespaceFilter;

        public IEnumerable<ScriptableObject> Dependencies => new ScriptableObject[] { MenuStrings };
        public IScriptableObject Parent => this;

        public void ApplyDebugSettings()
        {
            DebugTools.Editor_DebugTagSettings.Clear();
            foreach (var setting in DebugTags)
                DebugTools.Editor_DebugTagSettings.Add(setting.ID, setting.Mode);
            //Debug.Log($"DebugFirstFrames {DebugFirstFrames}");
            DebugTools.Editor_DebugFrameMax = DebugFirstFrames;
        }
    }

    public static class CoreSettingsAccess
    {
        static CoreSettings m_settings;
        static CoreSettings I
        {
            get
            {
                if (m_settings != null)
                    return m_settings;
                m_settings = AssetDatabase.LoadAssetAtPath<CoreSettings>(CoreSettings.K_ConfigLinksPath);
                return m_settings;
            }
        }

        public static List<CoreSettings.DebugTagSetting> DebugTags => I.DebugTags;
        public static EditorExtensions.MenuFromNamespace MenuFilter => I.MenuFromNamespaceFilter.MenuFilter;

        public static void ApplyDebugSettings() => I.ApplyDebugSettings();
    }

    internal static class CoreSettingsUpdater
    {
        const string k_coreSettingsInit = "CoreSettingsInitialized";
        const string k_packagePath = "Packages/com.modotools.core/";
        const string k_packagePathRuntime = k_packagePath + "Runtime/";
        const string k_packagePathEditor = k_packagePath + "Editor/";
        const string k_packagePathEditorResources = k_packagePath + "Editor Resources/";

        const string k_menuStringsFile = "menustrings.asset";

        const string k_net35File = "mcs.rsp";
        const string k_net4XFile = "csc.rsp";

        [InitializeOnLoadMethod]
        static void InitCore()
        {
            if (SessionState.GetBool(k_coreSettingsInit, false))
                return;
            if (GetOrCreateCoreSettings(out var settings) != OperationResult.OK) 
                return;
            SessionState.SetBool(k_coreSettingsInit, true);
            UpdateDefines(settings);
            GenerateMenuStrings(settings);
            settings.ApplyDebugSettings();
        }

        static void EnsureCorrectScriptGuidInAsset()
        {
            var contents = File.ReadAllText(CoreSettings.K_FullConfigLinksPath);
            
            var scriptIdx = contents.IndexOf("m_Script: ", StringComparison.Ordinal);
            var guidIdx = scriptIdx + contents.Substring(scriptIdx).IndexOf("guid: ", StringComparison.Ordinal) + 6;
            var endGuidIdx = guidIdx + contents.Substring(guidIdx).IndexOf(",", StringComparison.Ordinal);
            var oldGuid = contents.Substring(guidIdx, endGuidIdx-guidIdx);
            var newGuid = AssetDatabase.AssetPathToGUID(k_packagePath + "Editor/Settings/CoreSettings.cs");
            if (string.Equals(oldGuid, newGuid, StringComparison.Ordinal))
                return;
            Debug.LogWarning($"CoreSettings with wrong guid: {oldGuid} Should be {newGuid}!");
            contents = contents.Replace(oldGuid, newGuid);
            File.WriteAllText(CoreSettings.K_FullConfigLinksPath, contents);
        }

        internal static OperationResult GetOrCreateCoreSettings(out CoreSettings settings)
        {
            if (File.Exists(CoreSettings.K_FullConfigLinksPath))
            {
                EnsureCorrectScriptGuidInAsset();
                AssetDatabase.Refresh();
                Debug.Log($"CoreSettings exist!");

                //repeat
                settings = AssetDatabase.LoadAssetAtPath<CoreSettings>(CoreSettings.K_ConfigLinksPath);
                if (settings != null)
                {
                    Debug.Log($"CoreSettings found!");
                    return OperationResult.OK;
                }

                EditorApplication.delayCall += InitCore;
                return OperationResult.Error;
            }

            Debug.LogWarning("No CORE-Settings found... creating");
            settings = ScriptableObject.CreateInstance<CoreSettings>();

            var dir = Path.GetDirectoryName(CoreSettings.K_ConfigLinksPath);
            // ReSharper disable once AssignNullToNotNullAttribute
            if (!dir.IsNullOrEmpty() && !Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            AssetDatabase.CreateAsset(settings, CoreSettings.K_ConfigLinksPath);
            AssetDatabase.SaveAssets();
            return OperationResult.OK;
        }

        internal static void GenerateMenuStrings(CoreSettings settings)
        {
            if (settings.MenuStrings == null)
                LinkMenuStrings(settings);
            if (settings.MenuStrings == null)
            {
                Debug.LogError("Could not GenerateMenuStrings!");
                return;
            }

            CodeGeneratorConfigEditor.GenerateCode(settings.MenuStrings);
        }

        internal static void LinkMenuStrings(CoreSettings settings)
        {
            Debug.Log("LinkMenuStrings");
            var origMenuStrings = AssetDatabase.LoadAssetAtPath<CodeGeneratorConfig>(k_packagePathEditorResources + k_menuStringsFile);
            if (origMenuStrings == null)
                Debug.LogError($"Could not load at {k_packagePathEditorResources + k_menuStringsFile}");

            var clone = Object.Instantiate(origMenuStrings);
            clone.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;
            AssetDatabase.AddObjectToAsset(clone, settings);
            settings.MenuStrings = clone;
            EditorUtility.SetDirty(settings);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        internal static void UpdateDefines(CoreSettings settings)
        {
            var sb = new StringBuilder();
            foreach (var define in settings.ExtraDefinesForCore)
            {
                sb.AppendLine($"-define:{define}");
            }

            var contents = sb.ToString();
            File.WriteAllText(Path.GetFullPath(k_packagePathRuntime + k_net35File), contents);
            File.WriteAllText(Path.GetFullPath(k_packagePathRuntime + k_net4XFile), contents);
            File.WriteAllText(Path.GetFullPath(k_packagePathEditor + k_net35File), contents);
            File.WriteAllText(Path.GetFullPath(k_packagePathEditor + k_net4XFile), contents);
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }
    }
}