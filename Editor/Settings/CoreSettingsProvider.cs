﻿using System.IO;
using UnityEditor;
using UnityEngine.UIElements;

namespace Core.Editor.Settings
{
    internal class CoreSettingsProvider : SettingsProvider
    {
        UnityEditor.Editor m_coreSettingsEditor;

        public CoreSettingsProvider(string path, SettingsScope scope = SettingsScope.Project)
            : base(path, scope) {}

        public static bool IsSettingsAvailable() => File.Exists(CoreSettings.K_ConfigLinksPath);

        void InitEditor()
        {
            CoreSettingsUpdater.GetOrCreateCoreSettings(out var settings);
            if (settings != null)
                m_coreSettingsEditor = UnityEditor.Editor.CreateEditor(settings);
        }

        public override void OnActivate(string searchContext, VisualElement rootElement)
        {
            // This function is called when the user clicks on the MyCustom element in the Settings window.
            // m_configLinks = GetConfigLinkSerialized();
            // Debug.Log("Activated");
            InitEditor();
        }

        public override void OnGUI(string searchContext)
        {
            if (m_coreSettingsEditor == null)
                InitEditor();
            else m_coreSettingsEditor.OnInspectorGUI();
        }

        // Register the SettingsProvider
        [SettingsProvider]
        public static SettingsProvider CreateCoreSettingsProvider()
        {
            if (!IsSettingsAvailable())
                return null;
            var provider = new CoreSettingsProvider("Project/Core Settings");

            // Automatically extract all keywords from the Styles.
            // provider.keywords = GetSearchKeywordsFromGUIContentProperties<Styles>();
            return provider;

            // Settings Asset doesn't exist yet; no need to display anything in the Settings window.
        }
    }
}