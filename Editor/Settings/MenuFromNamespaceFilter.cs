﻿using System;
using System.IO;
using Core.Editor.Extensions;
using Core.Extensions;
using Core.Types;
using UnityEditor;
using UnityEngine;
using StrPair = System.Tuple<string, string>;

namespace Core.Editor.Settings
{
    [Serializable]
    public class MenuFromNamespaceFilter
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [Serializable] 
        public struct ReplaceString
        {
            public string Namespace;
            public string Category;
        }
#pragma warning restore 0649 // wrong warning

        public ReplaceString[] ReplaceNamespaces = new ReplaceString[0];
        public string[] SkipRoots = new string[0];

        public EditorExtensions.MenuFromNamespace MenuFilter
        {
            get
            {
                var replace = new StrPair[ReplaceNamespaces.Length];
                for (var i = 0; i < ReplaceNamespaces.Length; i++)
                    replace[i] = new StrPair(ReplaceNamespaces[i].Namespace, ReplaceNamespaces[i].Category);
                return new EditorExtensions.MenuFromNamespace()
                {
                    ReplaceNamespaces =replace,
                    SkipRoots = SkipRoots
                };
            }
        }
    }

    //public static class SettingsAccess
    //{
    //    public static EditorExtensions.MenuFromNamespace MenuFilter => ScriptableSettings.MenuFilter;
    //}
}