
using UnityEngine;

namespace Core.Editor.Inspector
{
    public struct ObjectChanged
    {
        public object SourceEditor;
        public Object Changed;
    }
}