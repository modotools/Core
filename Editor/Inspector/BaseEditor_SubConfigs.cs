﻿using System;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Editor.Utility.GUITools;
using Core.Extensions;
using Core.Unity;
using Core.Unity.Extensions;
using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;

using static Core.Editor.Settings.CoreSettingsAccess;

namespace Core.Editor.Inspector
{
    public partial class BaseEditor
    {
        #region Init Release
        public static void CreateMenu(Type configType, ref CommonSubConfigEditorGUI.ConfigMenuData menuData, GenericMenu.MenuFunction2 menu)
        {
            EditorExtensions.ReflectionGetAllTypes(configType, out var platformBehaviourTypes, out var names,
                out var namespaces);
            menuData.Menu =
                EditorExtensions.CreateTypeSelectorPopup(platformBehaviourTypes, names, namespaces, MenuFilter, menu);
        }

        public static void InitConfigDataDefault(out CommonSubConfigEditorGUI.ConfigData configData, string label,
            string propertyPath)
        {
            configData = new CommonSubConfigEditorGUI.ConfigData()
            {
                ConfigNotSetColor = CommonSubConfigEditorGUI.ConfigNotSetColorDefault,
                CutPasteEnabled = true,
                Label = new GUIContent(label),
                PropertyPath = propertyPath
            };
        }
        protected void InitConfigListDataDefault(out CommonSubConfigEditorGUI.ConfigListData configData, string propertyPath, string labelFormat)
        {
            configData = new CommonSubConfigEditorGUI.ConfigListData()
            {
                ConfigNotSetColor = CommonSubConfigEditorGUI.ConfigNotSetColorDefault,
                PropertyPath = propertyPath,
                TypeSelectionIdx = -1,
                CachedNestedEditor = new UnityEditor.Editor[SerializedObject.FindProperty(propertyPath).arraySize],
                LabelFormat = labelFormat
            };
        }

        protected static void ReleaseEditor(ref CommonSubConfigEditorGUI.ConfigData configData)
        {
            if (configData.CachedEditor != null)
                configData.CachedEditor.DestroyEx();
            configData.CachedEditor = null;
        }
        protected static void ReleaseEditor(ref CommonSubConfigEditorGUI.ConfigListData configListData)
        {
            foreach (var nestedEd in configListData.CachedNestedEditor)
                nestedEd.DestroyEx();
        }
        #endregion

        #region GUI
        protected void NameGUI(string namePropPath)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var prop = SerializedObject.FindProperty(namePropPath);
                EditorGUILayout.PropertyField(prop);

                if (!check.changed) 
                    return;
                SerializedObject.ApplyModifiedProperties();
                OnChanged();
            }
        }
        protected void DefaultConfigGUI(ref CommonSubConfigEditorGUI.ConfigData configData, ref CommonSubConfigEditorGUI.ConfigMenuData menuData, Action contextAction = null)
        {
            UpdateConfigData(ref configData);
            CommonSubConfigEditorGUI.ConfigGUI(ref configData, ref menuData, out var remove, out var set, out var cutPaste);

            var anyAction = remove || cutPaste || set;
            if (anyAction)
                contextAction?.Invoke();

            if (remove)
                OnRemoveBehaviour(ref configData);
            if (cutPaste)
                OnCutPaste(ref configData);
            if (set)
                PopupWindow.Show(menuData.ButtonRect, menuData.Menu);
        }

        static void NestedEditorGUI(ref CommonSubConfigEditorGUI.ConfigListData listData, int i, SerializedProperty seqElProp, ScriptableObject seq)
        {
            if (seq == null && listData.CachedNestedEditor[i] != null)
                listData.CachedNestedEditor[i].DestroyEx();

            if (!seqElProp.isExpanded || seq == null)
                return;

            UnityEditor.Editor.CreateCachedEditor(seq, null, ref listData.CachedNestedEditor[i]);
            if (listData.CachedNestedEditor[i] == null)
                return;

            var enabled = !(seq is IEnableData e) || e.Enabled; 
            using (new EditorGUI.DisabledGroupScope(!enabled))
                listData.CachedNestedEditor[i].OnInspectorGUI();
        }
        #endregion

        #region Utility
        protected void UpdateConfigData(ref CommonSubConfigEditorGUI.ConfigData configData)
        {
            configData.SObject = SerializedObject;
            configData.ConfigProperty = SerializedObject.FindProperty(configData.PropertyPath);
            configData.Config =  configData.ConfigProperty.objectReferenceValue as ScriptableObject;
        }

        protected void UpdateConfigListData(ref CommonSubConfigEditorGUI.ConfigListData configData)
        {
            configData.SObject = SerializedObject;
            configData.ConfigListProperty = SerializedObject.FindProperty(configData.PropertyPath);
        }
        #endregion

        #region Actions
        protected void OnCutPaste(ref CommonSubConfigEditorGUI.ConfigListData listData, int cutIdx)
        {
            var prop = listData.ConfigListProperty.GetArrayElementAtIndex(cutIdx);
            if (prop.objectReferenceValue != null)
                CopyPasteOperation.Cut(prop);
            else
                CopyPasteOperation.Paste(prop);
        }

        protected void OnDelete(ref CommonSubConfigEditorGUI.ConfigListData listData, int deleteIdx)
        {
            CommonSubConfigEditorGUI.SafeDestroy(TargetObj, listData.ConfigListProperty.GetArrayElementAtIndex(deleteIdx).objectReferenceValue);

            listData.ConfigListProperty.DeleteArrayElementAtIndex(deleteIdx);
            listData.SObject.ApplyModifiedProperties();
        }

        protected static void OnMove(ref CommonSubConfigEditorGUI.ConfigListData listData, int moveIdxA, int moveIdxB)
        {
            var arrAProp = listData.ConfigListProperty.GetArrayElementAtIndex(moveIdxA);
            var arrBProp = listData.ConfigListProperty.GetArrayElementAtIndex(moveIdxB);

            var seqA = arrAProp.objectReferenceValue;
            var seqB = arrBProp.objectReferenceValue;

            arrAProp.objectReferenceValue = seqB;
            arrBProp.objectReferenceValue = seqA;

            listData.SObject.ApplyModifiedProperties();
        }

        protected static void OnAdd(ref CommonSubConfigEditorGUI.ConfigListData listData)
        {
            var count = listData.ConfigListProperty.arraySize;
            listData.ConfigListProperty.InsertArrayElementAtIndex(count);
            listData.ConfigListProperty.GetArrayElementAtIndex(count).objectReferenceValue = null;
            listData.SObject.ApplyModifiedProperties();
        }

        protected void OnRemoveBehaviour(ref CommonSubConfigEditorGUI.ConfigData configData)
        {
            configData.ConfigProperty.objectReferenceValue.DestroyEx();
            configData.ConfigProperty.objectReferenceValue = null;
            configData.SObject.ApplyModifiedProperties();

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        protected static void OnCutPaste(ref CommonSubConfigEditorGUI.ConfigData configData)
        {
            var prop = configData.ConfigProperty;
            var isPaste = prop.objectReferenceValue == null;
            
            if (isPaste)
                CopyPasteOperation.Paste(prop);
            else
                CopyPasteOperation.Cut(prop);
        }

        protected void OnTypeSelected(CommonSubConfigEditorGUI.ConfigData configData, object typeData)
        {
            var config = CreateConfig(typeData);

            if (configData.Config != null)
                configData.Config.DestroyEx();
            configData.ConfigProperty.objectReferenceValue = config;
            configData.SObject.ApplyModifiedProperties();

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        protected void OnTypeSelected(CommonSubConfigEditorGUI.ConfigListData it, object data)
        {
            var config = CreateConfig(data);

            var elementProp = it.ConfigListProperty.GetArrayElementAtIndex(it.TypeSelectionIdx);
            if (elementProp.objectReferenceValue != null)
                elementProp.objectReferenceValue.DestroyEx();
            elementProp.objectReferenceValue = config;
            it.SObject.ApplyModifiedProperties();

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        protected ScriptableObject CreateConfig(object data)
        {
            var type = (Type) data;
            var obj = ScriptableObject.CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, TargetObj);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(obj));

            return obj;
        }
        #endregion

        #region GUI
        protected void ListGUI(ref CommonSubConfigEditorGUI.ConfigListData listData, ref CommonSubConfigEditorGUI.ConfigMenuData menuData,
            Action contextAction = null)
        {
            var mod = CommonSubConfigEditorGUI.ConfigListModifications.Default;
            var data = listData.GUIData ?? TargetObj as IGUIData;
  
            if (data?.UseColor == true)
            {
                EditorGUILayout.BeginHorizontal();
                CustomGUI.VSplitter(data.GUIColor, 2f);
            }
            using (new EditorGUILayout.VerticalScope())
            {
                if (listData.CachedNestedEditor.Length < listData.ConfigListProperty.arraySize)
                    Array.Resize(ref listData.CachedNestedEditor, listData.ConfigListProperty.arraySize);

                for (var i = 0; i < listData.ConfigListProperty.arraySize; i++)
                    SequenceElementGUI(ref listData, i, ref mod, ref menuData);
            }
            if (data?.UseColor == true)
                EditorGUILayout.EndHorizontal();

            GUILayout.Space(5);
            using (new EditorGUILayout.HorizontalScope())
            {
                if (data != null)
                    CommonSubConfigEditorGUI.ColorGUI(data);
                GUILayout.Space(15f);

                GUILayout.Label($"{listData.ConfigListProperty.arraySize} elements");
                var name = data?.Name ?? listData.ConfigListProperty.name;
                var add = GUILayout.Button($"+ Add to {name}");
                GUILayout.FlexibleSpace();

                var move = mod.MoveIdxA != -1 && mod.MoveIdxB != -1;
                var delete = mod.DeleteIdx != -1;
                var cutPaste = mod.CutPasteIdx != -1;
                var setBehaviour = mod.SetIdx != -1;

                var any = add || move || delete || cutPaste;
                if (any)
                    contextAction?.Invoke();

                if (add)
                    OnAdd(ref listData);
                else if (move)
                    OnMove(ref listData, mod.MoveIdxA, mod.MoveIdxB);
                else if (delete)
                    OnDelete(ref listData, mod.DeleteIdx);
                else if (cutPaste)
                    OnCutPaste(ref listData, mod.CutPasteIdx);
                else if (setBehaviour)
                    PopupWindow.Show(menuData.ButtonRect, menuData.Menu);

                if (any)
                    OnChanged();
            }

            VerificationGUI();

            GUILayout.Space(10);
        }

        void SequenceElementGUI(ref CommonSubConfigEditorGUI.ConfigListData listData, int i, 
            ref CommonSubConfigEditorGUI.ConfigListModifications mod, ref CommonSubConfigEditorGUI.ConfigMenuData menuData)
        {
            var data = listData.GUIData ?? TargetObj as IGUIData;
            var col = data?.GUIColor ?? default;

            var listProp = listData.ConfigListProperty;
            var elementProp = listProp.GetArrayElementAtIndex(i);

            var it = new CommonSubConfigEditorGUI.ConfigListIterator
            {
                Idx = i,
                Config = elementProp.objectReferenceValue as ScriptableObject,
                ConfigProperty = elementProp,
                Count = listProp.arraySize
            };
            using (new EditorGUILayout.HorizontalScope())
            {
                var hasCustomLabel = !listData.CustomLabels.IsNullOrEmpty() && listData.CustomLabels.IsIndexInRange(i);
                    using (new ColorScope(col))
                {
                    if (!hasCustomLabel)
                        GUILayout.Label(string.Format(listData.LabelFormat, i, i + 1), GUILayout.Width(25f));
                }

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    if (hasCustomLabel)
                        GUILayout.Label(listData.CustomLabels[i]);

                    CommonSubConfigEditorGUI.DefaultConfigListElementHeader(ref listData, ref it, ref mod, ref menuData);
                    NestedEditorGUI(ref listData, i, it.ConfigProperty, it.Config);
                }
            }
        }
        #endregion
    }
}
