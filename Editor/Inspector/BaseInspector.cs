﻿using Core.Editor.Interface;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Editor.Inspector
{
    [CustomEditor(typeof(Object), true, isFallback = true)]
    [CanEditMultipleObjects]
    public class BaseInspector : BaseInspector<BaseEditor>
    {
    }

    public abstract class BaseInspector<T> : UnityEditor.Editor, IBaseInspector, IRepaintable where T: IInspectorEditor, new ()
    {
        public virtual bool CanDetach => true;

        public virtual DefaultGUISettings DefaultGUISettings => DefaultGUISettings.Default;

        protected T m_editor;
        float m_currentWidth;

        public IEditor GetEditor() => m_editor;

        public virtual void OnEnable()
        {
            if (target == null || serializedObject == null)
                return;
            m_editor = new T();
            m_editor.SetTarget(target, serializedObject);
            m_editor.Init(this);

            m_editor.DefaultGUISettings = DefaultGUISettings;
        }

        public virtual void OnDisable() => m_editor?.Terminate();

        public override void OnInspectorGUI()
        {
            if (m_editor == null)
                return;
            using (new EditorGUILayout.HorizontalScope())
            {
                DrawDetachButton();
                UnlockGUIButton();
            }

            var ctrlRect = EditorGUILayout.GetControlRect(false, 1f);
            if (Event.current.type == EventType.Repaint)
                m_currentWidth = ctrlRect.width;

            //Debug.Log($"{Event.current} {ctrlRect.width} {EditorGUIUtility.currentViewWidth}");
            m_editor.OnGUI(m_currentWidth);
        }
        
        protected void DrawDetachButton()
        {
            if (!CanDetach)
                return;
            if (!GUILayout.Button("Detach", EditorStyles.miniButton, GUILayout.Width(75)))
                return;
            OpenDetached();
        }
        protected void UnlockGUIButton()
        {
            if (!DefaultGUISettings.CanUnlockDefaultGUI)
                return;
            var set = m_editor.DefaultGUISettings;
            var text = set.DefaultGUIEnabled ? "Lock Default GUI" : "Unlock Default GUI";
            if (!GUILayout.Button(text, EditorStyles.miniButton, GUILayout.Width(150)))
                return;
            set.DefaultGUIEnabled = !set.DefaultGUIEnabled;
            m_editor.DefaultGUISettings = set;
        }

        protected virtual void OpenDetached()
        {
            var ed = new T();
            ed.SetTarget(target, serializedObject);
            BaseEditorWindow.OpenWindow(ed);
        }

        public new static void DrawPropertiesExcluding(SerializedObject obj,
            params string[] propertyToExclude) =>
            UnityEditor.Editor.DrawPropertiesExcluding(obj, propertyToExclude);
    }

    public interface IBaseInspector
    {
        IEditor GetEditor();
    }
}