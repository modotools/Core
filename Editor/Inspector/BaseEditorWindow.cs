using Core.Editor.Interface;
using UnityEditor;

namespace Core.Editor.Inspector
{
    public class BaseEditorWindow : EditorWindow
    {
        IEditor m_baseEditor;

        public static void OpenWindow(IEditor baseEditor, bool utility = false, bool focus = true)
        {
            var editor = (BaseEditorWindow) GetWindow(typeof(BaseEditorWindow), utility, baseEditor.Name, focus);
            editor.m_baseEditor = baseEditor;
            baseEditor.Init(editor);

            editor.Show();
        }

        void OnEnable() { }

        void OnDisable() => m_baseEditor.Terminate();

        void OnGUI() => m_baseEditor.OnGUI(position.width);

        //void Update() 
        //    => m_baseEditor.Update();
    }
}