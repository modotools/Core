using System.Reflection;

namespace Core.Editor.Inspector
{
    public struct ContextMenuData
    {
        public readonly string MenuItem;
        public MethodInfo Function;
        public MethodInfo Validate;

        public ContextMenuData(string item)
        {
            MenuItem = item;
            Function = null;
            Validate = null;
        }
    }
}