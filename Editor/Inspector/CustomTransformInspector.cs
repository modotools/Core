﻿using System;
using System.Reflection;
using ExtUnityComponents;
using UnityEditor;
using UnityEngine;

#if CORE_ED_INSPECTOR_TRANSFORM
namespace Core.Editor.Inspector
{
    [CustomEditor(typeof(Transform), true)]
    [CanEditMultipleObjects]
    public class CustomTransformInspector : DecoratorComponentsEditor
    {
        // Unity's built-in editor
        UnityEditor.Editor m_defaultEditor;
        Transform m_transform;

        readonly LockChildren m_lockChildren = new LockChildren();
        public CustomTransformInspector() : base(BUILT_IN_EDITOR_COMPONENTS.TransformInspector) { }

        protected override void OnEnable()
        {
            base.OnEnable();

            // When this inspector is created, also create the built-in inspector
            m_defaultEditor = CreateEditor(targets, Type.GetType("UnityEditor.TransformInspector, UnityEditor"));
            m_transform = target as Transform;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            // When OnDisable is called, the default editor we created should be destroyed to avoid memory leakage.
            // Also, make sure to call any required methods like OnDisable
            if (m_defaultEditor == null)
                return;

            var disableMethod = m_defaultEditor.GetType().GetMethod("OnDisable", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            if (disableMethod != null)
                disableMethod.Invoke(m_defaultEditor, null);

            DestroyImmediate(m_defaultEditor);
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.LabelField("Local Space", EditorStyles.boldLabel);
            base.OnInspectorGUI();
        }

        protected override void OnCustomInspectorGUI()
        {
            // Show World Space Transform
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("World Space", EditorStyles.boldLabel);

            GUI.enabled = false;
            EditorGUILayout.Vector3Field($"{nameof(Transform.position)}", m_transform.position);
            EditorGUILayout.Vector3Field($"{nameof(Transform.rotation)}", m_transform.rotation.eulerAngles);
            EditorGUILayout.Vector3Field($"scale", m_transform.lossyScale);
            GUI.enabled = true;

            if (targets.Length < 2) 
                m_lockChildren.CustomOnInspectorGUI();
            //base.OnCustomInspectorGUI();
        }

        #region Lock Children
        /// <summary>
        /// need to clean when quitting
        /// </summary>
        public override void OnCustomDisable() => m_lockChildren.CustomDisable();

        /// <summary>
        /// this function is called on the first OnSceneGUI
        /// </summary>
        /// <param name="sceneView"></param>
        protected override void InitOnFirstOnSceneGUI(SceneView sceneView) 
            => m_lockChildren.InitOnFirstOnSceneGUI();

        protected override void CustomOnSceneGUI(SceneView sceneView) 
            => m_lockChildren.CustomOnSceneGUI();

        /// <summary>
        /// init the targets inspected and store them.
        /// This is called at the first OnInspectorGUI()
        /// Not in the constructor ! we don't have targets information in the constructor
        /// </summary>
        protected override void InitOnFirstInspectorGUI() => m_lockChildren.Init((Transform)ConcretTarget, this);

        /// <summary>
        /// This function is called at each OnInspectorGUI
        /// </summary>
        //protected override void OnCustomInspectorGUI()
        //{
        //    if (targets.Length < 2) 
        //        m_lockChildren.CustomOnInspectorGUI();
        //}

        /// <summary>
        /// this function is called at each EditorUpdate() when we need it
        /// </summary>
        protected override void OnEditorUpdate() => m_lockChildren.OnEditorUpdate();
        #endregion
    }
}
#endif