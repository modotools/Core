using System.IO;
using Core.Extensions;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Inspector
{
    [CustomEditor(typeof(DefaultAsset))]
    public class FolderInspector : UnityEditor.Editor
    {
        struct EditData
        {
            public DefaultAsset Target;
            public AssetImporter Importer;
            public string Desc;
        }

        // ReSharper disable once InconsistentNaming
        new DefaultAsset target => base.target as DefaultAsset;

        EditData lastData;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUI.enabled = true;

            var path = AssetDatabase.GetAssetPath(target);
            var ext = Path.GetExtension(path);
            if (!ext.IsNullOrEmpty())
                return;
            FolderGUI(path);
        }

        void FolderGUI(string path)
        {
            if (lastData.Target != target)
                InitData(path);

            GUILayout.Label("Description: ");
            lastData.Desc = GUILayout.TextArea(lastData.Desc);
        }

        void InitData(string path)
        {
            lastData.Target = target;
            lastData.Importer = AssetImporter.GetAtPath(path);
            lastData.Desc = lastData.Importer.userData;
        }

        void OnDisable()
        {
            if(lastData.Target == null)
                return;

            lastData.Importer.userData = lastData.Desc;
            lastData.Importer.SaveAndReimport();
            lastData = new EditData();
        }
    }
}
