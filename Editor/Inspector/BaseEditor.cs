﻿using Core.Editor.Interface;
using Core.Events;
using Core.Unity.Interface;
using System.Collections.Generic;
using Core.Editor.Utility;
using UnityEditor;
using UnityEngine;

namespace Core.Editor.Inspector
{
    //[CustomEditor(typeof(Object), true, isFallback = true)]
    //[CanEditMultipleObjects]
    public class BaseEditor<T> : BaseEditor, IInspectorEditor<T> where T: Object
    {
        public override string Name => $"{GetType().Name} {TargetName}";
        public virtual T Target => TargetObj as T;

        string TargetName => Target != null ? Target.name : "";

        public virtual void SetTarget(T t, SerializedObject s) => SetTarget(t as Object, s);
    }

    public partial class BaseEditor : IInspectorEditor, IRepaintable, IEventListener<ObjectChanged>
    {
        VerificationResult m_result = VerificationResult.Default;
        public VerificationResult Result => m_result;

        public virtual string Name => "BaseEditor";
        public object ParentContainer { get; private set; }

        public Object TargetObj { get; private set; }
        public SerializedObject SerializedObject { get; private set; }
        // todo: use Width instead of OnGUI(width)
        public float Width;

        public virtual void SetTarget(Object t, SerializedObject s)
        {
            TargetObj = t;
            SerializedObject = s;
            OnTargetChanged();
        }

        public bool IsDetached => ParentContainer is EditorWindow;
        public DefaultGUISettings DefaultGUISettings { get; set; } = DefaultGUISettings.Default;

        bool m_defaultGUIFoldOut;

        public virtual void Init(object parentContainer)
        {
            EventMessenger.AddListener(this);
            ParentContainer = parentContainer;
            Verify();
        }

        public virtual void Terminate() => EventMessenger.RemoveListener(this);
        public virtual void SetActive(bool active) { }

        // todo 2 CORE_EDITOR: take a look also at https://gist.github.com/LotteMakesStuff/e354cf6e8a4a8194fced3323b15fc1ba
        // ..Copy paste all props? via context menu? (CopyPasteAttribute)
        public virtual void OnGUI(float width)
        {
            BaseGUI();
            EndGUI();
        }

        public void EndGUI()
        {
            if (GUI.changed)
                OnChanged();

            VerificationGUI();
        }

        public void BaseGUI()
        {
            if (DefaultGUISettings.DrawDefaultGUIFoldout)
            {
                using (new FoldoutScope(ref m_defaultGUIFoldOut, "Default GUI"))
                {
                    if (m_defaultGUIFoldOut) DefaultGUI();
                }
            }
            else DefaultGUI();
        }

        void DefaultGUI()
        {
            var enabled = DefaultGUISettings.DefaultGUIEnabled;
            using (new EditorGUI.DisabledGroupScope(!enabled))
            {
                if (ParentContainer is UnityEditor.Editor ed) 
                    ed.DrawDefaultInspector();
                else
                    DoDrawDefaultInspector(SerializedObject);
            }
        }

        static void DoDrawDefaultInspector(SerializedObject obj)
        {
            obj.UpdateIfRequiredOrScript();
            var iterator = obj.GetIterator();
            for (var enterChildren = true; iterator.NextVisible(enterChildren); enterChildren = false)
            {
                using (new EditorGUI.DisabledScope("m_Script" == iterator.propertyPath))
                    EditorGUILayout.PropertyField(iterator, true);
            }
            obj.ApplyModifiedProperties();
        }

        public virtual void OnTargetChanged(){}
        
        public void VerificationGUI() => VerificationGUI(m_result);

        public void Verify()
        {
            m_result = VerificationResult.Default;
            if (TargetObj is IVerify verifyTarget)
                verifyTarget.Editor_Verify(ref m_result);
        }

        public static void VerificationGUI(VerificationResult result)
        {
            HelpBox(result.ErrorLogs, MessageType.Error);
            HelpBox(result.WarningLogs, MessageType.Warning);
            HelpBox(result.InfoLogs, MessageType.Info);
        }
        static void HelpBox(IEnumerable<LogData> logData, MessageType messageType)
        {
            foreach (var data in logData)
                EditorGUILayout.HelpBox($"{data.LogString}", messageType);
        }

        protected virtual void OnChanged()
        {
            //Debug.Log($"On Changed {target}");
            EditorUtility.SetDirty(TargetObj);
            SerializedObject.Update();
            EventMessenger.TriggerEvent(new ObjectChanged { SourceEditor = this, Changed = TargetObj });
            Verify();
            Repaint();
        }

        public void OnEvent(ObjectChanged eventType)
        {
            if (eventType.Changed != TargetObj)
                return;
            if (ReferenceEquals(eventType.SourceEditor, this))
                return;
            SerializedObject.Update();
            //Repaint();
        }

        public static void CreateEditor<T>(Object targetObject, ref T previousEditor)
            where T : BaseEditor, new()
        {
            var needEditor = targetObject != null;

            if (previousEditor == null)
            {
                if (needEditor)
                    previousEditor = new T() {TargetObj = targetObject};
            }
            else
            {
                previousEditor.TargetObj = targetObject;
                if (needEditor) 
                    return;
                
                previousEditor.Terminate();
                previousEditor = null;
            }
        }

        public void Repaint()
        {
            switch (ParentContainer)
            {
                case EditorWindow w: w.Repaint(); break;
                case UnityEditor.Editor e: e.Repaint(); break;
                case IRepaintable r: r.Repaint(); break;
            }
        }
    }

}