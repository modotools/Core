using Core.Unity.Interface;
using Core.Unity.Types.ID;
using Core.Unity.Utility.Debug;

namespace Core.Interface
{
    public interface IDebuggable
    {
        IDebugContext DebugContext { get; }
        DebugID DebugID { get; }
    }

    public interface IDebugComponent : IDebuggable, IComponent
    {
    }

    public static class DebugComponentOperations
    {
        public static DebugMode Mode(this IDebuggable debuggable) 
            => debuggable.DebugContext?.DebugMode ?? DebugMode.Off;
        public static string Name(this IDebuggable debuggable) 
            => debuggable.DebugContext?.Name ?? "";
    }
}