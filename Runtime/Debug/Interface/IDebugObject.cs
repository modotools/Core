using System;
using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Utility.Debug;

namespace Core.Interface
{
    public interface IDebugContext : INamed
    {
        DebugMode DebugMode { get; set; }
    }

    public interface IDebugObject : IDebugContext, IComponent {}

    [Serializable]
    public class RefIDebugObject : InterfaceContainer<IDebugObject>
    {

    }
}