using System;
using Core.Extensions;
using UnityEngine;

namespace Core
{
    public struct DebugString
    {
        public string String;
        public LogType Type;
    }
    public enum LogType
    {
        Log,
        Warning,
        Error,
    }
    public static class DebugStringOperations
    {
        public static void Reset(this ref DebugString debugString)
        {
            debugString.String = "";
            debugString.Type = LogType.Log;
        }
        public static void AddLog(this ref DebugString debugString, string text) 
            => debugString.String += $"{text}\n";
        public static void AddWarning(this ref DebugString debugString, string text)
        {
            debugString.String += $"{text}\n";
            if (LogType.Warning > debugString.Type)
                debugString.Type = LogType.Warning;
        }
        public static void AddError(this ref DebugString debugString, string text)
        {
            debugString.String += $"{text}\n";
            debugString.Type = LogType.Error;
        }

        public static void Prepend(this ref DebugString debugString, string prependString)
        {
            var post = debugString.String;
            debugString.String = $"{prependString}\n{post}";;
        }

        public static void Print(this ref DebugString debugString)
        {
            if (debugString.String.IsNullOrEmpty())
                return;
            switch (debugString.Type)
            {
                case LogType.Log: Debug.Log(debugString.String); break;
                case LogType.Warning: Debug.LogWarning(debugString.String); break;
                case LogType.Error: Debug.LogError(debugString.String); break;
                default: throw new ArgumentOutOfRangeException();
            }
            debugString.String = "";
            debugString.Type = LogType.Log;
        }
    }
}
