using Core.Unity.Utility.Debug;
using UnityEngine;

namespace Core.Interface
{
    public class DebugObject : MonoBehaviour, IDebugObject
    {
        [SerializeField] DebugMode m_debugMode;
        public DebugMode DebugMode
        {
            get => m_debugMode;
            set => m_debugMode = value;
        }

        public string Name => name;
    }
}