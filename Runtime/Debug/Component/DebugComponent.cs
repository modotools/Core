using Core.Unity.Extensions;
using Core.Unity.Types.ID;
using Core.Unity.Utility.Debug;
using UnityEngine;

namespace Core.Interface
{
    public class DebuggableBehaviour : MonoBehaviour, IDebugComponent
    {
        [SerializeField] protected GameObject m_rootEntity;
        [SerializeField] DebugID m_debugId;

        public IDebugObject DebugObject
        {
            get
            {
                if (m_rootEntity == null) 
                    return null;
                m_rootEntity.TryGet(out IDebugObject @debugObject);
                return @debugObject;
            }
        }

        public IDebugContext DebugContext => DebugObject;

        public DebugID DebugID => m_debugId;
        public DebugMode DebugMode => DebugObject?.DebugMode ?? DebugMode.Off;
    }
}