﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Extensions
{
    public static class UnityObjectExtensions
    {
        /// <summary>
        /// Destroys object correctly depending on whether we are in the editor or in the game
        /// </summary>
        public static void DestroyEx(this Object obj, bool undo = false)
        {
            var editor = Application.isEditor && !Application.isPlaying;
            if (editor)
            {
#if UNITY_EDITOR
                if (undo)
                {
                    Undo.DestroyObjectImmediate(obj);
                    return;
                }
#endif
                Object.DestroyImmediate(obj, true);
            }   
            else
                Object.Destroy(obj);
        }

        public static void DestroyEx<T>(this IEnumerable<T> obj, bool undo = false) where T : Component
        {
            foreach (var o in obj)
            {
                if (o == null)
                    continue;

                o.gameObject.DestroyEx();
            }
        }

        public static void DestroyEx_Safe(Object obj, bool undo = false)
        {
            if (obj != null)
                DestroyEx(obj, undo);
        }

    }
}
