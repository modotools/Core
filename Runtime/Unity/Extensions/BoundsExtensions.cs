﻿using Core.Types;
using UnityEngine;

namespace Core.Unity.Extensions
{
    public static class BoundsExtension
    {
        /// <summary> returns IntersectionType for two bounds </summary>
        public static IntersectionType GetIntersectionType(this Bounds a, Bounds b)
        {
            if (!a.Intersects(b))
                return IntersectionType.None;

            if (a.Contains(b.min) && a.Contains(b.max))
                return IntersectionType.Contains;
            if (b.Contains(a.min) && b.Contains(a.max))
                return IntersectionType.Contained;

            const float tolerance = float.Epsilon;

            var maxMinX = Mathf.Abs(a.max.x - b.min.x) < tolerance;
            var maxMinY = Mathf.Abs(a.max.y - b.min.y) < tolerance;
            var maxMinZ = Mathf.Abs(a.max.z - b.min.z) < tolerance;
            var minMaxX = Mathf.Abs(a.min.x - b.max.x) < tolerance;
            var minMaxY = Mathf.Abs(a.min.y - b.max.y) < tolerance;
            var minMaxZ = Mathf.Abs(a.min.z - b.max.z) < tolerance;

            if (maxMinX || maxMinY || maxMinZ || minMaxX || minMaxY || minMaxZ)
                return IntersectionType.Touch;
            return IntersectionType.Intersects;
        }
    }
}
