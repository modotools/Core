﻿using System.Linq;
using Core.Extensions;
using UnityEngine;

namespace Core.Unity.Extensions
{
    public static class AnimatorExtensions
    {
        public static bool IsPlaying(this Animator animator, string name = "", int layerIndex = 0)
        {
            var info = animator.GetCurrentAnimatorStateInfo(layerIndex);
            if (animator.IsInTransition(layerIndex))
                return true;
            var playing = info.normalizedTime < 1;
            return name.IsNullOrEmpty() ? playing : playing && info.IsName(name);
        }

        /// <summary>
        /// Determines if an animator contains a certain parameter, based on a type and a name
        /// </summary>
        /// <returns><c>true</c> if has parameter of type the specified self name type; otherwise, <c>false</c>.</returns>
        /// <param name="self">Self.</param>
        /// <param name="name">Name.</param>
        /// <param name="type">Type.</param>
        public static bool HasParameterOfType(this Animator self, string name, AnimatorControllerParameterType type)
        {
            if (string.IsNullOrEmpty(name))
                return false;

            var parameters = self.parameters;
            return parameters.Any(p => p.type == type && p.name == name);
            // foreach (var p in parameters)
            //    if (p.type == type && p.name == name)
            //        return true;
        }
    }
}
