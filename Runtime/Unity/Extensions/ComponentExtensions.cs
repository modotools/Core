﻿using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;

namespace Core.Unity.Extensions
{
    public static class ComponentExtensions 
    {
        public static T Get<T>(this Component c)
        {
            if (!c.TryGetComponent(out IProvider<T> prov))
                return c.TryGetComponent(out T @return) ? @return : default;
            prov.Get(out var provided);
            return provided;
        }

        public static bool TryGet<T>(this Component c, out T @value)
        {
            if (!c.TryGetComponent(out IProvider<T> prov))
                return c.TryGetComponent(out @value);
            prov.Get(out @value);
            return @value != null;
        }

        public static TOut GetComponent<TOut>(this IComponent inType)
        {
            switch (inType)
            {
                case IProvider<TOut> prov: prov.Get(out var provided);
                    return provided;
                case Component c: return c.Get<TOut>();
                default: return default;
            }
        }

        public static bool TryGetComponent<TOut>(this IComponent inType, out TOut @value)
        {
            @value = default;
            switch (inType)
            {
                case IProvider<TOut> prov: 
                    prov.Get(out @value);
                    return @value != null;
                case Component c: return c.TryGet(out @value);
                default: return false;
            }
        }
    }
}
