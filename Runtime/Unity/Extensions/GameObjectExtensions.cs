﻿using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Interface;
using Core.Unity.Utility;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;

namespace Core.Unity.Extensions
{
    public static class GameObjectExtensions
    {
        public static void DestroyComponents<T>(this GameObject parent) where T : Component
        {
            var comps = parent.GetComponentsInChildren<T>(true);
            foreach (var c in comps) 
                Object.Destroy(c);
        }

        public static void SetComponentsEnabled<T>(this GameObject parent, bool enabled = true) where T : MonoBehaviour
        {
            var comps = parent.GetComponentsInChildren<T>(enabled);
            foreach (var c in comps)
                c.enabled = enabled;
        }

        public static void SetColliderEnabled<T>(this GameObject parent, bool enabled = true) where T : Collider
        {
            var comps = parent.GetComponentsInChildren<T>(enabled);
            foreach (var c in comps) 
                c.enabled = enabled;
        }

        public static void SetActive(this IEnumerable<GameObject> go, bool active = true)
        {
            foreach (var g in go)
                g.SetActive(active);
        }

        public static void SetActiveWithTransition(this GameObject go, bool active, out bool isInTransition)
        {
            isInTransition = false;
            if (go.TryGetComponent<ITransitionActivation>(out var transitionActivation))
            {
                transitionActivation.SetActive(active);
                isInTransition = transitionActivation.IsInTransition;
            }
            else go.SetActive(active);
        }

        public static void SetActiveWithTransition(this GameObject go, bool active = true)
        {
            if (go.TryGetComponent<ITransitionActivation>(out var transitionActivation))
                transitionActivation.SetActive(active);
            else
                go.SetActive(active);
        }

        public static void SetHideFlagsRecursively(this GameObject go, HideFlags hideFlags)
        {
            go.hideFlags = hideFlags;
            foreach (var child in go.Children()) 
                child.gameObject.SetHideFlagsRecursively(hideFlags);
        }

        public static bool HasHideFlags(this GameObject go, HideFlags hideFlags) => (go.hideFlags & hideFlags) == hideFlags;

        public static IEnumerable<Transform> Children(this GameObject go) => go.transform.Children();
        
        public static bool IsActiveOrActivating(this GameObject go)
        {
            return go.TryGetComponent<ITransitionActivation>(out var transitionActivation)
                ? transitionActivation.IsActiveOrActivating
                : go.activeSelf;
        }
        public static bool IsInTransition(this GameObject go) => 
            go.TryGetComponent<ITransitionActivation>(out var transitionActivation)
            && transitionActivation.IsInTransition;

        public static void SetActiveWithTransition(this GameObject[] go, bool active = true)
        {
            foreach (var g in go)
                g.SetActiveWithTransition(active);
        }

        // ReSharper disable once FlagArgument
        public static void TryDespawn(this GameObject go, bool ignoreTransition = false)
        {
            if (go == null)
                return;
            if (!ignoreTransition && go.TryGetComponent<ITransitionActivation>(out var transitionActivation))
            {
                transitionActivation.InactiveFollowup = InactiveFollowup.Despawn;
                transitionActivation.SetActive(false);
            }
            else if (go.TryGetComponent<PoolEntity>(out var poolEntity))
                poolEntity.Despawn();
        }

        // ReSharper disable once FlagArgument
        public static void TryDespawnOrDestroy(this GameObject go, bool ignoreTransition = false)
        {
            if (go == null)
                return;
            if (!ignoreTransition && go.TryGetComponent<ITransitionActivation>(out var transitionActivation))
            {
                transitionActivation.InactiveFollowup = InactiveFollowup.DespawnOrDestroy;
                transitionActivation.SetActive(false);
            }
            else if (go.TryGetComponent<PoolEntity>(out var poolEntity))
                poolEntity.Despawn();
            else go.DestroyEx();
        }

        public static T Get<T>(this GameObject go)
        {
            if (!go.TryGetComponent(out IProvider<T> prov))
                return go.TryGetComponent(out T @return) ? @return : default;
            prov.Get(out var provided);
            return provided;
        }

        public static bool TryGet<T>(this GameObject go, out T @value)
        {
            if (!go.TryGetComponent(out IProvider<T> prov))
                return go.TryGetComponent(out @value);
            prov.Get(out @value);
            return @value != null;
        }
        public static void SetLayer(this GameObject gameObject, int layer, bool includeChildren = false) 
        {
            if (gameObject == null) 
                return;
            if (!includeChildren) 
            {
                gameObject.layer = layer;
                return;
            }
 
            foreach (var child in gameObject.GetComponentsInChildren(typeof(Transform), true))
                child.gameObject.layer = layer;
        }
#if UNITY_EDITOR
        public static GameObject Editor_CreateEditorManaged(string name)
        {
            return new GameObject(name, typeof(CleanupMarker))
            {
                hideFlags = HideFlags.DontSave | HideFlags.NotEditable
            };
        }

        public static void Editor_SetEditorManaged(this GameObject go)
        {
            var editorManagedFlags = HideFlags.DontSave | HideFlags.NotEditable;
            Editor_SetHideFlags(go, HideFlags.DontSave, editorManagedFlags, editorManagedFlags);
        }
        public static void Editor_SetHideFlags(this GameObject go, HideFlags thisGameObjectFlags, HideFlags thisComponentFlags, HideFlags childFlags)
        {
            go.hideFlags = thisGameObjectFlags;

            foreach (var c in go.GetComponents<Component>())
            {
                if (c == null) 
                    continue;
                if (c is Transform)
                    continue;

                c.hideFlags = thisComponentFlags;
            }
            foreach (var child in go.Children())
                child.gameObject.SetHideFlagsRecursively(childFlags);
        }
#endif
        // untested methods:
        //static readonly List<Component> m_componentCache = new List<Component>();
        ///// <summary>
        ///// Grabs a component without allocating memory uselessly
        ///// </summary>
        ///// <param name="this"></param>
        ///// <param name="componentType"></param>
        ///// <returns></returns>
        //public static Component GetComponentNoAlloc(this GameObject @this, System.Type componentType)
        //{
        //    @this.GetComponents(componentType, m_componentCache);
        //    var component = m_componentCache.Count > 0? m_componentCache[0] : null;
        //    m_componentCache.Clear();
        //    return component;
        //}
        ///// <summary>
        ///// Grabs a component without allocating memory uselessly
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="this"></param>
        ///// <returns></returns>
        //public static T GetComponentNoAlloc<T>(this GameObject @this) where T : Component
        //{
        //    @this.GetComponents(typeof(T), m_componentCache);
        //    var component = m_componentCache.Count > 0? m_componentCache[0] : null;
        //    m_componentCache.Clear();
        //    return component as T;
        //}
    }
}
