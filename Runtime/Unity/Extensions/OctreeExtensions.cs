﻿using Core.Interface;
using Core.Unity.Interface;
using Core.Unity.Utility.Debug;
using Core.Unity.Types;
using UnityEngine;

namespace Core.Extensions
{
    public static class OctreeExtensions
    {
        /// <summary> draws Octree </summary>
        public static void DrawBoxLines<T>(this Octree<T> tree, Color color) where T : IProvider<Bounds>
        {
            if (!tree.HasLeaves)
            {
                CustomDebugDraw.DrawBounds(tree.Bounds, color);
                return;
            }
            foreach (var childNode in tree.LeafNodes)
                childNode.DrawBoxLines(color);
        }
    }
}