using System.Collections.Generic;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;

public static class ImageExtensions
{
    public static void SetSprite(this Image image, Sprite sprite)
    {
        image.sprite = sprite;
    }
    public static void SetSprite(this IEnumerable<Image> image, Sprite sprite)
    {
        if (image.IsNullOrEmpty())
            return;

        foreach (var i in image)
        {
            if (i == null)
                continue;
            i.sprite = sprite;
        }
    }

    public static void SetEnabled(this IEnumerable<Image> image, bool enabled)
    {
        if (image.IsNullOrEmpty())
            return;

        foreach (var i in image)
        {
            if (i == null)
                continue;
            i.enabled = enabled;
        }
    }

    public static void SetColor(this Image image, Color color) => image.color = color;

    public static void SetColor(this IEnumerable<Image> image, Color color)
    {
        if (image.IsNullOrEmpty())
            return;

        foreach (var i in image)
        {
            if (i == null)
                continue;
            i.color = color;
        }
    }
}