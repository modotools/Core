using System.Collections.Generic;
using Core.Extensions;
using UnityEngine;
using UnityEngine.UI;

public static class TMPExtensions
{
    public static void SetText(this TMPro.TMP_Text ui_text, string text)
    {
        if (ui_text == null)
            return;

        ui_text.text = text;
    }

    public static void SetText(this IEnumerable<TMPro.TMP_Text> ui_text, string text)
    {
        if (ui_text.IsNullOrEmpty())
            return;

        foreach (var t in ui_text)
        {
            if (t == null)
                continue;
            t.text = text;
        }
    }
}