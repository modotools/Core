﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;
using static Core.Unity.Utility.PoolAttendant.PoolAttendantExtension;

namespace Core.Unity.Extensions
{
    public static class TransformExtensions 
    {
        public static IEnumerable<Transform> Children(this Transform tr)
        {
            for (var i = 0; i < tr.childCount; i++)
                yield return tr.GetChild(i);
        }
        public static IEnumerable<Transform> Children_ReverseIt(this Transform tr)
        {
            for (var i = tr.childCount-1; i >=0; --i)
                yield return tr.GetChild(i);
        }
        /// <summary> Saves few lines of code </summary>
        public static void CreateChildObject(this Transform parent, string name, 
            out GameObject createdGameObject, 
            HideFlags hideFlags = HideFlags.None, params Type[] components) => parent.CreateChildObject(name, 
            out createdGameObject, out _, 
            hideFlags, components);

        /// <summary> Saves few lines of code </summary>
        public static void CreateChildObject(this Transform parent, string name, 
            out Transform createdTransform, 
            HideFlags hideFlags = HideFlags.None, params Type[] components) => parent.CreateChildObject(name, 
            out _, out createdTransform, 
            hideFlags,  components);

        /// <summary> Saves few lines of code </summary>
        public static void CreateChildObject(this Transform parent, string name, 
            out GameObject createdGameObject, out Transform createdTransform, 
            HideFlags hideFlags = HideFlags.None, params Type[] components)
        {
            createdGameObject = new GameObject(name, components) { hideFlags = hideFlags };
            createdTransform = createdGameObject.transform;
            createdTransform.SetParent(parent, false);
        }

        /// <summary> Saves few lines of code </summary>
        public static void CreatePooledChildObject(this Transform parent, GameObject prefab, string name, 
            out GameObject createdGameObject, out Transform createdTransform, 
            HideFlags hideFlags = HideFlags.None)
        {
            // prefab probably has to be setup explicitly-managed
            createdGameObject = prefab.GetPooledInstance();
            createdGameObject.name = name;
            createdGameObject.hideFlags = hideFlags;
            createdTransform = createdGameObject.transform;
            createdTransform.SetParent(parent, false);
        }

        public static int GetActiveChildCount(this Transform parent)
        {
            var childCount = 0;
            for (var i = 0; i < parent.childCount; i++)
            {
                if (parent.GetChild(i).gameObject.activeSelf)
                    childCount++;
            }
            return childCount;
        }

        public static void DestroyAllChildren(this Transform transform)
        {
            for (var t = transform.childCount - 1; t >= 0; t--)
                transform.GetChild(t).gameObject.DestroyEx();
        }

        public static void LookAtFlat(this Transform transform, Vector3 point)
        {
            var dir = transform.position.Direction(point);
            var rotation = Quaternion.LookRotation(dir.Flattened());
            transform.rotation = rotation;
        }

        public static IEnumerable<Transform> Parents(this Transform transform)
        {
            var tr = transform;
            while (tr.parent != null)
            {
                tr = tr.parent;
                yield return tr;
            }
        }

        public static bool IsInParents(this Transform transform, Transform parent)
        {
            var foundTr = transform.Parents().FirstOrDefault(p => p == parent);
            return foundTr != null;
        }

    }
}
