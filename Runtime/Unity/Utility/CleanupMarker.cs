

using UnityEngine;

namespace Core.Unity.Utility
{
    /// <inheritdoc />
    /// <summary>
    /// Used for Cleaning stuff up, that is only used in Editor context
    /// </summary>
    public class CleanupMarker : MonoBehaviour
    {
    }
}