namespace Core.Unity.Utility.Debug
{
    public enum DebugMode
    {
        Off,
        On,
        All
    }
}