using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Types.ID;
using UnityEngine;

namespace Core.Unity.Utility.Debug
{
    public static class DebugTools
    {
#if UNITY_EDITOR
        public static readonly Dictionary<DebugID, DebugMode> Editor_DebugTagSettings = new Dictionary<DebugID, DebugMode>();

        public static int Editor_DebugVerbosityLevel = 0;
        public static int Editor_DebugFrameMin;
        public static int Editor_DebugFrameMax;
#endif

        /// <summary>
        /// Will compile to false for game and discard the debug code from application
        /// </summary>
        public static bool EDITOR_IF(bool condition)
        {
#if UNITY_EDITOR
            return condition;
#endif
            return false;
        }

        /// <summary>
        /// Will compile to false for game and discard the debug code from application
        /// </summary>
        public static bool DEBUGGING(this IDebuggable debug, int verbosity = 3, bool frameDebug = false)
        {
#if UNITY_EDITOR
            if (verbosity < Editor_DebugVerbosityLevel)
                return false;

            var frameOk = !frameDebug || (Time.frameCount >= Editor_DebugFrameMin && Time.frameCount <= Editor_DebugFrameMax);
            var modeGO = debug.Mode();
            if (modeGO == DebugMode.All)
                return frameOk;
            if (debug.DebugID == null || !Editor_DebugTagSettings.TryGetValue(debug.DebugID, out var mode))
                return false;

            switch (mode)
            {
                case DebugMode.Off: return false;
                case DebugMode.On: return frameOk && modeGO != DebugMode.Off;
                case DebugMode.All: return frameOk;
            }
#endif
            return false;
        }
    }
}