using System;
using Core.Unity.Interface;
using UnityEditor;
using UnityEngine;

namespace Core.Unity.Utility
{
    public class HierarchyHighlighter : MonoBehaviour, IHierarchyHighlighter
    {
        public static readonly Color Default_Background_Color = new Color(0.2196079f, 0.2196079f, 0.2196079f, 1f);

        public Color DefaultBackgroundColor => Default_Background_Color;

        static readonly Color k_default_background_color_inactive = new Color(0.306f, 0.396f, 0.612f, 1f);
        static readonly Color k_default_text_color = new Color(0.7843137f, 0.7843137f, 0.7843137f,1f);

        [SerializeField] HierarchyStyleData m_defaultState = new HierarchyStyleData()
        {
            Active = true,
            TextColor = k_default_text_color,
            BackgroundColor = Default_Background_Color,
            TexStyle = FontStyle.Normal
        };
        
        [SerializeField] HierarchyStyleData m_activeState;
        [SerializeField] HierarchyStyleData m_inactiveState;

        public HierarchyStyleData DefaultStyle => m_defaultState;
        public HierarchyStyleData SelectedStyle => m_activeState;
        public HierarchyStyleData InactiveStyle => m_inactiveState;

        public void SetStyles(HierarchyStyles styles)
        {
            m_defaultState = styles.DefaultStyle;
            m_activeState = styles.SelectedStyle;
            m_inactiveState = styles.InactiveStyle;
        }
    }

    [Serializable]
    public struct HierarchyStyles
    {
        public HierarchyStyleData DefaultStyle;
        public HierarchyStyleData SelectedStyle;
        public HierarchyStyleData InactiveStyle;

        public bool AnyActive => DefaultStyle.Active || SelectedStyle.Active || InactiveStyle.Active;
    }
    
    [Serializable]
    public struct HierarchyStyleData
    {
        public bool Active;

        public Color BackgroundColor;
        public Color TextColor;
        public FontStyle TexStyle;
    }
    
    public interface IHierarchyHighlighter : IComponent
    {
        Color DefaultBackgroundColor { get; }
        
        public HierarchyStyleData DefaultStyle { get; }
        public HierarchyStyleData SelectedStyle { get; }
        public HierarchyStyleData InactiveStyle { get; }
    }

}