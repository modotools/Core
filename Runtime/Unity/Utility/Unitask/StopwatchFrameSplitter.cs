using System.Diagnostics;
using Core.Editor;
using UnityEngine;

#if UNITASK
using System.Threading;
using Cysharp.Threading.Tasks;
#endif

namespace Core.Unity.Utility
{
    [CreateAssetMenu(fileName = nameof(StopwatchFrameSplitter),
        menuName = MenuStrings.CreateCoreAsset +"/"+ nameof(StopwatchFrameSplitter))]
    public class StopwatchFrameSplitter : ScriptableObject
    {
        [SerializeField] int m_allowedMillisecondsPerFrame = 30;
        readonly Stopwatch m_timeOnFrame = new Stopwatch();

#if UNITASK
        public async UniTask SplitFrame(CancellationToken cancellationToken)
        {
            if (m_timeOnFrame.Elapsed.Milliseconds > m_allowedMillisecondsPerFrame)
            {
                await UniTask.WaitForEndOfFrame(cancellationToken);
                ResetWatch();
            }
        }
#endif

        public void ResetWatch()
        {
            m_timeOnFrame.Reset();
            m_timeOnFrame.Start();
        }
    }
}