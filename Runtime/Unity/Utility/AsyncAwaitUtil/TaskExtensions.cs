using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;

namespace Core.Unity.Utility.AsyncAwaitUtil
{
    public static class TaskExtensions
    {
        public static IEnumerator AsIEnumerator(this Task task)
        {
            while (!task.IsCompleted)
                yield return null;
            

            if (task.IsFaulted)
                ExceptionDispatchInfo.Capture(task.Exception ?? throw new InvalidOperationException()).Throw();
        }

        public static IEnumerator<T> AsIEnumerator<T>(this Task<T> task)
            where T : class
        {
            while (!task.IsCompleted)
                yield return null;

            if (task.IsFaulted)
                ExceptionDispatchInfo.Capture(task.Exception ?? throw new InvalidOperationException()).Throw();

            yield return task.Result;
        }
    }
}
