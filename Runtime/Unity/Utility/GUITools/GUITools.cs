using System;
using UnityEngine;

namespace Core.Unity.Utility.GUITools
{
    public static class GUITools
    {
        static readonly Texture2D k_backgroundTexture = Texture2D.whiteTexture;
        static readonly GUIStyle k_textureStyle = new GUIStyle { normal = new GUIStyleState { background = k_backgroundTexture } };

        public static void DrawRect(Rect position, Color color, GUIContent content = null)
        {
            var backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
            GUI.Box(position, content ?? GUIContent.none, k_textureStyle);
            GUI.backgroundColor = backgroundColor;
        }

        public static void LayoutBox(Color color, GUIContent content = null)
        {
            var backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
            GUILayout.Box(content ?? GUIContent.none, k_textureStyle);
            GUI.backgroundColor = backgroundColor;
        }

    }

    //copied from UnityEngine.GUI, because it was internal
    public struct ColorScope : IDisposable
    {
        bool m_disposed;
        readonly Color m_previousColor;
        readonly Color m_previousGizmoColor;

#if UNITY_EDITOR
        readonly Color m_previousHandleColor;
#endif

        public ColorScope(Color newColor)
        {
            m_disposed = false;
            m_previousColor = GUI.color;
            m_previousGizmoColor = Gizmos.color;

            GUI.color = newColor;
            Gizmos.color = newColor;
#if UNITY_EDITOR
            m_previousHandleColor = UnityEditor.Handles.color;
            UnityEditor.Handles.color = newColor;
#endif
        }

        public ColorScope(float r, float g, float b, float a = 1f) 
            => this = new ColorScope(new Color(r, g, b, a));

        public void Dispose()
        {
            if (m_disposed)
                return;
            m_disposed = true;
            GUI.color = m_previousColor;
            Gizmos.color = m_previousGizmoColor;

#if UNITY_EDITOR
            UnityEditor.Handles.color = m_previousHandleColor;
#endif
        }
    }
    //copied from UnityEngine.GUI, because it was internal
    public struct BackgroundColorScope : IDisposable
    {
        bool m_disposed;
        readonly Color m_previousColor;

        public BackgroundColorScope(Color newColor)
        {
            m_disposed = false;
            m_previousColor = GUI.backgroundColor;
            GUI.backgroundColor = newColor;
        }

        public BackgroundColorScope(float r, float g, float b, float a = 1f) 
            => this = new BackgroundColorScope(new Color(r, g, b, a));

        public void Dispose()
        {
            if (m_disposed)
                return;
            m_disposed = true;
            GUI.backgroundColor = m_previousColor;
        }
    }

    public class HandleGUIScope : IDisposable
    {
        bool m_disposed;

        public HandleGUIScope()
        {
            m_disposed = false;
#if UNITY_EDITOR
            UnityEditor.Handles.BeginGUI();
#endif
        }

        public void Dispose()
        {
            if (m_disposed)
                return;
            m_disposed = true;
#if UNITY_EDITOR
            UnityEditor.Handles.EndGUI();
#endif
        }
    }

    public class HorizontalRectScope : IDisposable
    {
        public Rect Rect;

        public HorizontalRectScope(ref Rect pos, float height)
        {
            Rect = new Rect(pos.x, pos.y, pos.width, height);
            pos.y += height;
            pos.height -= height;
        }

        public Rect Get(float w)
        {
            var returnRect = new Rect(Rect.x, Rect.y, w, Rect.height);
            Rect.x += w;
            Rect.width -= w;
            return returnRect;
        }
        public void Dispose() { }
    }

    public class VerticalRectScope : IDisposable
    {
        public Rect Rect;

        public VerticalRectScope(ref Rect pos, float height)
        {
            Rect = new Rect(pos.x, pos.y, pos.width, height);
            pos.y += height;
            pos.height -= height;
        }

        public Rect Get(float h)
        {
            var returnRect = new Rect(Rect.x, Rect.y, Rect.width, h);
            Rect.y += h;
            Rect.height -= h;
            return returnRect;
        }
        public void Dispose() { }
    }


}