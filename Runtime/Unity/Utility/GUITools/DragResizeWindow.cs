﻿using System;
using UnityEngine;

namespace Core.Unity.Utility.GUITools
{
    public class DragResizeWindow
    {
        protected DragResizeWindow(GUISkin skin, Rect windowRect, string title, Action onClose)
        {
            m_skin = skin;
            WindowRect = windowRect;
            m_title = title;
            m_controlId = GUIUtility.GetControlID(FocusType.Passive);
            m_onClose = onClose;
        }

        readonly GUISkin m_skin;
        readonly string m_title;
        readonly Action m_onClose;

        bool m_isResize;
        Rect m_resizeStart;
        int m_controlId;

        GUIStyle ButtonStyle => m_skin.box;
        public Rect WindowRect { get; private set; }

        protected virtual void OnGUI() { }

        public void DrawWindow()
        {
            // Register the window.
            WindowRect = GUI.Window(m_controlId, WindowRect, DraggableGUI, m_title);

            CloseButton();

            WindowRect = ResizeWindow(WindowRect, ref m_isResize, ref m_resizeStart, new Vector2(100, 100));
        }

        void CloseButton()
        {
            var closeContent = new GUIContent("x");
            var closeButtonRect = new Rect(new Vector2(WindowRect.max.x, WindowRect.y), ButtonStyle.CalcSize(closeContent));
            if (GUI.Button(closeButtonRect, closeContent, ButtonStyle))
                m_onClose.Invoke();
        }

        // Make the contents of the window
        void DraggableGUI(int windowID)
        {
            // This will make the window be resizable by the top
            // title bar - no matter how wide it gets.
            OnGUI();
            GUI.DragWindow(new Rect(0, 0, 10000, 100));

        }

        Rect ResizeWindow(Rect windowRect, ref bool isResizing, ref Rect resizeStart, Vector2 minWindowSize)
        {
            var drag = new GUIContent("↨");
            var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

            var dragButtonRect = new Rect(windowRect.max, ButtonStyle.CalcSize(drag));
            //if (Event.current.type == EventType.MouseDown)
            //    Debug.Log($"{mouse} | {r.position} | {r.size} | {Event.current.type}");

            // ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
            switch (Event.current.type)
            {
                case EventType.MouseDown when dragButtonRect.Contains(mouse):
                    isResizing = true;
                    resizeStart = new Rect(mouse.x, mouse.y, windowRect.width, windowRect.height);
                    //Event.current.Use();// the GUI.Button below will eat the event, and this way it will show its active state
                    break;
                case EventType.MouseUp when isResizing:
                    isResizing = false;
                    break;
                default:
                    if (!Input.GetMouseButton(0))
                    {
                        // if the mouse is over some other window we won't get an event, this just kind of circumvents that by checking the button state directly
                        isResizing = false;
                    }
                    else if (isResizing)
                    {
                        windowRect.width = Mathf.Max(minWindowSize.x, resizeStart.width + (mouse.x - resizeStart.x));
                        windowRect.height = Mathf.Max(minWindowSize.y, resizeStart.height + (mouse.y - resizeStart.y));
                        windowRect.xMax =
                            Mathf.Min(Screen.width, windowRect.xMax); // modifying xMax affects width, not x
                        windowRect.yMax =
                            Mathf.Min(Screen.height, windowRect.yMax); // modifying yMax affects height, not y
                    }

                    break;
            }
            GUI.Button(dragButtonRect, drag, ButtonStyle);
            return windowRect;
        }
    }
}