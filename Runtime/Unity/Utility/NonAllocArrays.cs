using UnityEngine;

namespace Core.Utility
{
    public static class NonAllocArrays
    {
        const int k_maxCollider = 50;
        public static readonly Collider[] Collider = new Collider[k_maxCollider];
        public static readonly RaycastHit[] Hits = new RaycastHit[k_maxCollider];
    }
}