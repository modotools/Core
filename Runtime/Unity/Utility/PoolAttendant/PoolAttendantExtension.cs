using Core.Unity.Extensions;
using Core.Unity.Interface;
using UnityEngine;

namespace Core.Unity.Utility.PoolAttendant
{
    public static class PoolAttendantExtension
    {
        public static GameObject GetPooledInstance(this MonoBehaviour mono) => mono.gameObject.GetPooledInstance();

        public static GameObject GetPooledInstance(this MonoBehaviour mono, Vector3 position) => mono.gameObject.GetPooledInstance(position, Quaternion.identity);

        public static GameObject GetPooledInstance(this MonoBehaviour mono, Vector3 position, Quaternion rotation) => mono.gameObject.GetPooledInstance(position, rotation, Vector3.one);

        public static GameObject GetPooledInstance(this MonoBehaviour mono, Vector3 position, Quaternion rotation,
            Vector3 scale) =>
            mono.TryGetComponent<ISpawnAPrefab>(out var marker)
                ? marker.GetInstance() 
                : Pool.Instance.Get(mono.gameObject, position, rotation, scale);

        public static T GetPooledInstance<T>(this MonoBehaviour mono) where T : Component => 
            mono.gameObject.GetPooledInstance<T>(Vector3.zero);

        public static T GetPooledInstance<T>(this MonoBehaviour mono, Vector3 position) where T : Component => 
            mono.gameObject.GetPooledInstance<T>(position, Quaternion.identity);

        public static T GetPooledInstance<T>(this MonoBehaviour mono, Vector3 position, Quaternion rotation)
            where T : Component =>
            mono.gameObject.GetPooledInstance<T>(position, rotation, Vector3.one);

        public static T GetPooledInstance<T>(this MonoBehaviour mono, Vector3 position, Quaternion rotation,
            Vector3 scale) where T : Component =>
            Pool.Instance.Get<T>(mono.TryGetComponent<ISpawnAPrefab>(out var marker) 
                ? marker.Prefab : mono.gameObject, position, rotation, scale);

        public static GameObject GetPooledInstance(this GameObject prefab) => 
            prefab.GetPooledInstance(Vector3.zero);

        public static GameObject GetPooledInstance(this GameObject prefab, Vector3 position) 
            => prefab.GetPooledInstance(position, Quaternion.identity);

        public static GameObject GetPooledInstance(this GameObject prefab, Vector3 position, Quaternion rotation, string name = null) 
            => prefab.GetPooledInstance(position, rotation, prefab.transform.localScale, name);

        public static GameObject GetPooledInstance(this GameObject prefab, Vector3 position, Quaternion rotation,
            Vector3 scale, string name = null)
        {
            if (IsEditor_DefaultInstantiate(prefab, position, rotation, scale, name, out var instance))
                return instance;
            return prefab.TryGetComponent<ISpawnAPrefab>(out var marker) 
                ? marker.GetInstance() 
                : Pool.Instance.Get(prefab, position, rotation, scale, name);
        }
        
        public static T GetPooledInstance<T>(this GameObject prefab) where T : Component => 
            prefab.GetPooledInstance<T>(Vector3.zero);

        public static T GetPooledInstance<T>(this GameObject prefab, Vector3 position) where T : Component => 
            prefab.GetPooledInstance<T>(position, Quaternion.identity);

        public static T GetPooledInstance<T>(this GameObject prefab, Vector3 position, Quaternion rotation)
            where T : Component =>
            prefab.GetPooledInstance<T>(position, rotation, Vector3.one);

        public static T GetPooledInstance<T>(this GameObject prefab, Vector3 position, Quaternion rotation,
            Vector3 scale) where T : Component
        {
            if (IsEditor_DefaultInstantiate(prefab, position, rotation, scale, null, out var instance))
                return instance.Get<T>();
            return Pool.Instance.Get<T>(prefab.TryGetComponent<ISpawnAPrefab>(out var marker)
                ? marker.Prefab
                : prefab, position, rotation, scale);
        }
        
        static bool IsEditor_DefaultInstantiate(GameObject prefab, Vector3 position, Quaternion rotation, Vector3 scale, string name, out GameObject instance)
        {
            instance = null;
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                instance = Object.Instantiate(prefab, position, rotation);
                if (name != null)
                    instance.name = name;
                instance.transform.localScale = scale;
                return true;
            }
#endif

            return false;
        }

    }
}