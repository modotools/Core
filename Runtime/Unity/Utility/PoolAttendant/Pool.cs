using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Extensions;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Core.Unity.Utility.PoolAttendant
{
    struct PoolData
    {
        // an explicitly managed Pool-item is only free when their parent is Parent of the pool 
        public bool ExplicitlyManaged;

        public Transform Parent;
        public List<PooledItem> PooledItems;
    }

    struct PooledItem
    {
        public GameObject GameObject;
        public Transform Transform;
    }

    public class Pool
    {
        public const string SettingsLoadPath = "PoolSettings";
        const string k_poolName = "Pool";

        static Pool m_instance;

        readonly Dictionary<int, PoolData> m_poolData = new Dictionary<int, PoolData>();

        Transform m_container;
        PoolSettings m_settings;

        public static Pool Instance
        {
            get
            {
                if (m_instance != null) return m_instance;

                m_instance = new Pool
                {
                    m_container = new GameObject(k_poolName).transform,
                    m_settings = Resources.Load<PoolSettings>(SettingsLoadPath)
                };

                m_instance.m_container.position = new Vector3(0, 0, 0);

                if (m_instance.m_settings == null)
                    UnityEngine.Debug.LogWarning(
                        "PoolSettings not found at path \"Resources\\PoolSettings\", " +
                        "if you want to populate the pool with initial values you can create the" +
                        "settings with [\"Tools\\Pool\\Create PoolSettings\"]");

                if (m_instance.m_settings.AutoCreateDefaultItems)
                    m_instance.CreateDefaultItems();

                //SceneManager.activeSceneChanged += m_instance.DeactivateAllObjects;

                return m_instance;
            }
        }

        public static GameObject Initialize() => Instance.m_container.gameObject;

        public void DelayInactivate(GameObject prefab, int delayFrames)
        {
            var idx = Array.FindIndex(m_settings.Items, a => a.Prefab == prefab);
            if (idx == -1)
            {
                var l = m_settings.Items.ToList();
                var item = new DefaultPoolItem() {DelayInactivateForFrames = delayFrames, Prefab = prefab, Size = 5};
                l.Add(item);
                m_settings.Items = l.ToArray();
                return;
            }

            m_settings.Items[idx].DelayInactivateForFrames = delayFrames;
        }

        public int GetDelayFrames(GameObject prefab)
        {
            var item = Array.Find(m_settings.Items, a => a.Prefab == prefab);
            return item.DelayInactivateForFrames;
        }

        public void CreateDefaultItems()
        {
            foreach (var item in m_settings.Items) 
                Init(item);
        }
        public void Init(DefaultPoolItem item)
        {
            if (item.Prefab == null) 
                return;

            var key = item.Prefab.GetInstanceID();
            if (m_poolData.TryGetValue(key, out var poolData)) 
                return;

            m_poolData.Add(key, CreatePoolData(item.Prefab, item.ExplicitlyManaged));
            InstantiateListAndGetFirst(item.Prefab, item.Size);
        }

        //void DeactivateAllObjects(Scene currentScene, Scene newScene) => DeactivateAllObjects();
        public void DeactivateAllObjects()
        {
            foreach (var pool in m_poolData.Values)
            foreach (var p in pool.PooledItems)
                if (p.GameObject != null)
                    p.GameObject.SetActive(false);
        }

        public void DestroyAllObjects()
        {
            foreach (var pool in m_poolData.Values)
            {
                foreach (var p in pool.PooledItems)
                    if (p.GameObject != null)
                        p.GameObject.DestroyEx();
                pool.PooledItems.Clear();
            }
        }

        public void Clear() => m_poolData.Clear();

        GameObject InstantiateListAndGetFirst(GameObject prefab, int size = 1)
        {
            var key = prefab.GetInstanceID();
            var exists = m_poolData.TryGetValue(prefab.GetInstanceID(), out var poolData);
            if (!exists)
                m_poolData.Add(key, CreatePoolData(prefab, false));

            var items= m_poolData[key].PooledItems;
            for (var i = items.Count; i < size; i++)
            {
                var obj = CreateNew(prefab);
                obj.name = $"{prefab.name} {items.Count}";
                //UnityEngine.Debug.Log($"Created {obj.name}");
                items.Add(new PooledItem()
                {
                    GameObject = obj,
                    Transform = obj.transform
                });
            }

            m_poolData[key] = poolData;
            return items.FirstOrDefault().GameObject;
        }

        GameObject CreateNew(GameObject prefab)
        {
            var id = prefab.GetInstanceID();

            if (!m_poolData.TryGetValue(id, out var poolData))
                m_poolData.Add(id, CreatePoolData(prefab, false));

            var obj = Object.Instantiate(
                prefab,
                prefab.transform.position,
                prefab.transform.rotation,
                m_poolData[id].Parent);

            if (!obj.TryGetComponent(out PoolEntity pe))
                pe = obj.AddComponent<PoolEntity>();
            pe.Prefab = prefab;

            obj.SetActive(false);
            return obj;
        }

        PoolData CreatePoolData(GameObject prefab, bool explicitlyManaged)
        {
            return new PoolData()
            {
                Parent = CreateParent(prefab.name),
                ExplicitlyManaged = explicitlyManaged,
                PooledItems = new List<PooledItem>(),
            };
        }

        Transform CreateParent(string name)
        {
            var childContainer = new GameObject(name + string.Empty + k_poolName).transform;
            childContainer.transform.SetParent(m_container);

            return childContainer;
        }

        // ReSharper disable once MethodNameNotMeaningful
        internal GameObject Get(GameObject prefab, Vector3 position, Quaternion rotation, Vector3 scale, string name = null)
        {
            GameObject obj;

            var key = prefab.GetInstanceID();
            if (!m_poolData.TryGetValue(key, out var poolData))
            {
                m_poolData.Add(key, CreatePoolData(prefab, false));
                if (m_settings.DebugLogsEnabled)
                    UnityEngine.Debug.LogWarning($"No Pool for {prefab} setup!");
            }

            poolData = m_poolData[key];
            if (poolData.PooledItems.IsNullOrEmpty())
                obj = InstantiateListAndGetFirst(prefab);
            else
            {
                var items = poolData.PooledItems;
                obj = poolData.ExplicitlyManaged
                    ? items.Find(pi => !pi.GameObject.activeSelf && pi.Transform.parent == poolData.Parent).GameObject
                    : items.Find(pi => !pi.GameObject.activeSelf).GameObject;

                if (obj == null)
                {
                    if (m_settings.DebugLogsEnabled)
                        UnityEngine.Debug.LogWarning($"Pool for {prefab} was too small ({items.Count})!");
                    obj = CreateNew(prefab);
                    obj.name = $"{prefab.name} {items.Count}";

                    //UnityEngine.Debug.Log($"Created {obj.name}");
                    items.Add(
                        new PooledItem()
                        {
                            GameObject = obj,
                            Transform = obj.transform
                        });
                }

                if (poolData.ExplicitlyManaged)
                    obj.transform.SetParent(null);

                //else 
                //    UnityEngine.Debug.Log($"using {obj.name}");
            }

            obj.transform.position = position;
            obj.transform.rotation = rotation;
            obj.transform.localScale = scale;

            obj.SetActive(true);
            if (name != null)
                obj.name = name;

            var poolable = obj.GetComponentsInChildren<IPoolable>();
            foreach (var p in poolable)
                p.OnSpawn();

            return obj;
        }

        public T Get<T>(GameObject prefab, Vector3 position, Quaternion rotation, Vector3 scale) where T : Component => 
            Get(prefab, position, rotation, scale).Get<T>();

        public void Reparent(GameObject obj, int instanceId)
        {
            if (!m_poolData.TryGetValue(instanceId, out var poolData)) 
                return;
            if (obj.transform.parent != null && obj.transform.parent.Equals(poolData.Parent)) 
                return;
            obj.transform.SetParent(poolData.Parent, true);
        }

        public void Remove(GameObject gameObject, int instanceId)
        {
            if (!m_poolData.TryGetValue(instanceId, out var poolData))
                return;

            poolData.PooledItems.RemoveAll(pi => pi.GameObject == gameObject);
        }
    }
}