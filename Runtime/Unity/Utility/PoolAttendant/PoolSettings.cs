using System;
using Core.Unity.Attributes;
using UnityEngine;

namespace Core.Unity.Utility.PoolAttendant
{
    public class PoolSettings : ScriptableObject
    {
        [SerializeField, Header("Size           | Prefab             | DelayInactive | ExplicitlyManaged")]
        public DefaultPoolItem[] Items;

        public bool AutoCreateDefaultItems;
        public bool DebugLogsEnabled;

        [InspectorButton("Sort")]
        public bool ButtonBool;

        public void Sort() => Array.Sort(Items, (a, b)
            => string.Compare(a.Prefab.name, b.Prefab.name, StringComparison.Ordinal));
    }

    [Serializable]
    public struct DefaultPoolItem
    {
        public GameObject Prefab;
        public int Size;
        public int DelayInactivateForFrames;

        public bool ExplicitlyManaged;
    }
}