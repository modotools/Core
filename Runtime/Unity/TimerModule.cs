﻿using UnityEngine;

namespace Core.Unity
{
    public class TimerModule : MonoBehaviour, ITimer
    {
        [SerializeField] Timer m_timer;

        public bool IsRunning => m_timer.IsRunning;
        public float P => m_timer.P;
        public RunState State => m_timer.State;

        void Update()
        {
            if (m_timer.IsRunning)
                m_timer.Update(Time.deltaTime);
        }

        public void StartCoolDown() => m_timer.StartCoolDown();
        
        public void StartCoolDown(float time) => m_timer.StartCoolDown(time);
        
        public void StartTimer() => m_timer.StartTimer();

        public void StartTimer(float time) => m_timer.StartTimer(time);
        
        public void Run(float duration, float currentTime, float dir) => m_timer.Run(duration, currentTime, dir);

        public float Percentage() => m_timer.Percentage();

        public bool IsFinished(bool resetIfFinished = true) => m_timer.IsFinished(resetIfFinished);
        
        public void Pause() => m_timer.Pause();

        public void Resume() => m_timer.Resume();

        public void ResumeInDirection(float dir) => m_timer.ResumeInDirection(dir);
        public void ResetTimer() => m_timer.ResetTimer();
    }
}
