﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    public class EnumFlagsAttribute : MultiPropertyAttribute
    {
        //public EnumFlagsAttribute() { }

#if UNITY_EDITOR
        public override void EDITOR_OnGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            property.intValue = EditorGUI.MaskField(position, label, property.intValue, property.enumNames);
        }
#endif
    }
}