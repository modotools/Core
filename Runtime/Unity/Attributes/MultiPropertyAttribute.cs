﻿using System;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public abstract class MultiPropertyAttribute : PropertyAttribute
    {
        public IOrderedEnumerable<MultiPropertyAttribute> Stored = null;

#if UNITY_EDITOR
        public virtual void EDITOR_OnGUI(ref Rect position, SerializedProperty property, GUIContent label) 
            => EditorGUI.PropertyField(position, property, label, true);
        public virtual void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label) { }
        public virtual void EDITOR_OnPostGUI(ref Rect position, SerializedProperty property) { }
        public virtual bool EDITOR_IsVisible(SerializedProperty property) => true;

        public virtual float EDITOR_GetPropertyHeight(SerializedProperty property, GUIContent label, float height)
            => height;
#endif
    }
}
