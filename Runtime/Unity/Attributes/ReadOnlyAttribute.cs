﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    public class ReadOnlyAttribute : MultiPropertyAttribute
    {
        bool m_prevEnabled;

#if UNITY_EDITOR
        public override void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            m_prevEnabled = GUI.enabled;
            GUI.enabled = false;
        }

        public override void EDITOR_OnPostGUI(ref Rect position, SerializedProperty property) => 
            GUI.enabled = m_prevEnabled;
#endif

    }
}
