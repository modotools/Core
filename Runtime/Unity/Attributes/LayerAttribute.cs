﻿using UnityEngine;
using System.Collections.Generic;
using Core.Extensions;
using Core.Types;
using Core.Unity.Utility.GUITools;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    public class LayerAttribute : MultiPropertyAttribute
    {
#if UNITY_EDITOR
        public override void EDITOR_OnGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            using (var strListScoped = SimplePool<List<string>>.I.GetScoped())
            {
                var strList = strListScoped.Obj;
                for(var i = 0; i < 32; i++)
                {
                    var layerN = LayerMask.LayerToName(i);
                    if (!layerN.IsNullOrEmpty())
                        strList.Add(layerN);
                }

                var layerIdx = property.intValue;
                var selectedLayerName = LayerMask.LayerToName(layerIdx);
                using (var hs = new HorizontalRectScope(ref position, 18f))
                {
                    EditorGUI.LabelField(hs.Get(75f), label);
                    var idx = EditorGUI.Popup(hs.Get(100f), strList.IndexOf(selectedLayerName), strList.ToArray());
                    if (idx.IsInRange(strList))
                        property.intValue = LayerMask.NameToLayer(strList[idx]);
                }
            }
        }
#endif
    }
}