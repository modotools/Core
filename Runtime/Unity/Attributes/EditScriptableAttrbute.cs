﻿namespace Core.Unity.Attributes
{
    /// <summary>
    /// Display a ScriptableObject field with an inline editor
    /// </summary>
    public class EditScriptableAttribute : MultiPropertyAttribute
    {

    }
}