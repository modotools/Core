﻿using System.Reflection;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Field)]
    public class ToggleButtonAttribute : MultiPropertyAttribute
    {
        readonly string m_buttonText;
        readonly string m_activeBoolProperty;

        PropertyInfo m_propInfo;
        bool m_currentValue;
        double m_lastClickedTime;

        public ToggleButtonAttribute(string buttonText, string activeBoolProperty)
        {
            m_buttonText = buttonText;
            m_activeBoolProperty = activeBoolProperty;
        }

#if UNITY_EDITOR
        public override void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            float buttonLength = 50 + m_buttonText.Length * 6;
            var buttonRect = new Rect(position.x + (position.width - buttonLength) * 0.5f, position.y, buttonLength,
                position.height);

            EDITOR_GetProperty(property);
            if (m_currentValue == GUI.Toggle(buttonRect, m_currentValue, m_buttonText, "Button"))
                return;

            m_currentValue = !m_currentValue;
            EDITOR_SetProperty(property, m_currentValue);
        }

        public override void EDITOR_OnGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {

        }

        void EDITOR_SetProperty(SerializedProperty prop, bool val)
        {
            if (m_propInfo == null) return;
            m_propInfo.SetMethod.Invoke(prop.serializedObject.targetObject, new[] {(object) val});
            prop.serializedObject.Update();
        }

        void EDITOR_GetProperty(SerializedProperty prop)
        {
            if (m_propInfo != null)
                return;

            var eventOwnerType = prop.serializedObject.targetObject.GetType();
            var boolProp = m_activeBoolProperty;

            m_propInfo = eventOwnerType.GetProperty(boolProp,
                BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            if (m_propInfo == null)
            {
                Debug.LogWarning($"InspectorButton: Unable to find method {boolProp} in {eventOwnerType}");
                return;
            }

            var getter = m_propInfo.GetMethod;
            m_currentValue = (bool) getter.Invoke(prop.serializedObject.targetObject, null);
        }
#endif
    }
}