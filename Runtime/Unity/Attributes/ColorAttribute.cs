﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    public class ColorAttribute : MultiPropertyAttribute
    {
        readonly Color m_color;
        Color m_prevColor;

        public ColorAttribute(float r, float g, float b) => m_color = new Color(r, g, b);

#if UNITY_EDITOR
        public override void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            m_prevColor = GUI.color;
            GUI.color = m_color;
        }

        public override void EDITOR_OnPostGUI(ref Rect position, SerializedProperty property) => GUI.color = m_prevColor;
#endif
    }
}