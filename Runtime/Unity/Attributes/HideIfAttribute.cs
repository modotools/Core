﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
    public class HideIfAttribute : MultiPropertyAttribute
    {
        readonly string m_conditionBoolean;

        public HideIfAttribute(string conditionBoolean) => m_conditionBoolean = conditionBoolean;

#if UNITY_EDITOR
        public override bool EDITOR_IsVisible(SerializedProperty property)
        {
            var hideResult = EDITOR_GetConditionAttributeResult(property);
            return !hideResult;
        }

        bool EDITOR_GetConditionAttributeResult(SerializedProperty property)
        {
            var enabled = true;
            var propertyPath = property.propertyPath;
            var conditionPath = propertyPath.Replace(property.name, m_conditionBoolean);
            var sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);

            if (sourcePropertyValue != null)
                enabled = sourcePropertyValue.boolValue;
            else
                Debug.LogWarning("No matching boolean found for ConditionAttribute in object: " + m_conditionBoolean);

            return enabled;
        }
#endif
    }
}
