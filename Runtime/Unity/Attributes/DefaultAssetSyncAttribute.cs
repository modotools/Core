﻿using System;

namespace Core.Unity.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
    public class DefaultAssetSyncAttribute : MultiPropertyAttribute
    {
        public readonly string SyncVariable_PropertyPath;
        public readonly string ShouldGetStatic_PropertyPath;
        public readonly string ShouldSetStatic_PropertyPath;

        public DefaultAssetSyncAttribute(string syncVariable_propPath, string getStatic_propPath = null, string setStatic_propPath = null)
        {
            SyncVariable_PropertyPath = syncVariable_propPath;
            ShouldGetStatic_PropertyPath = getStatic_propPath;
            ShouldSetStatic_PropertyPath = setStatic_propPath;
        }
    }
}
