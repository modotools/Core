﻿using System;

namespace Core.Unity.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class CopyPasteAttribute : MultiPropertyAttribute
    {
    }
}
