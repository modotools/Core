﻿using System;
using System.Reflection;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{	
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
	public class InspectorButtonAttribute : MultiPropertyAttribute
	{
        readonly string m_methodName;
        MethodInfo m_eventMethodInfo;
        bool m_ignoreDraw;

        public InspectorButtonAttribute(string methodName, bool ignoreDraw = true)
        {
            m_methodName = methodName;
            m_ignoreDraw = ignoreDraw;
        }
#if UNITY_EDITOR

        public override void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            float buttonLength = 50 + m_methodName.Length * 6;
            var buttonRect = new Rect(position.x + (position.width - buttonLength) * 0.5f, position.y, buttonLength, 16f);
            position.y += buttonRect.height;
            if (!GUI.Button(buttonRect, m_methodName))
                return;

            var eventOwnerType = property.serializedObject.targetObject.GetType();
            var eventName = m_methodName;

            if (m_eventMethodInfo == null)
                m_eventMethodInfo = eventOwnerType.GetMethod(eventName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            if (m_eventMethodInfo != null)
                m_eventMethodInfo.Invoke(property.serializedObject.targetObject, null);
            else
                Debug.LogWarning($"InspectorButton: Unable to find method {eventName} in {eventOwnerType}");
        }

        public override float EDITOR_GetPropertyHeight(SerializedProperty property, GUIContent label, float height) 
            => base.EDITOR_GetPropertyHeight(property, label, height) + 16f;

        public override void EDITOR_OnGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            if (!m_ignoreDraw)
                base.EDITOR_OnGUI(ref position, property, label);
        }
#endif
    }

    [Serializable] public struct InspectorButtonData {}
}