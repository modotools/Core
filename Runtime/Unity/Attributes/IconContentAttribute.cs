﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Attributes
{
    public class IconContentAttribute : MultiPropertyAttribute
    {
        readonly GUIContent m_icon;
        //eg. "Prefab Icon"

#if UNITY_EDITOR

        public IconContentAttribute(string contentPath) => 
            m_icon = EditorGUIUtility.IconContent(contentPath);

        public override void EDITOR_OnPreGUI(ref Rect position, SerializedProperty property, GUIContent label) 
            => label.image = m_icon.image;
#else
        public IconContentAttribute(string contentPath) {}

#endif

    }
}