﻿using Core.Unity.Attributes;
using Core.Unity.Types.ID;

namespace Core.Loading
{
    [EditorIcon("icon-id")]
    public class LoadOperationID : IDAsset { }
}
