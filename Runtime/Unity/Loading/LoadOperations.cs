﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using Core.Extensions;
using UnityEngine;

namespace Core.Loading
{
    public static class LoadOperations
    {
        static readonly List<LoadOperationID> k_operationIds = new List<LoadOperationID>();
        public static bool IsLoading => k_operationIds.Count > 0;
        public static void StartLoading(LoadOperationID id)
        {
            if (!k_operationIds.Contains(id))
                k_operationIds.Add(id);
            else
            {
                Debug.LogError($"Already loading {id}\n{Environment.StackTrace} ");
                return;
            }

            EventMessenger.TriggerEvent(new LoadingStarted());
        }

        public static void LoadingDone(LoadOperationID id)
        {
            k_operationIds.Remove(id);
            if (k_operationIds.IsNullOrEmpty())
                EventMessenger.TriggerEvent(new LoadingDone());

            var log = $"Loading Done {id}\n";
            log = k_operationIds.Aggregate(log, (current, opId) => current + $"still loading: {opId}\n");
            Debug.Log(log);
        }
    }

    public readonly struct LoadingStarted { }
    public readonly struct LoadingDone { }

    public readonly struct LoadingScope : IDisposable
    {
        readonly LoadOperationID m_loadOperationID;
        public LoadingScope(LoadOperationID opId)
        {
            m_loadOperationID = opId;
            LoadOperations.StartLoading(opId);
        }

        public void Dispose() =>LoadOperations.LoadingDone(m_loadOperationID);
    }
}
