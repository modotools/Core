﻿using System;
using Core.Unity.Types.Attribute;
using UnityEngine;

namespace Core.Unity.Types
{
    // todo 3: drawer
    [Serializable]
    public struct SerializedEnum
    {
        public SerializedEnum(ClassTypeReference enumType, int intValue, string stringValue)
        {
            m_enumType = enumType;
            m_intValue = intValue;
            m_stringValue = stringValue;
        }

        [SerializeField]
        [ClassTypeConstraint(AllowRegular = false, AllowEnum = true)]
        internal ClassTypeReference m_enumType;
        [SerializeField]
        internal int m_intValue;
        [SerializeField]
        internal string m_stringValue;

        public Type Type => m_enumType.Type;
        public int Value => m_intValue;
        public string StringValue => m_stringValue;
    }
}
