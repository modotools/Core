﻿// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.
// see also ClassTypeReference.cs

using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using UnityEngine;

namespace Core.Unity.Types.Attribute
{
    public interface IClassTypeConstraint
    {
        bool IsConstraintSatisfied(Type type);
    }

    public struct DefaultClassTypeConstraint : IClassTypeConstraint
    {
        public bool AllowRegular;
        public bool AllowValue;
        public bool AllowAbstract;
        public bool AllowInterface;
        public bool AllowEnum;
        public bool AllowNoNamespace;

        public static DefaultClassTypeConstraint Default =>
            new DefaultClassTypeConstraint()
            {
                AllowRegular = true,
                AllowValue = true,
                AllowAbstract = false,
                AllowInterface = false,
                AllowEnum = false,
                AllowNoNamespace = false,
            };

        /// <summary>
        /// Determines whether the specified <see cref="Type"/> satisfies filter constraint.
        /// </summary>
        /// <param name="type">Type to test.</param>
        /// <returns>
        /// A <see cref="bool"/> value indicating if the type specified by <paramref name="type"/>
        /// satisfies this constraint and should thus be selectable.
        /// </returns>
        public bool IsConstraintSatisfied(Type type)
        {
            if (!AllowNoNamespace && type.Namespace.IsNullOrEmpty())
                return false;

            var regular = type.IsClass && !type.IsAbstract && !type.IsInterface;
            return (AllowRegular && regular) ||
                   (AllowValue &&  type.IsValueType) ||
                   (AllowAbstract && type.IsAbstract) ||
                   (AllowInterface && type.IsInterface) ||
                   (AllowEnum && type.IsEnum);
        }
    }

    public struct ClassExtendsTypeConstraint : IClassTypeConstraint
    {
        /// <summary>
        /// the type of class that selectable classes must derive from.
        /// </summary>
        public Type BaseType;

        /// <inheritdoc/>
        public bool IsConstraintSatisfied(Type type) => BaseType.IsAssignableFrom(type) && type != BaseType;
    }

    public struct ClassImplementsTypeConstraint : IClassTypeConstraint
    {
        /// <summary>
        /// Gets the type of interface that selectable classes must implement.
        /// </summary>
        public Type InterfaceType;

        /// <inheritdoc/>
        public bool IsConstraintSatisfied(Type type)
        {
            var iType = InterfaceType;
            return type.GetInterfaces().Any(interfaceType => interfaceType == iType);
        }
    }

    /// <inheritdoc />
    /// <summary>
    /// Base class for class selection constraints that can be applied when selecting
    /// a <see cref="T:Core.Unity.Types.ClassTypeReference" /> with the Unity inspector.
    /// </summary>
    public class ClassTypeConstraintAttribute : PropertyAttribute
    {
        DefaultClassTypeConstraint m_constraint = DefaultClassTypeConstraint.Default;

		public bool AllowRegular { get => m_constraint.AllowRegular; set => m_constraint.AllowRegular = value; }
        public bool AllowValue { get => m_constraint.AllowValue; set => m_constraint.AllowValue = value; }
        public bool AllowAbstract { get => m_constraint.AllowAbstract; set => m_constraint.AllowAbstract = value; }
        public bool AllowInterface { get => m_constraint.AllowInterface; set => m_constraint.AllowInterface = value; }
        public bool AllowEnum { get => m_constraint.AllowEnum; set => m_constraint.AllowEnum = value; }
        public bool AllowNoNamespace { get => m_constraint.AllowNoNamespace; set => m_constraint.AllowNoNamespace = value; }

        public virtual void GetConstraints(List<IClassTypeConstraint> constraints) => constraints.Add(m_constraint);
    }

    /// <inheritdoc />
    /// <summary>
    /// Constraint that allows selection of classes that extend a specific class when
    /// selecting a <see cref="T:Core.ClassTypeReference.ClassTypeReference" /> with the Unity inspector.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class ClassExtendsAttribute : ClassTypeConstraintAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClassExtendsAttribute"/> class.
        /// </summary>
        /// <param name="baseType">Type of class that selectable classes must derive from.</param>
        public ClassExtendsAttribute(Type baseType) => 
            m_constraint = new ClassExtendsTypeConstraint() {BaseType = baseType};

        readonly ClassExtendsTypeConstraint m_constraint;

        public override void GetConstraints(List<IClassTypeConstraint> constraints)
        {
            base.GetConstraints(constraints);
            constraints.Add(m_constraint);
        }
    }

    /// <inheritdoc />
    /// <summary>
    /// Constraint that allows selection of classes that implement a specific interface
    /// when selecting a <see cref="T:Core.ClassTypeReference.ClassTypeReference" /> with the Unity inspector.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class ClassImplementsAttribute : ClassTypeConstraintAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClassImplementsAttribute"/> class.
        /// </summary>
        /// <param name="interfaceType">Type of interface that selectable classes must implement.</param>
        public ClassImplementsAttribute(Type interfaceType) => m_constraint = new ClassImplementsTypeConstraint(){InterfaceType = interfaceType};

        readonly ClassImplementsTypeConstraint m_constraint;

        public override void GetConstraints(List<IClassTypeConstraint> constraints)
        {
            base.GetConstraints(constraints);
            constraints.Add(m_constraint);
        }
    }
}
