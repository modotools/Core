using System;
using UnityEngine;

namespace Core.Unity.Types
{
    [Serializable]
    public class ActivityState<T> : ActivityState where T : Enum
    {
        public void Init(Func<int, T> getIndex, Func<T, int> setIndex)
        {
            m_getIndex = getIndex;
            m_setIndex = setIndex;
        }

        Func<int, T> m_getIndex;
        Func<T, int> m_setIndex;

        public bool Initialized => m_getIndex != null && m_setIndex != null;

        public T State
        {
            get => m_getIndex(m_state);
            set => SetState(m_setIndex(value));
        }
    }

    [Serializable]
    public class ActivityState
    {
        [Serializable]
        public struct ActivityStateData
        {
            public int StateMask;
            public GameObject Target;
        }

        [SerializeField] int m_editorState;
        [SerializeField] ActivityStateData[] m_data;

        protected int m_state = -1;

        protected void SetState(int state)
        {
            if (state.Equals(m_state))
                return;

            m_state = state;
            var stateMask = 1 << state;

            foreach (var d in m_data)
                UpdateActivity(d, stateMask);

#if UNITY_EDITOR
            Editor_SetState(state);
#endif
        }

        void UpdateActivity(ActivityStateData data, int stateMask)
        {
            if (data.Target == null)
                return;

            // todo 3: with transition
            var activate = (data.StateMask & stateMask) == stateMask;
            if (data.Target.activeSelf == activate) 
                return;

            data.Target.SetActive(activate);
        }

        
#if UNITY_EDITOR
        public void Editor_SetState(int editorState) => m_editorState = editorState;
        public static string Editor_DataPropName => nameof(m_data);
        public static string Editor_EditorStatePropName => nameof(m_editorState);
#endif
    }
}
