using System.Collections.Generic;
using System.Linq;
using Core.Editor;
using Core.Extensions;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using Core.Unity.Types.Attribute;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Types.ID
{
    [EditorIcon("icon-id")]
    [CreateAssetMenu(fileName = nameof(IDConfig), menuName = MenuStrings.CreateCoreAsset+"/"+nameof(IDConfig))]
    public class IDConfig : ScriptableObject, IParentMarker
#if UNITY_EDITOR
        , IEnumGenerator
#endif
    {
        [ClassExtends(typeof(IDAsset))]
        public ClassTypeReference IDType; 
        public IDAsset[] Ids = new IDAsset[0];

#if UNITY_EDITOR
#pragma warning disable 0649 // wrong warning for SerializeField
        // optional:
        [SerializeField] internal DefaultAsset m_folder;
        [SerializeField] internal int m_namespaceStart;
        [SerializeField] internal string m_overrideNamespace;
#pragma warning restore 0649 // wrong warning

        public DefaultAsset Folder => m_folder;
        public int NameSpaceStart => m_namespaceStart;
        public string EnumName => name;

        public IEnumerable<string> EnumEntries => Ids.Select(id => id.name);
        public string OutputFile => $"{AssetDatabase.GetAssetPath(Folder)}/{EnumName}.cs";
        public string NameSpace
        {
            get
            {
                if (Folder == null)
                    return "";

                var folder = $"{AssetDatabase.GetAssetPath(Folder)}";
                var folders = folder.Split('/');
                if (folders.Length == 0)
                    return "";

                var nameSpace = m_overrideNamespace;
                if (!nameSpace.IsNullOrEmpty())
                    return nameSpace;
                for (var i = NameSpaceStart; i < (folders.Length - 1); i++) 
                    nameSpace += $"{folders[i]}.";

                nameSpace += folders[folders.Length - 1];
                return nameSpace;
            }
        }
        public static string Editor_FolderPropName => nameof(m_folder);
        public static string Editor_NamespaceStartPropName => nameof(m_namespaceStart);
        public static string Editor_NamespaceOverridePropName => nameof(m_overrideNamespace);
#endif
        public IScriptableObject Parent => this;
    }
}