using Core.Unity.Attributes;
using UnityEngine;

namespace Core.Unity.Types.ID
{
    /// <summary>
    /// Create assets to help with identification and linking
    /// </summary>
    [EditorIcon("icon-id")]
    public class IDAsset : ScriptableObject { }
}