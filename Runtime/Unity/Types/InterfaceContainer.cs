using System;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Unity.Types
{
    [Serializable]
    public abstract class InterfaceContainer<TResult> : InterfaceContainerBase, IInterfaceContainer
        where TResult : class
    {
        
        public TResult Result
        {
            get
            {
#if UNITY_EDITOR
                if (ObjectField == null && string.IsNullOrEmpty(ResultType))
                    return m_result = null;

                if (string.IsNullOrEmpty(ResultType))
                    m_result = null;
#endif

                return m_result ??= ObjectField as TResult;
            }
            set
            {
                m_result = value;
                ObjectField = m_result as Object;

#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    if (m_result != null && ObjectField == null)
                    {
                        Debug.LogWarning("InterfaceContainer: Cannot set Result property to non UnityEngine.Object derived types while application is not running.");
                        m_result = null;
                    }
                }
                ResultType = m_result != null ? GetResolvedName(m_result.GetType()) : "";
#endif
            }
        }

        TResult m_result;
        
        public Type GetContainedType() => typeof(TResult);
        public void SetObject(Object obj) => Result = obj as TResult;
        public Object GetObject() => Result as Object;
    }


    public interface IInterfaceContainer
    {
        Type GetContainedType();
        void SetObject(Object obj);
        Object GetObject();
    }
    
    [Serializable]
    public abstract class InterfaceContainerBase
    {
        [SerializeField]
        [HideInInspector]
        protected Object ObjectField;
        
        [SerializeField]
        [HideInInspector]
        protected string ResultType;

        static readonly Regex k_typeArgumentsReplace = new Regex(@"`[0-9]+");
        public static string GetResolvedName(Type type)
        {
            var typeName = type.Name;

            if (!type.IsGenericType)
                return typeName;

            var argumentsString = type.GetGenericArguments().Aggregate((string)null,
                (s, t) => s == null ? GetResolvedName(t) : $"{s}, {GetResolvedName(t)}");
            return k_typeArgumentsReplace.Replace(typeName, $"<{argumentsString}>");
        }

#if UNITY_EDITOR
        public static string Editor_ObjectFieldPropName => nameof(ObjectField);
#endif
    }
    
}

