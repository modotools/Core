﻿// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.
// see also ClassTypeConstraintAttribute.cs

using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

namespace Core.Unity.Types {

	/// <summary>
	/// Reference to a class <see cref="System.Type"/> with support for Unity serialization.
	/// </summary>
	[Serializable]
	public sealed class ClassTypeReference : ISerializationCallbackReceiver {

		public static string GetClassRef(Type type) {
			return type != null
				? type.FullName + ", " + type.Assembly.GetName().Name
				: "";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ClassTypeReference"/> class.
		/// </summary>
		public ClassTypeReference() {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ClassTypeReference"/> class.
		/// </summary>
		/// <param name="assemblyQualifiedClassName">Assembly qualified class name.</param>
		public ClassTypeReference(string assemblyQualifiedClassName) {
			Type = !string.IsNullOrEmpty(assemblyQualifiedClassName)
				? Type.GetType(assemblyQualifiedClassName)
				: null;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ClassTypeReference"/> class.
		/// </summary>
		/// <param name="type">Class type.</param>
		/// <exception cref="ArgumentException">
		/// If <paramref name="type"/> is not a class type.
		/// </exception>
		public ClassTypeReference(Type type) {
			Type = type;
		}
#pragma warning disable 0649 // wrong warning for SerializeField
		[SerializeField] string m_classRef;
#pragma warning restore 0649 // wrong warning

        #if UNITY_EDITOR
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, UsedImplicitly] Object m_fallbackScriptAsset;
#pragma warning restore 0649 // wrong warning

        public static string Editor_FallbackPropName => nameof(m_fallbackScriptAsset);
        public static string Editor_ClassRefPropName => nameof(m_classRef);
        #endif
        #region ISerializationCallbackReceiver Members

		void ISerializationCallbackReceiver.OnAfterDeserialize() {
			if (!string.IsNullOrEmpty(m_classRef)) {
				m_type = Type.GetType(m_classRef);

				if (m_type == null)
					Debug.LogWarning($"'{m_classRef}' was referenced but class type was not found.");
			}
			else {
				m_type = null;
			}
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize() {
		}

		#endregion

        Type m_type;

		/// <summary>
		/// Gets or sets type of class reference.
		/// </summary>
		/// <exception cref="ArgumentException">
		/// If <paramref name="value"/> is not a class type.
		/// </exception>
		public Type Type {
			get => m_type;
            set {
				if (value != null && !value.IsClass)
					throw new ArgumentException($"'{value.FullName}' is not a class type.", nameof(value));

				m_type = value;
				m_classRef = GetClassRef(value);
			}
		}

		public static implicit operator string(ClassTypeReference typeReference) {
			return typeReference.m_classRef;
		}

		public static implicit operator Type(ClassTypeReference typeReference) {
			return typeReference.Type;
		}

		public static implicit operator ClassTypeReference(Type type) {
			return new ClassTypeReference(type);
		}

		public override string ToString() {
			return (Type != null ? Type.FullName : "(None)") ?? throw new InvalidOperationException();
		}

	}

}
