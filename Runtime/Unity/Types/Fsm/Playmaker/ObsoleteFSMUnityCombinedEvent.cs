﻿// using System;
// using UnityEngine;
// using UnityEngine.Events;
// using UnityEngine.Serialization;
//
// #if CORE_PLAYMAKER
// #pragma warning disable 0649 // wrong warnings for SerializeField
//
// namespace Core.Unity.Types.Fsm.Obsolete
// {
//     [Serializable]
//     public struct FSMUnityCombinedEvent
//     {
//         [SerializeField, FSMDrawer]
//         public FSMEventRef FSMEvent;
//
//         [SerializeField] public UnityEvent UnityEvent;
//
//         public void Invoke()
//         {
//             FSMEvent.Invoke();
//             UnityEvent.Invoke();
//         }
//     }
// }
//
// #else
// using Core.Unity.Types.Fsm;
//
// namespace Core.Unity.Types.FSM
// {
//     public struct FSMUnityCombinedEvent
//     {
//         public FSMEventRef m_fsmEvent;
//         public UnityEvent m_unityEvent;
//
//         public void Invoke(){ }
//     }
// }
// #endif