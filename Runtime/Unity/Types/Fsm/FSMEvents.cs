using Core.Interface;
using UnityEngine;
using JetBrains.Annotations;

using System;
using Core.Extensions;
using Core.Unity.Types.Attribute;

#pragma warning disable 0649 // wrong warnings for SerializeField

namespace Core.Unity.Types.Fsm
{
    public static class FSMEventExt
    {
        internal static bool NoFsm(this IFSMEventRef ev)
        {
            ev.Get(out var fsm);
            return fsm == null;
            //Debug.LogWarning($"FSM for event {ev.EventName} is null");
        }

        public const string FSM_PropName = nameof(FSMEventRef.m_fsm);
        public const string FSM_EventNamePropName = nameof(FSMEventRef.m_eventName);
        public const string FSM_StateNamePropName = nameof(FSMStateRef.m_stateName);
    }

    [Serializable]
    public struct FSMEventRef : IFSMEventRef
    {
        [SerializeField] internal RefIFSMComponent m_fsm;
        [SerializeField] internal string m_eventName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public IFSMComponent FSM => m_fsm.Result;
        public string EventName => m_eventName;

        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Invoke()
        {
            if (this.NoFsm())
            {
                Debug.LogError($"NO FSM for event {m_eventName}");
                return;
            }

            if (m_eventName.IsNullOrEmpty() || m_eventName.Equals(Core.Name.Ignore))
                return;
            FSM.SendEvent(m_eventName);
        }
        // todo 1: Validation

        public void Get(out IFSM provided) => provided = FSM;
        public bool IsSet() => FSM != null && !EventName.IsNullOrEmpty();
    }


    [Serializable]
    public struct FSMStateRef : IFSMStateRef
    {
        [SerializeField] internal RefIFSMComponent m_fsm;
        [SerializeField] internal string m_stateName;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public IFSMComponent FSM => m_fsm.Result;
        public string StateName => m_stateName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public static implicit operator string(FSMStateRef s) => s.StateName;
        public void Get(out IFSM provided) => provided = FSM;
        public bool IsSet() => FSM != null && !StateName.IsNullOrEmpty();
    }

    [BaseTypeRequired(typeof(IProvider<IFSMConfig>))]
    public class FSMDrawerAttribute : ContextDrawerAttribute
    {
        public FSMDrawerAttribute(string autoSelectName = AutoSelectNameDefault,
            Condition setToSelf = ContextSelfDefault, 
            Condition showContext = ShowContextSelectorDefault,
            bool hideLabel = HideLabelDefault,
            bool showOnlyIfUnlinked = ShowOnlyIfUnlinkedDefault,
            bool breakAfterLabel = BreakAfterLabelDefault) : base(autoSelectName, setToSelf, showContext, hideLabel, showOnlyIfUnlinked, breakAfterLabel)
        {}
    }
}