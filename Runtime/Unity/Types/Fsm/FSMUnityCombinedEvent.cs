﻿using System;
using UnityEngine;
using UnityEngine.Events;

#pragma warning disable 0649 // wrong warnings for SerializeField

namespace Core.Unity.Types.Fsm
{
    [Serializable]
    public struct FSMUnityCombinedEvent
    {
        [SerializeField, FSMDrawer]
        public FSMEventRef FSMEvent;

        [SerializeField] public UnityEvent UnityEvent;

        public void Invoke()
        {
            FSMEvent.Invoke();
            UnityEvent.Invoke();
        }
    }
}