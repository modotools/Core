using UnityEngine;

#if NODE_CANVAS
using System;
using NodeCanvas.Framework;

#pragma warning disable 0649 // wrong warnings for SerializeField

namespace Core.Unity.Types.Fsm
{
    public interface INodeCanvasVariableRef
    {
        Blackboard Blackboard { get; }
        string VariableName { get; }
    }
    
    public static class VariableExt
    { 
        internal static bool NoBlackboard(this INodeCanvasVariableRef var)
        {
            if (var.Blackboard != null)
                return false;
            // Debug.LogWarning($"Blackboard for variable {var.VariableName} is null");
            return true;
        }
    }

    [Serializable]
    public struct BBStringRef : INodeCanvasVariableRef
    {
        [SerializeField] Blackboard m_blackboard;
        [SerializeField] string m_variableName;

        [SerializeField] string m_value;
        public string Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public Blackboard Blackboard => m_blackboard;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Sync()
        {
            if (this.NoBlackboard()) return;
            m_value = m_blackboard.GetVariableValue<string>(VariableName);
        }

        public void ApplyOverride()
        {
            if (this.NoBlackboard()) return;

            m_blackboard.SetVariableValue(VariableName, m_value);
        }
    }

    [Serializable]
    public struct BBFloatRef : INodeCanvasVariableRef
    {
        [SerializeField] Blackboard m_blackboard;
        [SerializeField] string m_variableName;

        [SerializeField] float m_value;
        public float Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public Blackboard Blackboard => m_blackboard;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Sync()
        {
            if (this.NoBlackboard()) return;
            m_value = m_blackboard.GetVariableValue<float>(VariableName);
        }

        public void ApplyOverride()
        {
            if (this.NoBlackboard()) return;
            m_blackboard.SetVariableValue(VariableName, m_value);
        }
    }

    [Serializable]
    public struct BBIntRef : INodeCanvasVariableRef
    {
        [SerializeField] Blackboard m_blackboard;
        [SerializeField] string m_variableName;

        [SerializeField] int m_value;
        public int Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public Blackboard Blackboard => m_blackboard;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Sync()
        {
            if (this.NoBlackboard()) return;
            m_value = m_blackboard.GetVariableValue<int>(VariableName);
        }

        public void ApplyOverride()
        {
            if (this.NoBlackboard()) return;
            m_blackboard.SetVariableValue(VariableName, m_value);
        }
    }

    [Serializable]
    public struct BBBoolRef : INodeCanvasVariableRef
    {
        [SerializeField] Blackboard m_blackboard;
        [SerializeField] string m_variableName;

        [SerializeField] bool m_value;
        public bool Value {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public Blackboard Blackboard => m_blackboard;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter
        public void Sync()
        {
            if (this.NoBlackboard()) return;
            m_value = m_blackboard.GetVariableValue<bool>(VariableName);
        }

        public void ApplyOverride()
        {
            if (this.NoBlackboard()) return;
            m_blackboard.SetVariableValue(VariableName, m_value);
        }
    }

    [Serializable]
    public struct BBColorRef : INodeCanvasVariableRef
    {
        [SerializeField] Blackboard m_blackboard;
        [SerializeField] string m_variableName;

        [SerializeField] Color m_value;
        public Color Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public Blackboard Blackboard => m_blackboard;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter
        public void Sync()
        {
            if (this.NoBlackboard()) return;
            m_value = m_blackboard.GetVariableValue<Color>(VariableName);
        }

        public void ApplyOverride()
        {
            if (this.NoBlackboard()) return;
            m_blackboard.SetVariableValue(VariableName, m_value);
        }
    }

    [Serializable]
    public struct BBGameObjectRef : INodeCanvasVariableRef
    {
        [SerializeField] Blackboard m_blackboard;
        [SerializeField] string m_variableName;

        [SerializeField] GameObject m_value;
        public GameObject Value
        {
            get => m_value;
            set
            {
                m_value = value;
                ApplyOverride();
            }
        }
        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public Blackboard Blackboard => m_blackboard;
        public string VariableName => m_variableName;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public void Sync()
        {
            if (this.NoBlackboard()) return;

            m_value = m_blackboard.GetVariableValue<GameObject>(VariableName);
        }

        public void ApplyOverride()
        {
            if (this.NoBlackboard()) return;
            m_blackboard.SetVariableValue(VariableName, m_value);
        }
    }
}
#endif