using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Unity.Types
{
    [Serializable]
    public struct PrefabData
    {
        public bool UseConfig;
        public GameObject PrefabGO;
        public PrefabAsset PrefabAsset;
        public GameObject Prefab => UseConfig ? PrefabAsset.Prefab : PrefabGO;

        public bool IsValidSetup => UseConfig ? PrefabAsset != null : PrefabGO != null;
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(PrefabData))]
    public class PrefabDataDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position = EditorGUI.PrefixLabel(position, label);

            var buttonRect = new Rect(position.x, position.y, 20f, 16f);
            var restRect = new Rect(position.x + 20f, position.y, position.width - 20f, 16f);

            var use = property.FindPropertyRelative(nameof(PrefabData.UseConfig));
            use.boolValue = GUI.Toggle(buttonRect, use.boolValue, "C", "Button");

            //var prevWidth = EditorGUIUtility.labelWidth;
            //EditorGUIUtility.labelWidth = 0f;
            EditorGUI.PropertyField(restRect,
                use.boolValue
                    ? property.FindPropertyRelative(nameof(PrefabData.PrefabAsset))
                    : property.FindPropertyRelative(nameof(PrefabData.PrefabGO)),
                GUIContent.none, true);
            //EditorGUIUtility.labelWidth = prevWidth;
        }
    }
#endif
}
