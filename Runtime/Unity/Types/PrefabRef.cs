﻿using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using Core.Unity.Utility;
using UnityEditor;
#endif

namespace Core.Unity.Types
{
    /// <summary>
    /// Used to preview a prefab without saving/ nesting it in the GameObject or Parent-Prefab.
    /// Then in the game we can f.e. use this to spawn the prefab at the gameObject-position via pool.
    ///
    /// This can be used in multiple ways. F.e. spawn instance once then use it forever. Or use this as
    /// PrefabData and Attachment-Point and detach the instance then GetInstance() will always return
    /// a new instance, because the old instance would be invalid.
    /// </summary>
    public class PrefabRef : MonoBehaviour, ISpawnAPrefab, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PrefabData m_prefabData;
        [SerializeField] bool m_parentAfterSpawn;
        [FormerlySerializedAs("m_spawnOnAwake")] 
        [SerializeField] bool m_spawnOnEnable;
        [SerializeField] bool m_applyLayerOnInstance;

        [SerializeField] bool m_storeInstanceForReuse;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable ConvertToAutoProperty
        public GameObject Prefab => m_prefabData.Prefab;
        public bool ParentAfterSpawn => m_parentAfterSpawn;

        public bool ApplyScale;
        // ReSharper restore ConvertToAutoProperty

        // instance stored for reuse 
        GameObject m_instance;

        // we expect the instance to be active and if parenting enabled, it should still sit under this transform
        bool IsInstanceValid() => (!m_parentAfterSpawn || m_instance.transform.parent == transform)
                                  && m_instance.activeSelf;

        void OnEnable()
        {
            if (m_spawnOnEnable)
                GetInstance();
        }

        public virtual GameObject GetInstance()
        {
            UpdateInstanceValid();
            var tr = transform;

            var instance = m_instance;
            if (instance == null)
            {
                instance = Prefab.GetPooledInstance(tr.position, tr.rotation);
                //Debug.Log($"instantiated with scale: "+ inst.transform.localScale);
                if (m_storeInstanceForReuse)
                    StoreInstance(instance);
            }

            if (ApplyScale)
                instance.transform.localScale = tr.lossyScale;
            if (m_applyLayerOnInstance)
                instance.SetLayer(gameObject.layer, true);
            if (ParentAfterSpawn)
            {
                instance.transform.SetParent(transform, true);
                //Debug.Log($"scale after parenting: "+ m_instance.transform.localScale);
            }
            return instance;
        }

        /// <summary>
        /// If this object gets disabled, disable Prefab as well
        /// </summary>
        void OnDisable()
        {
            if (m_instance == null)
                return;
            // small delay, because TryDespawnOrDestroy
            // would change parenting when activation changed
            // (unity cannot handle this well)
            Invoke(nameof(DespawnInstance), 0.1f);
        }

        void DespawnInstance()
        {
            UpdateInstanceValid();
            if (m_instance == null)
                return;
            m_instance.TryDespawnOrDestroy();
            // don't set instance null here, OnDespawnedEntity will be fired and takes care of it.
        }

        void StoreInstance(GameObject inst)
        {
            m_instance = inst;
            // we want to know when instance gets despawned, so we can set our instance variable null
            if (m_instance.TryGetComponent(out PoolEntity poolEntity))
                poolEntity.AddDespawnAction(OnDespawnedEntity);
        }

        void OnDespawnedEntity()
        {
            if (m_instance == null)
                return;
            ReleaseStoredInstance();
        }

        void UpdateInstanceValid()
        {
            if (m_instance == null)
                return;

            var valid = IsInstanceValid();
            if (valid)
                return;
            ReleaseStoredInstance();
        }

        void ReleaseStoredInstance()
        {
            if (m_instance.TryGetComponent(out PoolEntity poolEntity))
                poolEntity.RemoveDespawnAction(OnDespawnedEntity);
            m_instance = null;
        }

        #region Editor
#if UNITY_EDITOR
        public void Editor_RemovePreview()
        {
            for (var i = transform.childCount - 1; i >= 0; i--) 
                transform.GetChild(i).gameObject.DestroyEx();
        }

        public void Editor_SpawnPreview()
        {
            if (Application.isPlaying)
                return;
            if (Prefab == null)
                return;
            var tr = transform;
            var go = Instantiate(Prefab, tr.position, tr.rotation);
            if (ApplyScale)
                go.transform.localScale = tr.lossyScale;

            go.transform.SetParent(tr, !ParentAfterSpawn);
            go.transform.position = tr.position;
            go.transform.rotation = tr.rotation;

            go.name = "__Unsaved_Preview_" + Prefab.name;
            go.hideFlags = HideFlags.DontSave;
            go.AddComponent<CleanupMarker>();
        }
        public void Editor_Verify(ref VerificationResult result)
        {
            if (Prefab == null)
                result.Error("No Prefab set!", this);

            if (Application.isPlaying || transform.childCount <= 0)
                return;
            for (var i = 0; i < transform.childCount; i++)
            {
                if ((transform.GetChild(i).gameObject.hideFlags & HideFlags.DontSave) != HideFlags.DontSave)
                    result.Error($"Please don't add child-objects under GameObjects with component {nameof(PrefabRef)}! " +
                                   $"It is intended only for editor-preview! Found in {gameObject}", this);
            }
            if (ParentAfterSpawn && !m_storeInstanceForReuse)
                result.Warning("You are parenting the instance, but not storing it. " +
                               "GetInstance() will always create new instances under this object, which might overlap with existing instance.", this);
        }
#endif
        #endregion
    }
}