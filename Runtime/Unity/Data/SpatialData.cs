using UnityEngine;

namespace Core.Unity
{
    public struct SpatialData
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Scale;
    }
}