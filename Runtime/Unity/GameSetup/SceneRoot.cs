﻿using System;
using System.Linq;
using Core.Unity.Interface;
using Core.Unity.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Unity
{
    public class SceneRoot : MonoBehaviour, ISceneRoot, IHierarchyHighlighter
    {
        [Serializable]
        public struct EventData
        {
            public UnityEvent OnSceneActivated;
            public UnityEvent OnSceneDeactivated;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] EventData m_eventData;

        [SerializeField] RefITransitionActivation[] m_transitions;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public bool IsInTransition { get; private set; }
        public bool ActiveOrActivating { get; private set; }

        public void InitActive(bool activated)
        {
            IsInTransition = false;
            ActiveOrActivating = activated;

            gameObject.SetActive(activated);
            foreach (var t in m_transitions)
                t.Result?.InitActive(activated);
            
            if (activated)
                m_eventData.OnSceneActivated?.Invoke();
        }

        void Update() => UpdateTransition();

        void UpdateTransition()
        {
            if (!IsInTransition)
                return;

            if (m_transitions.Any(t => t.Result.IsInTransition))
                return;

            gameObject.SetActive(ActiveOrActivating);

            if (ActiveOrActivating)
                m_eventData.OnSceneActivated.Invoke();
            else
                m_eventData.OnSceneDeactivated.Invoke();

            IsInTransition = false;
        }

        public void SetActive(bool active)
        {
            IsInTransition = true;
            ActiveOrActivating = active;

            if (active)
                gameObject.SetActive(true);

            foreach (var t in m_transitions) 
                t?.Result?.SetActive(active);
        }


        public Color DefaultBackgroundColor => HierarchyHighlighter.Default_Background_Color;

        public HierarchyStyleData DefaultStyle => new HierarchyStyleData()
        {
            Active = true,
            TexStyle = FontStyle.Bold,
            BackgroundColor = DefaultBackgroundColor,
            TextColor = Color.white
        };
        public HierarchyStyleData SelectedStyle => default;
        public HierarchyStyleData InactiveStyle => default;
    }

}