using Core.Unity.Interface;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Unity.Operations
{
    public static class SceneRootOperations
    {
        public static GameObject GetSceneRoot(Scene scene, out ISceneRoot sceneRoot)
        {
            sceneRoot = null;
            var roots = scene.GetRootGameObjects();
            foreach (var r in roots)
            {
                if (r.TryGetComponent(out sceneRoot))
                    return r;
            }

            return null;
        }
    }
}