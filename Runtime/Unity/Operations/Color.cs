﻿using UnityEngine;

namespace Core.Unity.Operations
{
    public static class Color
    {
        public static bool Approximately(UnityEngine.Color color1, UnityEngine.Color color2)
        {
            return Mathf.Approximately(color1.r, color2.r) && Mathf.Approximately(color1.g, color2.g) &&
                   Mathf.Approximately(color1.b, color2.b) && Mathf.Approximately(color1.a, color2.a);
        }

        public static UnityEngine.Color FromIntRGBA(int r, int g, int b, int a) => new UnityEngine.Color(r / 255f, g / 255f, b / 255f, a / 255f);
    }
}
