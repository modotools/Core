using Core.Unity.Types;
using UnityEngine;

namespace Core.Unity.Interface
{
    // todo 3 [PRF_MGC]: automate by iterating over SerializedProperties instead of using interface and implementing it over and over
    public interface ISyncComponents : IComponent
    {
#if UNITY_EDITOR
        void Editor_Sync(GameObject prefabRoot);
#endif
    }

    public static class InterfaceContainerAssignExt
    {
        public static void AssignFrom<T>(this InterfaceContainer<T> c, GameObject root) where T : class => c.Result ??= root.GetComponentInChildren<T>();
    }

}