
using System.Collections.Generic;
using UnityEngine;

namespace Core.Unity.Interface
{
    public interface IScriptableObject : IUnityObject
    {}

    public interface IDependencies : IScriptableObject
    {
        IEnumerable<ScriptableObject> Dependencies { get; }
    }
    public interface IParentMarker : IScriptableObject
    {
        IScriptableObject Parent { get; }
    }
}