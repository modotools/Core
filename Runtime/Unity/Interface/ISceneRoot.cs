namespace Core.Unity.Interface
{
    public interface ISceneRoot : IComponent
    {
        void InitActive(bool activated);
        void SetActive(bool active);
    }
}