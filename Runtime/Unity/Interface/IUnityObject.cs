
namespace Core.Unity.Interface
{
    public interface IUnityObject
    {
        int GetInstanceID();
        // ReSharper disable once InconsistentNaming
        string name { get; set; }
    }
}