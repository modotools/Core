using System;
using Core.Interface;
using Core.Unity.Types;
using UnityEngine;

namespace Core.Unity.Interface
{
    public interface IBoundsComponent : IProvider<Bounds>
    {
        Bounds ObjectBounds { get; }
        Bounds GetObjectBounds(Vector3 pos, Quaternion rot);
    }

    [Serializable]
    public class RefIBoundsComponent: InterfaceContainer<IBoundsComponent> { }

    [Serializable]
    public class RefIBounds: InterfaceContainer<IProvider<Bounds>> { }
}
