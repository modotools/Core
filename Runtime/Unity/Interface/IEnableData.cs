﻿using Core.Interface;
using UnityEngine;

namespace Core.Unity
{
    public interface IEnableData
    {
        bool Enabled { get; set; }
    }
}