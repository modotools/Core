using System;
using Core.Unity.Types;
using UnityEngine;

namespace Core.Unity.Interface
{
    public interface IProvidePrefab
    {
        GameObject Prefab { get; }
    }

    /// <summary>
    /// Use this on GameObjects that are used as AttachmentPoints to spawn prefabs
    /// ! Don't put this on a prefab-root-object
    /// </summary>
    public interface ISpawnAPrefab : IProvidePrefab
    {
        bool ParentAfterSpawn { get; }
        GameObject GetInstance();
    }

    /// <summary>
    /// Used for instances of prefabs
    /// </summary>
    public interface IPrefabInstanceMarker : IProvidePrefab
    {
        new GameObject Prefab { get; set; }
    }

    [Serializable]
    public class RefISpawnAPrefab : InterfaceContainer<ISpawnAPrefab>{}
}
