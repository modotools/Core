﻿using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace Core.Unity.Interface
{

    public interface IVerify
    {
#if UNITY_EDITOR
        void Editor_Verify(ref VerificationResult result);
#endif
    }

    public struct LogData
    {
        public LogData(string str, Object ctx)
        {
            LogString = str;
            Context = ctx;
        }
        public string LogString;
        public Object Context;
    }

    public struct VerificationResult
    {
        public int Warnings => WarningLogs.Count;
        public int Errors => ErrorLogs.Count;

        public List<LogData> InfoLogs;
        public List<LogData> WarningLogs;
        public List<LogData> ErrorLogs;
        public void Info(string str, Object ctx) =>
            InfoLogs.Add(new LogData(str, ctx));
        public void Warning(string str, Object ctx) =>
            WarningLogs.Add(new LogData(str, ctx));
        public void Error(string str, Object ctx) =>
            ErrorLogs.Add(new LogData(str, ctx));

        public bool WasCanceled;

        public static VerificationResult Default => new VerificationResult()
        {
            InfoLogs = new List<LogData>(),
            WarningLogs = new List<LogData>(),
            ErrorLogs = new List<LogData>()
        };

        public void Merge(VerificationResult result)
        {
            InfoLogs.AddRange(result.InfoLogs);
            WarningLogs.AddRange(result.WarningLogs);
            ErrorLogs.AddRange(result.ErrorLogs);
        }
    }
}