﻿using Core.Interface;
using UnityEngine;

namespace Core.Unity.Interface
{
    public interface ITarget : 
        INamed // implement Name for debug purposes
    {
        // if this is unmoving, like a tree, then it is static
        bool IsStatic { get; }
        // f.e. if a target is killed IsTargetingPossible should be false
        bool IsTargetingPossible { get; }
        Vector3 Position { get; }
    }

    public interface ITargetComponent : ITarget, IComponent
    {
    }
}