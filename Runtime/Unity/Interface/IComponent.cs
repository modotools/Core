
using UnityEngine;

namespace Core.Unity.Interface
{
    public interface IComponent
    {
        string name { get; }
        bool enabled { get; set; }
        
        Transform transform { get; }
        GameObject gameObject { get; }
    }
}