﻿using System;
using Core.Unity.Types;
using UnityEngine;

namespace Core.Unity.Interface
{
    public interface IAudioBase
    {
        bool IsPlaying();

        void Play();
        void Stop();

        // currently only used for FMOD
        bool HasParameter(string parameter);
        void SetParameter(string parameter, float value);
    }
    public interface IAudioComponent : IAudioBase {
    }

    public interface IAudioAsset : IAudioBase
    {
        IAudioControl Play(Transform t);
        IAudioControl Play(GameObject g);
        void Stop(Transform t);
        void Stop(GameObject g);
    }

    public interface IAudioControl
    {
        float Volume { get; set; }
        void Stop();
    }

    public interface IAudioListener { }
    //[Serializable]
    //public class RefIAudioBase : InterfaceContainer<IAudioBase> { }

    [Serializable]
    public class RefIAudioComponent : InterfaceContainer<IAudioComponent> { }

    [Serializable]
    public class RefIAudioAsset : InterfaceContainer<IAudioAsset> { }
}
