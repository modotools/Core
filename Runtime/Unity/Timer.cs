﻿using System;
using Core.Unity.Types;
using UnityEngine;
using UnityEngine.Serialization;

namespace Core.Unity
{
    public enum RunState
    {
        Stop,
        Run,
        Pause,
        Finished
    }
    
    [Serializable]
    public class Timer : ITimer
    {
        [FormerlySerializedAs("m_time")] [SerializeField] 
        float m_defaultDuration;
        float m_duration;
        
        public bool IsRunning => State == RunState.Run;
        public float DefaultDuration => m_defaultDuration;
        
        float m_dir;
        float m_currentTime;

        public RunState State { get; private set; }

        #region public functions
        public void StartCoolDown() => StartCoolDown(m_defaultDuration);
        public void StartCoolDown(float time) => Run(time, time, -1f);
        public void StartTimer() => StartTimer(m_defaultDuration);
        public void StartTimer(float time) => Run(time, 0f, 1f);

        public void Run(float duration, float currentTime, float dir)
        {
            m_duration = Mathf.Max(duration, 0f);
            m_currentTime = currentTime;
            m_dir = Mathf.Sign(dir);
            State = RunState.Run;
        }
        
        public void Restart()
        {
            m_currentTime = m_dir > 0f ? 0f : m_duration;
            State = RunState.Run;
        }
        
        public void Update(float delta)
        {
            if (State != RunState.Run)
                return;
            
            m_currentTime += m_dir * delta;
            
            var finished = m_dir < 0f 
                ? m_currentTime <= 0f 
                : m_currentTime >= m_duration;
            
            if (finished)
                State = RunState.Finished;
        }

        public float P =>
            m_duration > float.Epsilon 
                ? m_currentTime / m_duration : 
                -1f;

        public float Percentage() => m_dir < 0f ? 1 - P : P;

        public bool IsFinished(bool resetIfFinished = true)
        {
            var fin = State == RunState.Finished;
            if (resetIfFinished && fin)
                ResetTimer();
            return fin;
        }

        public void Pause()
        {
            if (State == RunState.Run)
                State = RunState.Pause;
        }

        public void Resume()
        {
            if (State == RunState.Pause)
                State = RunState.Run;
        }

        public void ResumeInDirection(float dir)
        {
            m_dir = Mathf.Sign(dir);
            State = RunState.Run;
        }

        public void ResetTimer() => State = RunState.Stop;
        #endregion
    }
    
    public interface ITimer
    {
        bool IsRunning { get; }
        float P { get; }
        RunState State { get; }

        void StartCoolDown();
        void StartCoolDown(float time);
        void StartTimer();
        void StartTimer(float time);

        void Run(float duration, float currentTime, float dir);
        float Percentage();
        
        bool IsFinished(bool resetIfFinished = true);
        void Pause();
        void Resume();
        void ResumeInDirection(float dir);
        void ResetTimer();
    }
    
    [Serializable]
    public class RefITimer : InterfaceContainer<ITimer>
    {
    }
}
