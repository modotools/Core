using System.Collections.Generic;

namespace Core
{
    /// <summary> Various handy Helper </summary>
    public static class Utils
    {
        /// <summary> Helper to use null with extension methods, like IsAny()</summary>
        // ReSharper disable once InconsistentNaming
        public const object NULL = null;

        /// <summary>
        /// Helper to iterate over enum list
        /// </summary>
        public static List<T> GetEnumList<T>() where T : struct
        {
            if (!typeof(T).IsEnum)
                return null;
            var array = (T[])System.Enum.GetValues(typeof(T));
            var list = new List<T>(array);
            return list;
        }
    }

    public static class Name
    {
        public static readonly string Ignore = "-";
    }
}
