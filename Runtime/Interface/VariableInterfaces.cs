﻿// simplest variable interface

namespace Core.Interface
{
    public interface IVar<T> : IVar
    {
        T Value { get; set; }
    }

    public interface IVar
    {
        object ValueObject { get; set; }
    }
}