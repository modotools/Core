namespace Core.Interface
{
    // usually actions are invoked by FSM, here an action is invoked immediately through input
    public interface IInvokable
    {
        void Invoke();
    }
}