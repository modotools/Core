﻿using System;
using System.Collections.Generic;
using Core.Unity.Interface;
using Core.Unity.Types;

namespace Core.Interface
{
    public interface IFSMEventRef : IProvider<IFSM>
    {
        string EventName { get; }
        bool IsSet();
    }
    public interface IFSMStateRef : IProvider<IFSM>
    {
        string StateName { get; }
        bool IsSet();
    }

    public interface IFSM
    {
        void ResetFSM();
        void SetState(string stateName);
        void SendEvent(string eventName);
        string ActiveStateName { get; }

        IEnumerable<IFSMConfig> FsmConfigs { get; }
        bool CanInvoke(string eventName);
    }
    
    public interface IFSMComponent : IFSM, IComponent
    {
    }

    public interface IFSMConfig
    {
        string[] StateNames { get; }
        string[] EventNames { get; }
    }


    [Serializable]
    public class RefIFSMComponent : InterfaceContainer<IFSMComponent>{}
}