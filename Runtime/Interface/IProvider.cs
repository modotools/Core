﻿namespace Core.Interface
{
    /// <summary>
    /// Helps putting components anywhere on the GameObject and still being able to get them without iterating all children
    /// </summary>
    /// <typeparam name="TProvided"></typeparam>
    public interface IProvider<TProvided>
    {
        /// <summary>
        /// Implementation with out provider, so you can implement multiple
        /// </summary>
        /// <param name="provided"></param>
        // ReSharper disable once MethodNameNotMeaningful
        void Get(out TProvided provided);
    }
}
