﻿namespace Core.Interface
{
    public interface IDefaultNameProvider
    {
        string DefaultName { get; }
    }
}
