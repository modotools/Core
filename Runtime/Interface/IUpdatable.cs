namespace Core.Interface
{
    public interface IUpdatable
    {
        void Update();
    }

    public interface IDeltaUpdatable
    {
        void Update(float dt);
    }
}