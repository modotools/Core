﻿namespace Core.Interface
{
    public interface IRenamedHandler
    {
        void OnRenamed();
    }
}
