namespace Core.Editor
{
    public enum MenuStringNames
    {
        CoreMenuName,
        CreateEditorToolAsset,
        RegenerateGuids,
        CreatePoolSettings,
        GuidTool,
        NavigationWindow,
        NavigateForward,
        NavigateBackward
    }
    public static class MenuStrings
    {
        public static class Parts
        {
            internal const string CoreMenuName = "Tools";

            // ReSharper disable MemberHidesStaticFromOuterClass
            internal const string CreateCoreAsset = "Tools";

            internal const string RegenerateGuids = "Regenerate GUIDs";
            internal const string CreatePoolSettings = "Pool/Create PoolSettings";
            internal const string GuidTool = "Guid-Tool %&g";

            internal const string NavigationWindow = "Navigation History/Window %&n";
            internal const string NavigateForward = "Navigation History/Navigate Forth &y";
            internal const string NavigateBackward = "Navigation History/Navigate Back &z";
            // ReSharper restore MemberHidesStaticFromOuterClass
        }

        public const string CreateCoreAsset = Parts.CreateCoreAsset;
        public const string RegenerateGuids = Parts.CoreMenuName + "/" + Parts.RegenerateGuids;
        public const string CreatePoolSettings = Parts.CoreMenuName + "/" + Parts.CreatePoolSettings;
        public const string GuidTool = Parts.CoreMenuName + "/" + Parts.GuidTool;

        public const string NavigationWindow = Parts.CoreMenuName + "/" + Parts.NavigationWindow;
        public const string NavigateForward = Parts.CoreMenuName + "/" + Parts.NavigateForward;
        public const string NavigateBackward = Parts.CoreMenuName + "/" + Parts.NavigateBackward;
    }
}
