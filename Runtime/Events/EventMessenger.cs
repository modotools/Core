﻿// ReSharper disable InvalidXmlDocComment
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Events
{
    public static class EventMessenger
    {
        static readonly Dictionary<Type, List<IEventListenerBase>> k_subscribersList;

        static EventMessenger() => k_subscribersList = new Dictionary<Type, List<IEventListenerBase>>();
        
        public static void AddListener<T>(IEventListener<T> listener) where T : struct
        {
            var eventType = typeof(T);

            if (!k_subscribersList.ContainsKey(eventType))
                k_subscribersList[eventType] = new List<IEventListenerBase>();

            if (!SubscriptionExists(eventType, listener))
                k_subscribersList[eventType].Add(listener);
        }
        
        public static void RemoveListener<T>(IEventListener<T> listener) where T : struct
        {
            var eventType = typeof(T);

            if (!k_subscribersList.ContainsKey(eventType))
                return;
            
            var subscriberList = k_subscribersList[eventType];

            for (var i = 0; i < subscriberList.Count; i++)
            {
                if (subscriberList[i] != listener)
                    continue;

                subscriberList.Remove(subscriberList[i]);

                if (subscriberList.Count == 0)
                    k_subscribersList.Remove(eventType);

                return;
            }
        }
        
        public static void TriggerEvent<T>(T newEvent) where T : struct
        {
            if (!k_subscribersList.TryGetValue(typeof(T), out var list))
                return;
            using (var scoped = Types.SimplePool<List<IEventListenerBase>>.I.GetScoped())
            {
                scoped.Obj.AddRange(list);
                foreach (var t in scoped.Obj)
                    (t as IEventListener<T>)?.OnEvent(newEvent);
            }
        }
        
        static bool SubscriptionExists(Type type, IEventListenerBase receiver)
        {
            return k_subscribersList.TryGetValue(type, out var receivers) 
                   && receivers.Any(t => t == receiver);
        }
    }
    
    /// <summary>
    /// Event listener basic interface
    /// </summary>
    public interface IEventListenerBase { };

    /// <summary>
    /// A public interface you'll need to implement for each type of event you want to listen to.
    /// </summary>
    public interface IEventListener<in T> : IEventListenerBase
    {
        void OnEvent(T eventType);
    }
}