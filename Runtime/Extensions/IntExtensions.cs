using System;
using System.Collections.Generic;

namespace Core.Extensions
{
    /// <summary> Various handy Extensions </summary>
    public static class IntExtensions
    {
        public static bool IsInRange(this int i, int min, int exclusiveMax) => i >= min && i < exclusiveMax;
        public static bool IsInRange(this int i, Array arr) => arr != null && i >= 0 && i < arr.Length;
        public static bool IsInRange<T>(this int i, List<T> list) => list != null && i >= 0 && i < list.Count;


        public static bool IsInRange(this ushort i, int min, int exclusiveMax) => i >= min && i < exclusiveMax;
        public static bool IsInRange(this ushort i, Array arr) => arr != null && i >= 0 && i < arr.Length;
        public static bool IsInRange<T>(this ushort i, List<T> list) => list != null && i >= 0 && i < list.Count;
    }
}
