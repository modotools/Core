using UnityEngine;
using Core.Extensions;

namespace Core.Extensions
{
    public static class CustomPlayerPrefs
    {
        public static void SetColor(string key, Color value)
        {
            PlayerPrefs.SetFloat(key + "/R", value.r);
            PlayerPrefs.SetFloat(key + "/G", value.g);
            PlayerPrefs.SetFloat(key + "/B", value.b);
            PlayerPrefs.SetFloat(key + "/A", value.a);
        }

        public static Color GetColor(string key, Color defColor = default)
        {
            var r = PlayerPrefs.GetFloat(key + "/R", defColor.r);
            var g = PlayerPrefs.GetFloat(key + "/G", defColor.g);
            var b = PlayerPrefs.GetFloat(key + "/B", defColor.b);
            var a = PlayerPrefs.GetFloat(key + "/A", defColor.a);
            return new Color(r, g, b, a);
        }
        public static void ClearPrefsColor(string key)
        {
            PlayerPrefs.DeleteKey($"{key}/R");
            PlayerPrefs.DeleteKey($"{key}/G");
            PlayerPrefs.DeleteKey($"{key}/B");
            PlayerPrefs.DeleteKey($"{key}/A");
        }

        public static Vector3Int GetVector3Int(string key, Vector3Int defaultValue = default)
        {
            var x = PlayerPrefs.GetInt($"{key}.X", defaultValue.x);
            var y = PlayerPrefs.GetInt($"{key}.Y", defaultValue.y);
            var z = PlayerPrefs.GetInt($"{key}.Z", defaultValue.z);
            return new Vector3Int(x,y,z);
        }

        public static void SetVector3Int(string key, Vector3Int value)
        {
            PlayerPrefs.SetInt($"{key}.X", value.x);
            PlayerPrefs.SetInt($"{key}.Y", value.y);
            PlayerPrefs.SetInt($"{key}.Z", value.z);
        }
        public static void ClearPrefsVector3Int(string key)
        {
            PlayerPrefs.DeleteKey($"{key}.X");
            PlayerPrefs.DeleteKey($"{key}.Y");
            PlayerPrefs.DeleteKey($"{key}.Z");
        }

        public static Rect GetRect(string key, Rect defaultValue = default)
        {
            var x = PlayerPrefs.GetFloat($"{key}.X", defaultValue.x);
            var y = PlayerPrefs.GetFloat($"{key}.Y", defaultValue.y);
            var w = PlayerPrefs.GetFloat($"{key}.W", defaultValue.width);
            var h = PlayerPrefs.GetFloat($"{key}.H", defaultValue.height);

            return new Rect(x, y,w,h);
        }

        public static void SetRect(string key, Rect value)
        {
            PlayerPrefs.SetFloat($"{key}.X", value.x);
            PlayerPrefs.SetFloat($"{key}.Y", value.y);
            PlayerPrefs.SetFloat($"{key}.W", value.width);
            PlayerPrefs.SetFloat($"{key}.H", value.height);
        }

        public static void ClearPrefsRect(string key)
        {
            PlayerPrefs.DeleteKey($"{key}.X");
            PlayerPrefs.DeleteKey($"{key}.Y");
            PlayerPrefs.DeleteKey($"{key}.W");
            PlayerPrefs.DeleteKey($"{key}.H");
        }
    }
}
