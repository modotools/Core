using System;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Core.Extensions
{
    /// <summary> Various handy Extensions </summary>
    public static class ObjectExtensions
    {
        // Named format strings from object attributes. Eg:
        // string blaStr = aPerson.ToString("My name is {FirstName} {LastName}.")
        // From: http://www.hanselman.com/blog/CommentView.aspx?guid=fde45b51-9d12-46fd-b877-da6172fe1791
        public static string ToString_Reflective(this object anObject, string aFormat)
        {
            return ToString_Reflective(anObject, aFormat, null);
        }

        public static string ToString_Reflective(this object anObject, string aFormat, IFormatProvider formatProvider)
        {
            var sb = new StringBuilder();
            var type = anObject.GetType();
            var reg = new Regex(@"({)([^}]+)(})", RegexOptions.IgnoreCase);
            var mc = reg.Matches(aFormat);
            var startIndex = 0;
            foreach (Match m in mc)
            {
                var g = m.Groups[2]; //it's second in the match between { and }
                var length = g.Index - startIndex - 1;
                sb.Append(aFormat.Substring(startIndex, length));

                ToStringReflective.GetFormat(g, out var toFormat, out var toGet);
                //first try properties
                ToStringReflective.RetrievePropertyOrType(anObject, type, toGet, out var retrievedType, out var retrievedObject);
                ToStringReflective.Append(formatProvider, retrievedType, toFormat, retrievedObject, sb, g);
                
                startIndex = g.Index + g.Length + 1;
            }
            if (startIndex < aFormat.Length) //include the rest (end) of the string
                sb.Append(aFormat.Substring(startIndex));

            return sb.ToString();
        }
        
        static class ToStringReflective
        {
            public static void Append(IFormatProvider formatProvider, Type type, string toFormat, object obj,
                StringBuilder sb, Capture g)
            {
                if (type != null) //Cool, we found something
                {
                    var result = toFormat == string.Empty
                        ? InvokeToString(type, obj)
                        : InvokeToString(type, obj,
                            new object[] { toFormat, formatProvider });

                    sb.Append(result);
                }
                else //didn't find a property with that name, so be gracious and put it back
                    AppendCapture(sb, g);
            }

            public static void GetFormat(Capture g, out string toFormat, out string toGet)
            {
                toFormat = String.Empty;
                var formatIndex = g.Value.IndexOf(":", StringComparison.Ordinal); //formatting would be to the right of a :
                if (formatIndex == -1) //no formatting, no worries
                {
                    toGet = g.Value;
                    return;
                }

                toGet = g.Value.Substring(0, formatIndex);
                toFormat = g.Value.Substring(formatIndex + 1);
            }

            public static void RetrievePropertyOrType(object anObject, Type type, string toGet, out Type retrievedType,
                out object retrievedObject)
            {
                var retrievedProperty = type.GetProperty(toGet);
                retrievedType = null;
                retrievedObject = null;
                if (retrievedProperty != null)
                {
                    retrievedType = retrievedProperty.PropertyType;
                    retrievedObject = retrievedProperty.GetValue(anObject, null);
                    return;
                }

                //try fields
                var retrievedField = type.GetField(toGet);
                if (retrievedField == null)
                    return;

                retrievedType = retrievedField.FieldType;
                retrievedObject = retrievedField.GetValue(anObject);
            }

            static string InvokeToString(Type type, object obj, object[] parameters = null)
            {
                var result = type.InvokeMember("ToString",
                    BindingFlags.Public | BindingFlags.NonPublic |
                    BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.IgnoreCase
                    , null, obj, parameters) as string;
                return result;
            }

            static void AppendCapture(StringBuilder sb, Capture g)
            {
                sb.Append("{");
                sb.Append(g.Value);
                sb.Append("}");
            }
        }
    }
}
