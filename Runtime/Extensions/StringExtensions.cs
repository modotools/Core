namespace Core.Extensions
{
    /// <summary> Various handy Extensions </summary>
    public static class Extensions
    {
        /// <summary> Checks String for null or empty </summary>
        public static bool IsNullOrEmpty(this string str)
        {
            switch (str)
            {
                case null:
                case "":
                    return true;
                default:
                    return false;
            }
        }

        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value))
                return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }
}
