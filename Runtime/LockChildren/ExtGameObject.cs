﻿/// <summary>
/// MIT License - Copyright(c) 2019 Ugo Belfiore
/// </summary>

using UnityEngine;
public static class ExtGameObject
{
    /// <summary>
    /// create a gameObject, with a set of components
    /// ExtGameObject.CreateGameObject("game object name", Transform parent, Vector3.zero, Quaternion.identity, Vector3 Vector.One, Component [] components)
    /// set null at components if no component to add
    /// return the created gameObject
    /// </summary>
    public static GameObject CreateLocalGameObject(string name,
        Transform parent,
        Vector3 localPosition,
        Quaternion localRotation,
        Vector3 localScale)
    {
        GameObject newObject = new GameObject(name);
        //newObject.SetActive(true);
        newObject.transform.SetParent(parent);
        newObject.transform.SetAsLastSibling();
        newObject.transform.localPosition = localPosition;
        newObject.transform.localRotation = localRotation;
        newObject.transform.localScale = localScale;

        return (newObject);
    }

}
