﻿/// <summary>
/// MIT License - Copyright(c) 2019 Ugo Belfiore
/// </summary>

using System;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public static class ExtVector3
{
    /// <summary>
    /// return the middle of X Transform
    /// </summary>
    public static Vector3 GetMeanOfXPoints(Transform[] arrayTransform, bool middleBoundingBox = true)
    {
        Vector3[] arrayVect = new Vector3[arrayTransform.Length];
        for (int i = 0; i < arrayTransform.Length; i++)
        {
            arrayVect[i] = arrayTransform[i].position;
        }
        return (GetMiddleOfXPoints(arrayVect, middleBoundingBox));
    }

    /// <summary>
    /// return the middle of X points (POINTS, NOT vector)
    /// </summary>
    public static Vector3 GetMiddleOfXPoints(Vector3[] arrayVect, bool middleBoundingBox = true)
    {
        if (arrayVect.Length == 0)
            return (Vector3.zero);

        if (!middleBoundingBox)
        {
            Vector3 sum = Vector3.zero;
            for (int i = 0; i < arrayVect.Length; i++)
            {
                sum += arrayVect[i];
            }
            return (sum / arrayVect.Length);
        }
        else
        {
            if (arrayVect.Length == 1)
                return (arrayVect[0]);

            float xMin = arrayVect[0].x;
            float yMin = arrayVect[0].y;
            float zMin = arrayVect[0].z;
            float xMax = arrayVect[0].x;
            float yMax = arrayVect[0].y;
            float zMax = arrayVect[0].z;

            for (int i = 1; i < arrayVect.Length; i++)
            {
                if (arrayVect[i].x < xMin)
                    xMin = arrayVect[i].x;
                if (arrayVect[i].x > xMax)
                    xMax = arrayVect[i].x;

                if (arrayVect[i].y < yMin)
                    yMin = arrayVect[i].y;
                if (arrayVect[i].y > yMax)
                    yMax = arrayVect[i].y;

                if (arrayVect[i].z < zMin)
                    zMin = arrayVect[i].z;
                if (arrayVect[i].z > zMax)
                    zMax = arrayVect[i].z;
            }
            Vector3 lastMiddle = new Vector3((xMin + xMax) / 2, (yMin + yMax) / 2, (zMin + zMax) / 2);
            return (lastMiddle);
        }
    }
}
