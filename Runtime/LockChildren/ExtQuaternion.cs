﻿/// <summary>
/// MIT License - Copyright(c) 2019 Ugo Belfiore
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtQuaternion
{
    /// <summary>
    /// get an array of rotation from a list of transform
    /// </summary>
    public static Quaternion[] GetAllRotation(Transform[] rotations)
    {
        Quaternion[] array = new Quaternion[rotations.Length];
        for (int i = 0; i < rotations.Length; i++)
        {
            array[i] = rotations[i].rotation;
        }
        return (array);
    }

    // assuming qArray.Length > 1
    public static Quaternion AverageQuaternion(Quaternion[] qArray)
    {
        Quaternion qAvg = qArray[0];
        float weight;
        for (int i = 1; i < qArray.Length; i++)
        {
            weight = 1.0f / (float)(i + 1);
            qAvg = Quaternion.Slerp(qAvg, qArray[i], weight);
        }
        return qAvg;
    }
}
