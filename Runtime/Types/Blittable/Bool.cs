﻿namespace Core.Types.Blittable
{
    /// <summary> bool to use this as blittable type </summary>
    // ReSharper disable InconsistentNaming
    public struct TBool
    {
        readonly byte _value;
        /// <summary> constructor </summary>
        public TBool(bool value) { _value = (byte)(value ? 1 : 0); }

        /// <summary> helper for auto converting from bool </summary>
        public static implicit operator TBool(bool value) { return new TBool(value); }
        /// <summary> helper for auto converting to bool </summary>
        public static implicit operator bool(TBool value) { return value._value != 0; }
    }
}
// ReSharper restore InconsistentNaming