using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Types
{
    /// <inheritdoc />
    /// <summary>
    /// Simple pool for C# classes
    /// </summary>
    public sealed class SimplePool<T> : Singleton<SimplePool<T>> where T : new()
    {
        ConcurrentBag<T> m_freeObjects;

        /// <summary>
        ///     for easy auto-returning to pool with using-keyword, f.e.
        ///     using (var pObj = MyPool.GetScoped()){
        ///     var obj = pObj.Obj; // do stuff here
        ///     } // object returned
        /// </summary>
        public PooledObject GetScoped() => new PooledObject(this, Get());

        /// <inheritdoc />
        /// <summary> Initialization </summary>
        protected override void Init()
        {
            m_freeObjects = new ConcurrentBag<T>();
            #if UNITY_EDITOR
            m_objectsInUse = new List<T>();
            #endif
        }

#if UNITY_EDITOR
        List<T> m_objectsInUse;
#endif
        // ReSharper disable once MethodNameNotMeaningful
        /// <summary> gets object from pool </summary>
        public T Get()
        {
            //Debug.Log($" SimplePool {typeof(T)} m_freeObjects {m_freeObjects.Count} Get");
            if (!m_freeObjects.TryTake(out var obj)) obj = new T();

#if UNITY_EDITOR
            m_objectsInUse.Add(obj);
#endif

            return obj;
        }

        /// <summary> returns object to pool </summary>
        public void Return(T obj)
        {            
            //Debug.Log($" SimplePool {typeof(T)} m_freeObjects {m_freeObjects.Count} Return");
            if (obj == null)
                return;
#if UNITY_EDITOR
            var idx = m_objectsInUse.FindIndex((o) => (o.Equals(obj)));
            if (idx != -1)
                m_objectsInUse.RemoveAt(idx);
            else
            {
                Debug.LogError(m_freeObjects.ToList().Contains(obj)
                    ? $"Returning object of Type {typeof(T)} that was already returned to Pool!"
                    : $"Returning object of Type {typeof(T)} that was not created by Pool!");
            }
#endif

            if (typeof(T).GetInterface(nameof(IList)) != null)
                ((IList) obj).Clear();
            if (typeof(T).GetInterface(nameof(IDictionary)) != null)
                ((IDictionary) obj).Clear();

            m_freeObjects.Add(obj);
        }

        /// <inheritdoc cref="IDisposable" />
        /// <summary> container of object from pool for easy use with the using keyword </summary>
        public struct PooledObject : IDisposable
        {
            /// <summary> constructor </summary>
            public PooledObject(SimplePool<T> pool, T obj)
            {
                m_mPool = pool;
                Obj = obj;
            }

            readonly SimplePool<T> m_mPool;

            /// <summary> gets the Object </summary>
            public T Obj { get; }

            /// <inheritdoc />
            /// <summary> Dispose </summary>
            public void Dispose() => m_mPool.Return(Obj);
        }

        //internal void ReturnAll()
        //{
        //    if (typeof(T).GetInterface(nameof(System.Collections.IList))_!= null)
        //    {
        //        foreach(var obj in ObjectsInUse)
        //            ((System.Collections.IList)obj).Clear();
        //    }
        //    foreach (var obj in ObjectsInUse)
        //        FreeObjects.Add(obj);
        //    ObjectsInUse.Clear();
        //}
    }
}